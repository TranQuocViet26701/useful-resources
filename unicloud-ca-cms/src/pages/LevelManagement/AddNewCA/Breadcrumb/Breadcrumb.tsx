import BreadcrumbCustom from '@/components/BreadCrumb';
import { Breadcrumb } from 'antd';

interface BreadCrumbProps {
  title: string;
  path?: string;
}

const BuildBreadcrumbAddNewCA = (props: { routes?: BreadCrumbProps[] }) => {
  const { routes } = props;
  return (
    <>
      <BreadcrumbCustom>
        <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
        {routes &&
          routes?.map((item) => {
            return (
              <Breadcrumb.Item key={`key-${item?.title}`}>
                <a href={item?.path}> {item?.title}</a>
              </Breadcrumb.Item>
            );
          })}
      </BreadcrumbCustom>
    </>
  );
};

export default BuildBreadcrumbAddNewCA;
