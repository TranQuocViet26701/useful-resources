module.exports = {
  env: {
    browser: true,
    es2021: true,
    jest: true,
    node: true,
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
    },
    'import/extensions': ['.js', '.jsx', '.ts', '.tsx'],
    'import/core-modules': [],
    react: {
      version: 'detect',
    },
    'import/ignore': ['node_modules', '\\.(coffee|svg|json)$'],
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'airbnb',
    'airbnb-typescript',
    'prettier',
  ],
  ignorePatterns: ['.eslintrc.js'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 13,
    sourceType: 'module',
    project: './tsconfig.json',
    createDefaultProgram: true,
  },
  plugins: ['react', 'prettier', '@typescript-eslint'],
  rules: {
    'max-len': ['error', 120],
    'linebreak-style': ['error', process.platform === 'win32' ? 'windows' : 'unix'],
    'object-curly-newline': 'off',
    'import/no-unresolved': 0,
    'import/no-absolute-path': 0,
    'react/prop-types': 'off',
    'operator-linebreak': 0,
    'import/no-relative-packages': 'error',
    'arrow-body-style': 0,
    indent: 'off',
    'jsx-a11y/label-has-associated-control': [
      'error',
      {
        required: {
          some: ['nesting', 'id'],
        },
      },
    ],
    'jsx-a11y/label-has-for': [
      'error',
      {
        required: {
          some: ['nesting', 'id'],
        },
      },
    ],
    'react/function-component-definition': 'off',
    'react/jsx-one-expression-per-line': 'off',
    'jsx-a11y/control-has-associated-label': 'off',
    'no-param-reassign': 0,
    'import/extensions': 'off',
    'no-alert': 0,
  },
};
