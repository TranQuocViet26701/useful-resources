// @ts-nocheck
import fs from 'fs';
import path from 'path';
import express from 'express';
import compression from 'compression';
import serveStatic from 'serve-static';
import { fileURLToPath } from 'url';
// import EntryServer from './src/entry-server';
import Vite from 'vite';
import { Helmet } from 'react-helmet';

const isTest = process.env.NODE_ENV === 'test';
const filename = fileURLToPath(import.meta.url);
const dirname = path.dirname(filename);
async function createServer(root = process.cwd(), isProd = process.env.NODE_ENV === 'production') {
  const resolve = (p) => path.resolve(dirname, p);
  const indexProd = isProd ? fs.readFileSync(resolve('dist/client/index.html'), 'utf-8') : '';
  const app = express();

  /**
   * @type {import('vite').ViteDevServer}
   */
  let vite;
  if (!isProd) {
    vite = await Vite.createServer({
      root,
      logLevel: isTest ? 'error' : 'info',
      server: {
        middlewareMode: 'ssr',
        watch: {
          // During tests we edit the files too fast and sometimes chokidar
          // misses change events, so enforce polling for consistency
          usePolling: true,
          interval: 100,
        },
      },
    });
    // use vite's connect instance as middleware
    app.use(vite.middlewares);
  } else {
    // eslint-disable-next-line global-require
    app.use(compression());
    app.use(
      serveStatic(resolve('dist/client'), {
        index: false,
      }),
    );
  }

  // eslint-disable-next-line consistent-return
  app.use('*', async (req, res) => {
    try {
      const url = req.originalUrl;

      let template;
      let render;
      if (!isProd) {
        template = fs.readFileSync(resolve('index.html'), 'utf-8');
        template = await vite.transformIndexHtml(url, template);
        render = await vite.ssrLoadModule('/src/entry-server.tsx');
      } else {
        template = indexProd;
        render = await import('./dist/server/entry-server.js');
      }
      const appHtml = await render.default(url);
      const helmet = Helmet.renderStatic();
      const metaOverride = `${helmet?.title?.toString()}
      ${helmet?.meta?.toString()}
      ${helmet?.link?.toString()}
      ${helmet?.noscript?.toString()}
      ${helmet?.style?.toString()}
      ${helmet?.base?.toString()}
    `;
      const html = template
        .replace(`<div id="root"></div>`, `<div id="root">${appHtml}</div>`)
        .replace(`<title data-react-helmet="true">Unicloud Group</title>`, metaOverride);

      res.status(200).set({ 'Content-Type': 'text/html' }).end(html);
    } catch (e) {
      // eslint-disable-next-line @typescript-eslint/no-unused-expressions
      !isProd && vite.ssrFixStacktrace(e);
      res.status(500).end(e.stack);
    }
  });

  return { app, vite };
}

if (!isTest) {
  createServer().then(({ app }) =>
    app.listen(80, () => {
      console.log('http://0.0.0.0:80');
    }),
  );
}
