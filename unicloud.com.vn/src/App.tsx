/* eslint-disable react/jsx-fragments */
import '@assets/scss/style.scss';
import React, { useCallback, useEffect } from 'react';
import { Navigate, Route, Routes, useLocation } from 'react-router-dom';
import './App.scss';
import Footer from './layouts/footer/Footer';
import CustomNav from './layouts/navbar/Navbar';
import Ecosystem from './pages/ecosystem';
import EmagazineDetail from './pages/emagazine';
import DigitalTransformation from './pages/landing-page/digital-transformation';
import NewsDetail from './pages/news-detail';
import { appRouteList, routerPageRouteList } from './routes';
import { handleLocaleRedirect } from './utils';

const localeList = ['vi', 'en'];

function RouterPage() {
  const location = useLocation();
  const localeRedirect = handleLocaleRedirect();

  return (
    <>
      <CustomNav />
      <main className="main-unicloud">
        <Routes>
          {routerPageRouteList.map((route) => {
            return (
              <React.Fragment key={route.path}>
                <Route
                  path={route.path}
                  element={
                    <Navigate
                      replace
                      to={`/${localeRedirect}${location.pathname}${location.search}${location.hash}`}
                    />
                  }
                />
                {localeList.map((l) => (
                  <Route key={l + route.path} path={`/${l}${route.path}`} element={route.element} />
                ))}
              </React.Fragment>
            );
          })}
          <Route path="/ecosystem/*" element={<Ecosystem />} />
          <Route path="/news/*" element={<NewsDetail />} />
          <Route path="*" element={<Navigate replace to="/not-found" />} />
        </Routes>
      </main>
      <Footer />
    </>
  );
}

function App() {
  const location = useLocation();
  const localeRedirect = handleLocaleRedirect();

  const scrollTop = useCallback(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }, []);

  useEffect(() => {
    scrollTop();
  }, [location]);

  return (
    <Routes>
      <Route path="/*" element={<RouterPage />} />
      {appRouteList.map((route) => {
        return (
          <React.Fragment key={route.path}>
            <Route
              key={route.path}
              path={route.path}
              element={
                <Navigate
                  replace
                  to={`/${localeRedirect}${location.pathname}${location.search}${location.hash}`}
                />
              }
            />
            {localeList.map((l) => (
              <Route key={l + route.path} path={`/${l}${route.path}`} element={route.element} />
            ))}
          </React.Fragment>
        );
      })}
      <Route path="/emagazine/*" element={<EmagazineDetail />} />
      <Route path="/digital-transformation" element={<DigitalTransformation />} />
    </Routes>
  );
}
export default App;
