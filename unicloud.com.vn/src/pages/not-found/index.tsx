import * as React from 'react';
import img404 from '/assets/image/png/img-404.png';
import { useNavigate } from 'react-router-dom';
import './style.scss';
import { useTranslation } from 'react-i18next';
import SEO from '@src/seo/seo';
import CustomNav from '@src/layouts/navbar/Navbar';
import Footer from '@src/layouts/footer/Footer';

function NotFound() {
  const navigate = useNavigate();
  const { t, ready } = useTranslation();
  return (
    <>
      <SEO title="Page not found | Unicloud Group" />
      <CustomNav />
      <section className="container-child not-found">
        <img src={img404} alt="not found" className="img-not-found" />
        <h3>{ready && t('not.found.title')}</h3>
        <span className="desc-404">{ready && t('not.found.description')}</span>
        <button type="button" className="btn-home" onClick={() => navigate('/')}>
          <span>{ready && t('go.home')}</span>
        </button>
      </section>
      <Footer />
    </>
  );
}

export default NotFound;
