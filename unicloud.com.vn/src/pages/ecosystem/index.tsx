import React from 'react';
import { Route, Routes } from 'react-router-dom';

import EcoSystem from './Ecosystem';
import BizzoneCloud from './bizzone-cloud';

const DigitalTransformation = () => (
  <Routes>
    <Route path="/" element={<EcoSystem />} />
    <Route path="/*" element={<BizzoneCloud />} />
  </Routes>
);

export default DigitalTransformation;
