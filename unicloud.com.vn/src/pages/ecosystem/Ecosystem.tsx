import React from 'react';
import { Container } from 'react-bootstrap';
import RowTechnology from '@components/technology/RowTechnology';
import RowTopEcosystem from './main/RowTopEcosystem';
import RowSolution from './main/RowSolution';

import './Ecosystem.scss';

function EcoSystem() {
  return (
    <main>
      <Container className="max-width-100 px-0" fluid>
        <RowTopEcosystem />
        <RowSolution />
        <RowTechnology />
      </Container>
    </main>
  );
}

export default EcoSystem;
