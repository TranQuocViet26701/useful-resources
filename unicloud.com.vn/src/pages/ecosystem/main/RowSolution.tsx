import React, { useCallback, useState } from 'react';
import { Row, Col, Image } from 'react-bootstrap';
// import { NavLink } from 'react-router-dom';
import iconTopSolution from '/assets/image/ecosystem/icon-top-solution.svg';
import imgSolution1 from '/assets/image/png/img-solution-1.png';
import imgSolution2 from '/assets/image/png/img-solution-2.png';
import imgSolution3 from '/assets/image/png/img-solution-3.png';
import imgSolution4 from '/assets/image/png/img-solution-4.png';
import imgSolution5 from '/assets/image/png/img-solution-5.png';
import imgSolution6 from '/assets/image/png/img-solution-6.png';
import imgSmartCity1 from '/assets/image/ecosystem/img-smart-city-1.png';
import imgSmartCity2 from '/assets/image/ecosystem/img-smart-city-2.png';
import imgSmartCity3 from '/assets/image/ecosystem/img-smart-city-3.png';
import imgSmartCity4 from '/assets/image/ecosystem/img-smart-city-4.png';
import imgSmartCity5 from '/assets/image/ecosystem/img-smart-city-5.png';
import imgSmartCity6 from '/assets/image/ecosystem/img-smart-city-6.png';
import imgSmartCity7 from '/assets/image/ecosystem/img-smart-city-7.png';
import imgSmartCity8 from '/assets/image/ecosystem/img-smart-city-8.png';
import imgSmartCity9 from '/assets/image/ecosystem/img-smart-city-9.png';
import imgSmartCity10 from '/assets/image/ecosystem/img-smart-city-10.png';
import imgSmartCity11 from '/assets/image/ecosystem/img-smart-city-11.png';
import imgSmartCity12 from '/assets/image/ecosystem/img-smart-city-12.png';
// import imgDigital1 from '/assets/image/ecosystem/img-digital-1.png';
import imgDigital2 from '/assets/image/ecosystem/img-digital-2.png';
import imgDigital3 from '/assets/image/ecosystem/img-digital-3.png';
import imgDigital4 from '/assets/image/ecosystem/img-digital-4.png';
// import imgDigital5 from '/assets/image/ecosystem/img-digital-5.png';
import imgDigital6 from '/assets/image/ecosystem/img-digital-6.png';
import imgDigital7 from '/assets/image/ecosystem/img-digital-7.png';
import imgDigital8 from '/assets/image/ecosystem/img-digital-8.png';
// import imgDigital9 from '/assets/image/ecosystem/img-digital-9.png';
// import imgDigital10 from '/assets/image/ecosystem/img-digital-10.png';
// import imgDigital11 from '/assets/image/ecosystem/img-digital-11.png';
import imgVirtual1 from '/assets/image/ecosystem/img-virtual-1.png';
import imgVirtual2 from '/assets/image/ecosystem/img-virtual-2.png';
import imgVirtual3 from '/assets/image/ecosystem/img-virtual-3.png';
import imgVirtual4 from '/assets/image/ecosystem/img-virtual-4.png';
import imgVirtual5 from '/assets/image/ecosystem/img-virtual-5.png';
import imgDigital12 from '/assets/image/landingPage/bizzone-enterprise/img-horizontal.png';

function RowSolution() {
  const [solutionTable, setSolutionTable] = useState(1);

  const handleSlidesSolutionTable = useCallback(
    (numSolutionTable: number) => {
      const intNumSolutionTable = numSolutionTable;
      setSolutionTable(intNumSolutionTable);
    },
    [solutionTable],
  );
  return (
    <Row className="back-ground-solution px-0 d-flex justify-content-center">
      <Row className="max-width-1300 d-flex justify-content-center padding-left-right">
        <Row className="max-width-1180 d-flex justify-content-center">
          <Col className="px-0">
            <p className="top-text-solution">
              <Image alt="" src={iconTopSolution} className="icon-top-ecosystem-page" />
              <span>TỔNG HỢP GIẢI PHÁP</span>
            </p>
            <p className="top-title-solution">Giới Thiệu Các Dự Án Của Chúng Tôi</p>
            <p className="top-detail-solution">
              Với mong muốn mang tới những sản phẩm có chất lượng cao nhất tới khách hàng, ngoài quy
              trình thiết kế khép kín, mỗi sản phẩm điện tử của Unicloud còn áp dụng quy trình sản
              xuất được thực hiện một cách khép kín thông qua việc đầu tư vào nhà máy thông minh,
              dây chuyền lắp ráp linh kiện hiện đại.
            </p>
          </Col>
        </Row>
        <div className="d-flex justify-content-center" data-aos="fade-down">
          <ul className="ul-pricing-table">
            <li>
              <button
                type="button"
                // className="active-li-price-list"
                onClick={() => handleSlidesSolutionTable(1)}
                className={solutionTable === 1 ? 'active-li-price-list' : ''}
              >
                Digital Banking platform
              </button>
            </li>
            <li>
              <button
                type="button"
                onClick={() => handleSlidesSolutionTable(2)}
                className={solutionTable === 2 ? 'active-li-price-list' : ''}
              >
                SMART CITY
              </button>
            </li>
            <li>
              <button
                type="button"
                onClick={() => handleSlidesSolutionTable(3)}
                className={solutionTable === 3 ? 'active-li-price-list' : ''}
              >
                DIGITAL TRANSFORMATION
              </button>
            </li>
            <li>
              <button
                type="button"
                onClick={() => handleSlidesSolutionTable(4)}
                className={solutionTable === 4 ? 'active-li-price-list' : ''}
              >
                VIRTUAL REALITY
              </button>
            </li>
          </ul>
        </div>
        <div className={solutionTable === 1 ? 'solution-table-show-1' : 'display-none'}>
          <Row className="gy-3 gx-3">
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSolution1} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">SMART TELLER MACHINE/ STM</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">
                      Máy giao dịch thông minh - Trải nghiệm ngân hàng số thế hệ mới
                    </p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSolution2} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">DIGITAL BANKING</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">
                      Ngân hàng số Là gói giải pháp Neobank hướng đối tượng người dùng là Gen Z và
                      Cộng tác viên của ngân hàng.
                    </p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="row-item-solution" md={4} xs={12}>
              <Row className="row-">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSolution3} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">PAYMENT GATEWAY</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">
                      Payment gateway dễ dàng hỗ trợ các kênh thanh toán truyền thống và hiện đại:
                      thẻ, NFC, QR Code, Biometric Payment.
                    </p>
                  </span>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="gy-3 gx-3 row-item-2-solution">
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSolution4} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">FINTECH IoT</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">
                      Các sản phẩm công nghệ liên quan tới lĩnh vực IoT được Unicloud phát huy tối
                      đa hiệu quả
                    </p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSolution5} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">POS</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Điểm bán hàng tích hợp công nghệ số</p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="row-item-solution" md={4} xs={12}>
              <Row className="row-">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSolution6} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">UNI ALARM</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Hệ thống báo động thông minh</p>
                  </span>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>

        <div className={solutionTable === 2 ? 'solution-table-show-2' : 'display-none'}>
          <Row className="gy-3 gx-3">
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSmartCity1} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">SMART CITY</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Hệ sinh thái Thành Phố Thông Minh</p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSmartCity2} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">SLIVING APP</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Cuộc sống thông minh</p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="row-item-solution" md={4} xs={12}>
              <Row className="row-">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSmartCity3} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">SMART HOME</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Hệ thống nhà thông minh</p>
                  </span>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="gy-3 gx-3 row-item-2-solution">
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSmartCity4} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">SMART PARKING</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Hệ thống bãi đỗ xe thông minh</p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSmartCity5} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">SMART ELEVATOR & ACCESS CONTROL</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">
                      Hệ thống thang máy & kiểm soát ra/vào thông minh
                    </p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="row-item-solution" md={4} xs={12}>
              <Row className="row-">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSmartCity6} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">SMART BUILDING & OFFICE</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">
                      Hệ thống quản lý tòa nhà, văn phòng thông minh
                    </p>
                  </span>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="gy-3 gx-3 row-item-2-solution">
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSmartCity7} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">DOOR PHONE & FACE ID</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">
                      Hệ thống chuông cửa & nhận diện khuôn mặt
                    </p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSmartCity8} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">SMART METER</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Hệ thống kiểm soát điện nước thông minh</p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="row-item-solution" md={4} xs={12}>
              <Row className="row-">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSmartCity9} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">IoT SENSOR NETWORK</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">
                      Hệ thống IoT kết hợp cảm biến giám sát thu thập thông tin
                    </p>
                  </span>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="gy-3 gx-3 row-item-2-solution">
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSmartCity10} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">SMART ACCESS CONTROL</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Kiểm soát ra vào thông minh</p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSmartCity11} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">CAMERA AI</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Camera nhận diện khuôn mặt</p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="row-item-solution" md={4} xs={12}>
              <Row className="row-">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgSmartCity12} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">SMART SECURITY</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Hệ thống kiểm soát an ninh thông minh</p>
                  </span>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>

        <div className={solutionTable === 3 ? 'solution-table-show-3' : 'display-none'}>
          <Row className="gy-3 gx-3">
            <Col className="row-item-solution" md={4} xs={12}>
              <Row className="row-">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgDigital12} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">BIZZONE HR</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Phần mềm quản trị nhân sự</p>
                  </span>
                </Col>
              </Row>
            </Col>
            {/* <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span  className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgDigital1} className="img-item-solution" />
                    </div>
                  </span>
                  <span >
                    <p className="title-item-solution">BIZZONE CLOUD</p>
                  </span>
                  <span >
                    <p className="detail-item-solution">Hệ thống quản lý nhân sự</p>
                  </span>
                </Col>
              </Row>
            </Col> */}
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgDigital2} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">UNICLOUDCA</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Dịch vụ chứng thực chữ ký số</p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgDigital4} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">UNICLOUD INVOICE</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Dịch vụ hóa đơn điện tử</p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="row-item-solution" md={4} xs={12}>
              <Row className="row-">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgDigital6} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">UNICLOUD AI</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Trí tuệ thông minh nhân tạo</p>
                  </span>
                </Col>
              </Row>
            </Col>
            {/* <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span  className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgDigital5} className="img-item-solution" />
                    </div>
                  </span>
                  <span >
                    <p className="title-item-solution">CLOUD CRM</p>
                  </span>
                  <span >
                    <p className="detail-item-solution">Hệ thống chăm sóc khách hàng</p>
                  </span>
                </Col>
              </Row>
            </Col> */}
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgDigital7} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">EKYC</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Định danh điện tử</p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgDigital8} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">FACE MATCHING/DETECTION</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">
                      Nhận diện & xử lý các giao dịch bất thường
                    </p>
                  </span>
                </Col>
              </Row>
            </Col>
            {/* <Col className="row-item-solution" md={4} xs={12}>
              <Row className="row-">
                <Col>
                  <span  className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgDigital9} className="img-item-solution" />
                    </div>
                  </span>
                  <span >
                    <p className="title-item-solution">KSPOINT</p>
                  </span>
                  <span >
                    <p className="detail-item-solution">Ứng dụng tích điểm Công Nghệ blockchain</p>
                  </span>
                </Col>
              </Row>
            </Col> */}

            {/* <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span  className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgDigital10} className="img-item-solution" />
                    </div>
                  </span>
                  <span >
                    <p className="title-item-solution">UNICAB</p>
                  </span>
                  <span >
                    <p className="detail-item-solution">Ứng dụng đặt xe</p>
                  </span>
                </Col>
              </Row>
            </Col> */}
            <Col className="row-item-solution" md={4} xs={12}>
              <Row className="row-">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgDigital3} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">BIZZONE SHOP</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Phần mềm siêu thị</p>
                  </span>
                </Col>
              </Row>
            </Col>
          </Row>
          {/* <Row className="gy-3 gx-3 row-item-2-solution"></Row>
          <Row className="gy-3 gx-3 row-item-2-solution"></Row>
          <Row className="gy-3 gx-3 row-item-2-solution"></Row> */}
        </div>

        <div className={solutionTable === 4 ? 'solution-table-show-4' : 'display-none'}>
          <Row className="gy-3 gx-3">
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgVirtual1} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">UNIVR 360</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Ứng dụng thực tế ảo cho Website</p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgVirtual2} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">UNIVR MOBILE</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Ứng dụng thực tế ảo cho điện thoại</p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="row-item-solution" md={4} xs={12}>
              <Row className="row-">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgVirtual3} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">UNIVR TOUCH</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Thực tế ảo tương tác</p>
                  </span>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="gy-3 gx-3 row-item-2-solution">
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgVirtual4} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">UNIVR GAME</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Ứng dụng Game thực tế ảo</p>
                  </span>
                </Col>
              </Row>
            </Col>
            <Col className="" md={4} xs={12}>
              <Row className="row-item-solution">
                <Col>
                  <span className="px-0">
                    <div className="div-img-solution">
                      <Image alt="" src={imgVirtual5} className="img-item-solution" />
                    </div>
                  </span>
                  <span>
                    <p className="title-item-solution">METAVERSE</p>
                  </span>
                  <span>
                    <p className="detail-item-solution">Vũ trụ ảo đầu tiên tại Việt Nam</p>
                  </span>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </Row>
    </Row>
  );
}
export default RowSolution;
