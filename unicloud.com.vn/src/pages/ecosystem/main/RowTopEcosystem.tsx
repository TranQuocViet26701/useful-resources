import React from 'react';
import { Row, Col, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import iconHome from '/assets/image/svg/icon-home-white.svg';

function RowTopEcosystem() {
  return (
    <Row className="back-ground-ecosystem-top px-0 d-flex justify-content-center">
      <Row className="max-width-1180 padding-left-right">
        <Col className="px-0">
          <h2 className="title-ecosystem">HỆ SINH THÁI UNICLOUD GROUP</h2>
          <div className="div-sub-ecosystem">
            <Image src={iconHome} className="img-fuid icon-home-ecosystem" alt="" />
            <Link to="/" className="title-go-back-home">
              Trang Chủ
            </Link>
            <span className="middle-ecosystem">/</span>
            <Link to="/ecosystem" className="sub-title-ecosystem">
              Hệ Sinh Thái
            </Link>
          </div>
        </Col>
      </Row>
    </Row>
  );
}
export default RowTopEcosystem;
