import React from 'react';
import { useQuery } from '@apollo/client/react/hooks/useQuery';
import { gql } from 'graphql-tag';

import { Container } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';
// import RowTechnology from '@src/components/technology/RowTechnology';
import SEO from '@src/seo/seo';
import queryUtil from '@query/queryUtil';
import { PostContextProvider } from '../../../context/usePostContext';
import RowHeader from './main/RowHeader';
import RowSectionTow from './main/RowSectionTow';
import RowPotentialBenefits from './main/RowPotentialBenefits';
import RowBanner from './main/RowBanner';
import RowFeature from './main/RowFeatures';
import RowReason from './main/RowReason';
import RowLearnMore from './main/RowLearnMore';
import './bizzoneCloud.scss';
import RowProduct from './main/RowProduct';

const queryData = gql`
  query NewQuery($title: String = "") {
    posts(where: { title: $title }) {
      edges {
        node {
          formpost {
            isimagesticktop
            banner {
              fieldGroupName
              subtitle
              title
              textdownload
              link
              imagebanner {
                sourceUrl
                srcSet
              }
            }
            breadcrumd
            featurecontent {
              description
              fieldGroupName
              subtitle
              title
              image {
                sourceUrl
              }
            }
            featurecontent {
              description
              fieldGroupName
              subtitle
              title
            }
            header {
              descriptionpopup
              fieldGroupName
              subtitlepopup
              titlepopup
              typepopup
              imageheader {
                altText
                sourceUrl
                srcSet
              }
              imagestick {
                altText
                sourceUrl
                srcSet
              }
            }
            informationdetail {
              fieldGroupName
              descriptioninformationdetail
              subtitleinformationdetail
              titleinformationdetail
              linkcta
              imageleft {
                altText
                srcSet
                sourceUrl
              }
            }
            listfeature {
              desc
              icon {
                sourceUrl
              }
              imageright {
                altText
                srcSet
                sourceUrl
              }
              title
              num
              listfeatureitem {
                listfeatureitemdetail
                num
              }
            }
            potentialbenefitcontent {
              title
              subtitle
              linkdownloadapple
              linkdownloadchplay
              description
              imageright {
                altText
                srcSet
                sourceUrl
              }
            }
            potentialbenefitsaccordion {
              accordion
              fieldGroupName
              itemaccordion
            }
            reasondetail {
              description
              fieldGroupName
              subtitle
              title
              listreason {
                fieldGroupName
                listreasonitem
              }
              imageleft {
                altText
                srcSet
                sourceUrl
              }
            }
            typefeature
            typeproduct
            groupproduct {
              description
              fieldGroupName
              groupproductdetail {
                description {
                  desc
                  num
                }
                fieldGroupName
                image {
                  altText
                  sourceUrl
                }
                link
                subtitle
                title
              }
              slideproduct {
                image {
                  altText
                  sourceUrl
                }
                subtitle
                title
                link
              }
              subtitle
              title
              image {
                altText
                sourceUrl
              }
            }
            linkmore {
              link
              title
            }
            isdeleted
          }
        }
      }
    }
  }
`;

export const AppContext = React.createContext<any>({});

export default function BizzoneCloud() {
  const { pathname } = useLocation();
  // const navigate = useNavigate();
  const urlCurrent = pathname.substring(11, pathname.length);
  const { data, loading } = useQuery(queryData, {
    variables: { title: urlCurrent },
  });

  if (loading) return <div>...</div>;
  // if (error) return navigate('/coming-soon');
  // if (!data.posts.edges[0]) return navigate('/coming-soon');

  const {
    banner,
    breadcrumd,
    featurecontent,
    header,
    informationdetail,
    listfeature,
    potentialbenefitcontent,
    potentialbenefitsaccordion,
    reasondetail,
    typefeature,
    typeproduct,
    groupproduct,
    isimagesticktop,
    slideproduct,
    linkmore,
  } = data.posts.edges[0].node.formpost;

  const location = useLocation();

  return (
    <PostContextProvider
      value={{
        banner,
        breadcrumd,
        featurecontent,
        header,
        informationdetail,
        listfeature,
        potentialbenefitcontent,
        potentialbenefitsaccordion,
        reasondetail,
        typefeature,
        typeproduct,
        groupproduct,
        isimagesticktop,
        slideproduct,
        linkmore,
      }}
    >
      <SEO
        title={`${queryUtil.capitalizeFirstLetter(header?.titlepopup)} | Unicloud Group`}
        description={`${header?.descriptionpopup?.slice(0, 120)}`}
        url={`${location.pathname}`}
        imgthumbs={header?.imageheader?.sourceUrl}
      />
      <main className="template-ecosystem">
        <RowHeader />
        <Container className="max-width-100" fluid>
          <RowSectionTow />
          <RowPotentialBenefits />
          <RowBanner />
          <RowProduct />
          <RowFeature />
          <RowReason />
          <RowLearnMore />
        </Container>
      </main>
      {/* <RowTechnology /> */}
    </PostContextProvider>
  );
}
