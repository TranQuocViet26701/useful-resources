import React, { useContext } from 'react';
import { Col, Row } from 'react-bootstrap';
import iconCup from '/assets/image/svg/icon-cup.svg';
import usePostContext from '../../../../context/usePostContext';

function RowReason() {
  const { reasondetail } = useContext(usePostContext);

  return (
    <Row className="px-0">
      <Row className="px-0 container-child ecosystem-why-choose">
        <Col xs={12} md={6} className="ecosystem-why-choose-left">
          <img
            src={reasondetail.imageleft.sourceUrl}
            className="choose-left__item"
            width={600}
            alt=""
          />
          <div className="stick-cup">
            <img src={iconCup} width={70} height={70} alt="icon cup" />
            <h4>1250+</h4>
            <h5>Khách hàng sử dụng</h5>
          </div>
        </Col>
        <Col xs={12} md={6} className="ecosystem-why-choose-right">
          <h4>{reasondetail.subtitle}</h4>
          <h3>{reasondetail.title}</h3>
          <span>{reasondetail.description}</span>
          <div className="why-choose-item">
            <ul>
              {reasondetail.listreason.map((reason: any) => (
                <li key={reason.listreasonitem}>{reason.listreasonitem}</li>
              ))}
            </ul>
          </div>
        </Col>
      </Row>
    </Row>
  );
}
export default RowReason;
