import React from 'react';
import { Col, Row, Image, Container } from 'react-bootstrap';
import '../../../../news/News.scss';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Autoplay } from 'swiper';
import { Link } from 'react-router-dom';
import RowHorizontal from '@src/components/rowHorizontal/RowHorizontal';

function RowProductType2(props: any) {
  const { title, subtitle, description, image, groupproductdetail, slideproduct } = props;

  return (
    <Container className="feature-product container-child">
      <div className="title-center-divider">
        <h4 className="fs-4">{title}</h4>
        <h3 className="fs-3">{subtitle}</h3>
        <span>{description}</span>
      </div>
      <img src={image[0].sourceUrl} alt={image[0].altText} className="img-center-product" />

      {groupproductdetail?.slice(0, 1).map((prod: any, index: number) => (
        <RowHorizontal
          key={prod.title}
          title={prod.title}
          subTitle={prod.subtitle}
          image={prod.image.sourceUrl}
          link={prod.link}
          description={prod.description}
          reverse={index % 2 !== 0 && index !== 0}
        />
      ))}
      <Row className="row-btn-slide-latest justify-content-between px-0">
        <Col xs={8} className="px-0">
          <h3 className="title-category-product">Sản phẩm Smart Home</h3>
        </Col>
        <Col xs={4} className="d-flex justify-content-end col-arrow-latest-news">
          <button
            type="button"
            className="btn-arrow-left btn-arrow-latest-news pre-slide-latest-news me-4"
          />
          <button
            type="button"
            className="btn-arrow-right btn-arrow-latest-news next-slide-latest-news"
          />
        </Col>
      </Row>
      <div className="div-under-product" />
      <Row className="slide-product px-0">
        <Swiper
          modules={[Navigation, Autoplay]}
          spaceBetween={16}
          slidesPerView={4}
          navigation={{
            prevEl: '.pre-slide-latest-news',
            nextEl: '.next-slide-latest-news',
          }}
          className="px-0 slide-product-swiper"
          // autoplay={{ delay: 6000 }}
          breakpoints={{
            320: {
              slidesPerView: 1.2,
              spaceBetween: 16,
            },
            450: {
              slidesPerView: 1.5,
              spaceBetween: 16,
            },
            550: {
              slidesPerView: 2,
              spaceBetween: 16,
            },
            861: {
              slidesPerView: 3,
              spaceBetween: 16,
            },
            992: {
              slidesPerView: 4,
              spaceBetween: 16,
            },
          }}
        >
          {slideproduct?.map((prod: any) => (
            <SwiperSlide key={prod.title}>
              <Link to="/news/:1">
                <div className="row-item-slide-latest-news">
                  <div className="div-img-slide-news">
                    <Image src={prod.image.sourceUrl} className="img-fluid img-product" />
                  </div>
                  <div className="latest-product">
                    <div className="div-detail-latest-news">
                      <p className="author-product">{prod.title}</p>
                      <p className="detail-product">{prod.subtitle}</p>
                    </div>
                  </div>
                </div>
              </Link>
            </SwiperSlide>
          ))}
        </Swiper>
      </Row>
      {groupproductdetail?.slice(1, 2).map((prod: any, index: number) => (
        <RowHorizontal
          title={prod.title}
          subTitle={prod.subtitle}
          image={prod.image.sourceUrl}
          link={prod.link}
          description={prod.description}
          reverse={index % 2 !== 0 && index !== 0}
        />
      ))}
      <img
        src={image[1].sourceUrl}
        alt={image[1].altText}
        className="img-center-product margin-top-118 margin-bottom-80 "
      />
      {groupproductdetail?.slice(2, 3).map((prod: any, index: number) => (
        <RowHorizontal
          title={prod.title}
          subTitle={prod.subtitle}
          image={prod.image.sourceUrl}
          link={prod.link}
          description={prod.description}
          reverse={index % 2 !== 0 && index !== 0}
        />
      ))}
    </Container>
  );
}

export default RowProductType2;
