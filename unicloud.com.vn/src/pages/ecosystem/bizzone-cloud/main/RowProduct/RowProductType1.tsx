import RowHorizontal from '@src/components/rowHorizontal/RowHorizontal';
import React from 'react';
import { Container } from 'react-bootstrap';

function RowProductType1(props: any) {
  const { title, subtitle, description, image, groupproductdetail } = props;

  return (
    <section>
      <Container className="feature-product">
        <div className="title-center-divider">
          <h4 className="fs-4">{title}</h4>
          <h3 className="fs-3">{subtitle}</h3>
          <span>{description}</span>
        </div>
        {image && (
          <img
            src={image[0]?.sourceUrl}
            alt={image[0].altText || ''}
            className="img-center-product"
          />
        )}
        {groupproductdetail.map((prod: any, index: number) => (
          <RowHorizontal
            key={prod.title}
            title={prod.title}
            subTitle={prod.subtitle}
            image={prod.image.sourceUrl}
            link={prod.link}
            description={prod.description}
            reverse={index % 2 !== 0 && index !== 0}
          />
        ))}
      </Container>
    </section>
  );
}

export default RowProductType1;
