import React, { useContext } from 'react';
import { Row } from 'react-bootstrap';
import RowChooseItemFeature from './RowFeature/RowChooseItemFeature';
import RowFeatureBizones from './RowFeature/RowFeatureBizzone';
import RowFeatureCart from './RowFeature/RowFeatureCart';
import RowFeaturePhone from './RowFeature/RowFeaturePhone';
import RowFeatureUnicloudCA from './RowFeature/RowFeatureUnicloudCA';
import imgLeft from '/assets/image/ecosystem/img-left-feature.png';
import usePostContext from '../../../../context/usePostContext';
import RowFeatureType2Card from './RowFeature/RowFeatureType4';

function RowFeature() {
  const { listfeature, typefeature, featurecontent } = useContext(usePostContext);

  return (
    <section className="px-0 ecosystem-feature">
      <div className="image-left">
        <img src={imgLeft} alt="" />
      </div>
      <Row className="px-0 ecosystem-feature-wrap container-child">
        <div className="feature-wrap__detail">
          <h4>{featurecontent.subtitle}</h4>
          <h3>{featurecontent.title}</h3>
          <span>{featurecontent.description}</span>
        </div>

        {typefeature === '1' && <RowFeatureBizones listFeatures={listfeature || []} />}
        {typefeature === '2' && (
          <RowFeatureUnicloudCA image={featurecontent.image.sourceUrl} listFeatures={listfeature} />
        )}
        {typefeature === '3' && <RowChooseItemFeature listFeatures={listfeature} />}
        {typefeature === '4' && (
          <RowFeatureType2Card image={featurecontent.image.sourceUrl} listFeatures={listfeature} />
        )}
        {typefeature === '5' && <RowFeatureCart listFeatures={listfeature} />}
        {typefeature === '6' && (
          <RowFeaturePhone image={featurecontent.image.sourceUrl} listFeatures={listfeature} />
        )}
      </Row>
    </section>
  );
}

export default RowFeature;
