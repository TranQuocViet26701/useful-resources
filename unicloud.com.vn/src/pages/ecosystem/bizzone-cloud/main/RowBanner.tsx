import React, { useContext } from 'react';
import { Col, Row } from 'react-bootstrap';
import usePostContext from '../../../../context/usePostContext';

function RowBanner() {
  const { banner } = useContext(usePostContext);
  return (
    <div imgUrl={banner.imagebanner.sourceUrl} className="row px-0 ecosystem-banner">
      <Row className=" container-child ecosystem-banner-wrap align-items-center justify-content-between">
        <Col xs={12} md={7} className="px-0 banner-wrap__left">
          <h4>{banner.title}</h4>
          <span>{banner.subtitle}</span>
        </Col>
        <Col xs={12} md={3} className="banner-wrap__right">
          <a href={banner.link || '/#'} target="_blank" rel="noopener noreferrer">
            <span className="btn-download text-center">{banner.textdownload}</span>
          </a>
        </Col>
      </Row>
    </div>
  );
}

export default RowBanner;
