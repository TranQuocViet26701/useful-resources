import React, { useContext } from 'react';
import Breadcrumb from '@src/components/breadcrumb/Breadcrumb';
// import iconTitle from '/assets/image/svg/icon-half-triangle.svg';
import { Col, Row } from 'react-bootstrap';
import usePostContext from '../../../../context/usePostContext';

function RowSectionTow() {
  const { informationdetail, breadcrumd } = useContext(usePostContext);

  const breadcrumbs: any[] = [
    {
      title: 'navbar.navLink.home',
      url: '/',
    },
    {
      title: 'navbar.navLink.ecosystem',
      url: '/ecosystem',
    },
    {
      title: breadcrumd,
      url: '#',
    },
  ];

  return (
    <section className="row px-0 ecosystem-information container-child">
      <div className="ecosystem-breadcrumb px-0 container-child">
        <Breadcrumb listBreadCrumb={breadcrumbs} />
      </div>
      {/* <div className="d-flex align-items-center title-information px-0 container-child">
        <img src={iconTitle} width={16} height={16} alt="" />
        <h3 className="fs-4  title-information-wrap ms-2">THÔNG TIN CHI TIẾT</h3>
      </div> */}
      <Row className="ecosystem-information-wrap px-0  container-child">
        <Col xs={12} sm={12} md={8} className="px-0  information-left">
          <img
            src={informationdetail.imageleft.sourceUrl}
            alt={informationdetail.imageleft.altText}
          />
        </Col>
        <Col xs={12} sm={12} md={3} className="information-right">
          <div className="information-right-wrap">
            <h4 className="fs-4">{informationdetail.subtitleinformationdetail}</h4>
            <h3 className="fs-3">{informationdetail.titleinformationdetail}</h3>
            <p>{informationdetail.descriptioninformationdetail}</p>
            {informationdetail.linkcta && (
              <div className="btn-right text-right">
                {/* <button type="button" className="btn-cta-normal">
                <span>Bảng giá</span>
              </button> */}
                <button type="button" className="btn-cta-gradient">
                  <span>Xem chi tiết</span>
                </button>
              </div>
            )}
          </div>
        </Col>
      </Row>
    </section>
  );
}
export default RowSectionTow;
