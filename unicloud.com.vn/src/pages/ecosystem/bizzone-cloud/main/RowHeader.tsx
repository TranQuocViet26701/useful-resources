/* eslint-disable @typescript-eslint/dot-notation */
import React, { useContext } from 'react';
import { Container } from 'react-bootstrap';
import * as __styled from 'styled-components';
import usePostContext from '../../../../context/usePostContext';

const styled = __styled.default;
const defaultStyled = typeof styled === 'function' ? styled : styled['default'];

export const ContainerDiv = defaultStyled('div')<{ imgUrl: string }>`
  background-image: ${(props) => `url(${props.imgUrl})`};
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
`;

export const PopupEcosystem = defaultStyled('div')<{ color: string }>`
  background: ${(props) => `${props.color}`};
  max-width: 569px;
  max-height: 320px;
  padding: 35px 37px;
  position: absolute;
  top: 31%;
  right: 19%;
  border-radius: 16px;
`;

export const TitlePopup = defaultStyled('h2')<{ color?: string }>`
  font-family: 'Chakra Petch';
  font-weight: 700;
  font-size: 48px;
  line-height: 48px;
  letter-spacing: -0.274522px;
  text-transform: uppercase;
  margin-bottom: 3px !important;
  color: ${(props) => `${props.color ? props.color : '#ffffff'} `};
`;

export const SubTitlePopup = defaultStyled('h4')<{ color?: string }>`
  font-family: 'Chakra Petch';
  font-style: normal;
  font-weight: 600;
  font-size: 22px;
  line-height: 100%;
  margin-bottom: 14px !important;
  color: ${(props) => `${props.color ? props.color : '#ffffff'}`};
`;

export const DescriptionPopup = defaultStyled('p')<{ color?: string }>`
  font-family: 'Inter';
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 150%;
  text-align: justify;
  color: ${(props) => `${props.color ? props.color : '#EBEBEB'}`};
`;

function PopupHeader(props: {
  title: string;
  subTitle: string;
  description: string;
  liner: string;
  type: string;
}) {
  const { title, subTitle, description, liner, type } = props;
  return (
    <PopupEcosystem color={liner} className={`ecosystem-popup type-popup-${type}`}>
      <div>
        <TitlePopup>{title}</TitlePopup>
        <SubTitlePopup>{subTitle}</SubTitlePopup>
        <DescriptionPopup>{description}</DescriptionPopup>
      </div>
    </PopupEcosystem>
  );
}

function RowHeader() {
  const { header, isimagesticktop } = useContext(usePostContext);

  const liner =
    'linear-gradient(180deg, rgba(89, 99, 126, 0.66) 58.66%, rgba(89, 99, 126, 0) 96.81%)';
  return (
    <Container className="px-0 template-ecosystem-header" fluid>
      <ContainerDiv imgUrl={header.imageheader.sourceUrl} className="ecosystem-header-wrap">
        <PopupHeader
          liner={header.typepopup === '2' || header.typepopup === '3' ? 'transparent' : liner}
          title={header.titlepopup}
          subTitle={header.subtitlepopup}
          description={header.descriptionpopup}
          type={header.typepopup}
        />
        {header.imagestick && (
          <img
            src={header.imagestick.sourceUrl}
            className={`img-sticky-header ${isimagesticktop ? 'img-sticky-header-top' : ''}`}
            alt=""
          />
        )}
        <div className="background-bottom" />
        <div className="mouse-small" />
      </ContainerDiv>
    </Container>
  );
}

export default RowHeader;
