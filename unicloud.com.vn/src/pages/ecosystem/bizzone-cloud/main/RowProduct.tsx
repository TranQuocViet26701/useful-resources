import React, { useContext } from 'react';
import RowProductType1 from './RowProduct/RowProductType1';
import RowProductType2 from './RowProduct/RowProductType2';
import usePostContext from '../../../../context/usePostContext';

function RowProduct() {
  const { typeproduct, groupproduct } = useContext(usePostContext);
  const { title, subtitle, description, image, groupproductdetail, slideproduct } = groupproduct;

  return (
    <section>
      {typeproduct === 1 && (
        <RowProductType1
          title={title}
          description={description}
          image={image}
          subtitle={subtitle}
          groupproductdetail={groupproductdetail}
        />
      )}
      {typeproduct === 2 && (
        <RowProductType2
          title={title}
          description={description}
          image={image}
          subtitle={subtitle}
          groupproductdetail={groupproductdetail}
          slideproduct={slideproduct}
        />
      )}
    </section>
  );
}

export default RowProduct;
