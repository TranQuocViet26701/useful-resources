import React, { useContext } from 'react';
import { Row } from 'react-bootstrap';
import usePostContext from '@context/usePostContext';

function RowLearnMore() {
  const { linkmore } = useContext(usePostContext);

  return (
    <a
      href={linkmore[0]?.link || '/#'}
      target="_blank"
      rel="noreferrer"
      className="btn-learn-more ecosystem-learn-more"
    >
      <Row
        className="px-0 align-items-center justify-content-center container-child"
        md={12}
        xs={12}
      >
        <div className="btn-learn-more-wrap text-center">
          <span>{linkmore[0]?.title || 'LEARN MORE ABOUT'}</span>
        </div>
      </Row>
    </a>
  );
}
export default RowLearnMore;
