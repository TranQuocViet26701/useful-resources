import React, { useContext } from 'react';
import { Accordion, Col, Row } from 'react-bootstrap';
// import iconApple from '/assets/image/homePage/icon-down-apple.png';
// import iconChPlay from '/assets/image/homePage/icon-down-chplay.png';
import usePostContext from '../../../../context/usePostContext';

function RowPotentialBenefits() {
  const { potentialbenefitcontent, potentialbenefitsaccordion } = useContext(usePostContext);

  return (
    <Row className="ecosystem-potential">
      <section className="container-child">
        <Row className="ecosystem-potential-benefits">
          <Col xs={12} md={5} className="px-0 potential-benefits-left">
            <h5>{potentialbenefitcontent.subtitle}</h5>
            <h4>{potentialbenefitcontent.title}</h4>
            <span>{potentialbenefitcontent.description}</span>

            <div className="benefits-accordion">
              <Accordion defaultActiveKey="0">
                {potentialbenefitsaccordion?.map((value: any, index: number) => (
                  <Accordion.Item key={value.accordion} eventKey={index.toString()}>
                    <Accordion.Header>{value.accordion}</Accordion.Header>
                    <Accordion.Body>{value.itemaccordion}</Accordion.Body>
                  </Accordion.Item>
                ))}
              </Accordion>
            </div>
          </Col>
          <Col xs={12} md={7} className="potential-benefits-right">
            <div className="benefits-img-right">
              <img
                src={potentialbenefitcontent.imageright.sourceUrl}
                alt={potentialbenefitcontent.imageright.altText}
                className="benefits-img-right__child"
              />
              {/* <div className="group-icon d-flex align-items-center justify-content-center">
                <a href="/#" target="_blank" rel="noreferrer">
                  <img
                    src={iconApple}
                    alt=""
                    className="me-4"
                    style={{
                      boxShadow: '0px 2px 10px 2px rgba(112, 144, 176, 0.1)',
                      borderRadius: '8px',
                    }}
                  />
                </a>
                <a href="/#" target="_blank" rel="noreferrer">
                  <img
                    src={iconChPlay}
                    alt=""
                    style={{
                      boxShadow: '0px 2px 10px 2px rgba(112, 144, 176, 0.1)',
                      borderRadius: '8px',
                    }}
                  />
                </a>
              </div> */}
            </div>
          </Col>
        </Row>
      </section>
    </Row>
  );
}

export default RowPotentialBenefits;
