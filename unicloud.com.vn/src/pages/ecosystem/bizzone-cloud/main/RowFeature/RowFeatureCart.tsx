import React from 'react';
import { Col, Row, Image } from 'react-bootstrap';

function RowFeatureCart(props: { listFeatures: any[] }) {
  const { listFeatures } = props;

  return (
    <Row className="ecosystem-feature-wrap container-child">
      <Row className="px-0 container-child">
        <Row className="row-uniCart">
          {listFeatures.map((feature) => (
            <Col
              key={feature.num}
              xs={12}
              md={6}
              className={`${feature.num % 2 === 0 ? 'col-right-uniCart' : 'col-left-uniCart'}`}
            >
              <Row>
                <Col xs={12} md={9}>
                  <h3
                    className={` ${
                      feature.num % 2 === 0 ? 'title-unicart-right' : 'title-unicart'
                    }`}
                  >
                    {feature.title}
                  </h3>
                  <p
                    className={` ${
                      feature.num % 2 === 0 ? 'detail-unicart-right' : 'detail-unicart'
                    } `}
                  >
                    {feature.desc}
                  </p>
                </Col>
                <Col
                  xs={12}
                  md={3}
                  className={` ${
                    feature.num % 2 === 0 ? 'order-first' : 'order-first  order-md-last'
                  }`}
                >
                  <Image src={feature.icon.sourceUrl} className="respon-img mx-auto" alt="" />
                </Col>
              </Row>
            </Col>
          ))}
        </Row>
      </Row>
    </Row>
  );
}

export default RowFeatureCart;
