import React from 'react';
import { Col, Row } from 'react-bootstrap';
import imgRight from '/assets/image/ecosystem/bizzone/img-right-feature.png';

function RowFeatureBizones(props: { listFeatures: any[] }) {
  const { listFeatures } = props;

  return (
    <Row className="px-0 feature-wrap__detail-group container-child">
      {listFeatures.map((feature) => (
        <Col key={feature.num} xs={6} md={4} className="feature-wrap__detail-item">
          <div className="d-flex detail-item-title">
            <img src={feature.icon.sourceUrl} alt="" width={32} className="detail-item-icon" />
            <h4>{feature.title}</h4>
          </div>
          <span className="detail-item-description">{feature.desc}</span>
        </Col>
      ))}
      <div className="img-right-stick">
        <img src={imgRight} alt="" />
      </div>
    </Row>
  );
}

export default RowFeatureBizones;
