import React from 'react';
import { Col, Row } from 'react-bootstrap';

function RowFeaturePhone(props: { listFeatures: any[]; image: any }) {
  const { listFeatures, image } = props;

  const listFeatureLeft = listFeatures.slice(0, 3);
  const listFeatureRight = listFeatures.slice(3, 6);

  return (
    <Row className="px-0 container-child ecosystem-feature-phone">
      <Col xs={12} md={4} className="feature-phone-wrap-left">
        {listFeatureLeft.map((feature) => (
          <Row className="px-0 feature-phone-wrap-left__item" key={feature.num}>
            <Col xs={2} md={2} className="px-0 wrap-left__item-img">
              <img src={feature.icon.sourceUrl} alt="" />
            </Col>
            <Col xs={10} md={10}>
              <h5>{feature.title}</h5>
              <span>{feature.desc}</span>
            </Col>
          </Row>
        ))}
      </Col>
      <Col xs={12} md={4} className="feature-phone-wrap-center">
        <img src={image} alt="" />
      </Col>
      <Col xs={12} md={4} className="feature-phone-wrap-left">
        {listFeatureRight.map((feature) => (
          <Row className="px-0 feature-phone-wrap-left__item" key={feature.num}>
            <Col xs={2} md={2} className="px-0 wrap-left__item-img">
              <img src={feature.icon.sourceUrl} alt="" />
            </Col>
            <Col xs={10} md={10}>
              <h5>{feature.title}</h5>
              <span>{feature.desc}</span>
            </Col>
          </Row>
        ))}
      </Col>
    </Row>
  );
}

export default RowFeaturePhone;
