import React from 'react';
import { Col, Row, Tab, Tabs } from 'react-bootstrap';
import imgLeft from '/assets/image/ecosystem/bizzone/img-feature.jpg';
import iconArrowOrange from '/assets/image/svg/icon-arrow-right-orange.svg';

function RowChooseItemFeature(props: { listFeatures: any[] }) {
  const { listFeatures } = props;

  return (
    <section className="px-0 ecosystem-feature-choose">
      <Tabs defaultActiveKey="1" id="uncontrolled-tab-example" className="px-0 mb-3 ">
        {listFeatures.map((feature) => (
          <Tab
            key={feature.num}
            eventKey={feature.num}
            title={
              <div className="feature-choose-wrap">
                <img src={feature.icon.sourceUrl} alt="" className="choose-icon" width={40} />
                <span>{feature.title}</span>
              </div>
            }
          >
            <Row className="container-child feature-choose-wrap-detail">
              <Col xs={12} md={6} className="px-0">
                <div className="d-flex ">
                  <img src={imgLeft} alt="" className="" />
                </div>
              </Col>
              <Col xs={12} md={6} className="feature-choose-wrap-detail__right">
                <h4>{feature.title}</h4>
                <span>{feature.desc}</span>
                <div className="feature-choose-wrap-item">
                  <ul>
                    {feature?.listfeatureitem.map((item: any) => (
                      <li key={item.num}>{item.listfeatureitemdetail}</li>
                    ))}
                  </ul>
                </div>
                <div className="more-detail">
                  <span> Xem thêm</span>
                  <span>
                    <img src={iconArrowOrange} alt="" />
                  </span>
                </div>
              </Col>
            </Row>
          </Tab>
        ))}
      </Tabs>
    </section>
  );
}

export default RowChooseItemFeature;
