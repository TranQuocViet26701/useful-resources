import React from 'react';
import { Col, Row, Image } from 'react-bootstrap';
import imgRightUniCA from '/assets/image/ecosystem/unicloudCA/img-right-uniCA.png';

function RowFeatureUnicloudCA(props: { listFeatures: any[]; image: string }) {
  const { listFeatures, image } = props;

  return (
    <Row className="ecosystem-feature-wrap container-child">
      <Row className="px-0 container-child">
        <Col xs={12} md={6}>
          <Row className="gy-3 gx-3 mb-3">
            {listFeatures.slice(0, 2).map((feature, index) => (
              <Col xs={12} md={6} className={index === 0 ? 'col-push-margin-uniCA' : ''}>
                <div className="col-item-uniCA">
                  <div className="d-flex div-icon-title">
                    <Image src={feature.icon.sourceUrl} className="icon-usb-smartcard" alt="" />
                    <h4 className="text-title-unicloudCA">{feature.title} </h4>
                  </div>
                  <span className="detail-feature-uniCA">{feature.desc}</span>
                </div>
              </Col>
            ))}
          </Row>
          <Row className="gy-3 gx-3">
            {listFeatures.slice(2, 4).map((feature, index) => (
              <Col xs={12} md={6} className={index === 0 ? 'col-push-margin-uniCA' : ''}>
                <div className="col-item-uniCA">
                  <div className="d-flex div-icon-title">
                    <Image src={feature.icon.sourceUrl} className="icon-usb-smartcard" alt="" />
                    <h4 className="text-title-unicloudCA">{feature.title} </h4>
                  </div>
                  <span className="detail-feature-uniCA">{feature.desc}</span>
                </div>
              </Col>
            ))}
          </Row>
        </Col>
        <Col xs={12} md={6}>
          <Row>
            <Image src={image || imgRightUniCA} className="img-fluid" alt="" />
          </Row>
        </Col>
      </Row>
    </Row>
  );
}

export default RowFeatureUnicloudCA;
