import React from 'react';
import { Col, Row, Image } from 'react-bootstrap';
import imgRightUniCA from '/assets/image/ecosystem/unicloudCA/img-right-uniCA.png';
import imgInvoice from '/assets/image/ecosystem/bizzone/image-invoice.png';

function RowFeatureType2Card(props: { listFeatures: any[]; image: string }) {
  const { listFeatures, image } = props;
  if (listFeatures.length === 0) {
    return <div>...</div>;
  }

  return (
    <Row className="ecosystem-feature-wrap container-child">
      <Row className="px-0 container-child">
        <Col xs={12} md={6}>
          <Row>
            <Image src={image || imgRightUniCA} className="img-fluid" alt="" />
          </Row>
        </Col>
        <Col xs={12} md={6}>
          <Row className="gy-3 gx-3">
            {listFeatures.map((feature) => (
              <Col xs={12} md={12}>
                <div className="col-item-unitype4">
                  <div className="d-flex div-icon-title">
                    <Image src={feature.icon.sourceUrl} className="icon-invoice" alt="" />
                    <Image src={imgInvoice} alt="" className="image-invoice" />
                    <div>
                      <h4 className="">{feature.title} </h4>
                      <span className="">{feature.desc}</span>
                    </div>
                  </div>
                </div>
              </Col>
            ))}
          </Row>
        </Col>
      </Row>
    </Row>
  );
}

export default RowFeatureType2Card;
