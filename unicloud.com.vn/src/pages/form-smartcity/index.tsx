import React from 'react';
import { Container, Form, Image, FormControl, Modal } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { useLocation, Link } from 'react-router-dom';

import axios from 'axios';
import logoUni from '/assets/image/form-smartcity/logo-uni-form-event.svg';
import logoSmartcity from '/assets/image/form-smartcity/logo-smart-citi-form-event.svg';
import iconMap from '/assets/image/form-smartcity/icon-map.svg';
import iconWord from '/assets/image/form-smartcity/icon-word.svg';
import iconV from '/assets/image/form-smartcity/icon-v-event.svg';

import imgSmartcity from '/assets/image/form-smartcity/img-text-enevt-smartcity.png';

import './FormSmartcity.scss';
import SEO from '@src/seo/seo';

function ModalThanks(props: any) {
  return (
    <Modal
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className="modal-thanks-event-form"
    >
      <Modal.Body>
        <p className="title-thanks-popup">
          <Image src={iconV} className="img-fluid" /> <span>Cảm Ơn Bạn Đã Liên Hệ</span>
        </p>
        <p className="detail-thanks-popup">
          Thông tin của bạn đã được ghi nhận, chúng tôi sẽ liên hệ tư vấn đến bạn sớm nhất có thể.
          Rất vui khi được trợ giúp bạn.
        </p>
        <Link to="/" className="link-thanks-popup">
          <button type="button">
            <span>Đã hiểu</span>
          </button>
        </Link>
      </Modal.Body>
    </Modal>
  );
}
function HomePage() {
  const [modalShow, setModalShow] = React.useState(false);
  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm();
  const location = useLocation();
  const googleSheetAPI =
    'https://script.google.com/macros/s/AKfycbxTlQD1WgGBuFKoKVjf6tiUGERX6DHHhxJfywGZ6R4xuNEurMdCdW4fbRoZwBW4jK6M/exec';

  const onSubmit = (data: any) => {
    // console.log('data', data);

    const formData = new FormData();
    formData.append('name', data.fullName);
    formData.append('phone', data.telephone);
    formData.append('content', data.content);
    formData.append('email', data.email);
    formData.append('boolean', 'true');
    formData.append('list', '6xdCd892x7gSZoG7768926aeLA');
    formData.append('subform', 'yes');

    const googleSheetFormData = new FormData();
    googleSheetFormData.append('fullName', data.fullName);
    googleSheetFormData.append('email', data.email);
    googleSheetFormData.append('telephone', `'${data.telephone}`);
    googleSheetFormData.append('company', data.company);
    googleSheetFormData.append('content', data.content);
    googleSheetFormData.append('address', data.address);
    googleSheetFormData.append('consultingField', data.area);
    googleSheetFormData.append('timestamp', new Date().toLocaleDateString().substring(0, 10));
    googleSheetFormData.append('linkedBy', location.pathname);

    axios
      .post('/subscribe', formData)
      .then(() => {
        // console.log('response: ', response.data);
        // console.log('response.status: ', response.status);
        // console.log('response.data: ', response.data);
      })
      .catch(() => {
        // console.error('Something went wrong!', error);
        // alert('Lỗi hệ thống.');
      });

    axios
      .post(googleSheetAPI, googleSheetFormData)
      .then((response: any) => {
        // console.log('response', response);
        if (response.status === 200) {
          setModalShow(true);
        }
        // console.log('response: ', response.data);
        // console.log('response.status: ', response.status);
        // console.log('response: ', response.data);
        //
      })
      .catch(() => {
        // console.error('Something went wrong!', error);
        // alert('Lỗi hệ thống.');
      });

    reset();
  };

  return (
    <main>
      <SEO title="Form Contact | Unicloud Group" url={location.pathname} />
      <Container className="max-width-100 px-0 bg-event-smart-city" fluid>
        <div className="div-wrapper-logo">
          <Link to="/">
            <Image src={logoUni} className="img-fluid" />
          </Link>
          <Image src={logoSmartcity} className="img-fluid" />
        </div>
        <div className="div-wrapper-img">
          <span>
            <Image src={imgSmartcity} className="img-fluid" />
          </span>
          <p>26-28.05.2022 / SECC - HCMC</p>
        </div>
        <div className="div-wrapper-booth">
          <Image src={iconMap} className="img-fluid" />
          <span style={{ marginRight: 14 }}>Booth: E14.15</span>
          <Image src={iconWord} className="img-fluid" />
          <span>
            <Link to="/">https://unicloud.com.vn</Link>
          </span>
        </div>
        <div className="row-contact-event">
          <Form
            onSubmit={handleSubmit(onSubmit)}
            onReset={reset}
            className="form-contact-event-smartcity"
          >
            <p>Thông tin của bạn</p>
            <Form.Group className="mb-4" controlId="ControlFullName">
              <Form.Label>
                Họ và tên<span style={{ color: 'red' }}> *</span>
              </Form.Label>
              {/* <Form.Control type="text" placeholder="Nhập tên của bạn" /> */}
              <Controller
                control={control}
                name="fullName"
                defaultValue=""
                rules={{ required: 'Chưa nhập họ tên' }}
                render={({ field: { onChange, value, ref } }) => (
                  <FormControl
                    onChange={onChange}
                    value={value}
                    ref={ref}
                    isInvalid={errors.fullName}
                    aria-describedby="Nhập tên của bạn"
                    autoComplete="off"
                    type="text"
                    placeholder="Nhập tên của bạn"
                    // required
                  />
                )}
              />
              <Form.Control.Feedback type="invalid" className="mx-3">
                {Object.keys(errors).length !== 0 && errors.fullName?.type === 'required' && (
                  <span style={{ color: 'red' }}>{errors.fullName?.message}</span>
                )}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group className="mb-4" controlId="ControlEmail">
              <Form.Label>
                Email<span style={{ color: 'red' }}> *</span>
              </Form.Label>
              {/* <Form.Control type="email" placeholder="Nhập email của bạn" /> */}
              <Controller
                control={control}
                name="email"
                defaultValue=""
                rules={{
                  required: 'Chưa nhập email',
                  pattern: {
                    value: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                    message: 'Vui lòng nhập đúng Email',
                  },
                }}
                render={({ field: { onChange, value, ref } }) => (
                  <FormControl
                    onChange={onChange}
                    value={value}
                    ref={ref}
                    isInvalid={errors.email}
                    placeholder="Nhập email của bạn"
                    aria-label="email"
                    aria-describedby="email"
                    autoComplete="off"
                    // required
                  />
                )}
              />
              <Form.Control.Feedback type="invalid" className="mx-3">
                {Object.keys(errors).length !== 0 && errors.email?.type === 'pattern' && (
                  <span>{errors.email?.message}</span>
                )}
                {Object.keys(errors).length !== 0 && errors.email?.type === 'required' && (
                  <span>{errors.email?.message}</span>
                )}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group className="mb-4" controlId="ControlTelephone">
              <Form.Label>
                Số điện thoại<span style={{ color: 'red' }}> *</span>
              </Form.Label>
              {/* <Form.Control type="text" placeholder="Nhập số điện thoại của bạn" /> */}
              <Controller
                control={control}
                name="telephone"
                defaultValue=""
                rules={{
                  required: 'Chưa nhập số điện thoại',
                  pattern: {
                    value: /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/,
                    message: 'Số điện thoại không đúng',
                  },
                }}
                render={({ field: { onChange, value, ref } }) => (
                  <FormControl
                    onChange={onChange}
                    value={value}
                    ref={ref}
                    isInvalid={errors.telephone}
                    className="telephone"
                    placeholder="Nhập số điện thoại của bạn"
                    aria-label="telephone"
                    aria-describedby="telephone"
                    autoComplete="off"
                    // required
                  />
                )}
              />
              <Form.Control.Feedback type="invalid" className="mx-3">
                {Object.keys(errors).length !== 0 && errors.telephone?.type === 'pattern' && (
                  <span>{errors.telephone?.message}</span>
                )}
                {Object.keys(errors).length !== 0 && errors.telephone?.type === 'required' && (
                  <span>{errors.telephone?.message}</span>
                )}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group className="mb-4" controlId="ControlCompany">
              <Form.Label>Tên Công Ty</Form.Label>
              <Controller
                control={control}
                name="company"
                defaultValue=""
                render={({ field: { onChange, value, ref } }) => (
                  <FormControl
                    onChange={onChange}
                    value={value}
                    ref={ref}
                    placeholder="Nhập tên Công Ty"
                    aria-label="company"
                    aria-describedby="company"
                    autoComplete="off"
                    type="text"
                    // required
                  />
                )}
              />
            </Form.Group>
            <Form.Group className="mb-4" controlId="ControlAddress">
              <Form.Label>Địa chỉ</Form.Label>
              {/* <Form.Control type="text" placeholder="123/4 Đường gì đó, Quận 2 , TP.Hồ Chí..." /> */}
              <Controller
                control={control}
                name="address"
                defaultValue=""
                render={({ field: { onChange, value, ref } }) => (
                  <FormControl
                    onChange={onChange}
                    value={value}
                    ref={ref}
                    placeholder="123/4 Đường gì đó, Quận 2 , TP.Hồ Chí..."
                    aria-label="address"
                    aria-describedby="address"
                    autoComplete="off"
                    type="text"
                    // required
                  />
                )}
              />
            </Form.Group>
            <Form.Group className="mb-4" controlId="ControlFields">
              <Form.Label>
                Lĩnh vực muốn tư vấn<span style={{ color: 'red' }}> *</span>
              </Form.Label>
              <Controller
                control={control}
                name="area"
                defaultValue=""
                rules={{
                  required: 'Chọn lĩnh vực',
                }}
                render={({ field: { onChange, value, ref } }) => (
                  <Form.Select
                    onChange={onChange}
                    value={value}
                    ref={ref}
                    isInvalid={errors.area}
                    aria-label="Default select example"
                    required
                  >
                    <option>Chọn lĩnh vực</option>
                    <option value="Digital Banking Platform">Digital Banking Platform</option>
                    <option value="Digital Transformation">Digital Transformation</option>
                    <option value="Smart City">Smart City</option>
                    <option value="Virtual Reality">Virtual Reality</option>
                  </Form.Select>
                )}
              />
              <Form.Control.Feedback type="invalid" className="mx-3">
                {Object.keys(errors).length !== 0 && errors.area?.type === 'required' && (
                  <span>{errors.area?.message}</span>
                )}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group className="mb-4" controlId="ControlContent">
              <Form.Label>Nội dung của bạn</Form.Label>
              {/* <Form.Control as="textarea" rows={3} placeholder="name@example.com" /> */}
              <Controller
                control={control}
                name="content"
                defaultValue=""
                render={({ field: { onChange, value, ref } }) => (
                  <Form.Control
                    onChange={onChange}
                    value={value}
                    ref={ref}
                    isInvalid={errors.content}
                    aria-label="Default select example"
                    // required
                    as="textarea"
                    rows={3}
                    placeholder="Sản phẩm, lĩnh vực kinh doanh,..."
                  />
                )}
              />
            </Form.Group>
            <Controller
              control={control}
              name="boolean"
              defaultValue=""
              render={() => <input type="hidden" name="boolean" value="true" />}
            />
            <Controller
              control={control}
              name="hidden2"
              defaultValue=""
              render={() => <input type="hidden" name="list" value="6xdCd892x7gSZoG7768926aeLA" />}
            />

            <Controller
              control={control}
              name="hidden3"
              defaultValue=""
              render={() => <input type="hidden" name="subform" value="yes" />}
            />
            <button type="submit">
              <span>TƯ VẤN CHO TÔI</span>
            </button>
          </Form>
        </div>
        <ModalThanks show={modalShow} onHide={() => setModalShow(false)} />
      </Container>
    </main>
  );
}
export default HomePage;
