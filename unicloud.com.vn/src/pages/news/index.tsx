import React from 'react';
import { Container } from 'react-bootstrap';
import SEO from '@src/seo/seo';
import RowLatestAndRelatedNews from '@src/components/latest-and-related-news/RowLatestAndRelatedNews';
import { Navigate, useLocation } from 'react-router-dom';
import QueryPostNews from '@src/query/queryPostNew';

import { PostContextProvider } from '@src/context/usePostContext';
import { useTranslation } from 'react-i18next';
import RowTopNews from './main/RowTopNews';
import RowNews from './main/RowNews';
import './News.scss';

// import RowNewsFintech from './main/RowNewsFintech';
// import RowBanner from './main/RowBanner';
// import RowDigitalAndSmartCity from './main/RowDigitalAndSmartCity';
// import RowVirtualReality from './main/RowVirtualReality';

function News() {
  const { data, loading, error } = QueryPostNews();
  const { t, ready } = useTranslation();

  if (loading) return <div>...</div>;
  if (error) return <Navigate to="/coming-soon" />;
  if (!data.posts.nodes[0]) return <Navigate to="/coming-soon" />;

  const dataNews = data.posts;

  let dataPost = dataNews.nodes as Array<any>;
  let dataPostFilter = dataNews.nodes as Array<any>;
  if (dataPost.length > 0) {
    dataPost = dataPost
      .slice()
      .sort((a, b) => a?.groupnews && b?.groupnews && a.groupnews.index - b.groupnews.index);
    dataPostFilter = dataPost.filter((value) => value.groupnews.isdelete === null);
  }

  const location = useLocation();

  const lengthNews = dataNews.nodes.length;
  return (
    <>
      <SEO
        title="Tin tức | Unicloud Group"
        description="Tin tức | Unicloud Group"
        url={location.pathname}
      />
      <PostContextProvider value={{ dataPostFilter }}>
        <main>
          <Container className="max-width-100 px-0" fluid>
            <RowTopNews />
            <RowNews />
            {dataNews && (
              <RowLatestAndRelatedNews
                data={dataPostFilter.slice(5, lengthNews - 1)}
                title={ready ? t('news.posts') : ''}
              />
            )}
            {/* <RowNewsFintech />
          <RowBanner />
          <RowDigitalAndSmartCity />
          <RowVirtualReality /> */}
          </Container>
        </main>
      </PostContextProvider>
    </>
  );
}
export default News;
