import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Image } from 'react-bootstrap';
import iconHomeNews from '/assets/image/news/icon-home-news.svg';

import { useTranslation } from 'react-i18next';

function RowTopNews() {
  const { t, ready } = useTranslation();
  return (
    <Row className="back-ground-top-news px-0 d-flex justify-content-center">
      <Row className="max-width-1180 padding-left-right">
        <Col className="px-0">
          <h2 className="title-news-technology">{ready && t('news.title')}</h2>
          <div className="div-sub-news">
            <Image src={iconHomeNews} className="img-fuid icon-home-news" alt="" />
            <Link to="/" className="title-go-back-news">
              {ready && t('news.subtitle1')}
            </Link>
            <span className="middle-news">/</span>
            <Link to="/news" className="sub-title-news">
              {ready && t('news.subtitle2')}
            </Link>
          </div>
        </Col>
      </Row>
    </Row>
  );
}
export default RowTopNews;
