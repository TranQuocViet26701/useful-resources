import React from 'react';
import { Container } from 'react-bootstrap';

function RowBanner() {
  return (
    <div className="back-ground-main-banner">
      <Container className="max-width-1300 padding-left-right">
        <div className="back-ground-banner">
          <div className="div-respon-banner">
            <span className="title-banner-news-page">Unicloud</span> <br />
            <span className="detail-banner-news-page">
              Kiến tạo hệ sinh thái ngân hàng số phục vụ tương lai.
            </span>
          </div>
        </div>
      </Container>
    </div>
  );
}
export default RowBanner;
