import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Image, Container } from 'react-bootstrap';
import arrowViewMore from '/assets/image/svg/arrow-view-more.svg';

import imgNews1 from '/assets/image/news/img-news-1.png';
import imgNews2 from '/assets/image/news/img-news-2.png';
import imgNews3 from '/assets/image/news/img-news-3.png';
import imgNews4 from '/assets/image/news/img-news-4.png';
import imgNews5 from '/assets/image/news/img-news-5.png';
import calendarNewsPage from '/assets/image/svg/calendar-news-page.svg';

function RowNewsFintech() {
  return (
    <div className="back-ground-news-fintech">
      <Container className="max-width-1300 padding-left-right">
        <Row className="row-btn-slide-latest justify-content-between">
          <Col xs={8} lg={8} xl={9} className="pe-0">
            <h3 className="title-category-news">BẢN TIN FITECH</h3>
          </Col>
          <Col xs={4} lg={4} xl={3} className="d-flex justify-content-end">
            <Link to="/" className="btn-view-more-news-fitech">
              Xem thêm &nbsp;
              <Image src={arrowViewMore} alt="" />
            </Link>
          </Col>
        </Row>
        <div className="div-under-category" />
        <Row className="row-main-news-fitech gx-lg-5">
          <Col lg={6} className="col-item-news-fitech mt-3">
            <div className="div-img-fitech">
              <Image src={imgNews1} className="img-fluid img-news-fitech" alt="" />
              <div className="div-time-news-fitech">
                <p className="date-news-fitech">18</p>
                <p className="month-year-news-fitech">02,2022</p>
              </div>
              <div className="div-fitech-uni-1">Bản tin FITECH</div>
              <Link to="/news/:1">
                <h3 className="title-news-main">
                  Sống tiện nghi hơn với nhà thông minh của Sunshine Group
                </h3>
              </Link>
              <p className="detail-news-main">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when an
                unknown printer took a galley of type and scrambled it to make a type specimen book.
              </p>
            </div>
          </Col>
          <Col lg={6} className="col-item-news-fitech mt-3">
            <Row className="gx-3">
              <Col lg={4} xs={4}>
                <Link to="/news/:1">
                  <Image src={imgNews2} className="img-fluid img-sub-main-news" alt="" />
                </Link>
              </Col>
              <Col lg={8} xs={8}>
                <Link to="/news/:1">
                  <h3 className="title-news-main-sub">
                    Tập đoàn bất động sản phòng chống Covid-19 bằng công nghệ 4.0
                  </h3>
                </Link>
                <p className="detail-news-main-sub">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                  Ipsum has been the industry&apos;s standard ...
                </p>
                <div>
                  <Image src={calendarNewsPage} alt="" />
                  <span className="date-time-news-main-sub">18 tháng 03,2022</span>
                </div>
              </Col>
            </Row>
            <div className="div-under-news-main-sub" />
            <Row className="gx-3">
              <Col lg={4} xs={4}>
                <Link to="/news/:1">
                  <Image src={imgNews3} className="img-fluid img-sub-main-news" alt="" />
                </Link>
              </Col>
              <Col lg={8} xs={8}>
                <Link to="/news/:1">
                  <h3 className="title-news-main-sub">
                    Tập đoàn bất động sản phòng chống Covid-19 bằng công nghệ 4.0
                  </h3>
                </Link>
                <p className="detail-news-main-sub">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                  Ipsum has been the industry&apos;s standard ...
                </p>
                <div>
                  <Image src={calendarNewsPage} alt="" />
                  <span className="date-time-news-main-sub">18 tháng 03,2022</span>
                </div>
              </Col>
            </Row>
            <div className="div-under-news-main-sub" />
            <Row className="gx-3">
              <Col lg={4} xs={4}>
                <Link to="/news/:1">
                  <Image src={imgNews4} className="img-fluid img-sub-main-news" alt="" />
                </Link>
              </Col>
              <Col lg={8} xs={8}>
                <Link to="/news/:1">
                  <h3 className="title-news-main-sub">
                    Tập đoàn bất động sản phòng chống Covid-19 bằng công nghệ 4.0
                  </h3>
                </Link>
                <p className="detail-news-main-sub">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                  Ipsum has been the industry&apos;s standard ...
                </p>
                <div>
                  <Image src={calendarNewsPage} alt="" />
                  <span className="date-time-news-main-sub">18 tháng 03,2022</span>
                </div>
              </Col>
            </Row>
            <div className="div-under-news-main-sub" />
            <Row className="gx-3">
              <Col lg={4} xs={4}>
                <Link to="/news/:1">
                  <Image src={imgNews5} className="img-fluid img-sub-main-news" alt="" />
                </Link>
              </Col>
              <Col lg={8} xs={8}>
                <Link to="/news/:1">
                  <h3 className="title-news-main-sub">
                    Tập đoàn bất động sản phòng chống Covid-19 bằng công nghệ 4.0
                  </h3>
                </Link>
                <p className="detail-news-main-sub">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                  Ipsum has been the industry&apos;s standard ...
                </p>
                <div>
                  <Image src={calendarNewsPage} alt="" />
                  <span className="date-time-news-main-sub">18 tháng 03,2022</span>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default RowNewsFintech;
