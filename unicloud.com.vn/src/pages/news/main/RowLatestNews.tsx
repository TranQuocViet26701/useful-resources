import React, { useContext } from 'react';
import { Row, Col, Image, Container } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Autoplay } from 'swiper';
import imgSlideTechnology from '/assets/image/png/img-slide-technology.png';

import usePostContext from '../../../context/usePostContext';
import '../../../components/technology/RowTechnology.scss';

function RowLatestNews() {
  const { dataPost } = useContext(usePostContext);
  const dataCurrents = dataPost;

  return (
    <div className="back-ground-latest-news">
      <Container className="max-width-1300 padding-left-right-latest-news">
        <Row className="row-btn-slide-latest justify-content-between">
          <Col>
            <h3 className="title-category-news">BÀI VIẾT MỚI NHẤT</h3>
          </Col>
          <Col className="d-flex justify-content-end col-arrow-latest-news">
            <button
              type="button"
              className="btn-arrow-left btn-arrow-latest-news pre-slide-latest-news me-4"
            />
            <button
              type="button"
              className="btn-arrow-right btn-arrow-latest-news next-slide-latest-news"
            />
          </Col>
        </Row>
        <div className="div-under-category" />
        <Row>
          <Swiper
            modules={[Navigation, Autoplay]}
            spaceBetween={16}
            slidesPerView={4}
            navigation={{
              prevEl: '.pre-slide-latest-news',
              nextEl: '.next-slide-latest-news',
            }}
            className="swiper-latest-news"
            // autoplay={{ delay: 6000 }}
            breakpoints={{
              320: {
                slidesPerView: 1.2,
                spaceBetween: 16,
              },
              450: {
                slidesPerView: 1.5,
                spaceBetween: 16,
              },
              550: {
                slidesPerView: 2,
                spaceBetween: 16,
              },
              861: {
                slidesPerView: 3,
                spaceBetween: 16,
              },
              992: {
                slidesPerView: 4,
                spaceBetween: 16,
              },
            }}
          >
            {dataCurrents.map((post: any) => (
              <SwiperSlide key={post.id}>
                <div className="row-item-slide-latest-news">
                  <Image src={imgSlideTechnology} className="img-fluid img-latest-news" alt="" />
                  <div className="latest-news-content">
                    <div className="div-time-latest-news">
                      <p className="date-latest-news">18</p>
                      <p className="month-year-latest-news">02,2022</p>
                    </div>
                    <div className="div-detail-latest-news">
                      <p className="author-latest-news">
                        Đăng bởi: <span className="text-author-latest-news">Admin</span>
                      </p>
                      <p className="detail-latest-news">{post?.groupnews?.title}</p>
                    </div>
                  </div>
                </div>
              </SwiperSlide>
            ))}
          </Swiper>
        </Row>
      </Container>
    </div>
  );
}
export default RowLatestNews;
