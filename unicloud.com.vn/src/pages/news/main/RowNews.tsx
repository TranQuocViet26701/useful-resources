import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Image, Container } from 'react-bootstrap';
import queryUtil from '@src/query/queryUtil';
import { useTranslation } from 'react-i18next';
import usePostContext from '../../../context/usePostContext';

function RowNews() {
  const { dataPostFilter } = useContext(usePostContext);
  const dataCurrents = dataPostFilter;

  const { t, ready } = useTranslation();
  return (
    <div className="back-ground-news">
      <Container className="max-width-1300 px-0">
        <Row className="padding-left-right">
          <Col lg={6} className="col-item-news main-news ">
            <Link to={dataCurrents[0]?.groupnews?.link}>
              <div className="div-img-new">
                <Image
                  src={dataCurrents[0]?.groupnews?.image.sourceUrl}
                  className="img-fluid img-news-main main-news"
                  alt=""
                />
                <div className="div-absolute-news-1">
                  <div className="div-news-uni-1">{ready && t('news.unicloud')}</div>

                  <p className="title-news-uni-1">
                    {dataCurrents[0]?.groupnews?.title?.substring(0, 86)}
                    {dataCurrents[0]?.groupnews?.title?.length > 86 ? '...' : ''}
                  </p>

                  <p />
                </div>
                <div className="div-time-news-1">
                  <p className="date-news-1">
                    {queryUtil.convertDate(dataCurrents[0]?.date, 'date') > 10
                      ? queryUtil.convertDate(dataCurrents[0]?.date, 'date')
                      : `0${queryUtil.convertDate(dataCurrents[0]?.date, 'date')}`}
                  </p>
                  <p className="month-year-news-1">
                    {queryUtil.convertDate(dataCurrents[0]?.date, 'month') > 10
                      ? queryUtil.convertDate(dataCurrents[0]?.date, 'month')
                      : `0${queryUtil.convertDate(dataCurrents[0]?.date, 'month')}`}
                    , {queryUtil.convertDate(dataCurrents[0]?.date, 'year')}
                  </p>
                </div>
              </div>
            </Link>
          </Col>
          <Col lg={6} className="">
            <Row className="group-news">
              {dataCurrents &&
                dataCurrents.length > 1 &&
                dataCurrents.slice(1, 5).map((post: any) => (
                  <Col key={post.postId} md={6} lg={6} className="col-item-news">
                    <Link to={post?.groupnews?.link || '/coming-soon'}>
                      <div className="div-news-right">
                        <Image
                          src={post?.groupnews?.image?.sourceUrl}
                          className="img-fluid img-news-sub"
                          alt=""
                        />
                        <div className="div-absolute-news-2">
                          <div className="div-news-uni-2">{ready && t('news.unicloud')}</div>

                          <p className="title-news-uni-2">
                            {post?.groupnews?.title?.substring(0, 72)}
                            {post?.groupnews?.title?.length > 72 ? ' ...' : ''}
                          </p>
                        </div>
                        <div className="div-time-news-2">
                          <p className="date-news-2">
                            {queryUtil.convertDate(post?.date, 'date') > 10
                              ? queryUtil.convertDate(post?.date, 'date')
                              : `0${queryUtil.convertDate(post?.date, 'date')}`}
                          </p>
                          <p className="month-year-news-2">
                            {queryUtil.convertDate(post?.date, 'month') > 10
                              ? queryUtil.convertDate(post?.date, 'month')
                              : `0${queryUtil.convertDate(post?.date, 'month')}`}
                            , {queryUtil.convertDate(post?.date, 'year')}
                          </p>
                        </div>
                      </div>
                    </Link>
                  </Col>
                ))}
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default RowNews;
