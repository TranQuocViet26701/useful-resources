import React from 'react';
import { Col, Row, Image } from 'react-bootstrap';
// import { HashLink } from 'react-router-hash-link';
// import playIcon from '/assets/image/svg/icon-paly-blue.svg';
import bizzoneCloud from '/assets/image/homePage/img-bizzone-enterprite.png';
import unicloudCA from '/assets/image/homePage/unicloud-ca.png';
// import unicart from '/assets/image/homePage/unicart.png';
// import ekyc from '/assets/image/homePage/ekyc.png';
// import ekycImg from '/assets/image/homePage/img-eKYC.png';

// import ModalVideo from '@src/components/modal/modal-video';

// import unicab from '/assets/image/homePage/unicab.png';
import uniHome from '/assets/image/homePage/uni-home.png';
import iconArrowRightBlue from '/assets/image/svg/icon-arrow-right-blue.svg';
import { useTranslation } from 'react-i18next';
// import { Swiper, SwiperSlide } from 'swiper/react';
// import { Navigation, Autoplay } from 'swiper';import { useTranslation } from 'react-i18next';

function BuildImage(props: { url: string }) {
  const { url } = props;
  return (
    <div className="card-digital-image max-width-629 text-center">
      <Image src={url} alt="" loading="lazy" className="img-fluid" />
    </div>
  );
}

// function BuildSlide(props: { urls: any[] }) {
//   const { urls } = props;
//   return (
//     <Row className=" justify-content-center card-digital-slide">
//       <Col xs={2} md={2} className="px-0 text-center align-self-center">
//         <button type="button" className="btn-arrow-left btn-arrow-left-smart-living me-3" />
//       </Col>
//       <Col xs={8} md={6}>
//         <Swiper
//           modules={[Navigation, Autoplay]}
//           navigation={{
//             prevEl: '.pre-slide-technology',
//             nextEl: '.next-slide-technology',
//           }}
//           pagination={{ clickable: true }}
//           className="swiper-card-digital"
//           breakpoints={{
//             1196: {
//               slidesPerView: 1,
//               spaceBetween: 35,
//             },
//             375: {
//               slidesPerView: 1,
//               spaceBetween: 22,
//             },
//           }}
//         >
//           {urls.map((url, index) => (
//             <SwiperSlide key={`${index + url}`}>
//               <div className="card-img-slide">
//                 <Image src={url} className="img-fluid" loading="lazy" alt="" />
//               </div>
//             </SwiperSlide>
//           ))}
//         </Swiper>
//       </Col>
//       <Col xs={2} md={2} className="px-0 text-center align-self-center">
//         <button
//           type="button"
//           className="btn-arrow-right btn-arrow-right-smart-living  next-slide-smart-living"
//         />
//       </Col>
//     </Row>
//   );
// }

// function BuildCardSlide(props: {
//   title: string;
//   subTitle: string;
//   urlSlide: any[];
//   isShowViewDetail: boolean;
//   linkTo: string;
//   translate: any;
// }) {
//   const { title, subTitle, urlSlide, isShowViewDetail, linkTo, translate } = props;

//   return (
//     <>
//       <div className="card-item-title">
//         <h3>{title}</h3>
//         <h4>{subTitle}</h4>
//         {isShowViewDetail && (
//           <a href={linkTo} target="_blank" rel="noreferrer" className="btn-more">
//             <span>{translate('homepage.digitalBankingPlatform.viewDetail')}</span>
//             <span>
//               <img src={iconArrowRightBlue} alt="" />
//             </span>
//           </a>
//         )}
//       </div>
//       <BuildSlide urls={urlSlide} />
//     </>
//   );
// }

function BuildCardImage(props: {
  title: string;
  subTitle: string;
  urlImage: string;
  isShowViewDetail: boolean;
  translate: any;
  linkTo: string;
}) {
  const { title, subTitle, urlImage, isShowViewDetail, translate, linkTo } = props;

  //   const isBlank = title === 'UniCloudCA';
  //   let typeViewMore = 0;
  //   if (isShowViewDetail && isBlank) {
  //     typeViewMore = 1;
  //   } else if (isShowViewDetail && isBlank === false) {
  //     typeViewMore = 2;
  //   } else typeViewMore = 3;
  return (
    <>
      <div className="card-item-title">
        <h3>{title}</h3>
        <h4>{subTitle}</h4>
        {isShowViewDetail && (
          <a href={linkTo} target="_blank" rel="noreferrer" className="btn-more">
            <span>{translate('homepage.digitalBankingPlatform.viewDetail')}</span>
            <span>
              <img src={iconArrowRightBlue} alt="" />
            </span>
          </a>
        )}
      </div>
      <BuildImage url={urlImage} />
    </>
  );
}

// function BuildCardVideo(props: {
//   title: string;
//   subTitle: string;
//   isShowViewDetail: boolean;
//   linkTo: string;
//   translate: any;
// }) {
//   const { title, subTitle, isShowViewDetail, linkTo, translate } = props;
//   //   const [modalShow, setModalShow] = React.useState(false);

//   return (
//     <div className="card-video col-video" style={{ backgroundImage: `url(${ekyc})` }}>
//       <div className="card-item-title">
//         <h3 className="text-while">{title}</h3>
//         <h4>{subTitle}</h4>
//         {isShowViewDetail && (
//           <a href={linkTo} target="_blank" rel="noreferrer" className="btn-more">
//             <span>{translate('homepage.digitalBankingPlatform.viewDetail')}</span>
//             <img src={iconArrowRightBlue} alt="" />
//           </a>
//         )}
//       </div>
//       {/* <button
//         type="button"
//         className="text-center icon-play btn-play-video"
//         onClick={() => setModalShow(true)}
//       >
//         <img src={playIcon} alt="" />
//       </button>
//       <ModalVideo
//         show={modalShow}
//         onHide={() => setModalShow(false)}
//         urlVideo="https://www.youtube.com/watch?v=6VsQkaZTa0c"
//       /> */}
//     </div>
//   );
// }

function RowDigitalTransformation() {
  //   const urlSlide: any[] = [bizzoneCloud, bizzoneCloud, bizzoneCloud];
  const { t, ready } = useTranslation();

  return (
    <section className="digital-banking" id="digital-transformation">
      <div id="section-digital-transformation" />
      <Row className="container-child digital-banking-title">
        <a href="coming-soon" target="_blank">
          <h2 className="title">DIGITAL TRANSFORMATION</h2>
        </a>
        <p className="description description-padding">
          {ready && t('homepage.digitalTransformation.subTitle')}
        </p>
      </Row>
      <Row className="container-wrap grid-2">
        <Col xs={12} md={6} className="card-item grid-2-item">
          {/* <BuildCardSlide
            title="Bizzone Enterprite"
            subTitle={ready && t('homepage.digitalTransformation.bizzoneCluod.subTitle')}
            urlSlide={urlSlide}
            isShowViewDetail
            linkTo="/bizzone-enterprise"
            translate={t}
          /> */}
          <BuildCardImage
            title="Bizzone HR"
            subTitle={ready ? t('homepage.digitalTransformation.bizzoneCluod.subTitle') : ''}
            urlImage={bizzoneCloud}
            isShowViewDetail
            linkTo="https://bizzone.vn/bizzone-hr"
            translate={t}
          />
        </Col>
        <Col xs={12} md={6} className="card-item grid-2-item">
          <BuildCardImage
            title="UnicloudCA"
            subTitle={ready ? t('homepage.digitalTransformation.unicloudCA.subTitle') : ''}
            urlImage={unicloudCA}
            isShowViewDetail
            linkTo="https://unicloudca.vn/"
            translate={t}
          />
        </Col>
      </Row>
      {/* <Row className="container-wrap grid-2">
        <Col xs={12} md={6} className="card-item grid-2-item">
          <BuildCardImage
            title="Usee"
            subTitle={ready ? t('homepage.digitalTransformation.UniCART.subTitle') : ''}
            urlImage={unicart}
            isShowViewDetail
            translate={t}
            linkTo="/coming-soon"
          />
        </Col>
        <Col xs={12} md={6} className="card-item grid-2-item">
          <BuildCardImage
            title="eKYC"
            subTitle={ready ? t('homepage.digitalTransformation.EKYC.subTitle') : ''}
            urlImage={ekycImg}
            isShowViewDetail
            translate={t}
            linkTo="/ekyc"
          />
        </Col>
      </Row> */}
      <Row className="container-wrap grid-2">
        {/* <Col xs={12} md={6} className="card-item grid-2-item">
          <BuildCardImage
            title="UniCAB"
            subTitle={ready ? t('homepage.digitalTransformation.uniCAB.subTitle') : ''}
            urlImage={unicab}
            isShowViewDetail={false}
            translate={t}
            linkTo="#"
          />
        </Col> */}
        <Col xs={12} md={6} className="card-item grid-2-item">
          <BuildCardImage
            title="Bizzone Homes"
            subTitle={ready ? t('homepage.digitalTransformation.uniHOME.subTitle') : ''}
            urlImage={uniHome}
            isShowViewDetail
            translate={t}
            linkTo="/ecosystem/digital-transformation/bizzone-homes"
          />
        </Col>
      </Row>
    </section>
  );
}

export default RowDigitalTransformation;
