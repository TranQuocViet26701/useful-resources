import React from 'react';
import { Row, Col, Image, Container } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Autoplay } from 'swiper';
import { useTranslation } from 'react-i18next';
import logoSunshine from '/assets/image/homePage/logo-sunshine-home-main.svg';
import logoSmartConstruction from '/assets/image/homePage/logo-smart-construction-main.svg';
import logoKSFinance from '/assets/image/homePage/logo-ks-finance-main.svg';
import logoMediaPartner from '/assets/image/homePage/logo-media-partner-main.svg';
import logoKienLongBank from '/assets/image/homePage/logo-kienlong-bank.svg';
import iconHover from '/assets/image/homePage/icon-hover-partnership-logo.svg';

function RowPartnership() {
  const { t, ready } = useTranslation();

  return (
    <div className="back-ground-partnership ">
      <Container className="max-width-1180 padding-left-right-partnership">
        <h2 className="title title-partnership-1">{ready && t('homepage.partnership.title')}</h2>
        <p className="description title-partnership-2">{ready && t('homepage.partnership.subTitle')}</p>
        <Row className="row-slide-partnership">
          <Col
            xs={{ span: 6, order: 2 }}
            lg={{ span: 1, order: 1 }}
            className="col-arrow-left-partnership mt-lg-0 mt-4 
            justify-content-lg-center justify-content-end align-items-center pr-0"
          >
            <button
              type="button"
              className="btn-arrow-left btn-arrow-partnership pre-slide-partnership"
            />
          </Col>
          <Col xs={{ span: 12 }} lg={{ span: 10, order: 2 }} className="px-0 col-slide-partnership">
            <Swiper
              modules={[Navigation, Autoplay]}
              spaceBetween={16}
              slidesPerView={4}
              navigation={{
                prevEl: '.pre-slide-partnership',
                nextEl: '.next-slide-partnership',
              }}
              className="swiper-partnership"
              autoplay={{ delay: 3000 }}
              breakpoints={{
                320: {
                  slidesPerView: 1.65,
                  spaceBetween: 16,
                },
                550: {
                  slidesPerView: 2,
                },
                661: {
                  slidesPerView: 3,
                },
                992: {
                  slidesPerView: 4,
                },
              }}
            >
              <SwiperSlide>
                <a href="https://ssh.vn" rel="noreferrer" target="_blank" className="a-link-logo">
                  <div className="div-logo-partnership">
                    <Image src={logoSunshine} className="logo-slide-partnership img-fluid" alt="" />
                  </div>
                  <div className="overplay-logo-partnership">
                    <Image src={iconHover} className="icon-overplay-logo img-fluid" alt="" />
                    <div className="text-overplay-logo">
                      {ready && t('homepage.partnership.purchasingPartner')}
                    </div>
                  </div>
                </a>
              </SwiperSlide>
              <SwiperSlide>
                <a href="https://scgr.vn/" rel="noreferrer" target="_blank" className="a-link-logo">
                  <div className="div-logo-partnership">
                    <Image
                      src={logoSmartConstruction}
                      className="logo-slide-partnership img-fluid"
                      alt=""
                    />
                  </div>
                  <div className="overplay-logo-partnership">
                    <Image src={iconHover} className="icon-overplay-logo" alt="" />
                    <div className="text-overplay-logo">
                      {ready && t('homepage.partnership.constructionPartner')}
                    </div>
                  </div>
                </a>
              </SwiperSlide>
              <SwiperSlide>
                <a
                  href="https://ksfinance.vn/"
                  rel="noreferrer"
                  target="_blank"
                  className="a-link-logo"
                >
                  <div className="div-logo-partnership">
                    <Image
                      src={logoKSFinance}
                      className="logo-slide-partnership img-fluid"
                      alt=""
                    />
                  </div>
                  <div className="overplay-logo-partnership">
                    <Image src={iconHover} className="icon-overplay-logo" alt="" />
                    <div className="text-overplay-logo">
                      {ready && t('homepage.partnership.financialPartner')}
                    </div>
                  </div>
                </a>
              </SwiperSlide>
              <SwiperSlide>
                <a href="https://ode.vn/" rel="noreferrer" target="_blank" className="a-link-logo">
                  <div className="div-logo-partnership">
                    <Image
                      src={logoMediaPartner}
                      className="logo-slide-partnership img-fluid"
                      alt=""
                    />
                  </div>
                  <div className="overplay-logo-partnership">
                    <Image src={iconHover} className="icon-overplay-logo" alt="" />
                    <div className="text-overplay-logo">
                      {ready && t('homepage.partnership.communicationPartner')}
                    </div>
                  </div>
                </a>
              </SwiperSlide>
              <SwiperSlide>
                <a
                  href="https://kienlongbank.com/"
                  target="_blank"
                  rel="noreferrer"
                  className="a-link-logo"
                >
                  <div className="div-logo-partnership">
                    <Image
                      src={logoKienLongBank}
                      className="logo-slide-partnership img-fluid"
                      alt=""
                    />
                  </div>
                  <div className="overplay-logo-partnership">
                    <Image src={iconHover} className="icon-overplay-logo" alt="" />
                    <div className="text-overplay-logo">
                      {ready && t('homepage.partnership.bankingPartner')}
                    </div>
                  </div>
                </a>
              </SwiperSlide>
            </Swiper>
          </Col>
          <Col
            xs={{ span: 6, order: 2 }}
            lg={{ span: 1, order: 3 }}
            className="col-arrow-right-partnership mt-lg-0 mt-4 align-items-center 
            justify-content-lg-center justify-content-start align-items-center"
          >
            <button
              type="button"
              className="btn-arrow-right btn-arrow-partnership next-slide-partnership"
            />
          </Col>
          <Col className="col-mobile-btn">
            <button
              type="button"
              className="btn-arrow-left btn-arrow-partnership-mobile pre-slide-partnership"
            />
            <button
              type="button"
              className="btn-arrow-right btn-arrow-partnership next-slide-partnership"
            />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default RowPartnership;
