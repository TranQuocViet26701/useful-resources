import React, { useEffect, useState } from 'react';

import iconFintech from '/assets/image/png/icon-fintech.png';
import iconSmartCity from '/assets/image/png/icon-smart-city.png';
import iconTech from '/assets/image/png/icon-technology.png';
import iconVr from '/assets/image/png/icon-vr.png';
import bgPhone from '/assets/image/homePage/img-digital-banking-platform.png';
import bgSmartCity from '/assets/image/homePage/img-smart-city.png';
import bgDigital from '/assets/image/homePage/img-digital-transfromation.png';
import bgVr from '/assets/image/homePage/img-virtual-reality.png';

import iconApple from '/assets/image/homePage/icon-down-apple.png';
import iconChPlay from '/assets/image/homePage/icon-down-chplay.png';
import { Col, Row } from 'react-bootstrap';

interface Categories {
  id: number;
  iconUrl: string;
  title: string;
  description: string;
  isHover?: boolean;
  img: string;
}

function BuildTitle() {
  return (
    <>
      <h2 className="title">LÀN SÓNG CÔNG NGHỆ MỚI</h2>
      <p className="description description-padding">
        Unicloud Group nghiên cứu, phát triển, và cung cấp các sản phẩm, giải pháp công nghệ quan
        trọng cho hầu hết các lĩnh vực “xương sống” của nền kinh tế. Nhằm mục tiêu tăng cường năng
        lực và thúc đẩy sự phát triển của các ngành nghề, nâng cao chất lượng cuộc sống.
      </p>
    </>
  );
}

function RowSecondBodyHomePage() {
  const listCategories: Categories[] = [
    {
      id: 1,
      iconUrl: iconFintech,
      title: 'DIGITAL BANKING PLATFORM',
      description:
        'Áp dụng công nghệ số hoá trong ' +
        ' hoạt động tài chính ngân hàng nhằm cải tiến và tiết giảm tối đa thủ tục, thời gian,' +
        'mang lại những trải nghiệm đẳng cấp chưa từng có cho hệ thống khách hàng.',
      isHover: true,
      img: bgPhone,
    },
    {
      id: 2,
      iconUrl: iconSmartCity,
      title: 'HỆ SINH THÁI SMART CITY',
      description:
        'Hệ sinh thái các thiết bị thông minh được thiết kế đa dạng,' +
        ' sản xuất theo quy trình khép kín,' +
        'Smart Home của Unicloud hướng tới cung cấp cho người dùng trải nghiệm hiện đại,' +
        'đẳng cấp, tăng tính tiện nghi.',
      img: bgSmartCity,
    },
    {
      id: 3,
      iconUrl: iconTech,
      title: 'DIGITAL TRANSFORMATION',
      description:
        'Với những lợi ích to lớn cũng như tính tối ưu, ' +
        'hiệu quả cao mang lại từ việc số hoá các hoạt động, giao dịch cả trong và ngoài doanh nghiệp,' +
        'chuyển đổi số dần trở thành một xu hướng tất yếu trong sự phát triển kinh tế ở các doanh nghiệp.',
      img: bgDigital,
    },
    {
      id: 4,
      iconUrl: iconVr,
      title: 'GIẢI PHÁP CÔNG NGHỆ THỰC TẾ ẢO',
      description:
        'Với công nghệ thực tại ảo (VR) và thực tế ảo tăng cường (AR),' +
        'khách hàng có thể đắm chìm và hòa mình vào một thế giới mới hoặc địa điểm chưa từng được đặt chân tới. ',
      img: bgVr,
    },
  ];

  const [indexActive, setIndexActive] = useState(0);
  const [imgRight, setImgRight] = useState(bgPhone);
  function active(value: Categories, index: number) {
    setIndexActive(index);
    setImgRight(value.img);
  }

  const [itemCurrent, setItemCurrent] = useState(listCategories[0]);
  function activeMobile(item: any, index: number) {
    setItemCurrent(item);
    setIndexActive(index);
    setImgRight(item.img);
  }

  useEffect(() => {
    let count = indexActive;
    const intervalId = setInterval(() => {
      if (count === 4) {
        count = 0;
      }
      count += 1;
      if (window.innerWidth < 768) {
        activeMobile(listCategories[count - 1], count);
      } else {
        active(listCategories[count - 1], count);
      }
    }, 4000);
    return () => clearInterval(intervalId);
  }, [indexActive]);

  return (
    <section className="container-wrap technology-wrap">
      <div className="bg-earth" />
      <div className="container-child">
        <BuildTitle />
        <Row className="fintech-mobile">
          <div className="fintech-mobile-warp d-flex">
            {listCategories.map((icon, index) => (
              <div
                key={icon.title}
                className="fintech-mobile-warp-item"
                onClick={() => activeMobile(icon, index + 1)}
                onKeyDown={() => activeMobile(icon, index + 1)}
                tabIndex={0}
                role="button"
              >
                <img src={icon.iconUrl} alt="" className="icon-parent" />
                {itemCurrent.id === index + 1 && (
                  <div className={`icon-active  active-${index + 1}`} />
                )}
              </div>
            ))}
          </div>
          <div className="fintech-mobile-active">
            <div
              className={`card-categories-item d-flex align-items-center is-hover active-${itemCurrent.id}`}
            >
              <div className="categories-item-group">
                <h3 className="title-categories">{itemCurrent.title}</h3>
                <p className="desc-categories">{itemCurrent.description}</p>
              </div>
            </div>
          </div>
        </Row>
        <Row className="d-flex align-items-start categories-tech">
          <Col xs={12} md={6} className="left-content">
            <div className="card-categories">
              {listCategories.map((cate, index) => (
                <div
                  key={cate.title}
                  onClick={() => active(cate, index + 1)}
                  onKeyDown={() => active(cate, index + 1)}
                  tabIndex={0}
                  role="button"
                  className={`card-categories-item d-flex align-items-center active-${
                    indexActive === index + 1 ? `${indexActive} is-hover` : ''
                  } `}
                >
                  <img src={cate.iconUrl} alt="" width={84} />
                  <div className="categories-item-group">
                    <h3 className="title-categories">{cate.title}</h3>
                    <p className="desc-categories">{cate.description}</p>
                  </div>
                </div>
              ))}
            </div>
          </Col>
          <div className="bg-center-content" />
          <Col xs={12} md={6} className="right-content">
            <div className="img-content-right">
              <img
                src={imgRight}
                alt=""
                className={`img-right active-${indexActive}`}
                loading="lazy"
              />
              {indexActive === 1 && (
                <div className="group-icon d-flex align-items-center justify-content-center">
                  <img
                    src={iconApple}
                    alt=""
                    className="me-4 icon-download"
                    style={{
                      boxShadow: '0px 2px 10px 2px rgba(112, 144, 176, 0.1)',
                      borderRadius: '8px',
                    }}
                    loading="lazy"
                  />
                  <img
                    src={iconChPlay}
                    alt=""
                    className="icon-download"
                    style={{
                      boxShadow: '0px 2px 10px 2px rgba(112, 144, 176, 0.1)',
                      borderRadius: '8px',
                    }}
                    loading="lazy"
                  />
                </div>
              )}
            </div>
          </Col>
        </Row>
      </div>
    </section>
  );
}

export default RowSecondBodyHomePage;
