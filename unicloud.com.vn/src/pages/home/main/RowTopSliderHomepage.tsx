import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination, Autoplay } from 'swiper';
import { NavLink } from 'react-router-dom';

import 'react-multi-carousel/lib/styles.css';

function RowTopSliderHomepage() {
  return (
    <Row>
      <Swiper
        modules={[Pagination, Autoplay]}
        spaceBetween={20}
        slidesPerView={1}
        pagination={{
          clickable: true,
          bulletClass: 'custom-dots-slide-top-inactive',
          bulletActiveClass: 'custom-dots-slide-top-active',
          bulletElement: 'button',
        }}
        autoplay={{ delay: 6000 }}
      >
        <SwiperSlide>
          <Row className="back-ground-top-slide-home px-0 d-flex justify-content-center">
            <Row className="max-width-1180 padding-left-right">
              <Col md={7} className="px-0 col-left-slide-top">
                <div className="div-wrap-info">
                  <h1 className="title-slide-homepage-1">Tập Đoàn Công Nghệ Quốc Tế</h1>
                  <h1 className="title-slide-homepage-2">TIÊN PHONG CHUYỂN ĐỔI SỐ</h1>
                  <p className="detail-slide-homepage">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                    Ipsum has been the industry&apos;s standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type
                    specimen book.
                  </p>
                  <NavLink to="/about-us">
                    <button type="button" className="btn-slide-homepage">
                      CHÚNG TÔI LÀ AI?
                    </button>
                  </NavLink>
                </div>
              </Col>
              <Col md={5} className="px-0" />
            </Row>
          </Row>
        </SwiperSlide>
        <SwiperSlide>
          <Row className="back-ground-top-slide-home px-0 d-flex justify-content-center">
            <Row className="max-width-1180 padding-left-right">
              <Col md={7} className="px-0">
                <div className="div-wrap-info">
                  <h1 className="title-slide-homepage-1">Tập Đoàn Công Nghệ Quốc Tế</h1>
                  <h1 className="title-slide-homepage-2">TIÊN PHONG CHUYỂN ĐỔI SỐ</h1>
                  <p className="detail-slide-homepage">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                    Ipsum has been the industry&apos;s standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type
                    specimen book.
                  </p>
                  <NavLink to="/about-us">
                    <button type="button" className="btn-slide-homepage">
                      CHÚNG TÔI LÀ AI?
                    </button>
                  </NavLink>
                </div>
              </Col>
              <Col md={5} className="px-0" />
            </Row>
          </Row>
        </SwiperSlide>
        <SwiperSlide>
          <Row className="back-ground-top-slide-home px-0 d-flex justify-content-center">
            <Row className="max-width-1180 padding-left-right">
              <Col md={7} className="px-0">
                <div className="div-wrap-info">
                  <h1 className="title-slide-homepage-1">Tập Đoàn Công Nghệ Quốc Tế</h1>
                  <h1 className="title-slide-homepage-2">TIÊN PHONG CHUYỂN ĐỔI SỐ</h1>
                  <p className="detail-slide-homepage">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                    Ipsum has been the industry&apos;s standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type
                    specimen book.
                  </p>
                  <NavLink to="/about-us">
                    <button type="button" className="btn-slide-homepage">
                      CHÚNG TÔI LÀ AI?
                    </button>
                  </NavLink>
                </div>
              </Col>
              <Col md={5} className="px-0" />
            </Row>
          </Row>
        </SwiperSlide>
      </Swiper>
    </Row>
  );
}
export default RowTopSliderHomepage;
