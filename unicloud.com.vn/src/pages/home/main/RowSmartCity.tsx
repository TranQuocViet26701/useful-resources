import React from 'react';
import { Row, Col, Image, Container } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';
import { useTranslation } from 'react-i18next';
// import ModalVideo from '@src/components/modal/modal-video';
import imgSlideSliving from '/assets/image/homePage/img-slide-smart-living.png';
import imgSlideSliving2 from '/assets/image/homePage/img-slide-smart-living-2.png';
import imgSlideSliving3 from '/assets/image/homePage/img-slide-smart-living-3.png';
import imgSlideSliving4 from '/assets/image/homePage/img-slide-smart-living-4.png';
import imgSlideSliving5 from '/assets/image/homePage/img-slide-smart-living-5.png';

// import playIcon from '/assets/image/homePage/icon-play.png';
import iconViewMore from '/assets/image/homePage/icon-view-more.svg';
import imgSmartParking from '/assets/image/homePage/img-smart-parking.png';
import imgSmartMetter from '/assets/image/homePage/img-smart-metter.png';
import imgFaceID from '/assets/image/homePage/img-faceID-smart-city.png';
import imgSmartLighting from '/assets/image/homePage/img-lighting.png';

import imgIoT from '/assets/image/homePage/img-IoT-smart-city.png';
import imgGov from '/assets/image/homePage/img-gov.png';
import imgSliving from '/assets/image/homePage/sliving-image-home.png';

// import imgSmartElavator from '/assets/image/homePage/img-smart-elavator.png';

function RowSmartCity() {
  //   const [modalShow, setModalShow] = React.useState(false);
  const { t, ready } = useTranslation();
  const slivingSlide = [
    imgSlideSliving,
    imgSlideSliving2,
    imgSlideSliving3,
    imgSlideSliving4,
    imgSlideSliving5,
  ];

  return (
    <div className="back-ground-smart-living" id="smart-city">
      <div id="section-smatr-city" />
      <Container className="max-width-100 padding-left-right-smart-city">
        <a href="coming-soon" target="_blank" rel="noreferrer">
          <h2 className="title title-smart-city">SMART CITY</h2>
        </a>
        <p className="description detail-smart-city">{ready && t('homepage.city.description')}</p>
        <Row className="gx-3 gy-3 mb-3">
          <Col xs={12} md={6} lg={6}>
            <div className="col-smart-city">
              <h3 className="title-sub-smart-city">E-GOV</h3>
              <p className="detail-sub-smart-city">{ready && t('homepage.city.e-goverment')}</p>
              <p className="text-center mb-4">
                <a href="/coming-soon" className="go-to-detail" target="_blank" rel="noreferrer">
                  {ready && t('homepage.see.detail')}
                </a>
                <Image src={iconViewMore} width={16} height={16} alt="" />
              </p>
              <Row className="row-slide-smart-living">
                <Image src={imgGov} className="img-fluid img-smart-city" alt="" />
              </Row>
            </div>
          </Col>
          <Col xs={12} md={6} lg={6}>
            <div className="col-smart-city">
              <h3 className="title-sub-smart-city">IoT Sensor Network</h3>
              <p className="detail-sub-smart-city">{ready && t('homepage.city.sensor')}</p>
              <p className="text-center mb-4">
                <a href="/coming-soon" className="go-to-detail" target="_blank" rel="noreferrer">
                  {ready && t('homepage.see.detail')}
                </a>
                <Image src={iconViewMore} width={16} height={16} alt="" />
              </p>
              <Row className="row-slide-smart-living">
                <Image src={imgIoT} className="img-fluid img-smart-city" alt="" />
              </Row>
            </div>
          </Col>
          <Col xs={12} md={6} lg={6}>
            <div className="col-right-smart-city-no-video">
              <h3 className="title-sub-smart-city">Smart Meter</h3>
              <p className="detail-sub-smart-city">{ready && t('homepage.city.meter')}</p>
              <p className="text-center mb-4">
                <a href="/coming-soon" target="_blank" className="go-to-detail" rel="noreferrer">
                  {ready && t('homepage.see.detail')}
                </a>
                <Image src={iconViewMore} width={16} height={16} alt="" />
              </p>
              <Row className="row-slide-smart-living">
                <Image src={imgSmartMetter} className="img-fluid img-smart-city" alt="" />
              </Row>
            </div>
          </Col>

          <Col xs={12} md={6} lg={6}>
            <div className="col-smart-city">
              <h3 className="title-sub-smart-city">Smart Parking</h3>
              <p className="detail-sub-smart-city">{ready && t('homepage.city.parking')}</p>
              <p className="text-center mb-4">
                <a
                  href="https://sliving.vn/smart-parking/"
                  target="_blank"
                  rel="noreferrer"
                  className="go-to-detail"
                >
                  {ready && t('homepage.see.detail')}
                </a>
                <Image src={iconViewMore} width={16} height={16} alt="" />
              </p>
              <Row className="row-slide-smart-living">
                <Image alt="" src={imgSmartParking} className="img-fluid img-smart-city" />
              </Row>
            </div>
          </Col>
          <Col xs={12} md={6} lg={6}>
            <div className="col-smart-city">
              <h3 className="title-sub-smart-city">Sliving</h3>
              <p className="detail-sub-smart-city">{ready && t('homepage.city.slivingApp')}</p>
              <p className="text-center mb-4">
                <a
                  href="https://sliving.vn/"
                  target="_blank"
                  rel="noreferrer"
                  className="go-to-detail"
                >
                  {ready && t('homepage.see.detail')}
                </a>
                <Image src={iconViewMore} width={16} height={16} alt="" />
              </p>
              <Row className="row-slide-smart-living">
                <Image alt="" src={imgSliving} className="img-fluid img-smart-city" />
              </Row>
            </div>
          </Col>

          {/* <Col xs={12} md={6} lg={6}>
            <div className="col-smart-city col-video">
              <ReactPlayer
                //   ref={ref}
                className="iframe-youtube-sliving"
                light={thumnailSliving}
                url="https://www.youtube.com/watch?v=6VsQkaZTa0c"
                controls
                playing
                playIcon={<Image src={playIcon} />}
                // onStart=
              />
              <div className="title-video-smart-city">
                <h3 className="title-sub-smart-city-video">Smart Sliving</h3>
                <p className="detail-sub-smart-city-video">Thiết bị căn hộ thông minh (IoT)</p>
                <p className="text-center mb-4">
                  <a href="/" className="go-to-detail-video">
                    {ready && t('homepage.see.detail')}
                  </a>
                  <Image src={iconViewMore} width={16} height={16} />
                </p>
              </div>
            </div>
          </Col> */}

          <Col xs={12} md={6} lg={6}>
            <div className="col-smart-city">
              <h3 className="title-sub-smart-city">Sliving Smart Home</h3>
              <p className="detail-sub-smart-city">{ready && t('homepage.city.slivingApp')}</p>
              <p className="text-center mb-4">
                <a
                  href="https://sliving.vn/smart-home"
                  target="_blank"
                  rel="noreferrer"
                  className="go-to-detail"
                >
                  {ready && t('homepage.see.detail')}
                </a>
                <Image src={iconViewMore} width={16} height={16} alt="" />
              </p>
              <Row className="row-slide-smart-living">
                <Col lg={2} />
                <Col lg={1} xs={2} className="px-0 col-arrow-smart-living">
                  <button
                    type="button"
                    className="btn-arrow-left btn-arrow-left-smart-living pre-slide-smart-living"
                  />
                </Col>
                <Col lg={6} xs={8} className="px-0">
                  <Swiper
                    slidesPerView={1}
                    navigation={{
                      prevEl: '.pre-slide-smart-living',
                      nextEl: '.next-slide-smart-living',
                    }}
                    //   autoplay={{ delay: 6000 }}
                  >
                    {slivingSlide.map((url, index) => (
                      <SwiperSlide key={`${index + url}`}>
                        <div className="text-center">
                          <Image src={url} alt="" className="img-fluid img-slide-smart-living" />
                        </div>
                      </SwiperSlide>
                    ))}
                    {/* <SwiperSlide>
                        <div className="text-center">
                          <Image src={imgSlideSliving} alt="" className="img-fluid img-slide-smart-living" />
                        </div>
                      </SwiperSlide> */}
                  </Swiper>
                </Col>
                <Col lg={1} xs={2} className="px-0 col-arrow-smart-living">
                  <button
                    type="button"
                    className="btn-arrow-right btn-arrow-right-smart-living  next-slide-smart-living"
                  />
                </Col>
                <Col lg={2} />
              </Row>
            </div>
          </Col>

          <Col xs={12} md={6} lg={6}>
            <div className="col-right-smart-city-no-video">
              <h3 className="title-sub-smart-city">Camera AI</h3>
              <p className="detail-sub-smart-city">{ready && t('homepage.city.camera')}</p>
              <p className="text-center mb-4">
                <a href="/coming-soon" target="_blank" className="go-to-detail" rel="noreferrer">
                  {ready && t('homepage.see.detail')}
                </a>
                <Image src={iconViewMore} width={16} height={16} alt="" />
              </p>
              <Row className="row-slide-smart-living">
                <Image src={imgFaceID} className="img-fluid img-smart-city" alt="" />
              </Row>
            </div>
          </Col>
          <Col xs={12} md={6} lg={6}>
            <div className="col-right-smart-city-no-video">
              <h3 className="title-sub-smart-city">Smart Lighting</h3>
              <p className="detail-sub-smart-city">{ready && t('homepage.city.lighting')}</p>
              <p className="text-center mb-4">
                <a href="/coming-soon" target="_blank" className="go-to-detail" rel="noreferrer">
                  {ready && t('homepage.see.detail')}
                </a>
                <Image src={iconViewMore} width={16} height={16} alt="" />
              </p>
              <Row className="row-slide-smart-living">
                <Image src={imgSmartLighting} className="img-fluid img-smart-city" alt="" />
              </Row>
            </div>
          </Col>
          {/* <Col xs={12} md={6} lg={6}>
            <div className="col-right-smart-city-no-video">
              <h3 className="title-sub-smart-city">Smart Elevator</h3>
              <p className="detail-sub-smart-city">{ready && t('homepage.city.elevator')}</p>
              <p className="text-center mb-4">
                <a href="/" className="go-to-detail">
                  {ready && t('homepage.see.detail')}
                </a>
                <Image src={iconViewMore} width={16} height={16} alt="" />
              </p>
              <Row className="row-slide-smart-living">
                <Image src={imgSmartElavator} className="img-fluid img-smart-city" alt="" />
              </Row>
            </div>
          </Col> */}
        </Row>
      </Container>
    </div>
  );
}
export default RowSmartCity;
