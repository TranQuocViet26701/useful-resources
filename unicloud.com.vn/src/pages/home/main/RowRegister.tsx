import React from 'react';
import iconApple from '/assets/image/homePage/icon-down-apple.png';
import iconChPlay from '/assets/image/homePage/icon-down-chplay.png';
import bgHandPhone from '/assets/image/homePage/bg-mobile-hand.png';
import handPhone from '/assets/image/homePage/mobile-hand.png';
import handPhoneSmall from '/assets/image/homePage/mobile-hand-small.png';
import { Col, Form } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

function RowRegister() {
  const { t, ready } = useTranslation();

  return (
    <section className="container-warp banner-register">
      <div className="background-parent">
        <div className="container-child banner-register-wrap">
          <div className="register-info">
            <h3 className="text-white fw-bold fs-2 mb-2">
              {ready && t('homepage.banner.register.title')}
            </h3>
            <p className="text-white fs-6">{ready && t('homepage.banner.register.description')}</p>
            <div className="group-input">
              <form className="row px-0">
                <Col sm={4}>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Control type="text" placeholder="Email" className="input-register me-4" />
                  </Form.Group>
                </Col>
                <Col sm={4} className="group-input-right">
                  <button className="btn-register" type="submit">
                    {ready && t('homepage.banner.register.btn')}
                  </button>
                </Col>
              </form>
            </div>
          </div>
          <div className="group-hand-mobile-wrap">
            <img src={handPhoneSmall} alt="" className="img-hand-mobile" />
          </div>
          <div className=" container-child banner-item">
            <h4 className="fs-3 fw-bold text-gradient-origan">
              {ready && t('homepage.banner.access')}
            </h4>
            <p className="fw-bold banner-item-desc">
              {ready && t('homepage.banner.access.description')}
            </p>
            <div className="group-icon d-flex align-items-center">
              <img src={iconApple} alt="" className="me-3" />
              <img src={iconChPlay} alt="" />
            </div>
            <div className="group-hand-mobile">
              <img src={bgHandPhone} alt="" className="img-bg-hand-mobile" />
              <img src={handPhone} alt="" className="img-hand-mobile" />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default RowRegister;
