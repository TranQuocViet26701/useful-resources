import React from 'react';

import img1 from '/assets/image/homePage/img-1.png';
import img2 from '/assets/image/homePage/img-3.png';
import img3 from '/assets/image/homePage/img-4.png';
import img4 from '/assets/image/homePage/img-2.png';
import img5 from '/assets/image/homePage/icon-ellipse.png';
import { Col, Row } from 'react-bootstrap';
// import bgLeft from '/assets/image/homePage/ng-part-article.png';

interface Reason {
  id: string;
  title: string;
  description: string;
}

function ReasonLeft() {
  return (
    <>
      <div className="reason-left-wrap">
        <div className="img-reason" style={{ backgroundImage: `url(${img1})` }} />
        <div className="img-reason-group">
          <img className="stick-ellipse" src={img5} alt="" width={136} />
          <div className="img-reason" style={{ backgroundImage: `url(${img2})` }} />
        </div>
      </div>
      <div className="reason-left-wrap reason-left-2">
        <div className="img-reason" style={{ backgroundImage: `url(${img4})` }} />
        <div className="img-reason" style={{ backgroundImage: `url(${img3})` }} />
      </div>
    </>
  );
}

function ReasonRight(props: { listReason: Reason[] }) {
  const { listReason } = props;
  return (
    <>
      <h3 className="title text-start">LÝ DO CHỌN CHÚNG TÔI</h3>
      <p className="description-reason">
        Unicloud Group sở hữu hệ thống nhà máy sản xuất công nghệ riêng, văn phòng với những trang
        thiết bị và máy móc hiện đại cùng đội ngũ chuyên gia công nghệ, nhân viên nhiệt huyết và
        trách nhiệm đáp ứng đầy đủ tiêu chí Sản xuất thông minh - Sản phẩm thông minh.
      </p>
      <div className="collection-reason">
        {listReason.map((reason) => (
          <div key={reason.id} className="reason-item d-flex justify-content-start">
            <div className="reason-item-number">{reason.id}</div>
            <div className="reason-item-group">
              <h4 className="title-reason">{reason.title}</h4>
              <p style={{ fontSize: '14px' }} className="description-reason">
                {reason.description}
              </p>
            </div>
          </div>
        ))}
      </div>
    </>
  );
}

function RowReasonUs() {
  const listReason: Reason[] = [
    {
      id: '01',
      title: 'Khách hàng là trung tâm chuyển đổi số',
      description:
        'Chủ động lắng nghe và thấu hiểu khách hàng, khai thác tối đa tài nguyên' +
        ' mang lại sự hài lòng ở mức độ cao nhất.',
    },
    {
      id: '02',
      title: 'Đội ngũ kỹ sư giàu kinh nghiệm',
      description:
        'Chúng tôi luôn đặt vai trò và sứ mệnh phát triển đội ngũ kỹ sư giàu kinh nghiệm' +
        ' lên hàng đầu. Bên cạnh đó Unicloud Group sẽ liên tục phát triển môi trường làm' +
        ' việc theo tiêu chuẩn Quốc Tế để thu hút nhân tài.',
    },
    {
      id: '03',
      title: 'sẵn sàng thích nghi đổi mới',
      description:
        'Công Nghệ sẽ liên tục phát triển đổi mới mỗi ngày, Unicloud Group thấu hiểu' +
        ' được điều đó nên chúng tôi luôn luôn trau dồi học hỏi mỗi ngày để mang đến' +
        ' sản phẩm và giải pháp hiện đại toàn diện.',
    },
    {
      id: '04',
      title: 'Dữ liệu là vàng',
      description: 'DATA sẽ giữ vai trò quyết định trong chuyển đổi số.',
    },
  ];

  return (
    <section className="section-reason">
      <div className="container-wrap">
        <div className="container-child reason-us">
          <Row className="group-reason-us align-items-start">
            <Col xs={12} md={6} className="reason-left">
              <ReasonLeft />
            </Col>
            <Col xs={12} md={6} className="reason-right">
              <ReasonRight listReason={listReason} />
            </Col>
          </Row>
        </div>
      </div>
    </section>
  );
}

export default RowReasonUs;
