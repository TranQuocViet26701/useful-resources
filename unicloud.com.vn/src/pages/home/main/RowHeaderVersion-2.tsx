import React, { useEffect, useState } from 'react';

// Default theme
import '@splidejs/react-splide/css';

import { Splide, SplideSlide } from '@splidejs/react-splide';
import { Options } from '@splidejs/splide';

import imgSmartCity from '/assets/image/homePage/img-header-smart-city.jpg';
import imgDigitalTransformation from '/assets/image/homePage/img-header-digital-transformation.jpg';
import imgDigitalBanking from '/assets/image/homePage/img-header-digital-banking.jpg';
import imgHeaderVr from '/assets/image/homePage/img-header-vr.jpg';

import iconEcosystem from '/assets/image/svg/icon-ecosystem-earth.svg';
import iconEcosystemWhite from '/assets/image/svg/icon-ecosystem-earth-white.svg';

import iconFormWare from '/assets/image/svg/icon-form-ware.svg';
import iconFormWareWhite from '/assets/image/svg/icon-form-ware-white.svg';

import iconSmartCity from '/assets/image/svg/icon-smart-city.svg';
import iconSmartCityWhite from '/assets/image/svg/icon-smart-city-white.svg';

import iconVr from '/assets/image/svg/icon-vr.svg';
import iconVrWhite from '/assets/image/svg/icon-vr-white.svg';
import logoColor from '/assets/image/logo/logo-color.svg';

function RowHeaderVersion2() {
  const [activeIndex, setActiveIndex] = useState<number>(0);

  const mainRef = React.createRef<Splide>();

  /**
   * The thumbnail Splide component.
   */
  const thumbsRef = React.createRef<Splide>();

  const mainOptions: Options = {
    perPage: 1,
    cover: true,
    pagination: false,
    isNavigation: false,
    rewind: false,
    arrows: false,
    type: 'fade',
  };

  const thumbsOptions: Options = {
    fixedWidth: 'calc(20.8%)',
    fixedHeight: 155,
    gap: '48px',
    rewind: false,

    pagination: false,
    isNavigation: true,
    arrows: false,
    cover: true,
    perPage: 4,
    breakpoints: {
      768: {
        fixedWidth: 'calc(22%)',
        gap: '20px',
        fixedHeight: '100%',
      },
      475: {
        gap: '20px',
      },
      414: {
        gap: '5px',
      },
      390: {
        gap: '10px',
      },
    },
  };

  useEffect(() => {
    if (mainRef.current && thumbsRef.current && thumbsRef.current.splide) {
      mainRef.current.sync(thumbsRef.current.splide);
    }
  }, [mainRef, thumbsRef]);

  return (
    <section className="page-header-v2">
      <Splide
        options={mainOptions}
        ref={mainRef}
        id="main-carousel"
        className="splide"
        aria-label="My Awesome Gallery"
        onMove={(item) => setActiveIndex(item?.index)}
      >
        <SplideSlide className="img-slide-v2">
          <div
            className="img-slide-v2"
            style={{ backgroundImage: `url(${imgDigitalTransformation})` }}
          >
            <div className="popup-v2-tag">
              <div className="popup-v2-tag-wrap  popup-v2-tag-transformation">
                <img src={logoColor} alt="logo Unicloud" />
                <h2 className="popup-v2-tag-title">giải pháp chuyển đổi số cho doanh nghiệp</h2>
                <h4 className="popup-v2-tag-subTitle">Đáp ứng nhu cầu thị trường</h4>
                <a href="/coming-soon" target="_blank">
                  <button type="button">TÌM HIỂU THÊM</button>
                </a>
              </div>
            </div>
          </div>
        </SplideSlide>
        <SplideSlide className="img-slide-v2">
          <div className="img-slide-v2" style={{ backgroundImage: `url(${imgDigitalBanking})` }}>
            <div className="popup-v2-tag">
              <div className="popup-v2-tag-wrap  popup-v2-tag-city ">
                <img src={logoColor} alt="logo Unicloud" />
                <h2 className="popup-v2-tag-title">Giải pháp Ngân hàng kỹ thuật số tiên tiến</h2>
                <h4 className="popup-v2-tag-subTitle">Bảo mật tuyệt đối</h4>
                <a href="/neobank" target="_blank">
                  <button type="button">TÌM HIỂU THÊM</button>
                </a>
              </div>
            </div>
          </div>
        </SplideSlide>
        <SplideSlide className="img-slide-v2">
          <div className="img-slide-v2" style={{ backgroundImage: `url(${imgSmartCity})` }}>
            <div className="popup-v2-tag">
              <div className="popup-v2-tag-wrap  popup-v2-tag-city">
                <img src={logoColor} alt="logo Unicloud" />
                <h2 className="popup-v2-tag-title">Kiến tạo Xây dựng thành phố thông minh</h2>
                <h4 className="popup-v2-tag-subTitle">vươn tầm thế giới</h4>
                <a href="/coming-soon" target="_blank">
                  <button type="button">TÌM HIỂU THÊM</button>
                </a>
              </div>
            </div>
          </div>
        </SplideSlide>

        <SplideSlide className="img-slide-v2">
          <div className="img-slide-v2" style={{ backgroundImage: `url(${imgHeaderVr})` }}>
            <div className="popup-v2-tag ">
              <div className="popup-v2-tag-wrap popup-v2-tag-transformation is-background-popup">
                <img src={logoColor} alt="logo Unicloud" />
                <h2 className="popup-v2-tag-title">giải pháp thực tế ảo vr</h2>
                <h4 className="popup-v2-tag-subTitle">mang đến trải nghiệm chân thật nhất </h4>
                <a href="https://univr.vn/" target="_blank" rel="noreferrer">
                  <button type="button">TÌM HIỂU THÊM</button>
                </a>
              </div>
            </div>
          </div>
        </SplideSlide>
      </Splide>

      <Splide options={thumbsOptions} ref={thumbsRef} id="thumbnails" className="slide-v2-tag">
        <SplideSlide className="thumbnail">
          <div className={`btn-slide-v2 ${activeIndex === 0 ? 'is-active-btn-slide' : ''}`}>
            <div className="btn-slide-v2-img">
              {activeIndex !== 0 && (
                <img className="slide-v2-icon" src={iconEcosystemWhite} alt="" />
              )}
              {activeIndex === 0 && <img className="slide-v2-icon" src={iconEcosystem} alt="" />}
            </div>
            <h4 className="slide-v2-title">DIGITAL BANKING PLATFORM</h4>
          </div>
        </SplideSlide>

        <SplideSlide className="thumbnail">
          <div className={`btn-slide-v2 ${activeIndex === 1 ? 'is-active-btn-slide' : ''}`}>
            <div className="btn-slide-v2-img">
              {activeIndex !== 1 && (
                <img className="slide-v2-icon" src={iconFormWareWhite} alt="" />
              )}
              {activeIndex === 1 ? (
                <img className="slide-v2-icon" src={iconFormWare} alt="" />
              ) : null}
            </div>
            <h4 className="slide-v2-title">Digital transformation</h4>
          </div>
        </SplideSlide>
        <SplideSlide className="thumbnail">
          <div className={`btn-slide-v2 ${activeIndex === 2 ? 'is-active-btn-slide' : ''}`}>
            <div className="btn-slide-v2-img">
              {activeIndex !== 2 && (
                <img className="slide-v2-icon" src={iconSmartCityWhite} alt="" />
              )}
              {activeIndex === 2 && <img className="slide-v2-icon" src={iconSmartCity} alt="" />}
            </div>
            <h4 className="slide-v2-title">Smart city</h4>
          </div>
        </SplideSlide>
        <SplideSlide className="thumbnail">
          <div className={`btn-slide-v2 ${activeIndex === 3 ? 'is-active-btn-slide' : ''}`}>
            <div className="btn-slide-v2-img">
              {activeIndex !== 3 && <img className="slide-v2-icon" src={iconVrWhite} alt="" />}
              {activeIndex === 3 && <img className="slide-v2-icon" src={iconVr} alt="" />}
            </div>
            <h4 className="slide-v2-title">VIRTUAL REALITY (VR)</h4>
          </div>
        </SplideSlide>
      </Splide>
      <div className="tangle-bottom-header background-image" />
    </section>
  );
}

export default RowHeaderVersion2;
