/* eslint-disable import/no-relative-packages */
import React from 'react';
import { Row, Col, Image } from 'react-bootstrap';

import { useTranslation } from 'react-i18next';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Autoplay } from 'swiper';
// import { Link } from 'react-router-dom';
// import useWindowSize from '@src/hooks/useWindowsSize';
import imgSlideFuture1 from '/assets/image/png/img-slide-future-1.jpg';
import imgSlideFuture2 from '/assets/image/png/img-slide-future-2.jpg';
import imgSlideFuture3 from '/assets/image/png/img-slide-future-3.png';
import imgSlideFuture4 from '/assets/image/png/img-slide-future-4.jpg';
import imgSlideFuture5 from '/assets/image/png/img-slide-future-5.jpg';
import imgSlideFuture6 from '/assets/image/png/img-slide-future-6.jpg';
// import arrowRightCreateFeature from '/assets/image/svg/arrow-right-create-future.svg';
// import arrowRightCreateFeatureMobile from '/assets/image/svg/arrow-right-create-future-mobile.svg';

function RowCreateFuture() {
  //   const size = useWindowSize();
  //   const isMobile = useMemo(() => {
  //     if (size.width && size.width >= 992) {
  //       return false;
  //     }
  //     return true;
  //   }, [size.width]);

  const { t, ready } = useTranslation();

  return (
    <Row className="back-ground-create-future px-0 d-flex justify-content-center">
      <Row className="max-width-1180 padding-left-right">
        <Col>
          <h2 className="title title-create-future-1">
            {ready && t('homepage.createFuture.title')}
          </h2>
          <p className="description title-create-future-2">
            {ready && t('homepage.createFuture.subtitle')}
          </p>
          <div>
            <Row className="row-btn-slide-create-future">
              <Col className="px-0 d-flex">
                <button
                  type="button"
                  className="btn-arrow-left btn-arrow-create-future pre-slide-cerate-future"
                />
                <button type="button" className="btn-arrow-right next-slide-cerate-future" />
              </Col>
              <Col />
              <Col className="px-0 col-xem-them">
                <button type="button" className="btn-view-more-create-future">
                  {ready && t('homepage.createFuture.viewMore')}
                </button>
              </Col>
            </Row>
          </div>
          <div>
            <Swiper
              modules={[Navigation, Autoplay]}
              spaceBetween={50}
              slidesPerView={1}
              navigation={{
                prevEl: '.pre-slide-cerate-future',
                nextEl: '.next-slide-cerate-future',
              }}
              //   autoplay={{ delay: 6000 }}
            >
              <SwiperSlide>
                <Row className="gx-3 gy-3">
                  <Col md={4} xs={6}>
                    <div className="div-img-create-feature a-link-create-feature">
                      <Image
                        src={imgSlideFuture1}
                        className="img-slide-create-feature img-fluid"
                        alt=""
                      />
                      {/* <div className="overplay-text-create-feature">
                        <Row className="row-title-create-feature">
                          <Col lg={10}>
                            <Link to="/">
                              <h3 className="title-hover-create-feature">
                                Enter title text here...
                              </h3>
                            </Link>
                            <p className="title-sub-hover-create-feature">Enter sub title.</p>
                          </Col>
                          <Col lg={2} className="col-icon-view-more">
                            <Link to="/">
                              <Image
                                src={
                                  isMobile ? arrowRightCreateFeatureMobile : arrowRightCreateFeature
                                }
                                alt=""
                              />
                            </Link>
                          </Col>
                        </Row>
                      </div> */}
                    </div>
                  </Col>
                  <Col md={4} xs={6}>
                    <div className="div-img-create-feature a-link-create-feature">
                      <Image
                        src={imgSlideFuture2}
                        className="img-slide-create-feature img-fluid"
                        alt=""
                      />
                      {/* <div className="overplay-text-create-feature">
                        <Row className="row-title-create-feature">
                          <Col lg={10}>
                            <Link to="/">
                              <h3 className="title-hover-create-feature">
                                Enter title text here...
                              </h3>
                            </Link>
                            <p className="title-sub-hover-create-feature">Enter sub title.</p>
                          </Col>
                          <Col lg={2} className="col-icon-view-more">
                            <Link to="/">
                              <Image
                                src={
                                  isMobile ? arrowRightCreateFeatureMobile : arrowRightCreateFeature
                                }
                                alt=""
                              />
                            </Link>
                          </Col>
                        </Row>
                      </div> */}
                    </div>
                  </Col>
                  <Col md={4} xs={6}>
                    <div className="div-img-create-feature a-link-create-feature">
                      <Image
                        src={imgSlideFuture3}
                        className="img-slide-create-feature img-fluid"
                        alt=""
                      />
                      {/* <div className="overplay-text-create-feature">
                        <Row className="row-title-create-feature">
                          <Col lg={10}>
                            <Link to="/">
                              <h3 className="title-hover-create-feature">
                                Enter title text here...
                              </h3>
                            </Link>
                            <p className="title-sub-hover-create-feature">Enter sub title.</p>
                          </Col>
                          <Col lg={2} className="col-icon-view-more">
                            <Link to="/">
                              <Image
                                src={
                                  isMobile ? arrowRightCreateFeatureMobile : arrowRightCreateFeature
                                }
                                alt=""
                              />
                            </Link>
                          </Col>
                        </Row>
                      </div> */}
                    </div>
                  </Col>
                  <Col md={4} xs={6}>
                    <div className="div-img-create-feature a-link-create-feature">
                      <Image
                        src={imgSlideFuture4}
                        className="img-slide-create-feature img-fluid"
                        alt=""
                      />
                      {/* <div className="overplay-text-create-feature">
                        <Row className="row-title-create-feature">
                          <Col lg={10}>
                            <Link to="/">
                              <h3 className="title-hover-create-feature">
                                Enter title text here...
                              </h3>
                            </Link>
                            <p className="title-sub-hover-create-feature">Enter sub title.</p>
                          </Col>
                          <Col lg={2} className="col-icon-view-more">
                            <Link to="/">
                              <Image
                                src={
                                  isMobile ? arrowRightCreateFeatureMobile : arrowRightCreateFeature
                                }
                                alt=""
                              />
                            </Link>
                          </Col>
                        </Row>
                      </div> */}
                    </div>
                  </Col>
                  <Col md={4} xs={6}>
                    <div className="div-img-create-feature a-link-create-feature">
                      <Image
                        src={imgSlideFuture5}
                        className="img-slide-create-feature img-fluid"
                        alt=""
                      />
                      {/* <div className="overplay-text-create-feature">
                        <Row className="row-title-create-feature">
                          <Col lg={10}>
                            <Link to="/">
                              <h3 className="title-hover-create-feature">
                                Enter title text here...
                              </h3>
                            </Link>
                            <p className="title-sub-hover-create-feature">Enter sub title.</p>
                          </Col>
                          <Col lg={2} className="col-icon-view-more">
                            <Link to="/">
                              <Image
                                src={
                                  isMobile ? arrowRightCreateFeatureMobile : arrowRightCreateFeature
                                }
                                alt=""
                              />
                            </Link>
                          </Col>
                        </Row>
                      </div> */}
                    </div>
                  </Col>
                  <Col md={4} xs={6}>
                    <div className="div-img-create-feature a-link-create-feature">
                      <Image
                        src={imgSlideFuture6}
                        className="img-slide-create-feature img-fluid"
                        alt=""
                      />
                      {/* <div className="overplay-text-create-feature">
                        <Row className="row-title-create-feature">
                          <Col lg={10}>
                            <Link to="/">
                              <h3 className="title-hover-create-feature">
                                Enter title text here...
                              </h3>
                            </Link>
                            <p className="title-sub-hover-create-feature">Enter sub title.</p>
                          </Col>
                          <Col lg={2} className="col-icon-view-more">
                            <Link to="/">
                              <Image
                                src={
                                  isMobile ? arrowRightCreateFeatureMobile : arrowRightCreateFeature
                                }
                                alt=""
                              />
                            </Link>
                          </Col>
                        </Row>
                      </div> */}
                    </div>
                  </Col>
                </Row>
              </SwiperSlide>
            </Swiper>
          </div>
        </Col>
      </Row>
      {/* row slide */}
    </Row>
  );
}
export default RowCreateFuture;
