import useWindowSize from '@src/hooks/useWindowsSize';
import React, { useCallback, useEffect, useState } from 'react';
import imgLeft from '/assets/image/homePage/img-left.png';
import imgRight from '/assets/image/homePage/img-right.png';

interface CarouselModel {
  id: number;
  title: string;
  description: string;
  imgUrl?: string;
}

function BuildArrowCarousel(props: { funcPrev: Function; funcNext: any }) {
  const { funcPrev, funcNext } = props;

  return (
    <div className="d-flex align-items-center carousel-cus navigation-cus">
      <div
        className="btn-arrow-left"
        onClick={() => funcPrev()}
        onKeyDown={() => funcPrev()}
        tabIndex={0}
        role="button"
      />
      <div
        className="btn-arrow-right"
        tabIndex={0}
        role="button"
        onClick={() => funcNext()}
        onKeyDown={() => funcNext()}
      />
    </div>
  );
}

function RowEcosystem() {
  const size = useWindowSize();
  const [isMobile, setIsMobile] = useState(false);

  const [carouselItemsState, setCarouselItemsState] = useState<Element[]>([]);
  const [listCarouselModel] = useState<CarouselModel[]>([
    {
      id: 1,
      title: 'My Sunshine',
      description: 'Ứng dụng cho phép các doanh nghiệp quản lý nhân sự của mình.',
    },
    {
      id: 2,
      title: 'ODE TV',
      description:
        'Ứng dụng ODE TV đa nền tảng danh cho Smart TV với hàng trăm kênh truyền hình Việt Nam và quốc tế.',
    },
    {
      id: 3,
      title: 'KSFinance',
      description:
        'Ứng dụng Sunshine Homes - Điều khiển căn hộ ngay trên chiếc điện thoại của gia chủ.',
    },
    {
      id: 4,
      title: 'Kiên Long Bank',
      description: 'Ứng dụng  cho phép khách hàng thực hiện được các giao dịch tài chính.',
    },
    {
      id: 5,
      title: 'Sunshine Home',
      description: 'Ứng dụng cung cấp các chức năng thiết yếu đến doanh nghiệp.',
    },
  ]);

  const [carouselModel, setCarouselModel] = useState<CarouselModel>({
    id: listCarouselModel[3].id,
    title: listCarouselModel[3].title,
    description: listCarouselModel[3].description,
  });
  const getPos = (current: number, active: number) => {
    const diff = current - active;

    if (Math.abs(current - active) > 2) {
      return -current;
    }

    return diff;
  };

  const update = (elems: Element[], newActive: HTMLElement) => {
    const newActivePos: any = newActive.dataset.pos;

    const current = elems.find((elem) => (elem as HTMLElement).dataset.pos === '0') as Element;
    const prev = elems.find((elem) => (elem as HTMLElement).dataset.pos === '-1');
    const next = elems.find((elem) => (elem as HTMLElement).dataset.pos === '1');
    const first = elems.find((elem) => (elem as HTMLElement).dataset.pos === '-2');
    const last = elems.find((elem) => (elem as HTMLElement).dataset.pos === '2');
    current.classList.remove('carousel__item_active');

    window.setTimeout(() => {
      elems.forEach((value) => {
        const element = value as HTMLElement;
        if (element.dataset.pos === '-1' || element.dataset.pos === '1') {
          element.style.zIndex = '4';
        } else {
          element.style.zIndex = 'unset';
        }
      });
    }, 300);

    [current, prev, next, first, last].forEach((item) => {
      const itemPos: any = (item as HTMLElement).dataset.pos;
      if (itemPos === 0) return;
      (item as HTMLElement).dataset.pos = getPos(itemPos, newActivePos).toString();
      if (
        getPos(itemPos, newActivePos).toString() === '-1' ||
        getPos(itemPos, newActivePos).toString() === '1'
      ) {
        (item as HTMLElement).style.zIndex = '4';
      } else {
        (item as HTMLElement).style.zIndex = 'unset';
      }
    });
  };

  const updatePost = (element: any, list: any[]) => {
    if (isMobile) {
      update(list, element);
    } else {
      element.click();
    }
  };

  // Function Click Slide in Desktop
  const handlePrev = useCallback(() => {
    const prev = carouselItemsState?.find(
      (elem) => (elem as HTMLElement).dataset.pos === '-1',
    ) as HTMLElement;
    const prevNumber = parseInt(prev.innerHTML, 10);
    setCarouselModel(listCarouselModel[prevNumber]);
    updatePost(prev, carouselItemsState);
  }, [carouselModel, carouselItemsState]);

  const handleNext = useCallback(() => {
    const next = carouselItemsState?.find(
      (elem) => (elem as HTMLElement).dataset.pos === '1',
    ) as HTMLElement;
    const prevNumber = parseInt(next.innerHTML, 10);
    setCarouselModel(listCarouselModel[prevNumber]);
    updatePost(next, carouselItemsState);
  }, [carouselModel, carouselItemsState]);

  // Function touch Slide in Mobile
  const handlePrevMobile = (list: any[]) => {
    if (list.length <= 0) {
      return;
    }
    const prev = list?.find((elem) => (elem as HTMLElement).dataset.pos === '-1') as HTMLElement;
    const prevNumber = parseInt(prev.innerHTML, 10);
    prev.style.transitionDuration = '300ms';
    setCarouselModel(listCarouselModel[prevNumber]);
    update(list, prev);
  };

  const handleNextMobile = (list: any[]) => {
    if (list.length <= 0) {
      return;
    }
    const next = list?.find((elem) => (elem as HTMLElement).dataset.pos === '1') as HTMLElement;
    next.style.transitionDuration = '300ms';
    const prevNumber = parseInt(next.innerHTML, 10);
    setCarouselModel(listCarouselModel[prevNumber]);
    update(list, next);
  };

  useEffect(() => {
    if (size?.width! > 768) {
      setIsMobile(false);
    } else {
      setIsMobile(true);
    }

    // Setup Function Event in Slide
    const carouselList = document.querySelector('.carousel__list');
    const carouselItems = document.querySelectorAll('.carousel__item');
    const elems = Array.from(carouselItems);
    setCarouselItemsState(elems);

    // Function update slide active in Desktop
    const UpdateSlideWhenClick = (event: Event) => {
      event.preventDefault();
      const newActive = event.target as HTMLElement;
      if (newActive && newActive.innerHTML !== '' && newActive.innerHTML.length <= 1) {
        const prevNumber = parseInt(newActive.innerHTML, 10);
        setCarouselModel(listCarouselModel[prevNumber]);
        const isItem = newActive.closest('.carousel__item');
        if (!isItem || newActive.classList.contains('carousel__item_active')) {
          return;
        }
        update(elems, newActive);
      }
    };

    // Function Update slide active in Mobile
    let widthElementTouched: number = 0;
    let clientXTouched: number = 0;
    let touchCurrent: TouchEvent;
    const UpdateMobile = (e: TouchEvent) => {
      const touch = e as TouchEvent;
      widthElementTouched = touch.touches[0].screenX;
      clientXTouched = touch.touches[0].clientX;
      if (clientXTouched > widthElementTouched / 2.5) {
        handleNextMobile(elems);
      } else {
        handlePrevMobile(elems);
      }
    };

    if (!isMobile) {
      carouselList?.addEventListener('click', (event) => {
        const newActive = event.target as HTMLElement;
        if (newActive && newActive.innerHTML !== '' && newActive.innerHTML.length <= 1) {
          const prevNumber = parseInt(newActive.innerHTML, 10);
          setCarouselModel(listCarouselModel[prevNumber]);
          const isItem = newActive.closest('.carousel__item');
          if (!isItem || newActive.classList.contains('carousel__item_active')) {
            return;
          }
          update(elems, newActive);
        }
      });
    } else {
      carouselList?.addEventListener(
        'touchstart',
        (e) => {
          touchCurrent = e as TouchEvent;
        },
        { passive: false },
      );

      carouselList?.addEventListener(
        'touchend',
        () => {
          UpdateMobile(touchCurrent);
        },
        { passive: false },
      );
    }

    return () => {
      carouselList?.removeEventListener('click', UpdateSlideWhenClick);
      carouselList?.removeEventListener('touchend', () => {
        UpdateMobile(touchCurrent);
      });
    };
  }, [size?.width]);

  return (
    <section>
      <div className="carousel">
        <div className="container-wrap">
          <div className="container-child">
            <h3 className="title">HỆ SINH THÁI UNICLOUD</h3>
            <p className="description max-width-724">
              Unicloud đã dày công nghiên cứu, không ngừng sáng tạo để hướng tới mục tiêu mang tới
              những sản phẩm, dịch vụ tinh hoa nhất, đáp ứng và thoả mãn mọi nhu cầu của khách hàng,
              mang lại cuộc sống tốt đẹp hơn cho cộng đồng và xã hội.
            </p>
          </div>
        </div>
        <ul className="carousel__list">
          <li className="carousel__item" data-pos="-2">
            1
          </li>
          <li className="carousel__item" data-pos="-1">
            2
          </li>
          <li className="carousel__item" data-pos="3" />
          <li className="carousel__item" data-pos="0">
            3
          </li>
          <li className="carousel__item" data-pos="1">
            4
          </li>
          <li className="carousel__item" data-pos="2">
            0
          </li>
          <div className="item-left carousel__item-stick">
            <img src={imgLeft} alt="" />
          </div>

          <div className="item-right carousel__item-stick">
            <img src={imgRight} alt="" />
          </div>
        </ul>

        <div className="carousel-active">
          <h3 className="title text-center">{carouselModel.title}</h3>
          <p className="description width-313 desc-carousel my-3">{carouselModel.description}</p>
        </div>
        <BuildArrowCarousel funcPrev={handlePrev} funcNext={handleNext} />
      </div>
    </section>
  );
}

export default RowEcosystem;
