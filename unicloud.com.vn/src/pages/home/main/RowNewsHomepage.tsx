import React from 'react';
import { Row, Col, Image } from 'react-bootstrap';
import 'react-multi-carousel/lib/styles.css';
import iconArrowLeft from '/assets/image/svg/icon-arrow-left-news.svg';
import iconArrowRight from '/assets/image/svg/icon-arrow-right-news.svg';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Autoplay } from 'swiper';

function RowNewsHomepage() {
  return (
    <Row className="row-news-homepage">
      <Col md={2} className="px-0 d-flex">
        <h3 className="title-news">TIN MỚI NHẤT</h3>
        <button className="btn-slide-news-homepage pre-slide-news px-0" type="button">
          <Image src={iconArrowLeft} alt="" />
        </button>
        <button className="btn-slide-news-homepage next-slide-news px-0" type="button">
          <Image src={iconArrowRight} alt="" />
        </button>
      </Col>
      {/* <Col md={1}>
      </Col> */}
      <Col md={10} className="col-news-item">
        <Swiper
          modules={[Navigation, Autoplay]}
          spaceBetween={50}
          slidesPerView={1}
          initialSlide={0}
          navigation={{
            prevEl: '.pre-slide-news',
            nextEl: '.next-slide-news',
          }}
          autoplay={{ delay: 6000 }}
          className="swiper-news-homepage"
        >
          <SwiperSlide>
            <p className="mb-0">
              Chủ tịch tập đoàn công nghệ unicloud ĐỖ ANH TUẤN làm chủ công nghệ hiện thực hóa khát
              vọng xây dựng thung lũng silicon... (1)
            </p>
          </SwiperSlide>
          <SwiperSlide>
            <p className="mb-0">
              Chủ tịch tập đoàn công nghệ unicloud ĐỖ ANH TUẤN làm chủ công nghệ hiện thực hóa khát
              vọng xây dựng thung lũng silicon... (2)
            </p>
          </SwiperSlide>
          <SwiperSlide>
            <p className="mb-0">
              Chủ tịch tập đoàn công nghệ unicloud ĐỖ ANH TUẤN làm chủ công nghệ hiện thực hóa khát
              vọng xây dựng thung lũng silicon... (3)
            </p>
          </SwiperSlide>
          <SwiperSlide>
            <p className="mb-0">
              Chủ tịch tập đoàn công nghệ unicloud ĐỖ ANH TUẤN làm chủ công nghệ hiện thực hóa khát
              vọng xây dựng thung lũng silicon... (4)
            </p>
          </SwiperSlide>
        </Swiper>
      </Col>
    </Row>
  );
}
export default RowNewsHomepage;
