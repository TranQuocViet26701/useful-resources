import React from 'react';
import { Row, Col, Image, Container } from 'react-bootstrap';

// import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import imgSlideVRMobile from '/assets/image/homePage/img-slide-VR-mobile.png';
import imgSlideVRMobile2 from '/assets/image/homePage/img-virtual-reality.png';

import playIcon from '/assets/image/homePage/icon-play.png';
import ModalVideo from '@src/components/modal/modal-video';

import iconViewMore from '/assets/image/homePage/icon-view-more.svg';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Autoplay } from 'swiper';

function RowVirtualReality() {
  const { t, ready } = useTranslation();

  const [modalShowVrMobile, setModalShowVrMobile] = React.useState(false);
  const [modalShowVr360, setModalShowVr360] = React.useState(false);
  const [modalShowVrTouch, setModalShowVrTouch] = React.useState(false);
  const [modalShowVrGame, setModalShowVrGame] = React.useState(false);

  return (
    <div className="back-ground-smart-living" id="virtual-reality">
      <div id="section-virtual-reality" />
      <Container className="max-width-100 padding-left-right-smart-city">
        <a href="https://univr.vn/" target="_blank" rel="noreferrer">
          <h2 className="title title-smart-city">VIRTUAL REALITY</h2>
        </a>
        <p className="description detail-smart-city">{ready && t('homepage.vr.description')}</p>
        <Row className="gx-3 gy-3 mb-3">
          <Col xs={12} md={6} lg={6}>
            <div className="col-smart-city">
              <h3 className="title-sub-smart-city">UniVR Mobile</h3>
              <p className="detail-sub-smart-city">{ready && t('homepage.vr.uni.mobile')}</p>
              <p className="text-center mb-4">
                <a
                  href="https://univr.vn/univrmobile"
                  target="_blank"
                  rel="noreferrer"
                  className="go-to-detail"
                >
                  {ready && t('homepage.see.detail')}
                </a>
                <Image src={iconViewMore} width={16} height={16} alt="" />
              </p>
              <Row className="row-slide-smart-living">
                <Col lg={2} />
                <Col lg={1} xs={2} className="px-0 col-arrow-smart-living">
                  <button
                    type="button"
                    className="btn-arrow-left btn-arrow-left-smart-living pre-slide-univr-mobile"
                  />
                </Col>
                <Col lg={6} xs={8} className="px-0">
                  <Swiper
                    modules={[Navigation, Autoplay]}
                    slidesPerView={1}
                    navigation={{
                      prevEl: '.pre-slide-univr-mobile',
                      nextEl: '.next-slide-univr-mobile',
                    }}
                    //   autoplay={{ delay: 6000 }}
                  >
                    <SwiperSlide>
                      <div className="text-center">
                        <Image
                          src={imgSlideVRMobile}
                          className="img-fluid img-slide-smart-living"
                          alt=""
                        />
                      </div>
                    </SwiperSlide>
                    <SwiperSlide>
                      <div className="text-center">
                        <Image
                          src={imgSlideVRMobile2}
                          className="img-fluid img-slide-smart-living"
                          alt=""
                        />
                        <button
                          type="button"
                          className="btn-play-video btn-play-video-slide"
                          onClick={() => setModalShowVrMobile(true)}
                        >
                          <Image alt="" src={playIcon} />
                        </button>
                        <ModalVideo
                          show={modalShowVrMobile}
                          onHide={() => setModalShowVrMobile(false)}
                          urlvideo="https://data.unicloud.com.vn/wp-content/uploads/2022/04/App-mobile.mp4"
                        />
                      </div>
                    </SwiperSlide>
                  </Swiper>
                </Col>
                <Col lg={1} xs={2} className="px-0 col-arrow-smart-living">
                  <button
                    type="button"
                    className="btn-arrow-right btn-arrow-right-smart-living  next-slide-univr-mobile"
                  />
                </Col>
                <Col lg={2} />
              </Row>
            </div>
          </Col>
          <Col xs={12} md={6} lg={6}>
            <div className="col-smart-city col-video col-uniVR">
              <div className="div-title-video">
                <h3 className="title-sub-smart-city">UniVR 360</h3>
                <p className="detail-sub-smart-city">{ready && t('homepage.vr.uni.360')}</p>
                <p className="text-center mb-4">
                  <a
                    href="https://univr.vn/univr360"
                    target="_blank"
                    rel="noreferrer"
                    className="go-to-detail"
                  >
                    {ready && t('homepage.see.detail')}
                  </a>
                  <Image src={iconViewMore} width={16} height={16} alt="" />
                </p>
              </div>
              <button
                type="button"
                className="btn-play-video"
                onClick={() => setModalShowVr360(true)}
              >
                <Image alt="" src={playIcon} />
              </button>
              <ModalVideo
                show={modalShowVr360}
                onHide={() => setModalShowVr360(false)}
                urlvideo="https://data.unicloud.com.vn/wp-content/uploads/2022/04/univr-360.mp4"
              />
            </div>
          </Col>
        </Row>
        <Row className="gx-3 gy-3 mb-3">
          <Col xs={12} md={6} lg={6}>
            <div className="col-smart-city col-video col-uniVR-Touch">
              <div className="div-title-video">
                <h3 className="title-sub-smart-city-video">UniVR Touch</h3>
                <p className="detail-sub-smart-city-video">{ready && t('homepage.vr.uni.touch')}</p>
                <p className="text-center mb-4">
                  <a
                    href="https://univr.vn/univrtouch"
                    target="_blank"
                    rel="noreferrer"
                    className="go-to-detail-video"
                  >
                    {ready && t('homepage.see.detail')}
                    <Image
                      alt=""
                      src={iconViewMore}
                      width={16}
                      height={16}
                      style={{ marginLeft: 8 }}
                    />
                  </a>
                </p>
              </div>
              <button
                type="button"
                className="btn-play-video"
                onClick={() => setModalShowVrTouch(true)}
              >
                <Image alt="" src={playIcon} />
              </button>
              <ModalVideo
                show={modalShowVrTouch}
                onHide={() => setModalShowVrTouch(false)}
                urlvideo="https://data.unicloud.com.vn/wp-content/uploads/2022/04/Touch-devices.mp4"
              />
            </div>
          </Col>
          <Col xs={12} md={6} lg={6}>
            <div className="col-smart-city col-video col-uniVR-Game">
              <div className="div-title-video">
                <h3 className="title-sub-smart-city-video">UniVR Game</h3>
                <p className="detail-sub-smart-city-video">{ready && t('homepage.vr.uni.game')}</p>
                <p className="text-center mb-4">
                  <a
                    href="https://univr.vn/univrgame"
                    target="_blank"
                    rel="noreferrer"
                    className="go-to-detail-video"
                  >
                    {ready && t('homepage.see.detail')}
                    <Image
                      src={iconViewMore}
                      width={16}
                      height={16}
                      style={{ marginLeft: 8 }}
                      alt=""
                    />
                  </a>
                </p>
              </div>
              <button
                type="button"
                className="btn-play-video"
                onClick={() => setModalShowVrGame(true)}
              >
                <Image src={playIcon} alt="" />
              </button>
              <ModalVideo
                show={modalShowVrGame}
                onHide={() => setModalShowVrGame(false)}
                urlvideo="https://data.unicloud.com.vn/wp-content/uploads/2022/04/Demo_SmartFitness.mp4"
              />
            </div>
          </Col>
        </Row>
        <Row className="gx-3 gy-3 mb-lg-4 mb-3">
          <Col xs={12} md={6} lg={6}>
            <div className="col-smart-city col-video col-metaverse">
              <div className="div-title-video">
                <h3 className="title-sub-smart-city-video">Metaverse</h3>
                <p className="detail-sub-smart-city-video">
                  {ready && t('homepage.vr.uni.metaverse')}
                </p>
                <p className="text-center mb-4">
                  <a
                    href="/coming-soon"
                    target="_blank"
                    className="go-to-detail-video"
                    rel="noreferrer"
                  >
                    {ready && t('homepage.see.detail')}
                    <Image
                      src={iconViewMore}
                      width={16}
                      height={16}
                      style={{ marginLeft: 8 }}
                      alt=""
                    />
                  </a>
                </p>
              </div>
              {/* <button type="button" className="btn-play-video" onClick={() => setModalShow(true)}>
                <Image src={playIcon} alt="" />
              </button>
              <ModalVideo
                show={modalShow}
                onHide={() => setModalShow(false)}
                urlvideo="https://www.youtube.com/watch?v=6VsQkaZTa0c"
              /> */}
            </div>
          </Col>
          <Col xs={12} md={6} lg={6} />
        </Row>
      </Container>
    </div>
  );
}
export default RowVirtualReality;
