import React, { useCallback, useEffect, useState } from 'react';

import { Swiper, SwiperSlide } from 'swiper/react';

import { FreeMode, Navigation, Thumbs } from 'swiper';
import imgSmartCity from '/assets/image/homePage/img-header-smart-city.jpg';
import imgDigitalTransformation from '/assets/image/homePage/img-header-digital-transformation.jpg';
import imgDigitalBanking from '/assets/image/homePage/img-header-digital-banking.jpg';
import imgHeaderVr from '/assets/image/homePage/img-header-vr.jpg';

import iconEcosystem from '/assets/image/svg/icon-ecosystem-earth.svg';
import iconEcosystemWhite from '/assets/image/svg/icon-ecosystem-earth-white.svg';

import iconFormWare from '/assets/image/svg/icon-form-ware.svg';
import iconFormWareWhite from '/assets/image/svg/icon-form-ware-white.svg';

import iconSmartCity from '/assets/image/svg/icon-smart-city.svg';
import iconSmartCityWhite from '/assets/image/svg/icon-smart-city-white.svg';

import iconVr from '/assets/image/svg/icon-vr.svg';
import iconVrWhite from '/assets/image/svg/icon-vr-white.svg';
import logoColor from '/assets/image/logo/logo-color.svg';
import useWindowSize from '@src/hooks/useWindowsSize';
import { useTranslation } from 'react-i18next';

function RowHeaderVersion2() {
  const { ready, t } = useTranslation();
  // const currentLng = i18n.language;
  const [thumbsSwiper, setThumbsSwiper] = useState<null | undefined>(null);
  const [activeIndex, setActiveIndex] = useState<number>(0);

  const size = useWindowSize();
  const [spaceBetween, setSpaceBetween] = useState(48);

  useEffect(() => {
    if (size.width! > 768) {
      setSpaceBetween(48);
    } else {
      setSpaceBetween(22);
    }

    return () => {};
  }, [size.width]);

  const handleOnClick = useCallback(
    (index: any) => {
      setActiveIndex(index);
    },
    [activeIndex],
  );

  return (
    <section className="page-header-v2">
      <Swiper
        parallax
        thumbs={{ swiper: thumbsSwiper }}
        modules={[FreeMode, Thumbs]}
        effect="cube"
        className="mySwiper2"
        onRealIndexChange={(swiper) => setActiveIndex(swiper.realIndex)}
      >
        <SwiperSlide>
          <div
            className="img-slide-v2"
            style={{ backgroundImage: `url(${imgDigitalTransformation})` }}
          >
            <div className="popup-v2-tag">
              <div className="popup-v2-tag-wrap  popup-v2-tag-transformation">
                <img src={logoColor} className="logo-header-home" alt="logo Unicloud" />
                <h2 className="popup-v2-tag-title">
                  {ready && t('homepage.rowHeader.digitalTransformation.title')}
                </h2>
                <h4 className="popup-v2-tag-subTitle">
                  {ready && t('homepage.rowHeader.digitalTransformation.subTitle')}
                </h4>
                <a href="/coming-soon" target="_blank" rel="noreferrer">
                  <button type="button">{ready && t('homepage.rowHeader.btnLearnMore')}</button>
                </a>
              </div>
            </div>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="img-slide-v2" style={{ backgroundImage: `url(${imgDigitalBanking})` }}>
            <div className="popup-v2-tag">
              <div className="popup-v2-tag-wrap  popup-v2-tag-city ">
                <img src={logoColor} alt="logo Unicloud" className="logo-header-home" />
                <h2 className="popup-v2-tag-title">
                  {ready && t('homepage.rowHeader.digitalBanking.title')}
                </h2>
                <h4 className="popup-v2-tag-subTitle">
                  {ready && t('homepage.rowHeader.digitalBanking.subTitle')}
                </h4>
                <a href="/neobank" target="_blank" rel="noreferrer">
                  <button type="button">{ready && t('homepage.rowHeader.btnLearnMore')}</button>
                </a>
              </div>
            </div>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="img-slide-v2" style={{ backgroundImage: `url(${imgSmartCity})` }}>
            <div className="popup-v2-tag">
              <div className="popup-v2-tag-wrap  popup-v2-tag-city">
                <img src={logoColor} alt="logo Unicloud" className="logo-header-home" />
                <h2 className="popup-v2-tag-title">
                  {ready && t('homepage.rowHeader.smartCity.title')}
                </h2>
                <h4 className="popup-v2-tag-subTitle">
                  {ready && t('homepage.rowHeader.smartCity.subTitle')}
                </h4>
                <a href="/coming-soon" target="_blank" rel="noreferrer">
                  <button type="button">{ready && t('homepage.rowHeader.btnLearnMore')}</button>
                </a>
              </div>
            </div>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="img-slide-v2" style={{ backgroundImage: `url(${imgHeaderVr})` }}>
            <div className="popup-v2-tag ">
              <div className="popup-v2-tag-wrap popup-v2-tag-transformation is-background-popup">
                <img src={logoColor} alt="logo Unicloud" className="logo-header-home" />
                <h2 className="popup-v2-tag-title">
                  {ready && t('homepage.rowHeader.virtulaReality.title')}
                </h2>
                <h4 className="popup-v2-tag-subTitle">
                  {ready && t('homepage.rowHeader.virtulaReality.subTitle')}
                </h4>
                <a href="https://univr.vn/" target="_blank" rel="noreferrer">
                  <button type="button">{ready && t('homepage.rowHeader.btnLearnMore')}</button>
                </a>
              </div>
            </div>
          </div>
        </SwiperSlide>
      </Swiper>
      <Swiper
        onSwiper={(swiper: any) => setThumbsSwiper(swiper)}
        spaceBetween={spaceBetween}
        slidesPerView={4}
        speed={0}
        effect="cards"
        nested
        modules={[FreeMode, Navigation, Thumbs]}
        className="slide-v2-tag"
        onRealIndexChange={(swiper) => handleOnClick(swiper.realIndex)}
      >
        <SwiperSlide>
          <div className={`btn-slide-v2 ${activeIndex === 0 ? 'is-active-btn-slide' : ''}`}>
            <div className="btn-slide-v2-img">
              {activeIndex !== 0 && (
                <img className="slide-v2-icon" src={iconFormWareWhite} alt="" />
              )}
              {activeIndex === 0 ? (
                <img className="slide-v2-icon" src={iconFormWare} alt="" />
              ) : null}
            </div>
            <h4 className="slide-v2-title">Digital transformation</h4>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className={`btn-slide-v2 ${activeIndex === 1 ? 'is-active-btn-slide' : ''}`}>
            <div className="btn-slide-v2-img">
              {activeIndex !== 1 && (
                <img className="slide-v2-icon" src={iconEcosystemWhite} alt="" />
              )}
              {activeIndex === 1 && <img className="slide-v2-icon" src={iconEcosystem} alt="" />}
            </div>
            <h4 className="slide-v2-title">DIGITAL BANKING PLATFORM</h4>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className={`btn-slide-v2 ${activeIndex === 2 ? 'is-active-btn-slide' : ''}`}>
            <div className="btn-slide-v2-img">
              {activeIndex !== 2 && (
                <img className="slide-v2-icon" src={iconSmartCityWhite} alt="" />
              )}
              {activeIndex === 2 && <img className="slide-v2-icon" src={iconSmartCity} alt="" />}
            </div>
            <h4 className="slide-v2-title">Smart city</h4>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className={`btn-slide-v2 ${activeIndex === 3 ? 'is-active-btn-slide' : ''}`}>
            <div className="btn-slide-v2-img">
              {activeIndex !== 3 && <img className="slide-v2-icon" src={iconVrWhite} alt="" />}
              {activeIndex === 3 && <img className="slide-v2-icon" src={iconVr} alt="" />}
            </div>
            <h4 className="slide-v2-title">VIRTUAL REALITY (VR)</h4>
          </div>
        </SwiperSlide>
      </Swiper>
      <div className="tangle-bottom-header background-image" />
    </section>
  );
}

export default RowHeaderVersion2;
