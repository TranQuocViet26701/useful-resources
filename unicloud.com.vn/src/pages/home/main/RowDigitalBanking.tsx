import React from 'react';
import { Col, Row, Image } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';

import slide1 from '/assets/image/homePage/slide-stm.png';
import slide2 from '/assets/image/homePage/slide-stm-2.png';
import unicat1 from '/assets/image/homePage/unicat-1.png';
import unicat2 from '/assets/image/homePage/unicat-2.png';
import unicat3 from '/assets/image/homePage/unicat-3.png';
import unicat4 from '/assets/image/homePage/unicat-4.png';
import unicat5 from '/assets/image/homePage/unicat-5.png';
import unicat7 from '/assets/image/homePage/unicat-7.png';
import unicat8 from '/assets/image/homePage/unicat-8.png';
import unicat9 from '/assets/image/homePage/unicat-9.png';
import bankingDevice from '/assets/image/homePage/banking-device.png';
import klb from '/assets/image/homePage/klb.png';
import iconArrowRightBlue from '/assets/image/svg/icon-arrow-right-blue.svg';
import { useTranslation } from 'react-i18next';

function BuildImage(props: { url: string }) {
  const { url } = props;
  return (
    <Row className="card-digital-image max-width-629">
      <Image src={url} alt="" loading="lazy" />
    </Row>
  );
}

function BuildSlide(props: { urls: any[] }) {
  const { urls } = props;
  return (
    <Row className=" justify-content-center card-digital-slide row-slide-smart-living">
      <Col xs={2} md={2} className="px-0 text-center align-self-center">
        <button
          type="button"
          className="btn-arrow-left btn-arrow-left-smart-living pre-slide-technology"
        />
      </Col>
      <Col xs={8} md={5} className="col-slide-smart-living">
        <Swiper
          navigation={{
            prevEl: '.pre-slide-technology',
            nextEl: '.next-slide-technology',
          }}
          pagination={{ clickable: true }}
          className="swiper-card-digital"
          breakpoints={{
            1196: {
              slidesPerView: 1,
              spaceBetween: 35,
            },
            375: {
              slidesPerView: 1,
              spaceBetween: 22,
            },
          }}
        >
          {urls.map((url, index) => (
            <SwiperSlide key={`${index + url}`}>
              <div className="card-img-slide">
                <Image src={url} className="img-fluid" loading="lazy" alt="" />
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </Col>
      <Col xs={2} md={2} className="px-0 text-center align-self-center">
        <button
          type="button"
          className="btn-arrow-right btn-arrow-right-smart-living   next-slide-technology"
        />
      </Col>
    </Row>
  );
}

function BuildCardSlide(props: {
  title: string;
  subTitle: string;
  urlSlide: any[];
  linkTo: string;
  translate: any;
}) {
  const { title, subTitle, urlSlide, linkTo, translate } = props;

  return (
    <>
      <div className="card-item-title">
        <h3>{title}</h3>
        <h4>{subTitle}</h4>
        <a href={linkTo} target="_blank" rel="noreferrer" className="btn-more">
          <span>{translate('homepage.digitalBankingPlatform.viewDetail')}</span>
          <span>
            <img src={iconArrowRightBlue} alt="" />
          </span>
        </a>
      </div>
      <BuildSlide urls={urlSlide} />
    </>
  );
}

function BuildCardImage(props: {
  title: string;
  subTitle: string;
  urlImage: string;
  linkTo: string;
  translate: any;
}) {
  const { title, subTitle, urlImage, linkTo, translate } = props;

  return (
    <>
      <div className="card-item-title">
        <h3>{title}</h3>
        <h4>{subTitle}</h4>
        <a href={linkTo} target="_blank" rel="noreferrer" className="btn-more">
          <span>{translate('homepage.digitalBankingPlatform.viewDetail')}</span>
          <span>
            <img src={iconArrowRightBlue} alt="" />
          </span>
        </a>
      </div>
      <BuildImage url={urlImage} />
    </>
  );
}

function RowDigitalBanking() {
  const urlSlide: any[] = [slide1, slide2];
  const urlSlideUniCat: any[] = [
    unicat1,
    unicat2,
    unicat3,
    unicat4,
    unicat5,
    unicat7,
    unicat8,
    unicat9,
  ];

  const { t, ready } = useTranslation();

  return (
    <section className="digital-banking" id="digital-banking">
      <div id="section-digital-banking" />
      <Row className="container-child digital-banking-title">
        <a href="/neobank" target="_blank" rel="noreferrer">
          <h2 className="title">UNICLOUD DIGITAL BANKING PLATFORM</h2>
        </a>
        <p className="description description-padding">
          {ready && t('homepage.digitalBankingPlatform.subTitle')}
        </p>
      </Row>
      <Row className="container-wrap grid-2">
        <Col xs={12} md={6} className="card-item grid-2-item">
          <BuildCardSlide
            title="Smart Teller Machine"
            subTitle={ready ? t('homepage.digitalBankingPlatform.smartTellerMachine.subTitle') : ''}
            urlSlide={urlSlide}
            linkTo="/neobank#smart-teller-machine"
            translate={t}
          />
          {/* <div className="card-item-title">
            <h3>Smart Teller Machine</h3>
            <h4>{ready && t('homepage.digitalBankingPlatform.smartTellerMachine.subTitle')}</h4>
            <a
              href="/neobank#smart-teller-machine"
              target="_blank"
              rel="noreferrer"
              className="btn-more mb-4"
            >
              <span>{ready && t('homepage.digitalBankingPlatform.viewDetail')}</span>
              <span>
                <img src={iconArrowRightBlue} alt="" />
              </span>
            </a>
          </div>
          <Row>
            <Col className="col-slide-stm-home">
              <div className="wrapper-slide-stm-home">
                <Swiper
                  slidesPerView={1}
                  navigation={{
                    prevEl: '.pre-slide-home-stm',
                    nextEl: '.next-slide-home-stm',
                  }}
                  pagination={{ clickable: true }}
                  className="swiper-home-uni-cart"
                >
                  <SwiperSlide>
                    <Image src={slide2} className="img-fluid img-slide-stm-home" />
                  </SwiperSlide>
                  <SwiperSlide>
                    <Image src={slide1} className="img-fluid img-slide-stm-home" />
                  </SwiperSlide>
                </Swiper>
                <button
                  type="button"
                  className="btn-arrow-left btn-left-slide-home-stm pre-slide-home-stm"
                />
                <button
                  type="button"
                  className="btn-arrow-right btn-right-slide-home-stm next-slide-home-stm"
                />
              </div>
            </Col>
          </Row> */}
        </Col>
        <Col xs={12} md={6} className="card-item grid-2-item">
          <div className="card-item-title">
            <h3>UniCAT</h3>
            <h4>{ready && t('homepage.digitalBankingPlatform.uniCAT.subTitle')}</h4>
            <a
              href="/neobank#uni-cat-STM"
              target="_blank"
              rel="noreferrer"
              className="btn-more mb-4"
            >
              <span>{ready && t('homepage.digitalBankingPlatform.viewDetail')}</span>
              <span>
                <img src={iconArrowRightBlue} alt="" />
              </span>
            </a>
          </div>
          <Row className="row-slide-smart-living">
            <Col md={1} />
            <Col xs={1} md={1} className="px-0 text-center align-self-center">
              <button
                type="button"
                className="btn-arrow-left btn-arrow-left-smart-living pre-slide-home-unicat"
              />
            </Col>
            <Col xs={10} md={8} className="col-wrapper-slide-uni-cat">
              <div className="wrapper-slide-home-uni-cat">
                <Swiper
                  slidesPerView={1}
                  navigation={{
                    prevEl: '.pre-slide-home-unicat',
                    nextEl: '.next-slide-home-unicat',
                  }}
                  pagination={{ clickable: true }}
                  className="swiper-home-uni-cart"
                >
                  {urlSlideUniCat.map((url, index) => (
                    <SwiperSlide key={`${url + index}`}>
                      <Image src={url} className="img-fluid img-slide-home-uni-cat" />
                    </SwiperSlide>
                  ))}
                </Swiper>
              </div>
            </Col>
            <Col xs={1} md={1} className="px-0 text-center align-self-center">
              <button
                type="button"
                className="btn-arrow-right btn-arrow-right-smart-living next-slide-home-unicat"
              />
            </Col>
            <Col md={1} />
          </Row>
        </Col>
      </Row>
      <Row className="container-wrap grid-2">
        <Col xs={12} md={6} className="card-item grid-2-item">
          <BuildCardImage
            title="Mobile Banking & NeoBank"
            subTitle={
              ready ? t('homepage.digitalBankingPlatform.mobileBankingAndNeoBank.subTitle') : ''
            }
            urlImage={klb}
            linkTo="/neobank#digital-banking-software"
            translate={t}
          />
        </Col>
        <Col xs={12} md={6} className="card-item grid-2-item">
          <BuildCardImage
            title="UniAlarm"
            subTitle={ready ? t('homepage.digitalBankingPlatform.bankingDevices.subTitle') : ''}
            urlImage={bankingDevice}
            linkTo="/neobank#digital-banking-equipment"
            translate={t}
          />
        </Col>
      </Row>
    </section>
  );
}

export default RowDigitalBanking;
