import React from 'react';
import { Container } from 'react-bootstrap';
import SEO from '@src/seo/seo';
import RowPartnership from './main/RowPartnership';
import RowCreateFuture from './main/RowCreateFuture';
// import RowTopSliderHomepage from './main/RowTopSliderHomepage';
// import RowSecondBodyHomePage from './main/RowSecondBodyHomepage';
// import RowReasonUs from './main/RowReasonUs';
// import RowEcosystem from './main/RowEcosystem';
// import RowTechnology from '@components/technology/RowTechnology';
import RowRegister from './main/RowRegister';
import RowSmartCity from './main/RowSmartCity';
import RowVirtualReality from './main/RowVirtualReality';
import RowDigitalBanking from './main/RowDigitalBanking';
import RowDigitalTransformation from './main/RowDigitalTransformation';
import RowScrollEcosystem from './main/RowScrollEcosystem';
import RowHeaderVersion2 from './main/RowHeaderVersion2';

import './Homepage.scss';

function HomePage() {
  return (
    <main>
      <SEO
        title="Unicloud Group - Tập Đoàn Công Nghệ Quốc Tế Tiên Phong Chuyển Đổi Số"
        description="Unicloud Group nghiên cứu, phát triển, và cung cấp các sản phẩm, 
        giải pháp công nghệ quan trọng cho hầu hết các lĩnh vực “xương sống” của nền kinh tế.
         Nhằm mục tiêu tăng cường năng lực và thúc đẩy sự phát triển của các ngành nghề, nâng cao chất lượng cuộc sống."
        url="/"
      />
      <Container className="max-width-100 px-0" fluid>
        <div className="overlay" id="overlay" />
        <RowScrollEcosystem />
        <RowHeaderVersion2 />
        <RowDigitalBanking />
        <RowDigitalTransformation />
        <RowSmartCity />
        <RowVirtualReality />
        <RowPartnership />
        <RowCreateFuture />
        <RowRegister />
        {/* <RowTechnology /> */}

        {/* <RowTopSliderHomepage /> */}
        {/* <RowSecondBodyHomePage /> */}
        {/* <RowEcosystem /> */}
        {/* <RowReasonUs /> */}
      </Container>
    </main>
  );
}
export default HomePage;
