import React from 'react';
// import { Link } from 'react-router-dom';
import { Container, Row, Col, Image } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import ModalAdvise from '../../../../components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import imgUnderLine from '/assets/image/svg/title-line-under.svg';
import imgSolution from '/assets/image/landing-page/digital-banking/img-solution.png';

function RowSolutionStmAtmKios() {
  const { t, ready } = useTranslation();

  const [modalShow, setModalShow] = React.useState(false);
  return (
    <div className="back-ground-solution-digital" id="back-ground-solution-digital">
      <Container className="max-width-1180 px-0">
        <Row className="padding-left-right">
          <h1 className="title-banking-software">
            {ready && t('digitalBankingLandingPage.solutionSTM/ATM/KIOS.titleMain')}
          </h1>
          <div className="d-flex justify-content-center">
            <Image src={imgUnderLine} style={{ width: 224, height: 6 }} />
          </div>
          <Col>
            <Row>
              <Col md={5} className="col-left-STM">
                <h2 className="title-sub-digital">
                  {ready && t('digitalBankingLandingPage.solutionSTM/ATM/KIOS.title')}
                </h2>
                <p className="detail-digital-1">
                  {ready && t('digitalBankingLandingPage.solutionSTM/ATM/KIOS.titleSub')}
                </p>
                <p className="detail-digital-2">
                  {ready && t('digitalBankingLandingPage.solutionSTM/ATM/KIOS.des')}
                </p>
                <div>
                  <button
                    type="button"
                    className="btn-digital-banking"
                    onClick={() => setModalShow(true)}
                  >
                    {ready && t('digitalBankingLandingPage.getAdvice')}
                  </button>
                </div>
              </Col>
              <Col md={7} className="col-right-solution">
                <Row className="row-img-solution">
                  <Image src={imgSolution} className="img-fluid px-0 img-solution-STM" />
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
      </Container>
    </div>
  );
}
export default RowSolutionStmAtmKios;
