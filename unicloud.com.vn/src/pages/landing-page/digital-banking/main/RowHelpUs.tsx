import React from 'react';
import { Container, Image, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import imgUnderLine from '/assets/image/svg/title-line-under.svg';
import iconBusiness from '/assets/image/svg/icon-business.svg';
import iconSupport from '/assets/image/svg/icon-support.svg';
import iconMedia from '/assets/image/svg/icon-media.svg';

function RowHelpUs() {
  const { t, ready } = useTranslation();

  return (
    <div className="back-ground-row-contact-us">
      <Container className="max-width-1180 padding-left-right">
        <h1 className="title-banking-software">
          {ready && t('digitalBankingLandingPage.helpMore.title')}
        </h1>
        <div className="d-flex justify-content-center">
          <Image src={imgUnderLine} style={{ width: 224, height: 6 }} />
        </div>
        <p className="description-partnership-digital">
          {ready && t('digitalBankingLandingPage.helpMore.des')}
        </p>
        <Row className="contact-us-cards">
          <div className="contact-us-card">
            <img src={iconBusiness} alt="" className="contact-us-card-image" />
            <h4 className="contact-us-card-title">
              {ready && t('digitalBankingLandingPage.helpMore.item1')}
            </h4>
            <h3 className="contact-us-card-des">sales@unicloud.com.vn</h3>
          </div>
          <div className="contact-us-card">
            <img src={iconSupport} alt="" className="contact-us-card-image" />
            <h4 className="contact-us-card-title">
              {ready && t('digitalBankingLandingPage.helpMore.item2')}
            </h4>
            <h3 className="contact-us-card-des">supports@unicloud.com.vn</h3>
          </div>
          <div className="contact-us-card">
            <img src={iconMedia} alt="" className="contact-us-card-image" />
            <h4 className="contact-us-card-title">
              {ready && t('digitalBankingLandingPage.helpMore.item3')}
            </h4>
            <h3 className="contact-us-card-des">media@unicloud.com.vn</h3>
          </div>
        </Row>
      </Container>
    </div>
  );
}

export default RowHelpUs;
