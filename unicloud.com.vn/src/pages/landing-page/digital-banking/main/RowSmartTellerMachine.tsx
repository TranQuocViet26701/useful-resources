import React from 'react';
// import { Link } from 'react-router-dom';
import { Container, Row, Image } from 'react-bootstrap';
import imgSmartTellerMachine from '/assets/image/landing-page/digital-banking/img-smart-teller-machine.png';

function RowSmartTellerMachine() {
  return (
    <div className="back-ground-teller-machine" id="smart-teller-machine">
      <Container className="max-width-1180 px-0">
        <Row className="padding-left-right">
          <h1 className="title-banking-software">SMART TELLER MACHINE</h1>
          <Image src={imgSmartTellerMachine} className="img-fluid img-smart-teller-machine" />
        </Row>
      </Container>
    </div>
  );
}
export default RowSmartTellerMachine;
