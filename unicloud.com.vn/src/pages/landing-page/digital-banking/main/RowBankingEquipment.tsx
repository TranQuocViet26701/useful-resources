import React from 'react';
// import { Link } from 'react-router-dom';
import { Container, Row, Col, Image } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import ModalAdvise from '../../../../components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import imgUnderLine from '/assets/image/svg/title-line-under.svg';
import imgRight from '/assets/image/landing-page/digital-banking/img-banking-equipment.png';
import imgMPOS from '/assets/image/landing-page/digital-banking/img-mpos.png';
import imgUniQueue from '/assets/image/landing-page/digital-banking/img-uniqueue.png';

function RowBankingEquipment() {
  const { t, ready } = useTranslation();

  const [modalShow, setModalShow] = React.useState(false);
  return (
    <div className="back-ground-banking-equipment" id="digital-banking-equipment">
      <Container className="max-width-1180 px-0">
        <Row className="padding-left-right">
          <h1 className="title-banking-software">
            {ready && t('digitalBankingLandingPage.bankingEquipment.title')}
          </h1>
          <div className="d-flex justify-content-center">
            <Image src={imgUnderLine} style={{ width: 224, height: 6 }} />
          </div>
          <Col>
            <Row className="row-main-banking-software">
              <Col md={5} className="pd-r-6">
                <h2 className="title-sub-digital">
                  {ready && t('digitalBankingLandingPage.bankingEquipment.titleChild1')}
                </h2>
                <p className="detail-digital-1">
                  {ready && t('digitalBankingLandingPage.bankingEquipment.des1')}
                </p>
                <ul className="ul-uni-alarm">
                  <li>{ready && t('digitalBankingLandingPage.bankingEquipment.item1')}</li>
                  <li>{ready && t('digitalBankingLandingPage.bankingEquipment.item2')}</li>
                  <li>{ready && t('digitalBankingLandingPage.bankingEquipment.item3')}</li>
                  <li>{ready && t('digitalBankingLandingPage.bankingEquipment.item4')}</li>
                </ul>

                <div>
                  <button
                    type="button"
                    className="btn-digital-banking"
                    onClick={() => setModalShow(true)}
                  >
                    {ready && t('digitalBankingLandingPage.getAdvice')}
                  </button>
                </div>
              </Col>
              <Col md={7} className="pd-l-6 col-right-banking-software">
                <Row>
                  <Image src={imgRight} className="img-fluid" />
                </Row>
              </Col>
            </Row>

            <Row className="gy-5 row-main-banking-software">
              <Col md={7}>
                <Row>
                  <Image src={imgMPOS} className="img-fluid" />
                </Row>
              </Col>
              <Col md={5} className="order-first order-md-last">
                <h2 className="title-sub-digital">
                  {ready && t('digitalBankingLandingPage.bankingEquipment.titleChild2')}
                </h2>
                <p className="detail-digital-1">
                  {ready && t('digitalBankingLandingPage.bankingEquipment.des2')}
                </p>
                <p className="detail-digital-2">
                  {ready && t('digitalBankingLandingPage.bankingEquipment.item5')}
                </p>
                <div className="d-flex">
                  <button
                    type="button"
                    className="btn-digital-banking"
                    onClick={() => setModalShow(true)}
                  >
                    {ready && t('digitalBankingLandingPage.getAdvice')}
                  </button>
                </div>
              </Col>
            </Row>

            <div>
              <Row className="gy-5 row-main-banking-software" id="digital-banking-uniqueue">
                <Col md={5} className="">
                  <h2 className="title-sub-digital">
                    {ready && t('digitalBankingLandingPage.bankingEquipment.titleChild3')}
                  </h2>
                  <p className="detail-digital-1">
                    {ready && t('digitalBankingLandingPage.bankingEquipment.des3')}
                  </p>
                  <p className="detail-digital-2">
                    {ready && t('digitalBankingLandingPage.bankingEquipment.item6')}
                  </p>
                  <div className="d-flex">
                    <button
                      type="button"
                      className="btn-digital-banking"
                      onClick={() => setModalShow(true)}
                    >
                      {ready && t('digitalBankingLandingPage.getAdvice')}
                    </button>
                  </div>
                </Col>
                <Col md={7}>
                  <Row>
                    <Image src={imgUniQueue} className="img-fluid img-uni-queue" />
                  </Row>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
        <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
      </Container>
    </div>
  );
}
export default RowBankingEquipment;
