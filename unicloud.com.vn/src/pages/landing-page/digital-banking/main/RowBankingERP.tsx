import React from 'react';
// import { Link } from 'react-router-dom';
import { Container, Row, Col, Image } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay } from 'swiper';
import { useTranslation } from 'react-i18next';
import ModalAdvise from '../../../../components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import imgArrowLeftDiscover from '/assets/image/svg/arrow-left-slide-to-discover.svg';
import imgArrowRightDiscover from '/assets/image/svg/arrow-right-slide-to-discover.svg';
import iconMouseScroll from '/assets/image/svg/icon-mouse-scroll.svg';
import imgSlide1 from '/assets/image/landing-page/digital-banking/img-slide-banking-erp-1.png';
import imgSlide11 from '/assets/image/landing-page/digital-banking/img-slide-banking-erp-1-1.png';
import imgSlide2 from '/assets/image/landing-page/digital-banking/img-slide-banking-erp-2.png';
import imgSlide3 from '/assets/image/landing-page/digital-banking/img-slide-banking-erp-3.png';
import imgSlide4 from '/assets/image/landing-page/digital-banking/img-slide-banking-erp-4.png';
import imgSlide5 from '/assets/image/landing-page/digital-banking/img-slide-banking-erp-5.png';
import imgSlide6 from '/assets/image/landing-page/digital-banking/img-slide-banking-erp-6.png';

function RowBankingERP() {
  const { t, ready } = useTranslation();

  const [modalShow, setModalShow] = React.useState(false);
  return (
    <div className="back-ground-banking-erp" id="digital-banking-erp">
      <Container className="max-width-1180 px-0">
        <Row className="gx-5 padding-left-right">
          <Col xs={12} md={5} className="col-left-erp">
            <h2 className="title-sub-digital">
              {ready && t('digitalBankingLandingPage.bankingERP.title')}
            </h2>
            <p className="detail-digital-1">
              {ready && t('digitalBankingLandingPage.bankingERP.titleSub')}
            </p>
            <p className="detail-digital-2">
              {ready && t('digitalBankingLandingPage.bankingERP.des')}
            </p>
            <div>
              <button
                type="button"
                className="btn-digital-banking"
                onClick={() => setModalShow(true)}
              >
                {ready && t('digitalBankingLandingPage.getAdvice')}
              </button>
            </div>
            <div className="wrapper-mouse-scroll">
              <div className="text-center mt-5 mb-3">
                <Image src={iconMouseScroll} className="img-fluid img-mouse-scroll" />
              </div>
              <div className="text-center">
                <Image src={imgArrowLeftDiscover} />
                <span className="text-discover">
                  {ready && t('digitalBankingLandingPage.bankingERP.discover')}
                </span>
                <Image src={imgArrowRightDiscover} />
              </div>
            </div>
          </Col>
          <Col xs={12} md={7} className="col-slide-banking-erp">
            <Swiper
              modules={[Autoplay]}
              spaceBetween={20}
              slidesPerView={2.5}
              breakpoints={{
                320: {
                  slidesPerView: 1.3,
                },
                600: {
                  slidesPerView: 1.5,
                },
                768: {
                  slidesPerView: 1.3,
                },
                900: {
                  slidesPerView: 1.6,
                },
                1100: {
                  slidesPerView: 2,
                },
                1300: {
                  slidesPerView: 2.2,
                },
                1440: {
                  slidesPerView: 2.4,
                },
                1441: {
                  slidesPerView: 2.5,
                },
              }}
              className="swiper-banking-erp"
            >
              <SwiperSlide>
                <div className="wrapper-slide-banking-erp">
                  <h2 className="title-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideTitle1')}
                  </h2>
                  <div className="text-center">
                    <Image src={imgSlide1} className="img-fluid img-slide-banking-erp" />
                  </div>
                  <p className="detail-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideDes1')}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="wrapper-slide-banking-erp">
                  <h2 className="title-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideTitle2')}
                  </h2>
                  <div className="text-center">
                    <Image src={imgSlide11} className="img-fluid img-slide-banking-erp" />
                  </div>
                  <p className="detail-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideDes2')}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="wrapper-slide-banking-erp">
                  <h2 className="title-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideTitle3')}
                  </h2>
                  <div className="text-center">
                    <Image src={imgSlide2} className="img-fluid img-slide-banking-erp" />
                  </div>
                  <p className="detail-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideDes3')}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="wrapper-slide-banking-erp">
                  <h2 className="title-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideTitle4')}
                  </h2>
                  <div className="text-center">
                    <Image src={imgSlide3} className="img-fluid img-slide-banking-erp" />
                  </div>
                  <p className="detail-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideDes4')}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="wrapper-slide-banking-erp">
                  <h2 className="title-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideTitle5')}
                  </h2>
                  <div className="text-center">
                    <Image src={imgSlide4} className="img-fluid img-slide-banking-erp" />
                  </div>
                  <p className="detail-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideDes5')}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="wrapper-slide-banking-erp">
                  <h2 className="title-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideTitle6')}
                  </h2>
                  <div className="text-center">
                    <Image src={imgSlide5} className="img-fluid img-slide-banking-erp" />
                  </div>
                  <p className="detail-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideDes6')}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="wrapper-slide-banking-erp">
                  <h2 className="title-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideTitle7')}
                  </h2>
                  <div className="text-center">
                    <Image src={imgSlide6} className="img-fluid img-slide-banking-erp" />
                  </div>
                  <p className="detail-slide-banking-erp">
                    {ready && t('digitalBankingLandingPage.bankingERP.slideDes7')}
                  </p>
                </div>
              </SwiperSlide>
            </Swiper>
          </Col>
        </Row>
        <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
      </Container>
    </div>
  );
}
export default RowBankingERP;
