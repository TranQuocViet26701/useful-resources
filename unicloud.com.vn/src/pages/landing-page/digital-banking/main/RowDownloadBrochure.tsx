import React from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@apollo/client/react/hooks/useQuery';
import { gql } from 'graphql-tag';

import ModalAdvise from '../../../../components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import imgDownloadBrochure from '/assets/image/landing-page/digital-banking/img-download-brochure-desktop.png';
import imgDownloadBrochureMobile from '/assets/image/landing-page/digital-banking/img-download-brochure-mobile.png';

const queryData = gql`
  query NewQuery($title: String = "Link Download Bizzone Enterprise") {
    pages(where: { title: $title }) {
      nodes {
        linkdownload {
          link {
            mediaItemUrl
          }
        }
        title
      }
    }
  }
`;

function RowDownloadBrochure() {
  const { data } = useQuery(queryData, {
    variables: { title: 'Link DownLoad Digital Banking' },
  });
  const { t, ready } = useTranslation();
  const [modalShow, setModalShow] = React.useState(false);

  const linkDownload = data?.pages?.nodes[0]?.linkdownload?.link?.mediaItemUrl;

  const handleClickDownload = () => {
    window.open(linkDownload);
  };

  return (
    <div className="back-ground-row-download-brochure ">
      <Container className="max-width-1180 padding-left-right download-brochure-container">
        <Row className=" download-brochure-row mx-0">
          <Col className="px-0" lg={5} md={12} xs={12}>
            <Image src={imgDownloadBrochure} className="download-brochure-img-desktop img-fluid" />
            <Image src={imgDownloadBrochureMobile} className="download-brochure-img-mobile" />
          </Col>
          <Col className="px-0" lg={7} md={12} xs={12}>
            <div className="download-brochure-content">
              <h3 className="download-brochure-title">
                {ready && t('digitalBankingLandingPage.downloadBrochure.title')}
              </h3>
              <h4 className="download-brochure-title-sub">Digital Banking Platform Brochure</h4>
              <div className="download-brochure-btn">
                <Link
                  to="/unicloud-digital-banking-platformV1.4.pdf"
                  target="_blank"
                  download
                  onClick={handleClickDownload}
                >
                  <button
                    type="button"
                    className="btn-digital-banking btn-digital-banking-download"
                  >
                    {ready && t('digitalBankingLandingPage.downloadBrochure.download')}
                  </button>
                </Link>
                <button
                  type="button"
                  className="btn-learn-more-digital-banking margin-btn-learn-more"
                  onClick={() => setModalShow(true)}
                >
                  <span>{ready && t('digitalBankingLandingPage.downloadBrochure.advices')}</span>
                </button>
              </div>
            </div>
          </Col>
        </Row>
        <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
      </Container>
    </div>
  );
}

export default RowDownloadBrochure;
