import React, { useState, useCallback, useEffect } from 'react';
// import { Link } from 'react-router-dom';
import { Container, Row, Col, Image } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import ModalAdvise from '../../../../components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import imgSlide1 from '/assets/image/landing-page/digital-banking/img-slide-digital-banking-1.png';
import imgSlide2 from '/assets/image/landing-page/digital-banking/img-slide-digital-banking-2.png';
import imgSlide3 from '/assets/image/landing-page/digital-banking/img-slide-digital-banking-3.png';
import arrowLeft from '/assets/image/svg/arrow-left-white-slide-digital.svg';
import arrowRight from '/assets/image/svg/arrow-right-white-slide-digital.svg';

function RowTopNews() {
  const { t, ready } = useTranslation();

  const [modalShow, setModalShow] = React.useState(false);
  const [slides, setSlides] = useState(1);

  const handleSlides = useCallback(
    (numSlides: number) => {
      const intNumSlides = numSlides;
      // console.log("numslides hien tai ", intNumSlides);
      if (intNumSlides < 1) {
        setSlides(3);
      } else if (intNumSlides > 3) {
        setSlides(1);
      } else {
        setSlides(intNumSlides);
      }
    },
    [slides],
  );

  useEffect(() => {
    const tick = () => handleSlides(slides + 1);
    const responInterval = setTimeout(tick, 6000);
    return () => {
      clearTimeout(responInterval);
    };
  }, [slides]);

  return (
    <div className="back-ground-top-slide-digital-banking">
      <Container className="max-width-1180 px-0">
        <Row className="gy-5 padding-left-right">
          <Col md={6} className="col-respon-slide-digital">
            <div className="div-arrow-slide-digital">
              <button
                type="button"
                className="btn-arrow-left-digital"
                onClick={() => handleSlides(slides - 1)}
              >
                <Image src={arrowLeft} className="arrow-slide-digital" />
              </button>
              <button
                type="button"
                onClick={() => handleSlides(slides + 1)}
                className="btn-arrow-right-digital"
                style={{ marginLeft: 24 }}
              >
                <Image src={arrowRight} className="arrow-slide-digital" />
              </button>
            </div>
            {ready && (
              <Row className={` ${slides === 1 ? 'slide1 row-content-slide' : 'display-none'}`}>
                <h2 className="title-top-slide-digital">
                  {ready && t('digitalBankingLandingPage.topSlide.title')}
                </h2>
                <p className="detail-top-slide-digital">
                  {ready && t('digitalBankingLandingPage.topSlide.detail1')}
                </p>
                <div className="frame">
                  <button
                    className="btn-advise-digital-banking-slide"
                    type="button"
                    onClick={() => setModalShow(true)}
                  >
                    <span>{ready && t('digitalBankingLandingPage.topSlide.advise')}</span>
                  </button>
                </div>
              </Row>
            )}
            <Row className={` ${slides === 2 ? 'slide2 row-content-slide' : 'display-none'}`}>
              <h2 className="title-top-slide-digital">SMART TELLER MACHINE</h2>
              <p className="sub-title-top-slide-digital">
                {ready && t('digitalBankingLandingPage.topSlide.titleSub2')}
              </p>
              <p className="detail-top-slide-digital">
                {ready && t('digitalBankingLandingPage.topSlide.detail2')}
              </p>
              {/* custom-btn btn-10 */}
              <div className="frame">
                <button
                  className="btn-advise-digital-banking-slide"
                  type="button"
                  onClick={() => setModalShow(true)}
                >
                  <span>{ready && t('digitalBankingLandingPage.topSlide.advise')}</span>
                </button>
              </div>
            </Row>
            <Row className={` ${slides === 3 ? 'slide3 row-content-slide' : 'display-none'}`}>
              <h2 className="title-top-slide-digital">UNI SERVICES</h2>
              <p className="sub-title-top-slide-digital">
                {ready && t('digitalBankingLandingPage.topSlide.titleSub3')}
              </p>
              <p className="detail-top-slide-digital">
                {ready && t('digitalBankingLandingPage.topSlide.detail3')}
              </p>
              <div className="frame">
                <button
                  className="btn-advise-digital-banking-slide"
                  type="button"
                  onClick={() => setModalShow(true)}
                >
                  <span>{ready && t('digitalBankingLandingPage.topSlide.advise')}</span>
                </button>
              </div>
            </Row>
          </Col>
          <Col md={6}>
            <Row className={`text-center ${slides === 1 ? 'slide1' : 'display-none'}`}>
              <Image src={imgSlide1} className="img-fluid img-slide-2-digital" />
            </Row>
            <Row className={`text-center ${slides === 2 ? 'slide2' : 'display-none'}`}>
              <Image src={imgSlide2} className="img-fluid img-slide-2-digital" />
            </Row>
            <Row className={`text-center ${slides === 3 ? 'slide3' : 'display-none'}`}>
              <Image src={imgSlide3} className="img-fluid img-slide-2-digital" />
            </Row>
          </Col>
        </Row>
      </Container>
      <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
    </div>
  );
}
export default RowTopNews;
