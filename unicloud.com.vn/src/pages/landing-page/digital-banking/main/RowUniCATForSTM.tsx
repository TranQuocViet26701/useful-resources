import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Container, Row, Col, Image } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import ModalAdvise from '../../../../components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import imgArrowLeftDiscover from '/assets/image/svg/arrow-left-slide-to-discover.svg';
import imgArrowRightDiscover from '/assets/image/svg/arrow-right-slide-to-discover.svg';
import iconMouseScroll from '/assets/image/svg/icon-mouse-scroll.svg';
import arrowLeft from '/assets/image/svg/arrow-left-white-slide-digital.svg';
import arrowRight from '/assets/image/svg/arrow-right-white-slide-digital.svg';
import unicat1 from '/assets/image/homePage/unicat-1.png';
import unicat2 from '/assets/image/homePage/unicat-2.png';
import unicat3 from '/assets/image/homePage/unicat-3.png';
import unicat4 from '/assets/image/homePage/unicat-4.png';
import unicat5 from '/assets/image/homePage/unicat-5.png';
import unicat7 from '/assets/image/homePage/unicat-7.png';
import unicat8 from '/assets/image/homePage/unicat-8.png';
import unicat9 from '/assets/image/homePage/unicat-9.png';

function RowUniCATForSTM() {
  const { t, ready } = useTranslation();

  const [modalShow, setModalShow] = React.useState(false);
  const imgSlide = [unicat1, unicat2, unicat3, unicat4, unicat5, unicat7, unicat8, unicat9];
  return (
    <div className="back-ground-solution-digital" id="uni-cat-STM">
      <Container className="max-width-1180 px-0">
        <Row className="gx-5 gy-5 padding-left-right">
          <Col md={7}>
            <div className="wrapper-slide-uni-cat">
              <Swiper
                slidesPerView={1}
                navigation={{
                  prevEl: '.pre-slide-uni-cat',
                  nextEl: '.next-slide-uni-cat',
                }}
                className="swiper-uni-cart"
                autoplay={{ delay: 6000 }}
              >
                {imgSlide.map((url, index) => (
                  <SwiperSlide key={`${index + url}`}>
                    <Image
                      src={url}
                      className="img-fluid img-slide-uni-cat"
                      loading="lazy"
                      alt=""
                    />
                  </SwiperSlide>
                ))}
              </Swiper>
              <div className="div-wrapper-btn-slide-unicat">
                <button type="button" className="btn-arrow-left-uni-cat pre-slide-uni-cat">
                  <Image src={arrowLeft} className="arrow-slide-digital" />
                </button>
                <button
                  type="button"
                  className="btn-arrow-right-uni-cat next-slide-uni-cat"
                  style={{ marginLeft: 24 }}
                >
                  <Image src={arrowRight} className="arrow-slide-digital" />
                </button>
              </div>
            </div>
            <div className="div-wrapper-btn-slide-unicat-mobile">
              <button type="button" className="btn-arrow-left-uni-cat-mobile pre-slide-uni-cat">
                <svg
                  className="arrow-slide-digital"
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M9.06233 10L13.0223 13.96L11.8911 15.0912L6.79993 10L11.8911 4.90881L13.0223 6.04001L9.06233 10Z"
                    fill="white"
                  />
                </svg>

                {/* <Image src={arrowLeft} className="arrow-slide-digital" /> */}
              </button>
              <button
                type="button"
                className="btn-arrow-right-uni-cat-mobile next-slide-uni-cat"
                style={{ marginLeft: 24 }}
              >
                <svg
                  className="arrow-slide-digital"
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_5714_74011)">
                    <path
                      d="M10.9377 10L6.97767 13.96L8.10887 
					  15.0912L13.2001 10L8.10887 4.90881L6.97767 6.04001L10.9377 10Z"
                      fill="white"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_5714_74011">
                      <rect
                        width="19.2"
                        height="19.2"
                        fill="white"
                        transform="matrix(-1 0 0 1 19.6001 0.400024)"
                      />
                    </clipPath>
                  </defs>
                </svg>

                {/* <Image src={arrowRight} className="arrow-slide-digital" /> */}
              </button>
            </div>
            <div className="wrapper-mouse-scroll">
              <div className="text-center mt-4 mb-3">
                <Image src={iconMouseScroll} className="img-fluid img-mouse-scroll" />
              </div>
              <div className="text-center">
                <Image src={imgArrowLeftDiscover} />
                <span className="text-discover">
                  {ready && t('digitalBankingLandingPage.bankingERP.discover')}
                </span>
                <Image src={imgArrowRightDiscover} />
              </div>
            </div>
          </Col>
          <Col md={5} className="order-first order-md-last">
            <h2 className="title-sub-digital title-uni-cat">
              {ready && t('digitalBankingLandingPage.unicatSTM.title')}
            </h2>
            <p className="detail-digital-1 detail-uni-cat-1">
              {ready && t('digitalBankingLandingPage.unicatSTM.titleSub')}
            </p>
            <p className="detail-digital-2 detail-uni-cat-2">
              {ready && t('digitalBankingLandingPage.unicatSTM.des')}
            </p>
            <div>
              <button
                type="button"
                className="btn-digital-banking"
                onClick={() => setModalShow(true)}
              >
                {ready && t('digitalBankingLandingPage.getAdvice')}
              </button>
            </div>
          </Col>
        </Row>
        <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
      </Container>
    </div>
  );
}
export default RowUniCATForSTM;
