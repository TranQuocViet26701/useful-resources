import React from 'react';
// import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Container, Row, Col, Image } from 'react-bootstrap';
import ModalAdvise from '../../../../components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import imgUnderLine from '/assets/image/svg/title-line-under.svg';
import imgRight from '/assets/image/landing-page/digital-banking/img-right-banking-software-1.png';
import imgLeft from '/assets/image/landing-page/digital-banking/img-left-banking-software.png';
import logoAppStore from '/assets/image/svg/logo-app-store.svg';
import logoChplay from '/assets/image/svg/logo-chplay.svg';

function RowBankingSoftware() {
  const { t, ready } = useTranslation();

  const [modalShow, setModalShow] = React.useState(false);

  return (
    <div className="back-ground-banking-software" id="digital-banking-software">
      <Container className="max-width-1180 px-0">
        <Row className="padding-left-right">
          <h1 className="title-banking-software">
            {ready && t('digitalBankingLandingPage.bankingSoftware.title')}
          </h1>
          <div className="d-flex justify-content-center">
            <Image src={imgUnderLine} style={{ width: 224, height: 6 }} />
          </div>
          <Col>
            <Row className="row-main-banking-software">
              <Col md={5} className="pd-r-6">
                <h2 className="title-sub-digital">
                  {ready && t('digitalBankingLandingPage.bankingSoftware.itemTitle1')}
                </h2>
                <p className="detail-digital-1">
                  {ready && t('digitalBankingLandingPage.bankingSoftware.itemTitleSub1')}
                </p>
                <p className="detail-digital-2">
                  {ready && t('digitalBankingLandingPage.bankingSoftware.itemDes1')}
                </p>
                <div>
                  <button
                    type="button"
                    className="btn-digital-banking"
                    onClick={() => setModalShow(true)}
                  >
                    {ready && t('digitalBankingLandingPage.getAdvice')}
                  </button>
                </div>
                <div className="div-wrapper-download">
                  <div className="wrapper-icon">
                    <Image src={logoAppStore} />
                  </div>
                  <div className="wrapper-icon" style={{ marginLeft: 26 }}>
                    <Image src={logoChplay} />
                  </div>
                </div>
              </Col>
              <Col md={7} className="pd-l-6 col-right-banking-software ">
                <Row>
                  <Image src={imgRight} className="img-fluid" />
                </Row>
              </Col>
            </Row>

            <Row className="gy-5 row-main-banking-software" id="digital-banking-ekyc">
              <Col md={7}>
                <Row>
                  <Image src={imgLeft} className="img-fluid" />
                </Row>
              </Col>
              <Col md={5} className="order-first order-md-last">
                <h2 className="title-sub-digital">
                  {ready && t('digitalBankingLandingPage.bankingSoftware.itemTitle2')}
                </h2>
                <p className="detail-digital-1">
                  {ready && t('digitalBankingLandingPage.bankingSoftware.itemTitleSub2')}
                </p>
                <p className="detail-digital-2">
                  {ready && t('digitalBankingLandingPage.bankingSoftware.itemDes2')}
                </p>
                <div className="d-flex">
                  <button
                    type="button"
                    className="btn-digital-banking"
                    onClick={() => setModalShow(true)}
                  >
                    {ready && t('digitalBankingLandingPage.getAdvice')}
                  </button>
                  {/* <button
                    type="button"
                    className="btn-learn-more-digital-banking margin-btn-learn-more"
                  >
                    <span>{ready && t('digitalBankingLandingPage.bankingSoftware.learnMore')}</span>
                  </button> */}
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
        <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
      </Container>
    </div>
  );
}
export default RowBankingSoftware;
