import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import ModalAdvise from '../../../../components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import imgUniServices from '/assets/image/landing-page/digital-banking/img-uni-services.png';

function RowUniService() {
  const { t, ready } = useTranslation();

  const [modalShow, setModalShow] = React.useState(false);
  return (
    <div className="back-ground-solution-digital">
      <Container className="max-width-1180 px-0">
        <Row className="gx-5 gy-5 padding-left-right">
          <Col md={7}>
            <div className="text-center">
              <Image src={imgUniServices} className="img-fluid" style={{ borderRadius: 8 }} />
            </div>
          </Col>
          <Col md={5} className="order-first order-md-last col-left-solution">
            <h2 className="title-sub-digital">
              {ready && t('digitalBankingLandingPage.uniService.title')}
            </h2>
            <p className="detail-digital-1">
              {ready && t('digitalBankingLandingPage.uniService.titleSub')}
            </p>
            <p className="detail-digital-2">
              {ready && t('digitalBankingLandingPage.uniService.des')}
            </p>
            <div>
              <button
                type="button"
                className="btn-digital-banking"
                onClick={() => setModalShow(true)}
              >
                {ready && t('digitalBankingLandingPage.getAdvice')}
              </button>
            </div>
          </Col>
        </Row>
        <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
      </Container>
    </div>
  );
}
export default RowUniService;
