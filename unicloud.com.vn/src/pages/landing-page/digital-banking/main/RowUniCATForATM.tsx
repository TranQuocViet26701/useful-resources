import React, { useState, useCallback } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import ModalAdvise from '../../../../components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
// import icon from '/assets/image/svg/icon-unicat-atm-1.svg';

function RowUniCATForATM() {
  const { t, ready } = useTranslation();

  const [modalShow, setModalShow] = React.useState(false);
  const [pricingTable, setpricingTable] = useState(1);

  const handleSlidesPricingTable = useCallback(
    (numPricingTable: string) => {
      const intNumPricingTable = parseInt(numPricingTable, 10);
      // console.log("numslides hien tai ", intNumSlides);
      setpricingTable(intNumPricingTable);
    },
    [pricingTable],
  );

  return (
    <section className="back-ground-solution-digital">
      <Container className="max-width-1180 px-0">
        <Row className="gx-5 gy-5 padding-left-right">
          <Col md={5} className="col-left-solution">
            <h2 className="title-sub-digital title-uni-cat">
              {ready && t('digitalBankingLandingPage.unicatATM.title')}
            </h2>
            <p className="detail-digital-1 detail-uni-cat-1">
              {ready && t('digitalBankingLandingPage.unicatATM.titleSub')}
            </p>
            <p className="detail-digital-2 detail-uni-cat-2">
              {ready && t('digitalBankingLandingPage.unicatATM.des')}
            </p>
            <div>
              <button
                type="button"
                className="btn-digital-banking"
                onClick={() => setModalShow(true)}
              >
                {ready && t('digitalBankingLandingPage.getAdvice')}
              </button>
            </div>
          </Col>
          <Col md={7} className="">
            <div className="wrapper-btn-atm">
              <button
                type="button"
                className={pricingTable === 1 ? 'btn-unicat-atm' : ''}
                onClick={() => handleSlidesPricingTable('1')}
              >
                <span> {ready && t('digitalBankingLandingPage.unicatATM.update')}</span>
              </button>
              <button
                type="button"
                className={pricingTable === 2 ? 'btn-unicat-atm btn-right-atm' : 'btn-right-atm'}
                onClick={() => handleSlidesPricingTable('2')}
                style={{ marginLeft: 24 }}
              >
                <span>{ready && t('digitalBankingLandingPage.unicatATM.transaction')}</span>
              </button>
            </div>
            <div className={pricingTable === 1 ? 'pricingTable-show-1' : 'display-none'}>
              <Row className="div-row-unicat-atm">
                <Col xs={6} md={6} lg={4} className="mt-4 mb-4">
                  <div className="wrapper-icon-unicat-atm" style={{ float: 'left' }}>
                    <div className="icon-unicat-atm icon-unicat-1" />
                    <div className="detail-icon-unicat-atm">
                      {ready && t('digitalBankingLandingPage.unicatATM.item1')}
                    </div>
                  </div>
                </Col>
                <Col xs={6} md={6} lg={4} className="mt-4 mb-4 d-flex justify-content-center">
                  <div className="wrapper-icon-unicat-atm">
                    <div className="icon-unicat-atm icon-unicat-2" />
                    <div className="detail-icon-unicat-atm">
                      {ready && t('digitalBankingLandingPage.unicatATM.item2')}
                    </div>
                  </div>
                </Col>
                <Col xs={6} md={6} lg={4} className="mt-4 mb-4">
                  <div className="wrapper-icon-unicat-atm" style={{ float: 'right' }}>
                    <div className="icon-unicat-atm icon-unicat-3" />
                    <div className="detail-icon-unicat-atm">
                      {ready && t('digitalBankingLandingPage.unicatATM.item3')}
                    </div>
                  </div>
                </Col>

                <Col md={0} lg={2} className="col-empty-digital" />

                <Col xs={6} md={6} lg={4} className="mt-4 mb-4">
                  <div className="wrapper-icon-unicat-atm" style={{ float: 'left' }}>
                    <div className="icon-unicat-atm icon-unicat-4" />
                    <div className="detail-icon-unicat-atm">
                      {ready && t('digitalBankingLandingPage.unicatATM.item4')}
                    </div>
                  </div>
                </Col>
                <Col xs={12} md={12} lg={4} className="mt-4 mb-4 respon-final-icon-uni-stm">
                  <div className="wrapper-icon-unicat-atm">
                    <div className="icon-unicat-atm icon-unicat-5" />
                    <div className="detail-icon-unicat-atm">
                      {ready && t('digitalBankingLandingPage.unicatATM.item5')}
                    </div>
                  </div>
                </Col>

                <Col md={0} lg={2} className="col-empty-digital" />
              </Row>
            </div>
            <div className={pricingTable === 2 ? 'pricingTable-show-2' : 'display-none'}>
              <Row className="div-row-unicat-atm">
                <Col xs={6} md={6} lg={6} className="float-right mt-4 mb-4">
                  <div className="wrapper-icon-unicat-atm" style={{ float: 'right' }}>
                    <div className="icon-unicat-atm icon-unicat-6" />
                    <div className="detail-icon-unicat-atm">
                      {ready && t('digitalBankingLandingPage.unicatATM.item6')}
                    </div>
                  </div>
                </Col>
                <Col xs={6} md={6} lg={6}>
                  <div className="wrapper-icon-unicat-atm mt-4 mb-4" style={{ float: 'left' }}>
                    <div className="icon-unicat-atm icon-unicat-7" />
                    <div className="detail-icon-unicat-atm">
                      {ready && t('digitalBankingLandingPage.unicatATM.item7')}
                    </div>
                  </div>
                </Col>
                <Col xs={6} md={6} lg={6} className="float-right">
                  <div className="wrapper-icon-unicat-atm mt-4 mb-4" style={{ float: 'right' }}>
                    <div className="icon-unicat-atm icon-unicat-8" />
                    <div className="detail-icon-unicat-atm">
                      {ready && t('digitalBankingLandingPage.unicatATM.item8')}
                    </div>
                  </div>
                </Col>
                <Col xs={6} md={6} lg={6}>
                  <div className="wrapper-icon-unicat-atm mt-4 mb-4" style={{ float: 'left' }}>
                    <div className="icon-unicat-atm icon-unicat-9" />
                    <div className="detail-icon-unicat-atm">
                      {ready && t('digitalBankingLandingPage.unicatATM.item9')}
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
        <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
      </Container>
    </section>
  );
}
export default RowUniCATForATM;
