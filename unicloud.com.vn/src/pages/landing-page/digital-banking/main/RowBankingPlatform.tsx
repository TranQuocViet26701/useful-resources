import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import bgHalf1 from '/assets/image/svg/bg-dots-circle-half 1.svg';
import bgHalf2 from '/assets/image/svg/bg-dots-circle-half 2.svg';
import iconCheck from '/assets/image/svg/icon-check.svg';
import iconStandard from '/assets/image/svg/icon-standard.svg';
import iconDiversity from '/assets/image/svg/icon-diversity.svg';
import iconIntegration from '/assets/image/svg/icon-integration.svg';
import iconProfessional from '/assets/image/svg/icon-professional.svg';

function RowHighlights() {
  const { t, ready } = useTranslation();

  return (
    <div className="back-ground-banking-platform">
      <img src={bgHalf1} alt="bg-dots-circle-half" className="banking-platform-img" />
      <Container className="max-width-1180 px-0 banking-platform-container">
        <Row className="padding-left-right">
          <Col xs={12} md={6} lg={6} className="banking-platform-col-left">
            <h2 className="banking-platform-title">Unicloud Digital Banking Platform</h2>
            <h3 className="banking-platform-title-sub">
              {ready && t('digitalBankingLandingPage.bankingPlatform.titleSub')}
            </h3>
            <p className="banking-platform-title-des">
              {ready && t('digitalBankingLandingPage.bankingPlatform.titleDes')}
            </p>
            <ul className="banking-platform-group">
              <li className="banking-platform-item">
                <img
                  src={iconCheck}
                  alt="item-icon-banking-platform"
                  className="banking-platform-item-icon"
                />
                <span className="banking-platform-item-des">
                  {ready && t('digitalBankingLandingPage.bankingPlatform.itemDes1')}
                </span>
              </li>
              <li className="banking-platform-item">
                <img
                  src={iconCheck}
                  alt="item-icon-banking-platform"
                  className="banking-platform-item-icon"
                />
                <span className="banking-platform-item-des">
                  {ready && t('digitalBankingLandingPage.bankingPlatform.itemDes2')}
                </span>
              </li>
              <li className="banking-platform-item">
                <img
                  src={iconCheck}
                  alt="item-icon-banking-platform"
                  className="banking-platform-item-icon"
                />
                <span className="banking-platform-item-des">
                  {ready && t('digitalBankingLandingPage.bankingPlatform.itemDes3')}
                </span>
              </li>
            </ul>
          </Col>
          <Col xs={12} md={6} lg={6} className="banking-platform-col-right">
            <Row className="">
              <Col md={12} lg={6}>
                <div className="banking-platform-card card-1">
                  <div className="banking-platform-card-title">
                    <img
                      src={iconStandard}
                      alt="icon-name"
                      className="banking-platform-card-title-icon"
                    />
                    <h3 className="banking-platform-card-title-sub m-0">
                      {ready && t('digitalBankingLandingPage.bankingPlatform.cardTitle1')}
                    </h3>
                  </div>
                  <p className="banking-platform-card-des">
                    {ready && t('digitalBankingLandingPage.bankingPlatform.cardDes1')}
                  </p>
                </div>
                <div className="banking-platform-card card-2">
                  <div className="banking-platform-card-title">
                    <img
                      src={iconDiversity}
                      alt="icon-name"
                      className="banking-platform-card-title-icon"
                    />
                    <h3 className="banking-platform-card-title-sub m-0">
                      {ready && t('digitalBankingLandingPage.bankingPlatform.cardTitle2')}
                    </h3>
                  </div>
                  <p className="banking-platform-card-des">
                    {ready && t('digitalBankingLandingPage.bankingPlatform.cardDes2')}
                  </p>
                </div>
              </Col>
              <Col md={12} lg={6}>
                <div className="banking-platform-card card-3">
                  <div className="banking-platform-card-title">
                    <img
                      src={iconIntegration}
                      alt="icon-name"
                      className="banking-platform-card-title-icon"
                    />
                    <h3 className="banking-platform-card-title-sub m-0">
                      {ready && t('digitalBankingLandingPage.bankingPlatform.cardTitle3')}
                    </h3>
                  </div>
                  <p className="banking-platform-card-des">
                    {ready && t('digitalBankingLandingPage.bankingPlatform.cardDes3')}
                  </p>
                </div>
                <div className="banking-platform-card card-4">
                  <div className="banking-platform-card-title">
                    <img
                      src={iconProfessional}
                      alt="icon-name"
                      className="banking-platform-card-title-icon"
                    />
                    <h3 className="banking-platform-card-title-sub m-0">
                      {ready && t('digitalBankingLandingPage.bankingPlatform.cardTitle4')}
                    </h3>
                  </div>
                  <p className="banking-platform-card-des">
                    {ready && t('digitalBankingLandingPage.bankingPlatform.cardDes4')}
                  </p>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
      <img src={bgHalf2} alt="bg-dots-circle-half" className="banking-platform-img" />
    </div>
  );
}

export default RowHighlights;
