import React from 'react';
import { Row, Col, Image, Container } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Autoplay } from 'swiper';
import { useTranslation } from 'react-i18next';
import logoSunshine from '/assets/image/homePage/logo-sunshine-home-main.svg';
import logoSmartConstruction from '/assets/image/homePage/logo-smart-construction-main.svg';
import logoKSFinance from '/assets/image/homePage/logo-ks-finance-main.svg';
import logoMediaPartner from '/assets/image/homePage/logo-media-partner-main.svg';
import logoKienLongBank from '/assets/image/homePage/logo-kienlong-bank.svg';
import imgUnderLine from '/assets/image/svg/title-line-under.svg';

import '../../../home/Homepage.scss';

function RowPartnership() {
  const { t, ready } = useTranslation();

  return (
    <div className="back-ground-partnership-digital">
      <Container className="max-width-1180 padding-left-right-partnership">
        <h1 className="title-banking-software">
          {ready && t('digitalBankingLandingPage.partnership.title')}
        </h1>
        <div className="d-flex justify-content-center">
          <Image src={imgUnderLine} style={{ width: 224, height: 6 }} />
        </div>
        <p className="description-partnership-digital">
          {ready && t('digitalBankingLandingPage.partnership.des')}
        </p>
        <Row className="row-slide-partnership">
          <Col
            xs={{ span: 6, order: 2 }}
            lg={{ span: 1, order: 1 }}
            className="col-arrow-left-partnership mt-lg-0 mt-4 
            justify-content-lg-center justify-content-end align-items-center pr-0"
          >
            <button
              type="button"
              //   btn-arrow-left btn-arrow-partnership pre-slide-partnership
              className="btn-arrow-left-digital-banking pre-slide-partnership"
            />
          </Col>
          <Col xs={{ span: 12 }} lg={{ span: 10, order: 2 }} className="px-0 col-slide-partnership">
            <Swiper
              modules={[Navigation, Autoplay]}
              spaceBetween={16}
              slidesPerView={4}
              navigation={{
                prevEl: '.pre-slide-partnership',
                nextEl: '.next-slide-partnership',
              }}
              className="swiper-partnership-digital"
              //   autoplay={{ delay: 3000 }}
              breakpoints={{
                320: {
                  slidesPerView: 1.65,
                  spaceBetween: 16,
                },
                550: {
                  slidesPerView: 2,
                },
                661: {
                  slidesPerView: 3,
                },
                992: {
                  slidesPerView: 4,
                },
              }}
            >
              <SwiperSlide>
                <a
                  href="https://ssh.vn/en/"
                  rel="noreferrer"
                  target="_blank"
                  className="a-link-logo"
                >
                  <div className="div-logo-partnership">
                    <Image src={logoSunshine} className="logo-slide-partnership img-fluid" />
                  </div>
                  <div className="overplay-logo-partnership partnert-digital">
                    <div className="text-overplay-logo">
                      {ready && t('digitalBankingLandingPage.partnership.item1')}
                    </div>
                  </div>
                </a>
              </SwiperSlide>
              <SwiperSlide>
                <a href="https://scgr.vn/" rel="noreferrer" target="_blank" className="a-link-logo">
                  <div className="div-logo-partnership">
                    <Image
                      src={logoSmartConstruction}
                      className="logo-slide-partnership img-fluid"
                    />
                  </div>
                  <div className="overplay-logo-partnership partnert-digital">
                    <div className="text-overplay-logo">
                      {ready && t('digitalBankingLandingPage.partnership.item2')}
                    </div>
                  </div>
                </a>
              </SwiperSlide>
              <SwiperSlide>
                <a
                  href="https://ksfinance.vn/"
                  rel="noreferrer"
                  target="_blank"
                  className="a-link-logo"
                >
                  <div className="div-logo-partnership">
                    <Image src={logoKSFinance} className="logo-slide-partnership img-fluid" />
                  </div>
                  <div className="overplay-logo-partnership partnert-digital">
                    <div className="text-overplay-logo">
                      {ready && t('digitalBankingLandingPage.partnership.item3')}
                    </div>
                  </div>
                </a>
              </SwiperSlide>
              <SwiperSlide>
                <a href="https://ode.vn/" rel="noreferrer" target="_blank" className="a-link-logo">
                  <div className="div-logo-partnership">
                    <Image src={logoMediaPartner} className="logo-slide-partnership img-fluid" />
                  </div>
                  <div className="overplay-logo-partnership partnert-digital">
                    <div className="text-overplay-logo">
                      {ready && t('digitalBankingLandingPage.partnership.item4')}
                    </div>
                  </div>
                </a>
              </SwiperSlide>
              <SwiperSlide>
                <a
                  href="https://kienlongbank.com/"
                  target="_blank"
                  rel="noreferrer"
                  className="a-link-logo"
                >
                  <div className="div-logo-partnership">
                    <Image src={logoKienLongBank} className="logo-slide-partnership img-fluid" />
                  </div>
                  <div className="overplay-logo-partnership partnert-digital">
                    <div className="text-overplay-logo">
                      {ready && t('digitalBankingLandingPage.partnership.item5')}
                    </div>
                  </div>
                </a>
              </SwiperSlide>
            </Swiper>
          </Col>
          <Col
            xs={{ span: 6, order: 2 }}
            lg={{ span: 1, order: 3 }}
            className="col-arrow-right-partnership mt-lg-0 mt-4 align-items-center 
            justify-content-lg-center justify-content-start align-items-center"
          >
            <button
              type="button"
              //   btn-arrow-right btn-arrow-partnership next-slide-partnership
              className="btn-arrow-right-digital-banking next-slide-partnership"
            />
          </Col>
          <Col className="col-mobile-btn">
            <button
              type="button"
              className="btn-arrow-left btn-arrow-partnership-mobile pre-slide-partnership"
            />
            <button
              type="button"
              className="btn-arrow-right btn-arrow-partnership next-slide-partnership"
            />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default RowPartnership;
