import React from 'react';
import { Col, Container, Image, Nav, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import imgUnderLine from '/assets/image/svg/title-line-under.svg';
import iconAppBank from '/assets/image/svg/icon-app-bank.svg';
import iconSolutionATM from '/assets/image/svg/icon-solution-ATM.svg';
import iconDeviceBank from '/assets/image/svg/icon-device-bank.svg';
import iconArrowRightBlue from '/assets/image/svg/arrow-right.svg';

function RowOurSolution() {
  const { t, ready } = useTranslation();

  const handleClickScroll = (id: string) => {
    const Element = document.getElementById(id);
    Element?.scrollIntoView({ behavior: 'smooth' });
    // setAcviteEcosystem(id);
  };
  return (
    <div className="back-ground-our-solution padding-left-right">
      <Container className="max-width-1300 px-0">
        <div className="our-solution-title--hidden">
          <h1 className="title-banking-software">
            {ready && t('digitalBankingLandingPage.ourSolution.title')}
          </h1>
          <div className="d-flex justify-content-center">
            <Image src={imgUnderLine} style={{ width: 224, height: 6 }} />
          </div>
          <p className="text-center our-solution-des">
            {ready && t('digitalBankingLandingPage.ourSolution.des')}
          </p>
        </div>
        <Row className="gx-4 gy-4 our-solution-row--mobile">
          <Col md={12} lg={4}>
            <div className="solution-item-card">
              <img src={iconAppBank} alt="" className="solution-item-card-image" />
              <h3 className="solution-item-card-title">
                {ready && t('digitalBankingLandingPage.ourSolution.cardTitle1')}
              </h3>
              <div className="solution-item-card-content">
                <div>
                  <p className="solution-item-card-des">
                    {ready && t('digitalBankingLandingPage.ourSolution.cardDes1')}
                  </p>
                </div>
                <div className="solution-item-card-bottom">
                  <Nav.Link
                    // bsPrefix="custom-nav-link custom-link-digital"
                    onClick={() => handleClickScroll('digital-banking-software')}
                  >
                    <a
                      href="/#"
                      target="_blank"
                      rel="noreferrer"
                      className="solution-item-card-nav"
                    >
                      <span className="solution-item-card-view-detail">
                        {ready && t('digitalBankingLandingPage.ourSolution.viewDetail')}
                      </span>
                      <span>
                        <img src={iconArrowRightBlue} alt="" />
                      </span>
                    </a>
                  </Nav.Link>
                </div>
              </div>
            </div>
          </Col>
          <Col md={12} lg={4}>
            <div className="solution-item-card">
              <img src={iconSolutionATM} alt="" className="solution-item-card-image" />
              <h3 className="solution-item-card-title">
                {ready && t('digitalBankingLandingPage.ourSolution.cardTitle2')}
              </h3>
              <div className="solution-item-card-content">
                <div>
                  <p className="solution-item-card-des">
                    {ready && t('digitalBankingLandingPage.ourSolution.cardDes2')}
                  </p>
                </div>
                <div className="solution-item-card-bottom">
                  <Nav.Link
                    // bsPrefix="custom-nav-link custom-link-digital"
                    onClick={() => handleClickScroll('back-ground-solution-digital')}
                  >
                    <a
                      href="/#"
                      target="_blank"
                      rel="noreferrer"
                      className="solution-item-card-nav"
                    >
                      <span className="solution-item-card-view-detail">
                        {ready && t('digitalBankingLandingPage.ourSolution.viewDetail')}
                      </span>
                      <span>
                        <img src={iconArrowRightBlue} alt="" />
                      </span>
                    </a>
                  </Nav.Link>
                </div>
              </div>
            </div>
          </Col>
          <Col md={12} lg={4}>
            <div className="solution-item-card">
              <img src={iconDeviceBank} alt="" className="solution-item-card-image" />
              <h3 className="solution-item-card-title">
                {ready && t('digitalBankingLandingPage.ourSolution.cardTitle3')}
              </h3>
              <div className="solution-item-card-content">
                <div>
                  <p className="solution-item-card-des">
                    {ready && t('digitalBankingLandingPage.ourSolution.cardDes3')}
                  </p>
                </div>
                <div className="solution-item-card-bottom">
                  <Nav.Link
                    // bsPrefix="custom-nav-link custom-link-digital"
                    onClick={() => handleClickScroll('digital-banking-equipment')}
                  >
                    <a
                      href="/#"
                      target="_blank"
                      rel="noreferrer"
                      className="solution-item-card-nav"
                    >
                      <span className="solution-item-card-view-detail">
                        {ready && t('digitalBankingLandingPage.ourSolution.viewDetail')}
                      </span>
                      <span>
                        <img src={iconArrowRightBlue} alt="" />
                      </span>
                    </a>
                  </Nav.Link>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default RowOurSolution;
