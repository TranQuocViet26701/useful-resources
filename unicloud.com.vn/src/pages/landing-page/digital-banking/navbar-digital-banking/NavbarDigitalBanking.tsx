import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Container, Dropdown, Image, Nav, Navbar, Offcanvas } from 'react-bootstrap';
import { NavLink, useLocation } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

import useWindowSize from '@src/hooks/useWindowsSize';
import ModalAdvise from '../../../../components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';

import logoWhite from '/assets/image/svg/logo-white.svg';
import logoScroll from '/assets/image/svg/logo-scroll.svg';
import iconPhone from '/assets/image/svg/icon-phone.svg';

import '../../../../layouts/navbar/navbar.scss';
import './NavbarDigitalBanking.scss';

const NavbarChild = React.memo(
  (props: {
    id: string;
    children: JSX.Element | JSX.Element[];
    isMobile: boolean;
    show: boolean;
    onShow: () => void;
    onHide: () => void;
  }) => {
    const { id, isMobile, show, onShow, onHide } = props;

    if (!isMobile) {
      return <Navbar.Collapse id={id}>{props.children}</Navbar.Collapse>;
    }

    const location = useLocation();

    useEffect(() => {
      onHide();
    }, [location]);

    return (
      <Navbar.Offcanvas show={show} onShow={onShow} onHide={onHide} id={id}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title id="offcanvasNavbarLabel">
            <Navbar.Brand href="/">
              <img
                src={logoScroll}
                height="64"
                className="d-inline-block align-top"
                alt="Unicloud Logo"
              />
            </Navbar.Brand>
          </Offcanvas.Title>
        </Offcanvas.Header>
        {props.children}
      </Navbar.Offcanvas>
    );
  },
);

const CustomNav = React.memo(() => {
  const size = useWindowSize();
  const [show, setShow] = useState(false);
  const onShow = () => setShow(true);

  const onHide = () => setShow(false);

  const [logoNav, setLogoNav] = useState(logoWhite);

  const [stickyClass, setStickyClass] = useState('background-transparent');
  const isMobile = useMemo(() => {
    if (size.width && size.width >= 992) {
      return false;
    }
    return true;
  }, [size.width]);

  const location = useLocation().pathname;
  // xử lý sticky menu
  const stickNavbar = useCallback(() => {
    if (window !== undefined) {
      const windowHeightScroll = window.scrollY;
      if (windowHeightScroll > 20) {
        setLogoNav(logoScroll);
        setStickyClass('background-sticky box-shadow-sticky-on');
      } else {
        setLogoNav(logoWhite);
        setStickyClass('background-transparent');
      }
    }
  }, [location]);

  useEffect(() => {
    window.addEventListener('scroll', stickNavbar);
    return () => {
      window.removeEventListener('scroll', stickNavbar);
    };
  }, [location]);

  //   const [acviteEcosystem, setAcviteEcosystem] = useState('');

  const handleClickScroll = (id: string) => {
    const Element = document.getElementById(id);
    Element?.scrollIntoView({ behavior: 'smooth' });
    // setAcviteEcosystem(id);
  };

  const { t, ready, i18n } = useTranslation();

  const changeLanguage = useCallback((lng: any) => {
    i18n.changeLanguage(lng);
  }, []);

  const languageElement = useMemo(() => {
    const { languages, language } = i18n;
    const sortedLanguages = languages && [...languages];

    const flagPath = (locale: string) => `/flag-${locale}.svg`;
    return (
      <Dropdown className={`d-flex ${!isMobile ? '' : 'flex-column'}`} onSelect={changeLanguage}>
        <Dropdown.Toggle
          variant="none"
          id="dropdown-basic"
          className="box-shadow-unset btn-search-home btn-lang ms-auto"
        >
          {language?.toUpperCase()} &nbsp;&nbsp;&nbsp;
          <Image src={flagPath(language)} alt="icon flag" />
        </Dropdown.Toggle>
        <Dropdown.Menu align="end">
          {sortedLanguages
            ?.sort((a: string, b: string) => b.localeCompare(a))
            .map((locale) => {
              const fixedT = i18next.getFixedT(locale, 'translation', 'navbar.navLink');
              return (
                <Dropdown.Item eventKey={locale} key={locale} active={locale === language}>
                  <Image src={flagPath(locale)} alt="" />
                  <span className="text-dropdown-lang">
                    {ready && fixedT(language === 'en' ? 'languageEn' : 'languageVi').toString()}
                  </span>
                </Dropdown.Item>
              );
            })}
        </Dropdown.Menu>
      </Dropdown>
    );
  }, [isMobile, changeLanguage, i18n.language, ready]);

  const [modalShow, setModalShow] = React.useState(false);
  return (
    <Navbar
      as="header"
      role="banner"
      expand="lg"
      className={stickyClass}
      fixed="top"
      collapseOnSelect
      id="nav-bar-id"
    >
      <Container className="max-width-1600 menu-desktop">
        <Navbar.Brand href="/">
          <img
            src={logoNav}
            height="64"
            className="logo-menu d-inline-block align-top"
            alt="Unicloud Logo"
            id="logo-menu"
          />
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="offcanvasNavbar"
          bsPrefix="custom-navbar-toggle"
          onClick={onShow}
        />
        <NavbarChild
          id="offcanvasNavbar"
          isMobile={isMobile}
          show={show}
          onShow={onShow}
          onHide={onHide}
        >
          <Offcanvas.Body style={{ flexGrow: 1, overflowY: 'visible' }} className="navbar-bizzone">
            <Nav className="mx-auto" navbarScroll key="nav-item">
              {ready && (
                <>
                  <Nav.Link as={NavLink} to="/" bsPrefix="custom-nav-link custom-link-digital">
                    {ready && t('digitalBankingLandingPage.navLink.abuotUs')}
                  </Nav.Link>
                  <Nav.Link
                    bsPrefix="custom-nav-link custom-link-digital"
                    onClick={() => handleClickScroll('digital-banking-software')}
                  >
                    {ready && t('digitalBankingLandingPage.navLink.bankingSoftware')}
                  </Nav.Link>
                  <Nav.Link
                    bsPrefix="custom-nav-link custom-link-digital"
                    onClick={() => handleClickScroll('digital-banking-equipment')}
                  >
                    {ready && t('digitalBankingLandingPage.navLink.bankingEquipment')}
                  </Nav.Link>
                  <Nav.Link
                    bsPrefix="custom-nav-link custom-link-digital"
                    onClick={() => handleClickScroll('back-ground-contact-us')}
                  >
                    {ready && t('digitalBankingLandingPage.navLink.contactUs')}
                  </Nav.Link>
                </>
              )}
            </Nav>
            <Nav>
              <Nav.Link bsPrefix="custom-nav-link custom-nav-link-btn">
                <button
                  type="button"
                  className="btn-advise-digital-menu"
                  style={{ marginRight: 12 }}
                  onClick={() => setModalShow(true)}
                >
                  {ready && (
                    <span>{ready && t('digitalBankingLandingPage.navLink.getAdvice')}</span>
                  )}
                </button>
              </Nav.Link>
              <Nav.Link bsPrefix="custom-nav-link-btn" href="tel:19006054">
                <button type="button" className="btn-hotline-digital">
                  <Image src={iconPhone} className="img-fluid " style={{ marginRight: 12 }} />
                  1900 6054
                </button>
              </Nav.Link>
              <Nav.Link bsPrefix="custom-nav-link custom-nav-link-btn">{languageElement}</Nav.Link>
            </Nav>
          </Offcanvas.Body>
        </NavbarChild>
      </Container>
      <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
    </Navbar>
  );
});

export default CustomNav;
