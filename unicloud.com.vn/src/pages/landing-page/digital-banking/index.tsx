import React, { useEffect } from 'react';
import { Container } from 'react-bootstrap';
import NavbarDigitalBanking from './navbar-digital-banking/NavbarDigitalBanking';
import RowTopSlide from './main/RowTopSlide';
import RowBankingSoftware from './main/RowBankingSoftware';
import RowSolutionStmAtmKios from './main/RowSolutionSTM-ATM-KIOS';
import RowSmartTellerMachine from './main/RowSmartTellerMachine';
import RowUniCATForSTM from './main/RowUniCATForSTM';
import RowUniCATForATM from './main/RowUniCATForATM';
import RowUniService from './main/RowUniService';
import RowBankingEquipment from './main/RowBankingEquipment';
import RowPartnershipDigital from './main/RowPartnershipDigital';
import RowBankingERP from './main/RowBankingERP';
import RowContactUs from './main/RowContactUs';
import Footer from '../../../layouts/footer/Footer';
import SEO from '@src/seo/seo';
import { useLocation } from 'react-router-dom';
import RowOurSolution from './main/RowOurSolution';
import RowBankingPlatform from './main/RowBankingPlatform';
import RowDownloadBrochure from './main/RowDownloadBrochure';
import RowHelpUs from './main/RowHelpUs';
import imgSlide1 from '/assets/image/landing-page/digital-banking/img-slide-digital-banking-1.png';
import './DigitalBanking.scss';

function DigitalBanking() {
  useEffect(() => {
    if (typeof window !== undefined) {
      const pathname = window.location.href;
      const final = pathname.substring(pathname.indexOf('neobank#') + 8, pathname.length);
      const handleClickScroll = () => {
        const Element = document.getElementById(final);
        Element?.scrollIntoView({ behavior: 'smooth' });
      };
      setTimeout(handleClickScroll, 700);
    }
  }, []);

  const location = useLocation();

  return (
    <main>
      <SEO
        title="Giải pháp phần mềm ngân hàng số"
        description="Gói Giải Pháp Giao Dịch Ngân Hàng Trực Tuyến,
        Tận Dụng Sức Mạnh Của Thiết Bị Di Động Và
         Các Giải Pháp Tiên Tiến Của Các Nhà Sản Xuất Lớn."
        imgthumbs={`https://unicloud.com.vn${imgSlide1}`}
        url={location.pathname}
      />
      <Container className="max-width-100 px-0" fluid>
        <NavbarDigitalBanking />
        <RowTopSlide />
        <RowBankingPlatform />
        <RowOurSolution />
        <RowBankingSoftware />
        <RowBankingERP />
        <RowSolutionStmAtmKios />
        <RowSmartTellerMachine />
        <RowUniCATForSTM />
        <RowUniCATForATM />
        <RowUniService />
        <RowBankingEquipment />
        <RowDownloadBrochure />
        <RowPartnershipDigital />
        <RowContactUs />
        <RowHelpUs />
        <Footer />
      </Container>
    </main>
  );
}
export default DigitalBanking;
