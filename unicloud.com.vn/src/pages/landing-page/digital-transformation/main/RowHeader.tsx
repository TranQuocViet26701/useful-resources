/* eslint-disable max-len */
import React, { useMemo, useState } from 'react';
import { Swiper, SwiperSlide, useSwiper } from 'swiper/react';

import { Autoplay, FreeMode, Thumbs } from 'swiper';

import imgHeader1 from '/assets/image/landing-page/digital-transformation/img-header-digital-transformation-1.png';
import imgHeader2 from '/assets/image/landing-page/digital-transformation/img-header-digital-transformation-2.png';
import imgHeader3 from '/assets/image/landing-page/digital-transformation/img-header-digital-transformation-3.png';
import imgHeader4 from '/assets/image/landing-page/digital-transformation/img-header-digital-transformation-4.png';
import imgHeaderMobile1 from '/assets/image/landing-page/digital-transformation/img-header-digital-transformation-mobile-1.png';
import imgHeaderMobile2 from '/assets/image/landing-page/digital-transformation/img-header-digital-transformation-mobile-2.png';
import imgHeaderMobile3 from '/assets/image/landing-page/digital-transformation/img-header-digital-transformation-mobile-3.png';
import imgHeaderMobile4 from '/assets/image/landing-page/digital-transformation/img-header-digital-transformation-mobile-4.png';
import ModalAdviseDigitalBanking from '@src/components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import useWindowSize from '@src/hooks/useWindowsSize';

interface RowButtonProps {
  handleSetActiveIndex: (id: number) => void;
  activeIndex: number;
}

function RowButton({ handleSetActiveIndex, activeIndex }: RowButtonProps) {
  const swiper = useSwiper();

  const size = useWindowSize();

  const isMobile = useMemo(() => {
    if (size.width && size.width > 428) {
      return false;
    }
    return true;
  }, [size.width]);

  const listItem = [
    {
      id: 0,
      title1: 'Digital Banking Platform',
      title2: 'Chuyển đổi số',
      title3: 'ngành Tài chính - Ngân hàng',
      subTitle:
        'Các tổ chức tài chính và ngân hàng có thể áp dụng các công nghệ chuyển đổi số để tạo ra giá trị cho nhiều sản phẩm và dịch vụ phù hợp với nhu cầu của khách hàng. Thông qua việc tăng cường tính minh bạch - bảo mật, truy vấn - phân tích dữ liệu nhanh chóng và cung cấp trải nghiệm cá nhân hoá liền mạch cho người dùng.',
      img: isMobile ? imgHeaderMobile1 : imgHeader1,
    },
    {
      id: 1,
      title1: 'Bizzone Shop',
      title2: 'Chuyển đổi số ngành bán lẻ',
      title3: '',
      subTitle:
        'Tăng cường trải nghiệm mua sắm đa kênh đến khách hàng, tối ưu quy trình bán hàng, quản lý tồn kho liền mạch, chấm công, tính lương hay thanh toán nhanh hơn,... là những lợi ích thiết thực nhất từ giải pháp chuyển đổi số Bizzone Shop mang lại cho doanh nghiệp kinh doanh bán lẻ.',
      img: isMobile ? imgHeaderMobile2 : imgHeader2,
    },
    {
      id: 2,
      title1: 'Bizzone HR',
      title2: 'Chuyển đổi số',
      title3: 'ngành quản trị nhân sự',
      subTitle:
        'Chuyển đổi số với Bizzone HR giúp gia tăng năng suất lao động, tạo sự minh bạch trong nhân viên thông qua việc tối ưu hoá quy trình vận hành và quản lý nhân sự. Tiết kiệm được nhiều chi phí và thời gian trong công tác tuyển dụng, chấm công - tính lương, đào tạo - phát triển đội ngũ. ',
      img: isMobile ? imgHeaderMobile3 : imgHeader3,
    },
    {
      id: 3,
      title1: 'Bizzone homes',
      title2: 'ứng dụng quản lý cư dân',
      title3: '',
      subTitle:
        'Ứng dụng Bizzone Homes giúp giúp cư dân kết nối với ban quản lý tòa nhà/ chung cư mọi lúc mọi nơi, góp phần nâng cao chất lượng cuộc sống. Giải pháp còn giúp tối ưu khả năng quản lý, vận hành dành cho ban quản lý thông qua hệ thống xử lý dữ liệu chuyên nghiệp, tính bảo mật cao, giúp tiết kiệm thời gian và quản lý thu - chi hiệu quả.',
      img: isMobile ? imgHeaderMobile4 : imgHeader4,
    },
  ];

  const handleOnClick = (id: number) => {
    handleSetActiveIndex(id);
    swiper.slideTo(id);
  };

  return (
    <div className="wrapper-swiper-slide-tag">
      <div className="container-swiper-slide-tag max-width-1180">
        {listItem.map((item) => (
          <div key={item.id} className="wrapper-btn-slide">
            <button
              type="button"
              className={`btn-slide ${activeIndex === item.id ? 'is-active' : ''}`}
              onClick={() => handleOnClick(item.id)}
            >
              <h5 className="title-slide-digital-transformation">{`0${item.id + 1}`}</h5>
              <h5 className="title-slide-digital-transformation">
                {`${item.title2} ${item.title3}`}
              </h5>
            </button>
            <div className="dash-btn-slide">{}</div>
          </div>
        ))}
      </div>
    </div>
  );
}

function RowHeader() {
  const [activeIndex, setActiveIndex] = useState<number>(0);

  const [modalShow, setModalShow] = React.useState(false);

  const size = useWindowSize();

  const isMobile = useMemo(() => {
    if (size.width && size.width > 428) {
      return false;
    }
    return true;
  }, [size.width]);

  const listItem = [
    {
      id: 0,
      title1: 'Digital Banking Platform',
      title2: 'Chuyển đổi số',
      title3: 'ngành Tài chính - Ngân hàng',
      subTitle:
        'Các tổ chức tài chính và ngân hàng có thể áp dụng các công nghệ chuyển đổi số để tạo ra giá trị cho nhiều sản phẩm và dịch vụ phù hợp với nhu cầu của khách hàng. Thông qua việc tăng cường tính minh bạch - bảo mật, truy vấn - phân tích dữ liệu nhanh chóng và cung cấp trải nghiệm cá nhân hoá liền mạch cho người dùng.',
      img: isMobile ? imgHeaderMobile1 : imgHeader1,
    },
    {
      id: 1,
      title1: 'Bizzone Shop',
      title2: 'Chuyển đổi số ngành bán lẻ',
      title3: '',
      subTitle:
        'Tăng cường trải nghiệm mua sắm đa kênh đến khách hàng, tối ưu quy trình bán hàng, quản lý tồn kho liền mạch, chấm công, tính lương hay thanh toán nhanh hơn,... là những lợi ích thiết thực nhất từ giải pháp chuyển đổi số Bizzone Shop mang lại cho doanh nghiệp kinh doanh bán lẻ.',
      img: isMobile ? imgHeaderMobile2 : imgHeader2,
    },
    {
      id: 2,
      title1: 'Bizzone HR',
      title2: 'Chuyển đổi số',
      title3: 'ngành quản trị nhân sự',
      subTitle:
        'Chuyển đổi số với Bizzone HR giúp gia tăng năng suất lao động, tạo sự minh bạch trong nhân viên thông qua việc tối ưu hoá quy trình vận hành và quản lý nhân sự. Tiết kiệm được nhiều chi phí và thời gian trong công tác tuyển dụng, chấm công - tính lương, đào tạo - phát triển đội ngũ. ',
      img: isMobile ? imgHeaderMobile3 : imgHeader3,
    },
    {
      id: 3,
      title1: 'Bizzone homes',
      title2: 'ứng dụng quản lý cư dân',
      title3: '',
      subTitle:
        'Ứng dụng Bizzone Homes giúp giúp cư dân kết nối với ban quản lý tòa nhà/ chung cư mọi lúc mọi nơi, góp phần nâng cao chất lượng cuộc sống. Giải pháp còn giúp tối ưu khả năng quản lý, vận hành dành cho ban quản lý thông qua hệ thống xử lý dữ liệu chuyên nghiệp, tính bảo mật cao, giúp tiết kiệm thời gian và quản lý thu - chi hiệu quả.',
      img: isMobile ? imgHeaderMobile4 : imgHeader4,
    },
  ];

  const handleSetActiveIndex = (id: number) => {
    setActiveIndex(id);
  };

  const handleOnClick = (index: any) => {
    setActiveIndex(index);
  };

  return (
    <section className="page-header-digital-transformation">
      <Swiper
        modules={[FreeMode, Autoplay, Thumbs]}
        autoplay={{ delay: 5000 }}
        className="swiper-digital-transformation"
        onSlideChange={(swiper) => handleOnClick(swiper.realIndex)}
      >
        {listItem.map((item) => (
          <SwiperSlide key={item.id}>
            <div
              className="img-slide-digital-transformation"
              style={{
                backgroundImage: `url(${item.img})`,
              }}
            >
              <div className="wrapper-header-digital-transformation">
                <div className="max-width-1180" style={{ width: '100%' }}>
                  <h4 className="title-header">{item.title1}</h4>
                  <h2 className="title-header">{item.title2}</h2>
                  <h2 className="title-header">{item.title3}</h2>
                  <p className="sub-title-header">{item.subTitle}</p>
                  <button
                    className="btn-advise-digital-transformation"
                    type="button"
                    onClick={() => setModalShow(true)}
                  >
                    <span>Nhận tư vấn</span>
                  </button>
                </div>
              </div>
            </div>
          </SwiperSlide>
        ))}
        <RowButton handleSetActiveIndex={handleSetActiveIndex} activeIndex={activeIndex} />
      </Swiper>
      <ModalAdviseDigitalBanking show={modalShow} onHide={() => setModalShow(false)} />
    </section>
  );
}

export default RowHeader;
