/* eslint-disable max-len */
import * as React from 'react';
import { Image } from 'react-bootstrap';

import imgIcon1 from '/assets/image/landing-page/digital-transformation/svg/icon-retail-1.svg';
import imgIcon2 from '/assets/image/landing-page/digital-transformation/svg/icon-retail-2.svg';
import imgIcon3 from '/assets/image/landing-page/digital-transformation/svg/icon-retail-3.svg';
import imgIcon4 from '/assets/image/landing-page/digital-transformation/svg/icon-retail-4.svg';

import imgRetail from '/assets/image/landing-page/digital-transformation/img-retail-digital-transformation.png';
import ModalAdviseDigitalBanking from '@src/components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';

const listItem = [
  {
    id: 1,
    img: imgIcon1,
    title: 'Giải pháp',
    subTitle:
      'Quản lý tồn kho, quản lý nhân viên và tối ưu hoạt động bán hàng đa kênh. Tăng cường trải nghiệm khách hàng và đem đến doanh thu vượt trội cho doanh nghiệp của bạn. ',
  },
  {
    id: 2,
    img: imgIcon2,
    title: 'Lợi ích',
    subTitle:
      'Bizzone Shop hỗ trợ bán hàng đa nền tảng thông minh, giúp doanh nghiệp tiết kiệm được nhiều chi phí, thời gian trong khâu lập kế hoạch, vận hành và đo lường hiệu suất bán hàng.',
  },
  {
    id: 3,
    img: imgIcon3,
    title: 'Tính năng',
    subTitle:
      'Hỗ trợ quản lý kho, tối ưu quy trình vận hành; chấm công - tính lương cho nhân viên; thiết lập các chương trình khuyến mãi - giảm giá; hỗ trợ chăm sóc khách hàng.',
  },
  {
    id: 4,
    img: imgIcon4,
    title: 'Ứng dụng',
    subTitle:
      'Quản lý hoạt động kinh doanh bán lẻ một cách thông minh; tối ưu quy trình phục vụ - thu ngân - bar/bếp ngành F&B; hỗ trợ các dịch vụ du lịch khách sạn, chăm sóc sức khỏe, làm đẹp,... ',
  },
];

export default function RowRetailTransformation() {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <section id="retail-transformation">
      <div className="wrapper-retail-digital-transformation">
        <div className="max-width-1180 padding-left-right" style={{ width: '100%' }}>
          <div className="wrapper-title-digital-transformation">
            <h2 className="title-digital-transformation">Chuyển đổi số ngành bán lẻ</h2>
            <h3 className="title-digital-transformation">Bizzone Shop </h3>
            <p className="sub-title-digital-transformation">
              Giải pháp quản lý bán hàng thông minh được ứng dụng công nghệ hiện đại nhằm tự động
              hóa quy trình vận hành, tiết kiệm chi phí - nhân lực và cải thiện doanh thu bán lẻ
              hiệu quả.
            </p>
          </div>
          <div className="row-retail-digital-transformation">
            <div className="col-retail-digital-transformation-1">
              <ul className="list-card-item">
                {listItem.map((item) => (
                  <li key={item.id} className="card-item">
                    <Image src={item.img} className="img-fluid img-card-item" />
                    <h5 className="title-ecosystem-bizzone">{item.title}</h5>
                    <p className="sub-title-ecosystem-bizzone">{item.subTitle}</p>
                  </li>
                ))}
              </ul>
            </div>
            <div className="col-retail-digital-transformation-2">
              <Image src={imgRetail} className="img-fluid" />
              <div className="row-btn-digital-transformation">
                <button
                  className="btn-advise-digital-transformation-2"
                  type="button"
                  onClick={() => setModalShow(true)}
                >
                  <span>Nhận tư vấn</span>
                </button>
                <a href="https://bizzone.vn/usee">
                  <button
                    className="btn-more-info-digital-transformation"
                    type="button"
                  >
                    <span>Tìm hiểu thêm</span>
                  </button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ModalAdviseDigitalBanking show={modalShow} onHide={() => setModalShow(false)} />
    </section>
  );
}
