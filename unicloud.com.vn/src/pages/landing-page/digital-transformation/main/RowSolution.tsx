/* eslint-disable max-len */
import * as React from 'react';
import { Image } from 'react-bootstrap';

import imgSolution from '/assets/image/landing-page/digital-transformation/img-solution.png';
import iconArrow from '/assets/image/landing-page/digital-transformation/svg/icon-arrow-down.svg';

const listItem = [
  {
    id: '01',
    link: 'finance-bank',
    title: 'Tài chính ngân hàng',
    subTitle:
      'Tăng cường bảo mật, bảo vệ dữ liệu khách hàng và cho phép các ngân hàng phục vụ chính xác những gì khách hàng cần.',
  },
  {
    id: '02',
    link: 'retail-transformation',
    title: 'Bán lẻ',
    subTitle:
      'Nâng cao tính cạnh tranh cho doanh nghiệp bán lẻ trong việc tối ưu chi phí, thời gian và tăng cường hiệu quả vận hành kinh doanh.',
  },
  {
    id: '03',
    link: 'hr-management',
    title: 'Quản trị nhân sự',
    subTitle:
      'Truy cập dữ liệu nhân sự dễ dàng, xây dựng quy trình quản lý tự động, giúp cải thiện hiệu suất nhân viên và tiết kiệm chi phí cho doanh nghiệp. ',
  },
  {
    id: '04',
    link: 'resident-management',
    title: 'Quản lý cư dân thông minh',
    subTitle:
      'Giải pháp quản lý cư dân thông minh giúp kết nối hộ gia đình với ban quản lý và các dịch vụ bên ngoài thuận tiện, đem đến trải nghiệm tiện nghi và hiện đại.',
  },
];

function BuildListItem() {
  const handleClickScroll = (id: string) => {
    const Element = document.getElementById(id);
    Element?.scrollIntoView({ behavior: 'smooth' });
  };
  return (
    <ul className="list-card-item">
      {listItem.map((item) => (
        <li key={item.id} className="card-item">
          <h1 className="title-solution">{item.id}</h1>
          <div className="content-solution">
            <h4 className="title-content-solution">{item.title}</h4>
            <p className="sub-ttile-content-solution">{item.subTitle}</p>
            <button
              type="button"
              className="link-solution"
              onClick={() => handleClickScroll(item.link)}
            >
              Xem chi tiết
              <Image src={iconArrow} className="img-fluid" style={{ marginLeft: '6px' }} />
            </button>
          </div>
        </li>
      ))}
    </ul>
  );
}

export default function RowSolution() {
  return (
    <section id="solution">
      <div className="wrapper-solution-digital-transformation">
        <div className="max-width-1180 padding-left-right" style={{ width: '100%' }}>
          <div className="wrapper-title-digital-transformation">
            <h2 className="title-digital-transformation">Giải pháp của chúng tôi</h2>
            <p className="sub-title-digital-transformation">
              Cung cấp các giải pháp chuyển đổi số trong từng ngành, lĩnh vực với công nghệ mới hiện
              đại, giúp doanh nghiệp tối ưu quy trình doanh, nắm bắt khách hàng và gia tăng doanh số
              bán hàng.
            </p>
          </div>
          <div className="row-solution">
            <div className="col-solution">
              <Image src={imgSolution} className="img-fluid img-solution" />
            </div>
            <div className="col-solution">
              <BuildListItem />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
