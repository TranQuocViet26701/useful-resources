import React, { useMemo, useEffect, useState } from 'react';
import { Image } from 'react-bootstrap';
import useWindowSize from '@src/hooks/useWindowsSize';
import iconScrollEcosystem from '/assets/image/svg/icon-ul-li-scroll-ecosystem.svg';
import iconNotActive from '/assets/image/svg/icon-top-not-active-scroll-ecosystem.svg';
import iconActive from '/assets/image/svg/icon-top-active-scroll-ecosystem.svg';

function RowScrollEcosystem() {
  const size = useWindowSize();
  const isMobile = useMemo(() => {
    if (size.width && size.width >= 992) {
      return false;
    }
    return true;
  }, [size.width]);

  const [toggleEcosystem, setToggleEcosystem] = useState(false);
  const [acviteEcosystem, setAcviteEcosystem] = useState('');

  useEffect(() => {
    const func = (e: MouseEvent) => {
      const divOverplay = document.getElementById('overlay');
      const ulScrollEcosystem = document.getElementById('ul-scroll-ecosystem');
      const divScrollEcosystem = document.getElementById('div-scroll-ecosystem');

      const element = e as MouseEvent;
      const target = element.target as HTMLElement;
      if (target.id === 'overlay' && divScrollEcosystem && divOverplay && ulScrollEcosystem) {
        divOverplay.style.display = 'none';
        if (toggleEcosystem) {
          divScrollEcosystem.style.borderBottomLeftRadius = '40px';
          divScrollEcosystem.style.width = 'auto';
          ulScrollEcosystem.style.display = 'none';
          setToggleEcosystem(!toggleEcosystem);
        }
      }
    };

    const funcScroll = () => {
      const divScrollEcosystem = document.getElementById('div-scroll-ecosystem');
      const digitalBanking = document.getElementById('finance-bank');
      const digitalTransformation = document.getElementById('retail-transformation');
      const smartCity = document.getElementById('hr-management');
      const virtualReality = document.getElementById('resident-management');
      const windowHeightScroll = window.scrollY;
      const { innerHeight } = window;

      if (
        window !== undefined &&
        digitalBanking &&
        digitalTransformation &&
        smartCity &&
        virtualReality &&
        divScrollEcosystem
      ) {
        const maxHeightShowBtnEcosystem =
          digitalBanking.offsetHeight +
          digitalTransformation.offsetHeight +
          smartCity.offsetHeight +
          virtualReality.offsetHeight +
          innerHeight;
        if (
          windowHeightScroll + 90 >= innerHeight &&
          windowHeightScroll <= maxHeightShowBtnEcosystem
        )
          divScrollEcosystem.style.display = 'block';
        else divScrollEcosystem.style.display = 'none';
      }
    };
    window.addEventListener('click', func);
    window.addEventListener('scroll', funcScroll);
    return () => {
      window.removeEventListener('click', func);
      window.removeEventListener('scroll', funcScroll);
    };
  }, [toggleEcosystem]);

  const handleClickEcosystem = () => {
    const divOverplay = document.getElementById('overlay');
    const ulScrollEcosystem = document.getElementById('ul-scroll-ecosystem');
    const divScrollEcosystem = document.getElementById('div-scroll-ecosystem');
    if (ulScrollEcosystem && divOverplay && divScrollEcosystem) {
      if (!toggleEcosystem) {
        divOverplay.style.display = 'block';
        divScrollEcosystem.style.borderBottomLeftRadius = '8px';
        divScrollEcosystem.style.width = '96%';
        // divScrollEcosystem.style.animation = 'my-move 0.4s';
        ulScrollEcosystem.style.display = 'block';
      } else {
        divOverplay.style.display = 'none';
        divScrollEcosystem.style.borderBottomLeftRadius = '40px';
        divScrollEcosystem.style.width = 'auto';
        // divScrollEcosystem.style.animation = 'unset';
        ulScrollEcosystem.style.display = 'none';
      }
      setToggleEcosystem(!toggleEcosystem);
    }
  };
  const handleClickScroll = (id: string) => {
    const Element = document.getElementById(id);
    Element?.scrollIntoView({ behavior: 'smooth' });
    const divOverplay = document.getElementById('overlay');
    const ulScrollEcosystem = document.getElementById('ul-scroll-ecosystem');
    const divScrollEcosystem = document.getElementById('div-scroll-ecosystem');
    if (divOverplay && ulScrollEcosystem && divScrollEcosystem) {
      divOverplay.style.display = 'none';
      divScrollEcosystem.style.borderBottomLeftRadius = '40px';
      divScrollEcosystem.style.width = 'auto';
      ulScrollEcosystem.style.display = 'none';
      setToggleEcosystem(false);
    }
    setAcviteEcosystem(id);
  };

  if (!isMobile) return null;

  return (
    <div className="div-scroll-ecosystem" id="div-scroll-ecosystem">
      <div className="div-btn-ecosystem">
        <button type="button" className="btn-ecosystem-unactive" onClick={handleClickEcosystem}>
          <Image src={toggleEcosystem ? iconActive : iconNotActive} />
          <span>Chuyển đổi số</span>
        </button>
      </div>
      <ul className="ul-scroll-ecosystem" id="ul-scroll-ecosystem">
        <li>
          <button
            onClick={() => handleClickScroll('finance-bank')}
            className={`btn-scrolly-ecosystem ${
              acviteEcosystem === 'finance-bank' ? 'active-ecosystem' : ''
            }`}
            type="button"
          >
            <Image src={iconScrollEcosystem} />
            <span>Tài chính ngân hàng</span>
          </button>
        </li>
        <li>
          <button
            onClick={() => handleClickScroll('retail-transformation')}
            className={`btn-scrolly-ecosystem ${
              acviteEcosystem === 'retail-transformation' ? 'active-ecosystem' : ''
            }`}
            type="button"
          >
            <Image src={iconScrollEcosystem} />
            <span>Bán lẻ</span>
          </button>
        </li>
        <li>
          <button
            onClick={() => handleClickScroll('hr-management')}
            className={`btn-scrolly-ecosystem ${
              acviteEcosystem === 'hr-management' ? 'active-ecosystem' : ''
            }`}
            type="button"
          >
            <Image src={iconScrollEcosystem} />
            <span>Quản trị nhân sự</span>
          </button>
        </li>
        <li>
          <button
            onClick={() => handleClickScroll('resident-management')}
            className={`btn-scrolly-ecosystem ${
              acviteEcosystem === 'resident-management' ? 'active-ecosystem' : ''
            }`}
            type="button"
          >
            <Image src={iconScrollEcosystem} />
            <span>Quản lý cư dân thông minh</span>
          </button>
        </li>
      </ul>
    </div>
  );
}
export default RowScrollEcosystem;
