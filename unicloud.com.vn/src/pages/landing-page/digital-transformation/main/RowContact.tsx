import React from 'react';
import axios from 'axios';
import { Col, Form, FormControl, Image, InputGroup, Row } from 'react-bootstrap';
import { Controller, useForm } from 'react-hook-form';
import { useLocation } from 'react-router-dom';

import imgContact from '/assets/image/landing-page/digital-transformation/img-contact.png';
import iconBuilding from '/assets/image/landing-page/digital-transformation/svg/icon-building-contact.svg';
import iconChat from '/assets/image/landing-page/digital-transformation/svg/icon-chat-contact.svg';
import iconMail from '/assets/image/landing-page/digital-transformation/svg/icon-mail-contact.svg';
import iconPhone from '/assets/image/landing-page/digital-transformation/svg/icon-phone-contact.svg';
import iconUser from '/assets/image/landing-page/digital-transformation/svg/icon-user-contact.svg';

/**
 * Get first error from contact form else return ''
 */
function getFirstError(errors: any) {
  const arrObject = Object.values(errors);

  const arrMessage = arrObject.map((o: any) => o.message);

  return arrMessage.length === 0 ? '' : arrMessage[0];
}

const googleSheetAPI =
  'https://script.google.com/macros/s/AKfycbxTlQD1WgGBuFKoKVjf6tiUGERX6DHHhxJfywGZ6R4xuNEurMdCdW4fbRoZwBW4jK6M/exec';

export default function RowContact() {
  const location = useLocation();

  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm();

  const errorText = getFirstError(errors);

  const onSubmit = (data: any) => {
    const formData = new FormData();
    formData.append('name', data.fullName);
    formData.append('phone', data.telephone);
    formData.append('email', data.email);
    formData.append('content', `Công ty : ${data.business}`);
    formData.append('boolean', 'true');
    formData.append('list', '6xdCd892x7gSZoG7768926aeLA');
    formData.append('subform', 'yes');

    const googleSheetFormData = new FormData();
    googleSheetFormData.append('fullName', data.fullName);
    googleSheetFormData.append('email', data.email);
    googleSheetFormData.append('telephone', data.telephone);
    googleSheetFormData.append('company', data.business);
    googleSheetFormData.append('content', '');
    googleSheetFormData.append('timestamp', new Date().toLocaleDateString().substring(0, 10));
    googleSheetFormData.append('linkedBy', location.pathname);

    axios
      .post('/subscribe', formData)
      .then(() => {
        // console.log('response: ', response.data);
        // console.log('response.status: ', response.status);
        // console.log('response.data: ', response.data);
      })
      .catch(() => {
        // console.error('Something went wrong!', error);
      });

    axios
      .post(googleSheetAPI, googleSheetFormData)
      .then(() => {
        // console.log('response: ', response.data);
        // console.log('response.status: ', response.status);
        // console.log('response: ', response.data);
        //
      })
      .catch(() => {
        // console.error('Something went wrong!', error);
        // alert('Lỗi hệ thống.');
      });
    // console.log('data', formData);
    reset();
  };
  return (
    <section id="contact">
      <div className="wrapper-contact-digital-transformation">
        <div className="max-width-1180 padding-left-right" style={{ width: '100%' }}>
          <div className="row-contact-digital-transformation">
            <div className="col-content-1">
              <Image src={imgContact} className="img-fluid img-content" />
            </div>
            <div className="col-content-2">
              <div className="wrapper-form-contact-digital-transformation">
                <div className="wrapper-title-digital-transformation">
                  <h2 className="title-digital-transformation">LIÊN HỆ VỚI CHÚNG TÔI</h2>
                  <p className="sub-title-digital-transformation">
                    Sẵn sàng tư vấn và giải đáp thắc mắc của bạn vào tất cả các ngày trong tuần.
                  </p>
                </div>
                {errorText && <div className="row-alert-contact-form">{`*${errorText}*`}</div>}
                <Form onSubmit={handleSubmit(onSubmit)} onReset={reset} className="form-contact">
                  <Row>
                    <Col md={6}>
                      <Form.Group controlId="validationCustomFullName">
                        <Form.Label>
                          Họ và Tên <span style={{ color: 'red' }}>*</span>
                        </Form.Label>
                        <InputGroup className="mb-3">
                          <img src={iconUser} alt="icon-name" className="icon-input-text" />
                          <Controller
                            control={control}
                            name="fullName"
                            defaultValue=""
                            rules={{ required: 'Vui lòng nhập tên' }}
                            render={({ field: { onChange, value, ref } }) => (
                              <FormControl
                                onChange={onChange}
                                value={value}
                                ref={ref}
                                placeholder="Nhập tên của bạn"
                                aria-label="fullName"
                                aria-describedby="fullName"
                                autoComplete="off"
                              />
                            )}
                          />
                        </InputGroup>
                      </Form.Group>
                    </Col>
                    <Col md={6}>
                      <Form.Group controlId="validationCustomTelephone">
                        <Form.Label>
                          Số điện thoại <span style={{ color: 'red' }}>*</span>
                        </Form.Label>
                        <InputGroup className="mb-3">
                          <img src={iconPhone} alt="icon-telephone" className="icon-input-text" />
                          <Controller
                            control={control}
                            name="telephone"
                            defaultValue=""
                            rules={{
                              required: 'Vui lòng nhập số điện thoại',
                              pattern: {
                                value:
                                  /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/,
                                message: 'Số điện thoại không đúng',
                              },
                            }}
                            render={({ field: { onChange, value, ref } }) => (
                              <FormControl
                                onChange={onChange}
                                value={value}
                                ref={ref}
                                placeholder="Nhập số điện thoại của bạn"
                                aria-label="telephone"
                                aria-describedby="telephone"
                                autoComplete="off"
                              />
                            )}
                          />
                        </InputGroup>
                      </Form.Group>
                    </Col>
                  </Row>
                  <Form.Group controlId="validationCustomEmail">
                    <Form.Label>
                      Email <span style={{ color: 'red' }}>*</span>
                    </Form.Label>
                    <InputGroup className="mb-3">
                      <img src={iconMail} alt="icon-email" className="icon-input-text" />
                      <Controller
                        control={control}
                        name="email"
                        defaultValue=""
                        rules={{
                          required: 'Vui lòng nhập email',
                          pattern: {
                            value: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                            message: 'Vui lòng nhập đúng email',
                          },
                        }}
                        render={({ field: { onChange, value, ref } }) => (
                          <FormControl
                            onChange={onChange}
                            value={value}
                            ref={ref}
                            placeholder="Nhập email của bạn"
                            aria-label="email"
                            aria-describedby="email"
                            autoComplete="off"
                          />
                        )}
                      />
                    </InputGroup>
                  </Form.Group>
                  <Form.Group controlId="validationCustomBusiness">
                    <Form.Label>
                      Tên công ty <span style={{ color: 'red' }}>*</span>
                    </Form.Label>
                    <InputGroup className="mb-3">
                      <img src={iconBuilding} alt="icon-email" className="icon-input-text" />
                      <Controller
                        control={control}
                        name="business"
                        defaultValue=""
                        rules={{
                          required: 'Vui lòng nhập tên công ty',
                        }}
                        render={({ field: { onChange, value, ref } }) => (
                          <FormControl
                            onChange={onChange}
                            value={value}
                            ref={ref}
                            placeholder="Nhập tên công ty của bạn"
                            aria-label="business"
                            aria-describedby="business"
                            autoComplete="off"
                          />
                        )}
                      />
                    </InputGroup>
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="validationCustomTextarea">
                    <Form.Label>Nội dung</Form.Label>
                    <InputGroup>
                      <img src={iconChat} alt="icon-email" className="icon-input-text" />
                      <Controller
                        control={control}
                        name="content"
                        defaultValue=""
                        render={({ field: { onChange, value, ref } }) => (
                          <Form.Control
                            onChange={onChange}
                            value={value}
                            ref={ref}
                            as="textarea"
                            rows={5}
                            placeholder="Nhập nội dung của bạn"
                            autoComplete="off"
                          />
                        )}
                      />
                    </InputGroup>
                  </Form.Group>
                  <div className="d-flex justify-content-center">
                    <button className="btn-submit-contact" type="submit">
                      <span>Tư vấn ngay</span>
                    </button>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
