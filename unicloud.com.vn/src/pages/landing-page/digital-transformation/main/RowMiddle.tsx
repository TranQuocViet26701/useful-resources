/* eslint-disable max-len */
import ModalAdviseDigitalBanking from '@src/components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import * as React from 'react';
import { Image } from 'react-bootstrap';
import SwiperCore, { Autoplay, Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

import img1 from '/assets/image/landing-page/digital-transformation/img-bizzone-hr.png';
import img3 from '/assets/image/landing-page/digital-transformation/img-middle-3.png';
import img2 from '/assets/image/landing-page/digital-transformation/img-unioffice.png';
import iconCheck from '/assets/image/landing-page/digital-transformation/svg/icon-check.svg';
import imgArrowLeft from '/assets/image/landing-page/digital-transformation/img-arrow-middle-left.png';
import imgArrowRight from '/assets/image/landing-page/digital-transformation/img-arrow-middle-right.png';

SwiperCore.use([Navigation]);

function RowBizzoneHr() {
  const listSlide = [
    {
      id: 1,
      img: img1,
      title1: 'Bizzone HR',
      title2: 'Hệ thống quản lí nhân sự',
      subTitle:
        'Tự động hoá các quy trình thủ công trong khâu quản lý nhân sự, giúp doanh nghiệp đề ra chiến lược quản lý nguồn nhân lực hiệu quả thông qua các phân tích nâng cao và số liệu báo cáo theo thời gian thực.',
      features: [
        'Quản lý nhân sự',
        'Tuyển dụng',
        'Bảo hiểm xã hội',
        'Lương thuế',
        'Chấm công',
        'Đào tạo, đánh giá - xếp loại',
      ],
    },
    {
      id: 2,
      img: img2,
      title1: 'UniOffice',
      title2: 'Ứng dụng quản lý chấm công',
      subTitle:
        'Chấm công nhanh chóng với giải pháp xác thực bằng khuôn mặt, tích hợp công nghệ eKYC xác minh danh tính nhân viên chính xác. UniOffice giúp quản lý bảng công, thời gian nghỉ phép của nhân viên hiệu quả.',
      features: [
        'Quản lý ca làm việc',
        'Hồ sơ nhân viên',
        'Phê duyệt giải trình công',
        'Quản lý thẻ xe',
        'Bảng công nhân viên',
        'Quản lý danh bạ',
      ],
    },
  ];

  const [modalShow, setModalShow] = React.useState(false);

  return (
    <section className="wrapper-hr-digital-transformation" id="hr-management">
      <div className="max-width-1180 padding-left-right" style={{ width: '100%' }}>
        <div className="wrapper-title-digital-transformation">
          <h2 className="title-digital-transformation">Chuyển đổi số ngành quản trị nhân sự</h2>
          <p className="sub-title-digital-transformation">
            Tự động hoá các quy trình nhân sự như: tuyển dụng, chấm công - tính lương, thuế - BHXH,
            nâng cao chất lượng quản lý nhân viên với quy trình đào tạo, đánh giá - xếp loại thông
            minh và minh bạch.
          </p>
        </div>
        <div className="container-hr-swiper">
          <button className="btn-arrow-left prev-slide-hr" type="button" />
          <button className="btn-arrow-right next-slide-hr" type="button" />
          <Swiper
            modules={[Autoplay, Navigation]}
            navigation={{
              prevEl: '.prev-slide-hr',
              nextEl: '.next-slide-hr',
            }}
            className="swiper-hr-digital-transformation"
            slidesPerView={1}
            spaceBetween={30}
            autoplay={{ delay: 5000 }}
          >
            {listSlide.map((slide) => (
              <SwiperSlide key={slide.id}>
                <div className="wrapper-content-middle-digital-transformation">
                  <div className="container-image-mobile">
                    <Image src={slide.img} className="img-fluid img-content-mobile" />
                  </div>
                  <div className="container-content-middle-digital-transformation flex-reverse">
                    <div className="col-content-1">
                      <h3 className="title-content">{slide.title1}</h3>
                      <h4 className="title-content">{slide.title2}</h4>
                      <p className="sub-title-content">{slide.subTitle}</p>
                      <ul className="ul-content">
                        {slide.features.map((f) => (
                          <li key={f} className="li-content">
                            <Image src={iconCheck} className="img-fluid" />
                            {f}
                          </li>
                        ))}
                      </ul>
                      <div className="row-btn-digital-transformation">
                        <button
                          className="btn-advise-digital-transformation-2"
                          type="button"
                          onClick={() => setModalShow(true)}
                        >
                          <span>Nhận tư vấn</span>
                        </button>
                        <a href="https://bizzone.vn/bizzone-hr">
                          <button
                            className="btn-more-info-digital-transformation"
                            type="button"
                          >
                            <span>Tìm hiểu thêm</span>
                          </button>
                        </a>
                      </div>
                    </div>
                    <div className="col-content-2">
                      <Image src={slide.img} className="img-fluid img-content" />
                    </div>
                  </div>
                </div>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
      <ModalAdviseDigitalBanking show={modalShow} onHide={() => setModalShow(false)} />
    </section>
  );
}

function RowBizzoneHome() {
  const content = {
    id: 3,
    img: img3,
    title1: 'Bizzone Home',
    title2: 'Ứng dụng quản lí cư dân',
    subTitle:
      'Bizzone Homes là giải pháp thông minh giúp kết nối cư dân với ban quản lý căn hộ/ chung cư để nhận thông báo, xử lý vấn đề hiệu quả. Phần mềm giúp tối ưu hoạt động quản lý cư dân với nhiều tính năng hiện đại, hỗ trợ báo cáo thu - chi nhanh chóng, chính xác. Ngoài ra, ứng dụng Bizzone Homes còn giúp thanh toán và đăng ký dịch vụ nội khu dễ dàng, nâng cao trải nghiệm sống cho cư dân.',
    features: [
      'Quản lý cư dân',
      'Quản lý công việc',
      'Quản lý công nợ',
      'Tích hợp thanh toán',
      'Quản lý nhân viên',
      'Quản lý đăng ký dịch vụ',
    ],
  };
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <section className="wrapper-hr-digital-transformation" id="resident-management">
      <Image src={imgArrowLeft} className="img-fluid img-bg-left" />
      <Image src={imgArrowRight} className="img-fluid img-bg-right" />
      <div className="max-width-1180 padding-left-right" style={{ width: '100%' }}>
        <div className="wrapper-title-digital-transformation" style={{ marginBottom: '40px' }}>
          <h2 className="title-digital-transformation">Quản lý cư dân thông minh</h2>
          <p className="sub-title-digital-transformation">
            Bizzone Homes cung cấp giải pháp tối ưu trong hoạt động quản lý cư dân, quản lý thông
            báo và tăng cường sự tiện ích trong khâu thanh toán dịch vụ.
          </p>
        </div>
        <div className="wrapper-content-middle-digital-transformation">
          <div className="container-content-middle-digital-transformation">
            <div className="col-content-1">
              <h3 className="title-content">{content.title1}</h3>
              <h4 className="title-content">{content.title2}</h4>
              <p className="sub-title-content">{content.subTitle}</p>
              <ul className="ul-content">
                {content.features.map((f, index) => {
                  const id = index + 1;
                  return (
                    <li key={id} className="li-content">
                      <Image src={iconCheck} className="img-fluid" />
                      {f}
                    </li>
                  );
                })}
              </ul>
              <div className="row-btn-digital-transformation">
                <button
                  className="btn-advise-digital-transformation-2"
                  type="button"
                  onClick={() => setModalShow(true)}
                >
                  <span>Nhận tư vấn</span>
                </button>
                <button
                  className="btn-more-info-digital-transformation"
                  type="button"
                >
                  <span>Tìm hiểu thêm</span>
                </button>
              </div>
            </div>
            <div className="col-content-2">
              <Image src={img3} className="img-fluid img-content" />
            </div>
          </div>
        </div>
      </div>
      <ModalAdviseDigitalBanking show={modalShow} onHide={() => setModalShow(false)} />
    </section>
  );
}

export default function RowMiddle() {
  return (
    <div className="wrapper-middle-digital-transformation">
      <RowBizzoneHr />
      <RowBizzoneHome />
    </div>
  );
}
