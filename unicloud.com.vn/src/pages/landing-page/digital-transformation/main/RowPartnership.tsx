import React from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import { Autoplay, Navigation, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
// import { useTranslation } from 'react-i18next';
import logoKienLongBank from '/assets/image/homePage/logo-kienlong-bank.svg';
import logoKSFinance from '/assets/image/homePage/logo-ks-finance-main.svg';
import logoMediaPartner from '/assets/image/homePage/logo-media-partner-main.svg';
import logoSmartConstruction from '/assets/image/homePage/logo-smart-construction-main.svg';
import logoSunshine from '/assets/image/homePage/logo-sunshine-home-main.svg';

import iconLeft from '/assets/image/landing-page/digital-transformation/svg/icon-leaf-left.svg';
import iconRight from '/assets/image/landing-page/digital-transformation/svg/icon-leaf-right.svg';

function RowPartnership() {
  // const { t } = useTranslation();

  return (
    <section className="wrapper-partnership-digital-transformation" id="partnership">
      <Container className="max-width-1180 padding-left-right">
        <div className="wrapper-title-digital-transformation">
          <h2 className="title-digital-transformation">KHÁCH HÀNG CỦA CHÚNG TÔI</h2>
          <p className="sub-title-digital-transformation">
            Với mục tiêu chung là mang lại nhiều giá trị hơn đồng thời giảm thiểu rủi ro và chi phí
            cho khách hàng, Unicloud Group và các đối tác chiến lược của chúng tôi hợp lực để mang
            lại lợi thế hàng đầu nhờ hiểu biết kinh doanh và đổi mới công nghệ.
          </p>
        </div>
        <Row className="row-slide-partnership">
          <Col
            xs={{ span: 6, order: 2 }}
            lg={{ span: 1, order: 1 }}
            className="col-arrow-left-partnership"
          >
            <Image src={iconLeft} className="img-fluid" />
          </Col>
          <Col xs={{ span: 12 }} lg={{ span: 10, order: 2 }} className="px-0 col-slide-partnership">
            <Swiper
              modules={[Navigation, Autoplay, Pagination]}
              pagination={{ clickable: true }}
              spaceBetween={16}
              slidesPerView={4}
              className="swiper-partnership-digital-transformation"
              autoplay={{ delay: 3000 }}
              breakpoints={{
                320: {
                  slidesPerView: 1.65,
                  spaceBetween: 16,
                },
                550: {
                  slidesPerView: 2,
                },
                661: {
                  slidesPerView: 3,
                },
                992: {
                  slidesPerView: 4,
                },
              }}
            >
              <SwiperSlide>
                <a href="https://ssh.vn" rel="noreferrer" target="_blank" className="a-link-logo">
                  <div className="div-logo-partnership-digital-transformation">
                    <Image src={logoSunshine} className="logo-slide-partnership img-fluid" alt="" />
                  </div>
                </a>
              </SwiperSlide>
              <SwiperSlide>
                <a href="https://scgr.vn/" rel="noreferrer" target="_blank" className="a-link-logo">
                  <div className="div-logo-partnership-digital-transformation">
                    <Image
                      src={logoSmartConstruction}
                      className="logo-slide-partnership img-fluid"
                      alt=""
                    />
                  </div>
                </a>
              </SwiperSlide>
              <SwiperSlide>
                <a
                  href="https://ksfinance.vn/"
                  rel="noreferrer"
                  target="_blank"
                  className="a-link-logo"
                >
                  <div className="div-logo-partnership-digital-transformation">
                    <Image
                      src={logoKSFinance}
                      className="logo-slide-partnership img-fluid"
                      alt=""
                    />
                  </div>
                </a>
              </SwiperSlide>
              <SwiperSlide>
                <a href="https://ode.vn/" rel="noreferrer" target="_blank" className="a-link-logo">
                  <div className="div-logo-partnership-digital-transformation">
                    <Image
                      src={logoMediaPartner}
                      className="logo-slide-partnership img-fluid"
                      alt=""
                    />
                  </div>
                </a>
              </SwiperSlide>
              <SwiperSlide>
                <a
                  href="https://kienlongbank.com/"
                  target="_blank"
                  rel="noreferrer"
                  className="a-link-logo"
                >
                  <div className="div-logo-partnership-digital-transformation">
                    <Image
                      src={logoKienLongBank}
                      className="logo-slide-partnership img-fluid"
                      alt=""
                    />
                  </div>
                </a>
              </SwiperSlide>
            </Swiper>
          </Col>
          <Col
            xs={{ span: 6, order: 2 }}
            lg={{ span: 1, order: 3 }}
            className="col-arrow-right-partnership"
          >
            <Image src={iconRight} className="img-fluid" />
          </Col>
        </Row>
      </Container>
    </section>
  );
}
export default RowPartnership;
