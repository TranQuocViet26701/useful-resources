/* eslint-disable max-len */
import ModalAdviseDigitalBanking from '@src/components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import React, { useState } from 'react';
import { Accordion, Image } from 'react-bootstrap';

import img1 from '/assets/image/landing-page/digital-transformation/img-finance-bank-1.png';
import img2 from '/assets/image/landing-page/digital-transformation/img-finance-bank-2.png';
import img3 from '/assets/image/landing-page/digital-transformation/img-finance-bank-3.png';
import iconItem from '/assets/image/landing-page/digital-transformation/svg/icon-item.svg';

const serviceList = [
  {
    id: 1,
    title1: 'UnicloudCA',
    title2: 'Dịch Vụ Chứng Thực Chữ Ký Số',
    subTitle:
      'UnicloudCA giúp khách hàng cá nhân lẫn doanh nghiệp thực hiện thao tác ký số nhanh chóng, đảm bảo an toàn trong giao dịch và thanh toán theo tiêu chuẩn về ký số eIDAS của Châu Âu.',
    img: img1,
    benefits: [
      'Chứng thực chữ ký số đảm bảo tính xác thực, tính toàn vẹn của dữ liệu và tính chống chối bỏ trong các giao dịch điện tử.',
      'Tiết kiệm nhiều chi phí và thời gian trong các giao dịch thanh toán, thư điện tử, hợp đồng điện tử đối với cá nhân và tổ chức, doanh nghiệp.',
      'Giải pháp tăng cường bảo mật thông tin và góp phần nâng cao uy tín của doanh nghiệp thông qua dịch vụ ký số tốt nhất.',
    ],
    features: [
      'Không dùng USB Token/Smart Card: Không cần dùng các thiết bị phần cứng chuyên dụng, không cần cài đặt thêm môi trường mà vẫn có thể ký số từ xa tiện lợi.',
      'Bảo mật theo tiêu chuẩn eIDAS: Tuân thủ tiêu chuẩn cao nhất về sự an toàn, tin cậy, bảo mật trong giao dịch điện tử của Châu Âu (eIDAS).',
      'Lưu trữ cặp khóa và chứng thư số trên HSM Cloud: Tất cả các khóa được quản lý tập trung trong HSM Cloud giúp bảo mật và xuất dữ liệu nhanh chóng.',
    ],
    applications: [
      'Ứng dụng thương mại điện tử: giao dịch thanh toán, giao dịch thư điện tử, mua hàng trực tuyến, hợp đồng điện tử, đầu tư chứng khoán trực tuyến,...',
      'Chính phủ điện tử: khai báo hải quan qua điện tử, thuế điện tử, hệ thống hồ sơ, các loại giấy tờ và chứng chỉ, chứng từ liên quan,...',
      'Giao dịch ngân hàng điện tử: các biểu mẫu đăng ký, thông tin người dùng, thay đổi dịch vụ nội bộ,... ngăn chặn khả năng làm giả tài liệu và giả mạo chữ ký.',
    ],
  },
  {
    id: 2,
    title1: 'UniCloud Invoice',
    title2: 'Dịch Vụ Hóa Đơn Điện Tử',
    img: img2,
    subTitle:
      'Quản lý các nghiệp vụ hoá đơn từ cơ bản đến nâng cao cách chuyên nghiệp hơn, loại bỏ các   hạn chế của hoá đơn giấy và giúp doanh nghiệp tiết kiệm được nhiều chi phí với Unicloud Invoice.',
    benefits: [
      'Nhanh chóng & tiết kiệm: rút ngắn thời gian phát hành hoá đơn, tiết kiệm chi phí khởi tạo, quản lý, lưu trữ và truy xuất hoá đơn nhanh chóng.',
      'An toàn & bảo mật: Tăng cường tính bảo mật cao cho hoá đơn điện tử, tránh tình trạng làm giả, giảm rủi ro về việc thất lạc, hư hỏng hay mất hoá đơn.',
      'Dễ dàng tra cứu và đối chiếu: giúp khách hàng dễ tra cứu và đối chiếu hoá đơn điện tử khi mua hàng, tăng cường sự uy tín cho người bán, doanh nghiệp.',
    ],
    features: [
      'Quản lý hoá đơn chuyên nghiệp: quản lý phê duyệt, xác thực cùng lúc nhiều hoá đơn và quản trị tình hình sử dụng hóa đơn của công ty hiệu quả.',
      'Tích hợp linh hoạt: kết nối dễ dàng với các phần mềm SAP, ERP, CRM, phần mềm kế toán và các phần mềm khác của doanh nghiệp hoặc nhà cung cấp khác.',
      'Đơn giản hoá việc lập và phát hành hoá đơn: theo ký hiệu mẫu số, hóa đơn mới  và cơ chế cấp số hoá đơn mới theo quy định pháp luật.',
    ],
    applications: [
      'Khối ngân hàng: thay thế hoá đơn giấy trong công tác lập, sản xuất, quản lý và lưu trữ hóa đơn với khối lượng hóa đơn lớn và phức tạp.',
      'Bảo hiểm: rút ngắn các quy trình lập và phát hành hoá đơn liên quan đến khoản thanh toán phí bảo hiểm.',
      'Các dịch vụ tài chính khác: doanh nghiệp bán và mua chứng khoán, cổ phiếu công ty, các quỹ đầu tư, quản lý tài sản,...có thể áp dụng hoá đơn điện tử.',
    ],
  },
  {
    id: 3,
    title1: 'eKYC On-Premise Server',
    title2: 'Giải pháp định danh điện tử',
    subTitle:
      'eKYC ngày càng được ứng dụng rộng rãi trong các hoạt động tài chính ngân hàng nhằm xác thực thông tin khách hàng nhanh chóng và tăng cường tính bảo mật cao cho dữ liệu.',
    img: img3,
    benefits: [
      'Tăng cường trải nghiệm khách hàng: tối ưu được quy trình và các thủ tục ngân hàng, đẩy nhanh quá trình thực hiện giao dịch mà không cần điền vào nhiều biểu mẫu',
      'Bảo mật thông tin khách hàng cao: tự động lưu trữ dữ liệu hoạt động giao dịch và tăng cường bảo mật cao, tránh tình trạng đánh cắp thông tin và giảo mạo.',
      'Tiết kiệm chi phí: eKYC giúp tiết kiệm được nhiều chi phí trong quá trình vận hành, đồng thời cũng giúp nâng cao hiệu suất làm việc của ngân hàng.',
    ],
    features: [
      'Công nghệ OCR - nhận dạng ký tự quang học giúp đẩy nhanh quá trình trích xuất thông tin từ tài liệu nhận dạng mà khách hàng cung cấp với độ chính xác cao.',
      'Công nghệ Face Matching mang lại tính chính xác cao nhờ khả năng phân tích và so sánh ảnh/video khuôn mặt thật với ảnh chụp chân dung trên giấy tờ.',
      'Công nghệ Liveness Detection giúp ngăn chặn đánh cắp thông tin người dùng, xác định thời gian thực diễn ra là của người dùng thật chứ không phải giả mạo',
    ],
    applications: [
      'Công nghệ Liveness Detection giúp ngăn chặn đánh cắp thông tin người dùng, xác định thời gian thực diễn ra là của người dùng thật chứ không phải giả mạo',
      'Ứng dụng giải pháp eKYC để mở tài khoản chứng khoán và giao dịch chứng khoán nhanh chóng.',
      'Mở tài khoản đăng ký và mua gói bảo hiểm, mở tài khoản thanh toán thông qua công nghệ eKYC',
    ],
  },
];

interface BuildButtonListProps {
  service: number;
  handleSetService: (id: number) => void;
}

function BuildButtonList({ service, handleSetService }: BuildButtonListProps) {
  const handleServiceClick = (id: number) => {
    handleSetService(id);
  };

  return (
    <div className="wrapper-btn-finance-bank-digital-transformation">
      <div className="row-btn-finance-bank-digital-transformation">
        <div className={`div-btn-background active-${service}`}>{}</div>
        {serviceList.map((s) => (
          <button
            key={s.id}
            type="button"
            className={`btn-finance-bank ${service === s.id ? 'btn-active' : ''}`}
            onClick={() => handleServiceClick(s.id)}
          >
            <span>{s.title1}</span>
          </button>
        ))}
      </div>
    </div>
  );
}

interface BuildContentProps {
  service: number;
}

function BuildContent({ service }: BuildContentProps) {
  const foundService = serviceList.find((s) => s.id === service);

  const [modalShow, setModalShow] = useState(false);

  return (
    <div className="container-content-finance-bank-digital-transformation">
      <div className="wrapper-title-content">
        <h2 className="title-content">{foundService?.title1}</h2>
        <h3 className="title-content">{foundService?.title2}</h3>
        <p className="sub-title-content">{foundService?.subTitle}</p>
      </div>
      <div className="row-content">
        <div className="col-content-1">
          <Image src={foundService?.img} className="img-fluid" />
        </div>
        <div className="col-content-2">
          <Accordion defaultActiveKey="2" flush>
            <Accordion.Item key="benefits" eventKey="0">
              <Accordion.Header>Lợi ích</Accordion.Header>
              <Accordion.Body>
                <ul className="accordion-list-item">
                  {foundService?.benefits.map((value) => (
                    <li key={value} className="accordion-li">
                      <Image src={iconItem} className="img-fluid img-item" />
                      <span className="accordion-desc">{value}</span>
                    </li>
                  ))}
                </ul>
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item key="features" eventKey="1">
              <Accordion.Header>Tính năng</Accordion.Header>
              <Accordion.Body>
                <ul className="accordion-list-item">
                  {foundService?.features.map((value) => (
                    <li key={value} className="accordion-li">
                      <Image src={iconItem} className="img-fluid img-item" />
                      <span className="accordion-desc">{value}</span>
                    </li>
                  ))}
                </ul>
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item key="applications" eventKey="2">
              <Accordion.Header>Ứng dụng</Accordion.Header>
              <Accordion.Body>
                <ul className="accordion-list-item">
                  {foundService?.applications.map((value) => (
                    <li key={value} className="accordion-li">
                      <Image src={iconItem} className="img-fluid img-item" />
                      <span className="accordion-desc">{value}</span>
                    </li>
                  ))}
                </ul>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
          <div className="row-btn-digital-transformation">
            <button
              className="btn-advise-digital-transformation-2"
              type="button"
              onClick={() => setModalShow(true)}
            >
              <span>Nhận tư vấn</span>
            </button>
            <button
              className="btn-more-info-digital-transformation"
              type="button"
            >
              <span>Tìm hiểu thêm</span>
            </button>
          </div>
        </div>
      </div>
      <ModalAdviseDigitalBanking show={modalShow} onHide={() => setModalShow(false)} />
    </div>
  );
}

export default function RowFinanceBank() {
  const [service, setService] = useState(1);

  const handleSetService = (id: number) => {
    setService(id);
  };

  return (
    <section id="finance-bank">
      <div className="wrapper-finance-bank-digital-transformation">
        <div className="max-width-1180 padding-left-right" style={{ width: '100%' }}>
          <div className="wrapper-title-digital-transformation">
            <h2 className="title-digital-transformation">
              CHUYỂN ĐỔI SỐ NGÀNH TÀI CHÍNH - NGÂN HÀNG
            </h2>
          </div>
          <BuildButtonList service={service} handleSetService={handleSetService} />
          <BuildContent service={service} />
        </div>
      </div>
    </section>
  );
}
