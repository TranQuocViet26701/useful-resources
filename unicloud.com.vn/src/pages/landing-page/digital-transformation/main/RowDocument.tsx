import ModalAdviseDigitalBanking from '@src/components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import * as React from 'react';
import { Image } from 'react-bootstrap';

import imgDocument from '/assets/image/landing-page/digital-transformation/img-document.png';

export default function RowDocument() {
  const [modalShow, setModalShow] = React.useState(false);
  return (
    <section id="document">
      <div className="wrapper-document-digital-transformation">
        <div
          className="div-document-digital-transformation max-width-1180 padding-left-right"
          style={{ width: '100%' }}
        >
          <Image src={imgDocument} className="img-fluid img-document" />
          <div className="wrapper-title-document-digital-transformation">
            <h2 className="title-digital-transformation">
              HÃY ĐỂ CHÚNG TÔI PHỤC VỤ QUÝ KHÁCH HÀNG
            </h2>
            <p className="sub-title-digital-transformation">
              Nhận tài liệu về Digital Transformation để khám phá các giải pháp chuyển đổi số hiệu
              quả cho doanh nghiệp của bạn.
            </p>
            <div className="row-btn-digital-transformation">
              <button className="btn-advise-digital-transformation-2" type="button">
                <span>Tải về ngay</span>
              </button>
              <button
                className="btn-more-info-digital-transformation"
                type="button"
                onClick={() => setModalShow(true)}
              >
                <span>Nhận tư vấn</span>
              </button>
            </div>
          </div>
        </div>
      </div>
      <ModalAdviseDigitalBanking show={modalShow} onHide={() => setModalShow(false)} />
    </section>
  );
}
