/* eslint-disable max-len */
import * as React from 'react';
import { Image } from 'react-bootstrap';

import imgIcon1 from '/assets/image/landing-page/digital-transformation/svg/icon-ecosystem-bizzone-1.svg';
import imgIcon2 from '/assets/image/landing-page/digital-transformation/svg/icon-ecosystem-bizzone-2.svg';
import imgIcon3 from '/assets/image/landing-page/digital-transformation/svg/icon-ecosystem-bizzone-3.svg';
import imgIcon4 from '/assets/image/landing-page/digital-transformation/svg/icon-ecosystem-bizzone-4.svg';

const listItem = [
  {
    id: 1,
    img: imgIcon1,
    title: 'Giải pháp chuyển đổi số đa ngành',
    subTitle:
      'Tối ưu hoá quy trình kinh doanh bán lẻ, tài chính ngân hàng, quản trị nhân sự, quản lý chung cư/dân cư,...',
  },
  {
    id: 2,
    img: imgIcon2,
    title: 'Cải thiện năng suất hiệu quả kinh doanh ',
    subTitle:
      'Tối ưu quy trình vận hành, đổi mới sáng tạo trong kinh doanh để đạt được hiệu suất, doanh thu mong muốn',
  },
  {
    id: 3,
    img: imgIcon3,
    title: 'Nâng cao trải nghiệm cho khách hàng',
    subTitle:
      'Nâng cao trải nghiệm khách hàng góp phần thay đổi cách hoạt động của nhóm bán hàng, tiếp thị hay dịch vụ khách hàng ',
  },
  {
    id: 4,
    img: imgIcon4,
    title: 'Tiết kiệm chi phí cho doanh nghiệp',
    subTitle:
      'Giải pháp chuyển đổi số Bizzone giúp tối ưu chi phí, tiết kiệm thời gian và nhân lực trong khâu quản lý và tổ chức',
  },
];

export default function RowEcoSystemtBizzone() {
  return (
    <section id="ecosystem">
      <div className="wrapper-ecosystem-bizzone">
        <div className="max-width-1180 padding-left-right" style={{ width: '100%' }}>
          <div className="wrapper-title-digital-transformation">
            <h2 className="title-digital-transformation">Hệ sinh thái chuyển đổi số Bizzone</h2>
          </div>
          <ul className="list-card-item">
            {listItem.map((item) => (
              <li key={item.id} className="card-item">
                <Image src={item.img} className="img-fluid img-card-item" />
                <h5 className="title-ecosystem-bizzone">{item.title}</h5>
                <p className="sub-title-ecosystem-bizzone">{item.subTitle}</p>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </section>
  );
}
