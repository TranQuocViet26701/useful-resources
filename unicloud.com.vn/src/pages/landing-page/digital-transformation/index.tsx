import React from 'react';
import { Container } from 'react-bootstrap';
import './DigitalTransformation.scss';
import RowContact from './main/RowContact';
import RowDocument from './main/RowDocument';
import RowEcoSystemtBizzone from './main/RowEcosystemBizzone';
import RowFinanceBank from './main/RowFinanceBank';
import RowHeader from './main/RowHeader';
import RowMiddle from './main/RowMiddle';
import RowPartnership from './main/RowPartnership';
import RowRetailTransformation from './main/RowRetailTransformation';
import RowScrollEcosystem from './main/RowScrollEcosystem';
import RowSolution from './main/RowSolution';
import Navbar from './navbar';
import Footer from '../../../layouts/footer/Footer';

function DigitalTransformation() {
  return (
    <>
      <Navbar />
      <main>
        <Container className="max-width-100 px-0" fluid>
          <div className="overlay" id="overlay" />
          <RowScrollEcosystem />
          <RowHeader />
          <RowEcoSystemtBizzone />
          <RowSolution />
          <RowFinanceBank />
          <RowRetailTransformation />
          <RowMiddle />
          <RowDocument />
          <RowPartnership />
          <RowContact />
        </Container>
      </main>
      <Footer />
    </>
  );
}
export default DigitalTransformation;
