/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Accordion, Container, Dropdown, Image, Nav, Navbar, Offcanvas } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { NavLink, useLocation } from 'react-router-dom';

import useWindowSize from '@src/hooks/useWindowsSize';

import i18next from 'i18next';
import './navbar.scss';
import iconPhone from '/assets/image/landing-page/digital-transformation/svg/icon-phone-white.svg';
import logoScroll from '/assets/image/svg/logo-scroll.svg';
import logoWhite from '/assets/image/svg/logo-white.svg';

const NavbarChild = React.memo(
  (props: {
    id: string;
    children: JSX.Element | JSX.Element[];
    show: number;
    onShow: () => void;
    onHide: () => void;
  }) => {
    const { id, show, onShow, onHide } = props;

    const location = useLocation();

    useEffect(() => {
      onHide();
    }, [location]);

    return (
      <Navbar.Offcanvas show={show} onShow={onShow} onHide={onHide} id={id}>
        {props.children}
      </Navbar.Offcanvas>
    );
  },
);

const CustomNav = React.memo(() => {
  const { t, ready, i18n } = useTranslation();
  const location = useLocation();

  const size = useWindowSize();
  const isMobile = useMemo(() => {
    if (size.width && size.width >= 992) {
      return false;
    }
    return true;
  }, [size.width]);

  const [show, setShow] = useState(false);
  const onShow = () => setShow(true);

  const onHide = () => setShow(false);

  const [logoNav, setLogoNav] = useState(logoWhite);
  const [stickyClass, setStickyClass] = useState('background-transparent');

  const stickNavbar = useCallback(() => {
    if (typeof window !== 'undefined') {
      const windowHeightScroll = window.scrollY;
      if (windowHeightScroll > 20) {
        setLogoNav(logoScroll);
        setStickyClass('background-sticky box-shadow-sticky-on');
      } else {
        setLogoNav(logoWhite);
        setStickyClass('background-transparent');
      }
    }
  }, [location]);

  useEffect(() => {
    window.addEventListener('scroll', stickNavbar);
    return () => {
      window.removeEventListener('scroll', stickNavbar);
    };
  }, [location]);

  const changeLanguage = useCallback((lng: string | null) => {
    if (lng) i18n.changeLanguage(lng);
  }, []);

  const convertHash = (hash: string) => {
    const hashNew = `#${hash}`;
    return hashNew;
  };

  const compareHash = (hash: string, hashes: string[]) => {
    return hashes.includes(hash);
  };

  const handleClickScroll = (id: string) => {
    const Element = document.getElementById(id);
    Element?.scrollIntoView({ behavior: 'smooth' });
  };

  const languageElement = useMemo(() => {
    const { languages, language } = i18n;
    const flagPath = (locale: string) => `/flag-${locale}.svg`;
    return (
      <Dropdown className={`d-flex ${!isMobile ? '' : 'flex-column'}`} onSelect={changeLanguage}>
        <Dropdown.Toggle
          variant="none"
          id="dropdown-basic"
          className="box-shadow-unset btn-search-home btn-lang ms-auto"
        >
          {language?.toUpperCase()} &nbsp;&nbsp;&nbsp;
          <Image src={flagPath(language)} alt="icon flag" />
        </Dropdown.Toggle>

        <Dropdown.Menu>
          {languages?.map((locale) => {
            const fixedT = i18next.getFixedT(locale, 'translation', 'navbar.navLink');
            return (
              <Dropdown.Item eventKey={locale} key={locale} active={locale === language}>
                <Image src={flagPath(locale)} alt="" />
                <span className="text-dropdown-lang">{ready && fixedT('language').toString()}</span>
              </Dropdown.Item>
            );
          })}
        </Dropdown.Menu>
      </Dropdown>
    );
  }, [isMobile, changeLanguage, i18n.language, ready]);

  const navItems = useMemo(() => {
    const items = [
      <Nav className="mx-auto navbar-digital-transformation" navbarScroll key="nav-item">
        {ready && (
          <>
            <Nav.Link as={NavLink} to="/" bsPrefix="custom-nav-link">
              về trang chủ
            </Nav.Link>
            {!isMobile ? (
              <div className="dropdown-digital-transformation">
                <span
                  style={{ cursor: 'pointer' }}
                  className={`custom-nav-link ${
                    compareHash(location.hash, [
                      '#finance-bank',
                      '#retail-transformation',
                      '#hr-management',
                      '#resident-management',
                    ])
                      ? 'active'
                      : ''
                  }`}
                >
                  chuyển đổi số
                </span>
                <ul className="dropdown-list">
                  <li className="dropdown-item-digital-transformation">
                    <a
                      href="#finance-bank"
                      className={`${
                        location.hash === convertHash('finance-bank') ? 'active' : ''
                      } `}
                    >
                      Tài chính ngân hàng
                    </a>
                  </li>
                  <li className="dropdown-item-digital-transformation">
                    <a
                      href="#retail-transformation"
                      className={`${
                        location.hash === convertHash('retail-transformation') ? 'active' : ''
                      } `}
                    >
                      Bán lẻ
                    </a>
                  </li>
                  <li className="dropdown-item-digital-transformation">
                    <a
                      href="#hr-management"
                      className={`${
                        location.hash === convertHash('hr-management') ? 'active' : ''
                      } `}
                    >
                      Quản trị nhân sự
                    </a>
                  </li>
                  <li className="dropdown-item-digital-transformation">
                    <a
                      href="#resident-management"
                      className={`${
                        location.hash === convertHash('resident-management') ? 'active' : ''
                      } `}
                    >
                      Quản lý cư dân thông minh
                    </a>
                  </li>
                </ul>
              </div>
            ) : (
              <Accordion>
                <Accordion.Item
                  eventKey="0"
                  className={`navbar-accordion-item-digital-transformation ${
                    compareHash(location.hash, [
                      '#finance-bank',
                      '#retail-transformation',
                      '#hr-management',
                      '#resident-management',
                    ])
                      ? 'active'
                      : ''
                  }`}
                >
                  <Accordion.Header>chuyển đổi số</Accordion.Header>
                  <Accordion.Body>
                    <a
                      href="#finance-bank"
                      className={`${
                        location.hash === convertHash('finance-bank') ? 'active' : ''
                      } `}
                      onClick={() => handleClickScroll('finance-bank')}
                    >
                      Tài chính ngân hàng
                    </a>
                    <a
                      href="#retail-transformation"
                      className={`${
                        location.hash === convertHash('retail-transformation') ? 'active' : ''
                      } `}
                      onClick={() => handleClickScroll('retail-transformation')}
                    >
                      Bán lẻ
                    </a>
                    <a
                      href="#hr-management"
                      className={`${
                        location.hash === convertHash('hr-management') ? 'active' : ''
                      } `}
                      onClick={() => handleClickScroll('hr-management')}
                    >
                      Quản trị nhân sự
                    </a>
                    <a
                      href="#resident-management"
                      className={`${
                        location.hash === convertHash('resident-management') ? 'active' : ''
                      } `}
                      onClick={() => handleClickScroll('resident-management')}
                    >
                      Quản lý cư dân thông minh
                    </a>
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>
            )}

            <a
              href="#solution"
              className={`custom-nav-link ${
                location.hash === convertHash('solution') ? 'active' : ''
              } `}
            >
              giải pháp ngành
            </a>
            <a
              href="#contact"
              className={`custom-nav-link ${
                location.hash === convertHash('contact') ? 'active' : ''
              } `}
            >
              liên hệ
            </a>
          </>
        )}
        {isMobile && (
          <>
            <br />
            <a href="#contact" className="group-contact-wrap-digital-transformation mb-3 mt-3">
              <span>NHẬN TƯ VẤN</span>
            </a>
            <div className="group-contact-wrap-digital-transformation mb-3">
              <span className="d-flex">
                <img src={iconPhone} alt="" height={15} style={{ marginRight: '13px' }} />
                1900 4565
              </span>
            </div>
          </>
        )}
      </Nav>,
      <Nav bsPrefix="custom-action-nav" key="nav-actions">
        {languageElement}
      </Nav>,
    ];
    if (isMobile) return items.reverse();
    return items;
  }, [isMobile, t, languageElement, location.hash]);

  return (
    <Navbar
      as="header"
      role="banner"
      expand="lg"
      className={stickyClass}
      fixed="top"
      collapseOnSelect
      id="nav-bar-id"
    >
      <Container className="max-width-1600 menu-desktop">
        <Navbar.Brand href="/">
          <img
            src={logoNav}
            height="64"
            className="logo-menu d-inline-block align-top"
            alt="Unicloud Logo"
            id="logo-menu"
          />
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="offcanvasNavbar"
          bsPrefix="custom-navbar-toggle"
          onClick={onShow}
        />
        <NavbarChild id="offcanvasNavbar" show={show ? 1 : 0} onShow={onShow} onHide={onHide}>
          <Offcanvas.Header closeButton>
            <Offcanvas.Title id="offcanvasNavbarLabel">
              <Navbar.Brand href="/">
                <img
                  src={logoScroll}
                  height="64"
                  className="d-inline-block align-top"
                  alt="Unicloud Logo"
                />
              </Navbar.Brand>
            </Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body style={{ flexGrow: 1 }}>{navItems}</Offcanvas.Body>
        </NavbarChild>
      </Container>
    </Navbar>
  );
});

export default CustomNav;
