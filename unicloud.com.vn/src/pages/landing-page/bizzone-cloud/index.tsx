import * as React from 'react';
import RowHeader from '@src/pages/landing-page/bizzone-cloud/main/RowHeader';
import RowContact from '@src/pages/landing-page/bizzone-cloud/main/RowContact';
import NavBarBizzone from '@src/pages/landing-page/bizzone-cloud/navbar-bizzone/navbar-bizzone';
import Footer from '@src/layouts/footer/Footer';
import SEO from '@src/seo/seo';
import { useLocation } from 'react-router-dom';
import RowBenefit from './main/RowBenefit';
import RowBanner from './main/RowBanner';
import RowFeature from './main/RowFeature';
import RowProcessUser from './main/RowProcessUser';
import RowInterFace from './main/RowInterFace';
import RowApplication from './main/RowApplication';
import RowSupportCustomer from './main/RowSupportCustomer';
import laptopHeroBanner from '/assets/image/homePage/img-bizzone-laptop-landing-page.png';
import './style.scss';

function BizzoneCloudPage() {
  const location = useLocation();

  return (
    <>
      <SEO
        title="Giải pháp quản trị hiệu suất toàn diện giúp bạn"
        description="Giải pháp phần mềm nhân sự lưu trữ và 
        truy xuất thông tin một cách nhanh chóng, là giải pháp 
        hiệu quả nhất cho nhà quản trị nhân sự chuyên nghiệp."
        imgthumbs={`https://unicloud.com.vn${laptopHeroBanner}`}
        url={location.pathname}
      />
      <header>
        <NavBarBizzone />
      </header>
      <main className="bizzone-cloud template-landing">
        <RowHeader />
        <RowBenefit />
        <RowBanner />
        <RowFeature />
        <RowProcessUser />
        <RowInterFace />

        <RowApplication />
        <RowContact />
        <RowSupportCustomer />
      </main>
      <footer>
        <Footer />
      </footer>
    </>
  );
}

export default BizzoneCloudPage;
