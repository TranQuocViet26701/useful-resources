import React, { useEffect, useMemo, useState } from 'react';
import { Container, Nav, Navbar, Offcanvas } from 'react-bootstrap';
import logoColor from '/assets/image/svg/logo-scroll.svg';
import { useLocation, NavLink } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import useWindowSize from '@src/hooks/useWindowsSize';
import ModalAdvise from '@components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import './navbar-bizzone.scss';

interface MenuModel {
  id: number;
  title: string;
  link: string;
  isActive: boolean;
  elementId: string;
}

const NavbarChild = React.memo(
  (props: {
    id: string;
    children: JSX.Element | JSX.Element[];
    isMobile: boolean;
    show: boolean;
    onShow: () => void;
    onHide: () => void;
  }) => {
    const { id, isMobile, show, onShow, onHide } = props;

    if (!isMobile) {
      return <Navbar.Collapse id={id}>{props.children}</Navbar.Collapse>;
    }

    const location = useLocation();

    useEffect(() => {
      onHide();
    }, [location]);

    return (
      <Navbar.Offcanvas show={show} onShow={onShow} onHide={onHide} id={id}>
        {props.children}
      </Navbar.Offcanvas>
    );
  },
);

const NavBarBizzone = React.memo(() => {
  const [showPopup, setShowPopup] = useState(false);

  const size = useWindowSize();
  const { t, ready } = useTranslation();
  const [show, setShow] = useState(false);
  const onShow = () => setShow(true);

  const onHide = () => setShow(false);

  const isMobile = useMemo(() => {
    if (size.width && size.width >= 992) {
      return false;
    }
    return true;
  }, [size.width]);

  const listMenu: MenuModel[] = [
    {
      id: 0,
      title: 'navbar.bizzone.home',
      link: '/',
      isActive: true,
      elementId: '/',
    },
    {
      id: 1,
      title: 'navbar.bizzone.benefit',
      link: '',
      isActive: false,
      elementId: 'benefit',
    },
    {
      id: 2,
      title: 'navbar.bizzone.feature',
      link: '',
      isActive: false,
      elementId: 'feature',
    },
    {
      id: 3,
      title: 'navbar.bizzone.contact',
      link: '',
      isActive: false,
      elementId: 'contact',
    },
    {
      id: 4,
      title: 'navbar.bizzone.support',
      link: '',
      isActive: false,
      elementId: 'support',
    },
  ];

  const location = useLocation();
  const convertHash = (hash: string) => {
    const hashNew = `#${hash}`;
    return hashNew;
  };

  useEffect(() => {
    const detectHash = () => {
      for (let menu = 0; menu < listMenu.length; menu += 1) {
        const eleMenu = listMenu[menu];

        const el = document.querySelector(`[id='${eleMenu.elementId}']`);

        const observer = new window.IntersectionObserver(([entry]) => {
          if (entry.isIntersecting) {
            const hashNew = `#${eleMenu.elementId}`;
            location.hash = hashNew;
            location.state = { hash: hashNew };
            window.history.pushState({}, '', hashNew);
          }
        });
        if (el) {
          observer.observe(el);
        }
      }
    };

    detectHash();
    return () => {
      detectHash();
    };
  }, []);

  const navItems = useMemo(() => {
    return (
      <>
        <Nav className="mx-auto navbar-bizzone-wrap" navbarScroll key="nav-item">
          {listMenu.map((menu) => {
            return menu.link ? (
              <NavLink key={menu.id} className="nav-link-bizzone" to={menu.link}>
                {ready && <> {t(`${menu.title}`)}</>}
              </NavLink>
            ) : (
              <a
                key={menu.id}
                className={`nav-link-bizzone ${
                  location.hash === convertHash(menu.elementId) ? 'active' : null
                } `}
                href={`#${menu.elementId}`}
              >
                {ready && <>{t(`${menu.title}`)}</>}
              </a>
            );
          })}
        </Nav>
        <Nav.Link bsPrefix="custom-nav-link" key="nav-actions">
          <button type="button" className="btn-cost" onClick={() => setShowPopup(true)}>
            <span>Nhận tư vấn</span>
          </button>
        </Nav.Link>
      </>
    );
  }, [t, location.hash]);

  return (
    <Navbar
      as="header"
      role="banner"
      expand="lg"
      className="background-sticky box-shadow-sticky-on"
      fixed="top"
      collapseOnSelect
      id="nav-bar-id"
    >
      <Container className="container-child menu-desktop">
        <Navbar.Brand to="/" as={NavLink}>
          <img
            src={logoColor}
            height="64"
            className="logo-menu d-inline-block align-top"
            alt="Unicloud Logo"
            id="logo-menu"
          />
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="offcanvasNavbar"
          bsPrefix="custom-navbar-toggle"
          onClick={onShow}
        />
        <NavbarChild
          id="offcanvasNavbar"
          isMobile={isMobile}
          show={show}
          onShow={onShow}
          onHide={onHide}
        >
          <Offcanvas.Header closeButton>
            <Offcanvas.Title id="offcanvasNavbarLabel">
              <Navbar.Brand href="/">
                <img
                  src={logoColor}
                  height="64"
                  className="d-inline-block align-top"
                  alt="Unicloud Logo"
                />
              </Navbar.Brand>
            </Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body style={{ flexGrow: 1 }} className="navbar-bizzone">
            {navItems}
          </Offcanvas.Body>
        </NavbarChild>
      </Container>
      <ModalAdvise show={showPopup} onHide={() => setShowPopup(false)} />
    </Navbar>
  );
});

export default NavBarBizzone;
