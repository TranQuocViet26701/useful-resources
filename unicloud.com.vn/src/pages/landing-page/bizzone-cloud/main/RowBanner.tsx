import React from 'react';

import { useTranslation } from 'react-i18next';

interface SolutionModel {
  id: number;
  title: string;
  desc: string;
}

function RowBanner() {
  const { t } = useTranslation();

  const listSolution: SolutionModel[] = [
    {
      id: 0,
      title: '99%',
      desc: t('bizzoneLandingPage.rowBanner.desc1'),
    },
    {
      id: 1,
      title: '500%',
      desc: t('bizzoneLandingPage.rowBanner.desc2'),
    },
    {
      id: 2,
      title: '200+',
      desc: t('bizzoneLandingPage.rowBanner.desc3'),
    },
    {
      id: 3,
      title: '300+',
      desc: t('bizzoneLandingPage.rowBanner.desc4'),
    },
  ];

  return (
    <article className="bizzone-banner" id="banner">
      <div className="container-child bizzone-banner-wrap">
        <h2 className="bizzone-banner-detail">{t('bizzoneLandingPage.rowBanner.title')}</h2>
        <div className="divider-white" />
        <div className="bizzone-group-solution">
          {listSolution.map((solution) => (
            <div className="group-solution-item" key={solution.id}>
              <h1 className="bizzone-detail">{solution.title}</h1>
              <p className="bizzone-detail">{solution.desc}</p>
            </div>
          ))}
        </div>
      </div>
    </article>
  );
}

export default RowBanner;
