import React from 'react';
import { Image } from 'react-bootstrap';
import ModalAdvise from '@components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';

import laptopHeroBanner from '/assets/image/homePage/img-bizzone-laptop-landing-page.png';

import { useTranslation } from 'react-i18next';

import '../style.scss';

function RowHeader() {
  const [modalShow, setModalShow] = React.useState(false);
  const { t, ready } = useTranslation();

  return (
    <section id="header">
      <div className="row-header-banner  background-image">
        <div className="row-header-content">
          <div className="row-header-left-content">
            <h1 className="row-header-title">{t('bizzoneLandingPage.rowHeader.title')}</h1>
            <span className="row-header-subtitle">
              {ready && t('bizzoneLandingPage.rowHeader.subTitle')}
            </span>

            <button className="btn-advise" type="button" onClick={() => setModalShow(true)}>
              <span>{ready && t('bizzoneLandingPage.rowHeader.contactBtn')}</span>
            </button>
          </div>

          <div className="row-header-right-content">
            <Image src={laptopHeroBanner} alt="" className="row-header-laptop-img" />
          </div>
        </div>
      </div>
      <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
    </section>
  );
}
export default RowHeader;
