import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay } from 'swiper';
import { useTranslation } from 'react-i18next';
import ModalAdvise from '../../../../components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import imgArrowLeftDiscover from '/assets/image/svg/arrow-left-slide-to-discover.svg';
import imgArrowRightDiscover from '/assets/image/svg/arrow-right-slide-to-discover.svg';
import iconMouseScroll from '/assets/image/svg/icon-mouse-scroll.svg';
import imgSlide1 from '/assets/image/landingPage/bizzone-enterprise/img-slide-row-banking-1.png';
import imgSlide2 from '/assets/image/landingPage/bizzone-enterprise/img-slide-row-banking-2.png';
import imgSlide3 from '/assets/image/landingPage/bizzone-enterprise/img-slide-row-banking-3.png';
import imgSlide4 from '/assets/image/landingPage/bizzone-enterprise/img-slide-row-banking-4.png';
import imgSlide5 from '/assets/image/landingPage/bizzone-enterprise/img-slide-row-banking-5.png';
import imgSlide6 from '/assets/image/landingPage/bizzone-enterprise/img-slide-row-banking-6.png';

function RowBanking() {
  const [modalShow, setModalShow] = React.useState(false);
  const { t } = useTranslation();
  return (
    <div className="row-banking-back-ground">
      <Container className="max-width-1180 px-0">
        <Row className="gx-5 padding-left-right">
          <Col xs={12} md={5} className="row-banking-col-left">
            <h2 className="row-banking-title-sub">{t('bizzoneLandingPage.rowBanking.subTitle')}</h2>

            <p className="row-banking-detail-2">{t('bizzoneLandingPage.rowBanking.detail')}</p>
            <div>
              <button type="button" className="btn-row-banking" onClick={() => setModalShow(true)}>
                {t('bizzoneLandingPage.rowBanking.advise')}
              </button>
            </div>
            <div className="wrapper-mouse-scroll">
              <div className="text-center mt-5 mb-3 d-flex justify-content-center">
                <Image src={iconMouseScroll} className="img-fluid img-mouse-scroll" />
              </div>
              <div className="text-center d-flex justify-content-center">
                <Image src={imgArrowLeftDiscover} />
                <span className="text-discover">
                  {t('bizzoneLandingPage.rowBanking.textDiscover')}
                </span>
                <Image src={imgArrowRightDiscover} />
              </div>
            </div>
          </Col>
          <Col xs={12} md={7} className="col-slide-row-banking">
            <Swiper
              modules={[Autoplay]}
              spaceBetween={20}
              slidesPerView={2.5}
              breakpoints={{
                320: {
                  slidesPerView: 1.3,
                },
                600: {
                  slidesPerView: 1.5,
                },
                768: {
                  slidesPerView: 1.3,
                },
                900: {
                  slidesPerView: 1.6,
                },
                1100: {
                  slidesPerView: 2,
                },
                1300: {
                  slidesPerView: 2.5,
                },
              }}
              className="swiper-row-banking"
            >
              <SwiperSlide>
                <div className="wrapper-slide-row-banking">
                  <h4 className="title-slide-row-banking">
                    {t('bizzoneLandingPage.rowBanking.titleSlide')}
                    <br />
                    {t('bizzoneLandingPage.rowBanking.subTitleSlide1')}
                  </h4>
                  <div className="text-center">
                    <Image src={imgSlide1} className="img-fluid img-slide-row-banking" />
                  </div>
                  <p className="detail-slide-row-banking">
                    {t('bizzoneLandingPage.rowBanking.detailSlide1')}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="wrapper-slide-row-banking">
                  <h4 className="title-slide-row-banking">
                    {t('bizzoneLandingPage.rowBanking.titleSlide')}
                    <br />
                    {t('bizzoneLandingPage.rowBanking.subTitleSlide2')}
                  </h4>
                  <div className="text-center">
                    <Image src={imgSlide2} className="img-fluid img-slide-row-banking" />
                  </div>

                  <p className="detail-slide-row-banking">
                    {t('bizzoneLandingPage.rowBanking.detailSlide2')}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="wrapper-slide-row-banking">
                  <h2 className="title-slide-row-banking">
                    {t('bizzoneLandingPage.rowBanking.titleSlide')}
                    <br /> {t('bizzoneLandingPage.rowBanking.subTitleSlide3')}
                  </h2>
                  <div className="text-center">
                    <Image src={imgSlide3} className="img-fluid img-slide-row-banking" />
                  </div>
                  <p className="detail-slide-row-banking">
                    {t('bizzoneLandingPage.rowBanking.detailSlide3')}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="wrapper-slide-row-banking">
                  <h2 className="title-slide-row-banking">
                    {t('bizzoneLandingPage.rowBanking.titleSlide')}
                    <br />
                    {t('bizzoneLandingPage.rowBanking.subTitleSlide4')}
                  </h2>
                  <div className="text-center">
                    <Image src={imgSlide4} className="img-fluid img-slide-row-banking" />
                  </div>
                  <p className="detail-slide-row-banking">
                    {t('bizzoneLandingPage.rowBanking.detailSlide4')}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="wrapper-slide-row-banking">
                  <h2 className="title-slide-row-banking">
                    {t('bizzoneLandingPage.rowBanking.titleSlide')}
                    <br /> {t('bizzoneLandingPage.rowBanking.subTitleSlide5')}
                  </h2>
                  <div className="text-center">
                    <Image src={imgSlide5} className="img-fluid img-slide-row-banking" />
                  </div>
                  <p className="detail-slide-row-banking">
                    {t('bizzoneLandingPage.rowBanking.detailSlide5')}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="wrapper-slide-row-banking">
                  <h2 className="title-slide-row-banking">
                    {t('bizzoneLandingPage.rowBanking.titleSlide')}
                    <br /> {t('bizzoneLandingPage.rowBanking.subTitleSlide6')}
                  </h2>
                  <div className="text-center">
                    <Image src={imgSlide6} className="img-fluid img-slide-row-banking" />
                  </div>
                  <p className="detail-slide-row-banking">
                    {t('bizzoneLandingPage.rowBanking.detailSlide6')}
                  </p>
                </div>
              </SwiperSlide>
            </Swiper>
          </Col>
        </Row>
        <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
      </Container>
    </div>
  );
}
export default RowBanking;
