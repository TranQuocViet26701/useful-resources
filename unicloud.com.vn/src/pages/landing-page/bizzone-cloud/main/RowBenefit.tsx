import React from 'react';
import iconBenefit from '/assets/image/landingPage/bizzone-enterprise/icon/icon-search.svg';
import iconSecurity from '/assets/image/landingPage/bizzone-enterprise/icon/icon-security.svg';
import iconGroup from '/assets/image/landingPage/bizzone-enterprise/icon/icon-group-user.svg';
import iconCost from '/assets/image/landingPage/bizzone-enterprise/icon/icon-cost.svg';
import iconExactly from '/assets/image/landingPage/bizzone-enterprise/icon/icon-exactly.svg';
import iconAdmin from '/assets/image/landingPage/bizzone-enterprise/icon/icon-admin.svg';

import { useTranslation } from 'react-i18next';

interface BenefitModel {
  id: number;
  title: string;
  icon: string;
  desc: string;
}

function RowBenefit() {
  const { t } = useTranslation();

  const listBenefit: BenefitModel[] = [
    {
      id: 0,
      title: t('bizzoneLandingPage.rowBenefit.title1'),
      icon: iconBenefit,
      desc: t('bizzoneLandingPage.rowBenefit.desc1'),
    },
    {
      id: 1,
      title: t('bizzoneLandingPage.rowBenefit.title2'),
      icon: iconSecurity,
      desc: t('bizzoneLandingPage.rowBenefit.desc2'),
    },
    {
      id: 2,
      title: t('bizzoneLandingPage.rowBenefit.title3'),
      icon: iconGroup,
      desc: t('bizzoneLandingPage.rowBenefit.desc3'),
    },
    {
      id: 3,
      title: t('bizzoneLandingPage.rowBenefit.title4'),
      icon: iconCost,
      desc: t('bizzoneLandingPage.rowBenefit.desc4'),
    },
    {
      id: 4,
      title: t('bizzoneLandingPage.rowBenefit.title5'),
      icon: iconExactly,
      desc: t('bizzoneLandingPage.rowBenefit.desc5'),
    },
    {
      id: 5,
      title: t('bizzoneLandingPage.rowBenefit.title6'),
      icon: iconAdmin,
      desc: t('bizzoneLandingPage.rowBenefit.desc6'),
    },
  ];
  return (
    <section className="container-child bizzone-benefit" id="benefit">
      <h2 className="bizzone">{t('bizzoneLandingPage.rowBenefit.bannerTitle')}</h2>
      <div className="divider-blue" />

      <div className="benefit-cards">
        {listBenefit.map((benefit) => (
          <div key={benefit.id} className="benefit-cards-item">
            <img className="benefit-card" src={benefit.icon} alt="icon benefit" width={100} />
            <h4 className="benefit-card">{benefit.title}</h4>
            <p className="benefit-card">{benefit.desc}</p>
          </div>
        ))}
      </div>
    </section>
  );
}

export default RowBenefit;
