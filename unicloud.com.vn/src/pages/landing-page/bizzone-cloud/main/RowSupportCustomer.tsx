import React from 'react';
import imgRight from '/assets/image/landingPage/bizzone-enterprise/img-bizzone.png';
import { Accordion } from 'react-bootstrap';
import { useQuery } from '@apollo/client/react/hooks/useQuery';

import imgCloudDownload from '/assets/image/svg/icon-cloud-download.svg';
import { Link } from 'react-router-dom';

import { useTranslation } from 'react-i18next';
import { gql } from 'graphql-tag';

const queryData = gql`
  query NewQuery($title: String = "Link Download Bizzone Enterprise") {
    pages(where: { title: $title }) {
      nodes {
        linkdownload {
          link {
            mediaItemUrl
          }
        }
        title
      }
    }
  }
`;

interface SupportModel {
  id: number;
  title: string;
  desc: string[];
}

function RowSupportCustomer() {
  const { data } = useQuery(queryData, {
    variables: { title: 'Link Download Bizzone Enterprise' },
  });

  const { t } = useTranslation();

  const linkDownload = data?.pages?.nodes[0]?.linkdownload?.link?.mediaItemUrl;

  const listSupport: SupportModel[] = [
    {
      id: 0,
      title: t('bizzoneLandingPage.rowSupportCustomer.title1'),
      desc: [
        t('bizzoneLandingPage.rowSupportCustomer.title1.desc1'),
        t('bizzoneLandingPage.rowSupportCustomer.title1.desc2'),
        t('bizzoneLandingPage.rowSupportCustomer.title1.desc3'),
        t('bizzoneLandingPage.rowSupportCustomer.title1.desc4'),
      ],
    },
    {
      id: 1,
      title: t('bizzoneLandingPage.rowSupportCustomer.title2'),
      desc: [
        t('bizzoneLandingPage.rowSupportCustomer.title2.desc1'),
        t('bizzoneLandingPage.rowSupportCustomer.title2.desc2'),
        t('bizzoneLandingPage.rowSupportCustomer.title2.desc3'),
        t('bizzoneLandingPage.rowSupportCustomer.title2.desc4'),
      ],
    },
    {
      id: 2,
      title: t('bizzoneLandingPage.rowSupportCustomer.title3'),
      desc: [t('bizzoneLandingPage.rowSupportCustomer.title3.desc1')],
    },
    {
      id: 3,
      title: t('bizzoneLandingPage.rowSupportCustomer.title4'),
      desc: [t('bizzoneLandingPage.rowSupportCustomer.title4.desc1')],
    },
  ];

  const handleClickDownload = () => {
    window.open(linkDownload);
  };
  return (
    <section className="bizzone-support" id="support">
      <div className="container-child bizzone-support-wrap">
        <div className="template-grid-2">
          <div className="bizzone-support-left">
            <div className="">
              <h3 className="bizzone-process-detail text-start">
                {t('bizzoneLandingPage.rowSupportCustomer.bannerTitle')}
              </h3>
              <div className="divider-blue-normal" />
            </div>
            <Accordion defaultActiveKey="0" flush>
              {listSupport.map((listItem, index) => (
                <Accordion.Item key={listItem.id} eventKey={index.toString()}>
                  <Accordion.Header>{listItem.title}</Accordion.Header>
                  <Accordion.Body>
                    <ul className="accordion-list-item">
                      {listItem?.desc.map((value) => (
                        <li key={value} className="">
                          <span className="accordion-desc">{value}</span>
                        </li>
                      ))}
                    </ul>

                    {index === listSupport.length - 1 && (
                      <div className="accordion-btn-container">
                        <Link
                          to="/unicloud-digital-banking-platformV1.4.pdf"
                          target="_blank"
                          download
                          onClick={handleClickDownload}
                        >
                          <button
                            className="accordion-desc-btn"
                            type="submit"
                            onClick={handleClickDownload}
                          >
                            {t('bizzoneLandingPage.rowSupportCustomer.downloadBtn')}
                            <img src={imgCloudDownload} className="accordion-desc-icon" alt="" />
                          </button>
                        </Link>
                      </div>
                    )}
                  </Accordion.Body>
                </Accordion.Item>
              ))}
            </Accordion>
          </div>
          <div className="bizzone-support-right">
            <img src={imgRight} alt="bizzone" />
          </div>
        </div>
      </div>
    </section>
  );
}

export default RowSupportCustomer;
