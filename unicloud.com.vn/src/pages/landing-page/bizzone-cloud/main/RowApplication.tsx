import React, { useMemo } from 'react';

import logoSmart from '/assets/image/landingPage/logo/logo-smart-color.svg';
import logoSunshine from '/assets/image/landingPage/logo/logo-sunshine-home-color.svg';
import logoKSF from '/assets/image/landingPage/logo/logo-ksf-color.svg';
import logoODE from '/assets/image/landingPage/logo/logo-ode-color.svg';
import logoKLB from '/assets/image/landingPage/logo/logo-klb-color.svg';

import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay } from 'swiper';

import { useTranslation } from 'react-i18next';

function RowApplication() {
  const { t } = useTranslation();

  const listSlide: any[] = [
    {
      id: 1,
      img: logoKLB,
    },
    {
      id: 2,
      img: logoODE,
    },
    {
      id: 3,
      img: logoKSF,
    },
    {
      id: 4,
      img: logoSunshine,
    },
    {
      id: 5,
      img: logoSmart,
    },
  ];

  const BuildSlide = useMemo(
    () => (
      <Swiper
        modules={[Autoplay]}
        spaceBetween={25}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        breakpoints={{
          475: {
            slidesPerView: 1,
            spaceBetween: 10,
          },
          640: {
            slidesPerView: 3,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 4,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 5,
            spaceBetween: 50,
          },
        }}
      >
        {listSlide.map((slide) => (
          <SwiperSlide key={slide.id}>
            <img src={slide.img} className="swiper-partnership-application" alt="" />
          </SwiperSlide>
        ))}
      </Swiper>
    ),
    [listSlide],
  );

  return (
    <section className="bizzone-application">
      <div className="bizzone-application-wrap container-child">
        <h3 className="bizzone-process-detail text-center">
          {t('bizzoneLandingPage.rowApplication.title')}
        </h3>
        <div className="divider-gradient" />
      </div>
      <div className="bizzone-apllication-slide">
        <div className="container-child">{BuildSlide}</div>
      </div>
    </section>
  );
}

export default RowApplication;
