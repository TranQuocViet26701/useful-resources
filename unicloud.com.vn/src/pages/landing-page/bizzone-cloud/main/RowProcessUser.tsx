import React from 'react';
import imgRight from '/assets/image/landingPage/bizzone-enterprise/img-horizontal.png';

import imgSurvey from '/assets/image/svg/icon-survey.svg';
import imgCallCenter from '/assets/image/svg/icon-call-center.svg';
import imgAgreement from '/assets/image/svg/icon-agreement.svg';
import imgResearch from '/assets/image/svg/icon-research.svg';
import imgApprove from '/assets/image/svg/icon-approve.svg';

import { useTranslation } from 'react-i18next';

interface ProcessModel {
  id: number;
  title: string;
  desc: string[];
  icon: string;
  alt: string;
}

function RowProcessUser() {
  const { t } = useTranslation();

  const listProcess: ProcessModel[] = [
    {
      id: 1,
      title: t('bizzoneLandingPage.rowProcessUser.title1'),
      desc: [
        t('bizzoneLandingPage.rowProcessUser.title1.desc1'),
        t('bizzoneLandingPage.rowProcessUser.title1.desc2'),
        t('bizzoneLandingPage.rowProcessUser.title1.desc3'),
        t('bizzoneLandingPage.rowProcessUser.title1.desc4'),
      ],
      icon: imgSurvey,
      alt: 'icon survey',
    },
    {
      id: 2,
      title: t('bizzoneLandingPage.rowProcessUser.title2'),
      desc: [
        t('bizzoneLandingPage.rowProcessUser.title2.desc1'),
        t('bizzoneLandingPage.rowProcessUser.title2.desc2'),
        t('bizzoneLandingPage.rowProcessUser.title2.desc3'),
      ],
      icon: imgCallCenter,
      alt: 'icon call center',
    },
    {
      id: 3,
      title: t('bizzoneLandingPage.rowProcessUser.title3'),
      desc: [
        t('bizzoneLandingPage.rowProcessUser.title3.desc1'),
        t('bizzoneLandingPage.rowProcessUser.title3.desc2'),
        t('bizzoneLandingPage.rowProcessUser.title3.desc3'),
      ],
      icon: imgAgreement,
      alt: 'icon agreement',
    },
    {
      id: 4,
      title: t('bizzoneLandingPage.rowProcessUser.title4'),
      desc: [
        t('bizzoneLandingPage.rowProcessUser.title4.desc1'),
        t('bizzoneLandingPage.rowProcessUser.title4.desc2'),
        t('bizzoneLandingPage.rowProcessUser.title4.desc3'),
        t('bizzoneLandingPage.rowProcessUser.title4.desc4'),
      ],
      icon: imgResearch,
      alt: 'icon research',
    },
    {
      id: 5,
      title: t('bizzoneLandingPage.rowProcessUser.title5'),
      desc: [
        t('bizzoneLandingPage.rowProcessUser.title5.desc1'),
        t('bizzoneLandingPage.rowProcessUser.title5.desc2'),
        t('bizzoneLandingPage.rowProcessUser.title5.desc3'),
      ],
      icon: imgApprove,
      alt: 'icon approve',
    },
  ];
  return (
    <section className="bizzone-process">
      <div className="container-child bizzone-process-wrap">
        <h3 className="bizzone-process-detail text-center padding-20">
          {t('bizzoneLandingPage.rowProcessUser.bannerTitle')}
        </h3>
        <div className="divider-gradient" />
        <div className="process-wrap-detail template-grid-2">
          <div className="process-left">
            {listProcess.map((process) => (
              <div
                className={`process-item template-grid-4 ${
                  process.id % 2 === 0 ? 'margin-left-auto' : ''
                }`}
                key={process.id}
              >
                <h3 className="bizzone-process-detail">0{process.id}</h3>
                <h4 className="bizzone-process-detail d-flex align-items-center">
                  {process.title}
                </h4>
                <img src={process.icon} width={64} alt={process.alt} />
                <ul className="bizzone-process-detail">
                  {process.desc.map((item) => (
                    <li key={item} className="bizzone-process-detail">
                      {item}
                    </li>
                  ))}
                </ul>
              </div>
            ))}
          </div>
          <div className="process-right">
            <img src={imgRight} alt="Process user" />
          </div>
        </div>
      </div>
    </section>
  );
}

export default RowProcessUser;
