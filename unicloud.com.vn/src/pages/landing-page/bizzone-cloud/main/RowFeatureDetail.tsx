import React from 'react';
import imgLaptop1 from '/assets/image/landingPage/bizzone-enterprise/img-laptop-first.png';
import imgLeft from '/assets/image/landingPage/bizzone-enterprise/bg-left.png';

import imgLaptop2 from '/assets/image/landingPage/bizzone-enterprise/img-laptop-second.png';
import imgRight from '/assets/image/landingPage/bizzone-enterprise/bg-right.png';

import imgImac1 from '/assets/image/landingPage/bizzone-enterprise/img-imac-first.png';
import imgImacLeft from '/assets/image/landingPage/bizzone-enterprise/bg-imac.png';
import { useTranslation } from 'react-i18next';
import ModalAdvise from '@components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';

import imgLaptop3 from '/assets/image/landingPage/bizzone-enterprise/img-macbook-pro.png';
import imgLaptop3Right from '/assets/image/landingPage/bizzone-enterprise/bg-macbook-pro.png';

function RowFeatureDetail() {
  const [modalShow, setModalShow] = React.useState(false);
  const { t } = useTranslation();

  return (
    <section className="bizzone-feature-detail">
      <div className="template-grid-2-auto feature-detail-wrap">
        <div className="feature-left">
          <img className="stick-bg" src={imgLeft} alt="" />
          <img className="stick-img" src={imgLaptop1} alt="laptop" />
        </div>
        <div className="feature-right">
          <h2 className="service-detail">{t('bizzoneLandingPage.rowFeatureDetail.title1')}</h2>
          <span className="detail-item">{t('bizzoneLandingPage.rowFeatureDetail.subTitle1')}</span>
          <button
            className="btn-advise margin-top-54"
            type="button"
            onClick={() => setModalShow(true)}
          >
            <span>{t('bizzoneLandingPage.rowFeatureDetail.contactBtn')}</span>
          </button>
        </div>
      </div>
      <div className="template-grid-2-auto feature-detail-wrap feature-detail-wrap-revert">
        <div className="feature-left">
          <img className="stick-bg" src={imgRight} alt="" />
          <img className="stick-img" src={imgLaptop2} alt="laptop" />
        </div>
        <div className="feature-right">
          <h2 className="service-detail">{t('bizzoneLandingPage.rowFeatureDetail.title2')}</h2>
          <span className="detail-item">{t('bizzoneLandingPage.rowFeatureDetail.subTitle2')}</span>
          <button
            className="btn-advise margin-top-54"
            type="button"
            onClick={() => setModalShow(true)}
          >
            <span>{t('bizzoneLandingPage.rowFeatureDetail.contactBtn')}</span>
          </button>
        </div>
      </div>

      <div className="template-grid-2-auto feature-detail-wrap">
        <div className="imac-feature-left">
          <img className="stick-bg" src={imgImacLeft} alt="" />
          <img className="stick-img" src={imgImac1} alt="imac" />
        </div>
        <div className="feature-right">
          <h2 className="service-detail">{t('bizzoneLandingPage.rowFeatureDetail.title3')}</h2>
          <span className="detail-item">{t('bizzoneLandingPage.rowFeatureDetail.subTitle3')}</span>
          <button
            className="btn-advise margin-top-54"
            type="button"
            onClick={() => setModalShow(true)}
          >
            <span>{t('bizzoneLandingPage.rowFeatureDetail.contactBtn')}</span>
          </button>
        </div>
      </div>

      <div className="template-grid-2-auto feature-detail-wrap feature-detail-wrap-revert">
        <div className="macbook-feature-left">
          <img className="stick-bg" src={imgLaptop3Right} alt="" />
          <img className="stick-img" src={imgLaptop3} alt="laptop3" />
        </div>
        <div className="feature-right">
          <h2 className="service-detail">{t('bizzoneLandingPage.rowFeatureDetail.title4')}</h2>
          <span className="detail-item">{t('bizzoneLandingPage.rowFeatureDetail.subTitle4')}</span>
          <button
            className="btn-advise margin-top-54"
            type="button"
            onClick={() => setModalShow(true)}
          >
            <span>{t('bizzoneLandingPage.rowFeatureDetail.contactBtn')}</span>
          </button>
        </div>
      </div>
      <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
    </section>
  );
}

export default RowFeatureDetail;
