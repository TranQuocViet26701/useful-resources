import * as React from 'react';
import slideInterface1 from '/assets/image/landingPage/bizzone-enterprise/slide-interface-1.png';
import slideInterface2 from '/assets/image/landingPage/bizzone-enterprise/slide-interface-2.png';
import slideInterface3 from '/assets/image/landingPage/bizzone-enterprise/slide-interface-3.png';
import slideInterface4 from '/assets/image/landingPage/bizzone-enterprise/slide-interface-4.png';
import slideInterface5 from '/assets/image/landingPage/bizzone-enterprise/slide-interface-5.png';
import slideInterface6 from '/assets/image/landingPage/bizzone-enterprise/slide-interface-6.png';

import SwiperCore, { Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { useTranslation } from 'react-i18next';

SwiperCore.use([Navigation]);

function RowInterFace() {
  const { t } = useTranslation();

  const listSlide: any[] = [
    {
      id: 1,
      img: slideInterface1,
    },
    {
      id: 2,
      img: slideInterface2,
    },
    {
      id: 3,
      img: slideInterface3,
    },
    {
      id: 4,
      img: slideInterface4,
    },
    {
      id: 5,
      img: slideInterface5,
    },
    {
      id: 6,
      img: slideInterface6,
    },
  ];

  return (
    <section className="bizzone-interface">
      <div className="container-child bizzone-interface-wrap">
        <h3 className="bizzone-process-detail text-center">
          {t('bizzoneLandingPage.rowInterFace.title')}
        </h3>
        <div className="divider-gradient" />
        <div className="bizzone-interface-slide template-grid-3">
          <div className="prev prev-slide">
            <svg
              width="24"
              height="25"
              viewBox="0 0 24 25"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M14.4297 5.97192L20.4997 12.0419L14.4297 18.1119"
                stroke="white"
                strokeWidth="1.5"
                strokeMiterlimit="10"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M3.49976 12.0419H20.3298"
                stroke="white"
                strokeWidth="1.5"
                strokeMiterlimit="10"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </div>
          <Swiper
            navigation={{
              prevEl: '.prev',
              nextEl: '.next',
            }}
            className="swiper-interface-img-wrapper"
            slidesPerView={1}
            spaceBetween={30}
          >
            {listSlide.map((slide) => (
              <SwiperSlide key={slide.id}>
                <img src={slide.img} className="swiper-interface-img" alt="" />
              </SwiperSlide>
            ))}
          </Swiper>
          <div className="next next-slide">
            <svg
              width="24"
              height="25"
              viewBox="0 0 24 25"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M14.4297 5.97192L20.4997 12.0419L14.4297 18.1119"
                stroke="white"
                strokeWidth="1.5"
                strokeMiterlimit="10"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M3.49976 12.0419H20.3298"
                stroke="white"
                strokeWidth="1.5"
                strokeMiterlimit="10"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </div>
        </div>
      </div>
    </section>
  );
}

export default RowInterFace;
