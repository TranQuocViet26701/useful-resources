import * as React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from 'swiper';

import imgAvatar from '/assets/image/landingPage/bizzone-enterprise/avatar.png';

interface UserModel {
  key: number;
  fullName: string;
  position: string;
  desc: string;
  avatar: string;
}

function RowUserAbout() {
  const listSlide: UserModel[] = [
    {
      key: 1,
      fullName: 'Ông Bùi Anh Dũng',
      position: 'CEO Google',
      desc:
        '“Dễ dàng theo dõi các dữ liệu,' +
        ' báo cáo ngay trên điện thoại là điểm tuyệt vời mà UniOffice đem lại cho chúng tôi”',
      avatar: imgAvatar,
    },
    {
      key: 2,
      fullName: 'Ông Bùi Anh Dũng',
      position: 'CEO Google',
      desc:
        '“Dễ dàng theo dõi các dữ liệu,' +
        ' báo cáo ngay trên điện thoại là điểm tuyệt vời mà UniOffice đem lại cho chúng tôi”',
      avatar: imgAvatar,
    },
    {
      key: 3,
      fullName: 'Ông Bùi Anh Dũng',
      position: 'CEO Google',
      desc:
        '“Dễ dàng theo dõi các dữ liệu,' +
        ' báo cáo ngay trên điện thoại là điểm tuyệt vời mà UniOffice đem lại cho chúng tôi”',
      avatar: imgAvatar,
    },
    {
      key: 4,
      fullName: 'Ông Bùi Anh Dũng',
      position: 'CEO Google',
      desc:
        '“Dễ dàng theo dõi các dữ liệu,' +
        ' báo cáo ngay trên điện thoại là điểm tuyệt vời mà UniOffice đem lại cho chúng tôi”',
      avatar: imgAvatar,
    },
    {
      key: 5,
      fullName: 'Ông Bùi Anh Dũng',
      position: 'CEO Google',
      desc:
        '“Dễ dàng theo dõi các dữ liệu,' +
        ' báo cáo ngay trên điện thoại là điểm tuyệt vời mà UniOffice đem lại cho chúng tôi”',
      avatar: imgAvatar,
    },
    {
      key: 6,
      fullName: 'Ông Bùi Anh Dũng',
      position: 'CEO Google',
      desc:
        '“Dễ dàng theo dõi các dữ liệu,' +
        ' báo cáo ngay trên điện thoại là điểm tuyệt vời mà UniOffice đem lại cho chúng tôi”',
      avatar: imgAvatar,
    },
    {
      key: 7,
      fullName: 'Ông Bùi Anh Dũng',
      position: 'CEO Google',
      desc:
        '“Dễ dàng theo dõi các dữ liệu,' +
        ' báo cáo ngay trên điện thoại là điểm tuyệt vời mà UniOffice đem lại cho chúng tôi”',
      avatar: imgAvatar,
    },
  ];

  const pagination = {
    clickable: true,
  };

  const BuildSlide = React.useMemo(
    () => (
      <Swiper
        modules={[Pagination]}
        pagination={pagination}
        spaceBetween={50}
        breakpoints={{
          450: {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          475: {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          640: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 2,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 50,
          },
        }}
      >
        {listSlide.map((slide) => (
          <SwiperSlide key={slide.key}>
            <div className="slide-user-item">
              <img className="user-item-detail" src={slide.avatar} alt="" />
              <h3 className="user-item-detail">{slide.fullName}</h3>
              <h5 className="user-item-detail">{slide.position}</h5>
              <div className="divider-blue-small" />
              <blockquote className="user-item-detail">{slide.desc}</blockquote>
              <div className="icon-blockquote">
                <svg
                  width="36"
                  height="26"
                  viewBox="0 0 36 26"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M28.125 16.5C26.5675 16.5 25.0449 16.0381 23.7499 
                        15.1728C22.4549 14.3075 21.4455 13.0776 20.8495 11.6386C20.2534 
                        10.1997 20.0975 8.61627 20.4013 7.08867C20.7052 5.56107 21.4552 
                        4.15788 22.5565 3.05654C23.6579 1.9552 25.0611 1.20518 
                        26.5887 0.901319C28.1163 0.597461 29.6997 0.753412 31.1386 1.34945C32.5776 1.94549 33.8075
                        2.95485 34.6728 4.24989C35.5381 5.54493 36 7.06748 36 8.625L36.036 9.75C36.036 
                        11.8183 35.6286 13.8664 34.8371 15.7773C34.0456 17.6881 32.8855 19.4244 31.4229 
                        20.8869C29.9604 22.3495 28.2241 23.5096 26.3133 24.3011C24.4024 25.0926 22.3543 
                        25.5 20.286 25.5V21C21.764 21.004 23.2282 20.7149 24.5938 20.1495C25.9594 19.5841
                        27.1994 18.7536 28.242 17.706C28.6472 17.3015 29.0211 16.8668 29.3603
                        16.4055C28.9516 16.4697 28.5386 16.5021 28.125 16.5023V16.5ZM7.875
                        16.5C6.31748 16.5 4.79492 16.0381 3.49989 15.1728C2.20485 14.3075 1.19549 
                        13.0776 0.599452 11.6386C0.00341217 10.1997 -0.152539 8.61627 0.151319 
                        7.08867C0.455178 5.56107 1.2052 4.15788 2.30654 3.05654C3.40788 1.9552 4.81107
                        1.20518 6.33867 0.901319C7.86627 0.597461 9.44967 0.753412 10.8886 1.34945C12.3276
                        1.94549 13.5575 2.95485 14.4228 4.24989C15.2881 5.54493 15.75 7.06748 15.75 8.625L15.786
                        9.75C15.786 13.9272 14.1266 17.9332 11.1729 20.8869C8.21924 23.8406 4.21316 25.5 
                        0.0360035 25.5V21C1.51402 21.004 2.97817 20.7149 4.34378 20.1495C5.70939 19.5841
                        6.94936 18.7536 7.992 17.706C8.39724 17.3015 8.77107 16.8668 9.11025 16.4055C8.70163
                        16.4697 8.28864 16.5021 7.875 16.5023V16.5Z"
                    fill="#F3F9FF"
                  />
                </svg>
              </div>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    ),
    [listSlide],
  );
  return (
    <section className="bizzone-about">
      <div className="bizzone-about-wrap">
        <h3 className="bizzone-process-detail text-center">Khách hàng nói gì về chúng tôi</h3>
        <div className="divider-gradient" />
        <div className="about-wrap-slide container-child">{BuildSlide}</div>
      </div>
    </section>
  );
}

export default RowUserAbout;
