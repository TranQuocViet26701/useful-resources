import React from 'react';
import RowBanking from './RowBanking';
import { useTranslation } from 'react-i18next';
import ModalAdvise from '@components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import RowFeatureDetail from './RowFeatureDetail';

import imgRight from '/assets/image/landingPage/bizzone-enterprise/img-illutration.png';

interface ServiceItemModel {
  id: number;
  desc: string;
}

function RowFeature() {
  const [modalShow, setModalShow] = React.useState(false);

  const { t } = useTranslation();

  const listService: ServiceItemModel[] = [
    {
      id: 0,
      desc: t('bizzoneLandingPage.rowFeature.listItem1'),
    },
    {
      id: 1,
      desc: t('bizzoneLandingPage.rowFeature.listItem2'),
    },
    {
      id: 2,
      desc: t('bizzoneLandingPage.rowFeature.listItem3'),
    },
    {
      id: 3,
      desc: t('bizzoneLandingPage.rowFeature.listItem4'),
    },
    {
      id: 4,
      desc: t('bizzoneLandingPage.rowFeature.listItem5'),
    },
    {
      id: 5,
      desc: t('bizzoneLandingPage.rowFeature.listItem6'),
    },
  ];

  return (
    <section className="bizzone-feature" id="feature">
      <div className="bizzone-feature-detail">
        <h1 className="feature-detail"> {t('bizzoneLandingPage.rowFeature.title')}</h1>
      </div>
      <div className="bizzone-feature-service container-child">
        <div className="feature-service-wrap template-grid-2">
          <div className="service-wrap_left">
            <h2 className="service-detail">{t('bizzoneLandingPage.rowFeature.subTitle')}</h2>
            <ul className="service-detail-list">
              {listService.map((service) => (
                <li key={service.id} className="detail-list-item">
                  <span className="detail-item"> {service.desc}</span>
                </li>
              ))}
            </ul>
            <button
              className="btn-advise margin-top-54"
              type="button"
              onClick={() => setModalShow(true)}
            >
              <span>{t('bizzoneLandingPage.rowFeature.contactBtn')}</span>
            </button>
          </div>
          <div className="service-wrap_right">
            <div className="wrap_right__image">
              <img src={imgRight} alt="feature" loading="eager" />
            </div>
          </div>
        </div>
      </div>
      <RowBanking />
      <RowFeatureDetail />
      <ModalAdvise show={modalShow} onHide={() => setModalShow(false)} />
    </section>
  );
}

export default RowFeature;
