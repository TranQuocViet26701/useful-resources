import React, { useState } from 'react';
import imgRight from '/assets/image/landingPage/ekyc/img-animation.png';
import iconArrowRight from '/assets/image/svg/icon-arrow-right-white-short.svg';
import ModalAdvise from '@components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';

import { useTranslation } from 'react-i18next';

function RowHeader() {
  const [showPopup, setShowPopup] = useState(false);

  const { t, ready } = useTranslation();

  return (
    <section className="ekyc-header" id="introduce">
      <div className="container-child">
        <div className="template-grid-2">
          <div className="ekyc-header-left">
            <h6 className="ekyc-header-title ekyc-sub-tag">eKYC by Unicloud</h6>
            <h2 className="ekyc-header-title">{ready && t('ekyc.header.title')}</h2>
            <p className="ekyc-service-detail">{ready && t('ekyc.header.subtitle')}</p>
            <div className="ekyc-group-btn d-flex align-items-center ">
              <a href="https://demo.unicloud.ai/" target="_blank" rel="noreferrer">
                <button type="button" className="btn-looking me-3">
                  <span>{ready && t('ekyc.header.button')}</span>
                </button>
              </a>
              <a href="#contact" target="_self">
                <button
                  type="button"
                  className="btn-contact-advise"
                  onClick={() => setShowPopup(true)}
                >
                  <span>{ready && t('ekyc.navbar.button')}</span>
                  <img
                    className="ekyc-icon-arrow"
                    src={iconArrowRight}
                    width={24}
                    height={24}
                    alt="icon arrow"
                  />
                </button>
              </a>
            </div>
          </div>

          <div className="ekyc-header-right">
            <img src={imgRight} alt="img auth" />
          </div>
        </div>
      </div>
      <ModalAdvise show={showPopup} onHide={() => setShowPopup(false)} />
    </section>
  );
}

export default RowHeader;
