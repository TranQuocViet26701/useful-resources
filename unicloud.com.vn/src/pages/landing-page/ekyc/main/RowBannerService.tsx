import React from 'react';
import iconEarth from '/assets/image/svg/icon-earth.svg';
import iconSecurity from '/assets/image/svg/icon-security-check.svg';
import iconUser from '/assets/image/svg/icon-user.svg';

import { useTranslation } from 'react-i18next';

function RowBannerService() {
  const { t, ready } = useTranslation();

  const listService: any[] = [
    {
      id: 1,
      title: ready ? t('ekyc.bannerservice.title1') : '',
      icon: iconEarth,
      desc: t('ekyc.bannerservice.subtitle1'),
    },
    {
      id: 2,
      title: ready ? t('ekyc.bannerservice.title2') : '',
      icon: iconSecurity,
      desc: ready ? t('ekyc.bannerservice.subtitle2') : '',
    },
    {
      id: 3,
      title: ready ? t('ekyc.bannerservice.title2') : '',
      icon: iconUser,
      desc: ready ? t('ekyc.bannerservice.subtitle3') : '',
    },
  ];
  return (
    <section className="ekyc-banner-service">
      <div className="container-child ekyc-banner-service-wrap template-grid-3-auto">
        {listService.map((service) => (
          <div className="ekyc-banner-service-item" key={service.id}>
            <img
              className="ekyc-banner-icon"
              src={service.icon}
              width={64}
              height={64}
              alt="icon"
            />
            <h4 className="ekyc-service-detail">{service.title}</h4>
            <span className="ekyc-service-detail">{service.desc}</span>
          </div>
        ))}
      </div>
    </section>
  );
}

export default RowBannerService;
