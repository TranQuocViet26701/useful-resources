import React from 'react';
import iconArrowRight from '/assets/image/svg/icon-arrow-right-white-short.svg';
import iconArrowRightBlue from '/assets/image/svg/icon-arrow-right-blue-short.svg';

import { useTranslation } from 'react-i18next';

function RowReadyLearn() {
  const { t, ready } = useTranslation();
  return (
    <section className="ekyc-ready-learn" id="support">
      <div className="container-child ekyc-ready-learn-wrap">
        <div className="ready-learn-group template-grid-3-auto template-grid-3-responsive-2">
          <div className="ready-to-learn">
            <h2 className="ekyc-service-detail">{ready && t('ekyc.readylearn.title1')}</h2>
            <p className="ekyc-service-detail">{ready && t('ekyc.readylearn.subtitle1')}</p>
            <a href="#contact">
              <button type="button" className="btn-contact-advise">
                <span>{ready && t('ekyc.navbar.button')}</span>
                <img
                  className="ekyc-icon-arrow"
                  src={iconArrowRight}
                  width={24}
                  height={24}
                  alt="icon arrow"
                />
              </button>
            </a>
          </div>
          <div className="learn-document">
            <h5 className="ekyc-service-detail">{ready && t('ekyc.readylearn.title2')}</h5>
            <span className="ekyc-service-detail">{ready && t('ekyc.readylearn.subtitle2')}</span>
            <a
              href="https://docs.unicloud.ai/ekyc/"
              target="_blank"
              rel="noreferrer"
              className="view-document"
            >
              <span className="ekyc-service-detail">
                {ready && t('ekyc.readylearn.document.button')}
              </span>
              <img className="ekyc-icon-arrow" src={iconArrowRightBlue} alt="" />
            </a>
          </div>
          <div className="version-web">
            <h5 className="ekyc-service-detail">{ready && t('ekyc.readylearn.title3')}</h5>
            <span className="ekyc-service-detail">{ready && t('ekyc.readylearn.subtitle3')}</span>
            <a
              href="https://demo.unicloud.ai/"
              target="_blank"
              rel="noreferrer"
              className="view-document"
            >
              <span className="ekyc-service-detail">
                {ready && t('ekyc.readylearn.free.button')}
              </span>
              <img className="ekyc-icon-arrow" src={iconArrowRightBlue} alt="" />
            </a>
          </div>
        </div>
      </div>
    </section>
  );
}

export default RowReadyLearn;
