import React, { useEffect, useState } from 'react';
import iconFintech from '/assets/image/landingPage/ekyc/icon/icon-fintech.svg';
import iconInsurance from '/assets/image/landingPage/ekyc/icon/icon-shield-check.svg';
import iconGovernment from '/assets/image/landingPage/ekyc/icon/icon-bank.svg';
import iconMedtech from '/assets/image/landingPage/ekyc/icon/icon-doctor.svg';
import iconECommerce from '/assets/image/landingPage/ekyc/icon/icon-shopping-cart.svg';
import iconTele from '/assets/image/landingPage/ekyc/icon/icon-wifi-alt.svg';
import { Pagination, Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import logoKLB from '/assets/image/landingPage/logo/logo-klb-white.svg';
import useWindowSize from '@src/hooks/useWindowsSize';
// import logoKSF from '/assets/image/landingPage/logo/logo-ksf-white.svg';
// import logoODE from '/assets/image/landingPage/logo/logo-ode-white.svg';
// import logoSmart from '/assets/image/landingPage/logo/logo-smart-construction-white.svg';
// import logoSunshine from '/assets/image/landingPage/logo/logo-sunshine-home-white.svg';

import { useTranslation } from 'react-i18next';

function BuildSlide() {
  const size = useWindowSize();
  const [isMobile, setIsMobile] = useState(false);

  const { t, ready } = useTranslation();

  useEffect(() => {
    if (size.width! >= 1126) {
      setIsMobile(false);
    } else {
      setIsMobile(true);
    }
  }, []);

  const listQuote: any[] = [
    {
      id: 1,
      desc: ready ? t('ekyc.application.desc1') : '',
      fullName: ready ? t('ekyc.application.fullName') : '',
      position: ready ? t('ekyc.application.position') : '',
      branchLogo: logoKLB,
    },
    {
      id: 2,
      desc: ready ? t('ekyc.application.desc2') : '',
      fullName: ready ? t('ekyc.application.fullName') : '',
      position: ready ? t('ekyc.application.position') : '',
      branchLogo: logoKLB,
    },
    {
      id: 3,
      desc: ready ? t('ekyc.application.desc3') : '',
      fullName: ready ? t('ekyc.application.fullName') : '',
      position: ready ? t('ekyc.application.position') : '',
      branchLogo: logoKLB,
    },
  ];

  return (
    <article className="container-child ekyc-slide-position">
      <Swiper
        spaceBetween={30}
        pagination={{
          clickable: true,
        }}
        navigation={!isMobile}
        modules={[Pagination, Navigation]}
        className="mySwiper"
      >
        {listQuote.map((quote) => (
          <SwiperSlide key={quote.id}>
            <div className="template-grid-1 max-width-1020">
              <div className="ekyc-quote-left">
                <blockquote>{quote.desc}</blockquote>
                <span className="quote-full-name">{quote.fullName}</span>
                <span className="quote-position">{quote.position}</span>
              </div>
              {/* <div className="ekyc-quote-right">
                <img src={quote.branchLogo} alt="logo branch" />
              </div> */}
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </article>
  );
}

function RowCommonApplication() {
  const { t, ready } = useTranslation();
  const listService: any[] = [
    {
      id: 1,
      title: 'Fintech',
      icon: iconFintech,
    },
    {
      id: 2,
      title: 'Insurance',
      icon: iconInsurance,
    },
    {
      id: 3,
      title: 'Government',
      icon: iconGovernment,
    },
    {
      id: 4,
      title: 'Medtech',
      icon: iconMedtech,
    },
    {
      id: 5,
      title: 'E-Commerce',
      icon: iconECommerce,
    },
    {
      id: 6,
      title: 'Telecommunication',
      icon: iconTele,
    },
  ];
  return (
    <section className="ekyc-common" id="integration">
      <div className="container-child ekyc-common-wrap">
        <div className="template-grid-2 ekyc-common-group">
          <div className="ekyc-service-text ekyc-common-left">
            <h2 className="ekyc-service-detail">{ready && t('ekyc.application.title')}</h2>
            <p className="ekyc-service-detail">{ready && t('ekyc.application.subtitle')}</p>
          </div>

          <div className="ekyc-common-right template-grid-2">
            {listService.map((service) => (
              <div className="ekyc-common-item" key={service.id}>
                <img
                  className="ekyc-banner-icon"
                  src={service.icon}
                  width={28}
                  height={28}
                  alt="icon"
                />
                <h4 className="ekyc-service-detail">{service.title}</h4>
              </div>
            ))}
          </div>
        </div>
      </div>
      <BuildSlide />
    </section>
  );
}

export default RowCommonApplication;
