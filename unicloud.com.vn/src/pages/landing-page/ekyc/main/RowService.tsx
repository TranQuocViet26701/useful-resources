import React from 'react';

import imgRight from '/assets/image/landingPage/ekyc/img-passport-animation.png';
import imgDocument from '/assets/image/landingPage/ekyc/img-document.png';
import imgLeft from '/assets/image/landingPage/ekyc/img-left-group.png';

import { useTranslation } from 'react-i18next';

interface ServiceModel {
  id: number;
  title: string;
  desc: string;
  link: string;
  image: string;
}

function RowService() {
  const { t, ready } = useTranslation();

  const listService: ServiceModel[] = [
    {
      id: 0,
      title: ready ? t('ekyc.service.title1') : '',
      desc: ready ? t('ekyc.service.subtitle1') : '',
      link: 'https://demo.unicloud.ai/',
      image: imgRight,
    },
    {
      id: 1,
      title: ready ? t('ekyc.service.title2') : '',
      desc: ready ? t('ekyc.service.subtitle2') : '',
      link: 'https://demo.unicloud.ai/',
      image: imgLeft,
    },
    {
      id: 2,
      title: ready ? t('ekyc.service.title3') : '',
      desc: ready ? t('ekyc.service.subtitle3') : '',
      link: 'https://demo.unicloud.ai/',
      image: imgDocument,
    },
  ];

  return (
    <section className="ekyc-service" id="feature">
      <div className="ekyc-service-wrap container-child">
        {listService.map((service) => (
          <div
            key={service.id}
            className={`template-grid-2 ekyc-service-group 
          ${service.id % 2 === 0 ? '' : 'ekyc-service-revert'}`}
          >
            <div className="ekyc-service-left ekyc-service-text">
              <h2 className="ekyc-service-detail">{service.title}</h2>
              <p className="ekyc-service-detail">{service.desc}</p>
              <a href={service.link} target="_blank" rel="noreferrer">
                <button type="button" className="btn-looking">
                  <span>{ready && t('ekyc.service.button')}</span>
                </button>
              </a>
            </div>
            <div className="ekyc-service-right">
              <img src={service.image} alt="img document" />
            </div>
          </div>
        ))}
      </div>
    </section>
  );
}

export default RowService;
