import * as React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay } from 'swiper';
import logoKLB from '/assets/image/landingPage/logo/logo-klb-white.svg';
import logoKSF from '/assets/image/landingPage/logo/logo-ksf-white.svg';
import logoODE from '/assets/image/landingPage/logo/logo-ode-white.svg';
import logoSmart from '/assets/image/landingPage/logo/logo-smart-construction-white.svg';
import logoSunshine from '/assets/image/landingPage/logo/logo-sunshine-home-white.svg';

import logoKLBColor from '/assets/image/landingPage/logo/logo-klb-color.svg';
import logoKSFColor from '/assets/image/landingPage/logo/logo-ksf-color.svg';
import logoODEColor from '/assets/image/landingPage/logo/logo-ode-color.svg';
import logoSmartColor from '/assets/image/landingPage/logo/logo-smart-color.svg';
import logoSunshineColor from '/assets/image/landingPage/logo/logo-sunshine-home-color.svg';

function RowBanner() {
  const listSlide: any[] = [
    {
      id: 1,
      img: logoKLB,
      imgColor: logoKLBColor,
      url: 'https://kienlongbank.com/',
      isActive: false,
    },
    {
      id: 2,
      img: logoKSF,
      imgColor: logoKSFColor,
      url: 'https://ksfinance.vn/en/',
      isActive: false,
    },
    {
      id: 3,
      img: logoODE,
      imgColor: logoODEColor,
      url: 'https://ode.vn/',
      isActive: false,
    },
    {
      id: 4,
      img: logoSmart,
      imgColor: logoSmartColor,
      url: 'https://scgr.vn/',
      isActive: false,
    },
    {
      id: 5,
      img: logoSunshine,
      imgColor: logoSunshineColor,
      url: 'https://ssh.vn/',
      isActive: false,
    },
  ];

  const [indexActive, setIndexActive] = React.useState(0);

  const BuildSlide = React.useMemo(
    () => (
      <Swiper
        modules={[Autoplay]}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        breakpoints={{
          475: {
            slidesPerView: 3,
            spaceBetween: 20,
          },
          640: {
            slidesPerView: 3,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 4,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 5,
            spaceBetween: 54,
          },
        }}
      >
        {listSlide.map((slide) => (
          <SwiperSlide key={slide.id}>
            <a href={slide.url} target="_blank" rel="noopener noreferrer">
              <img
                onMouseMove={() => setIndexActive(slide.id)}
                onMouseLeave={() => setIndexActive(0)}
                src={indexActive === slide.id ? slide.imgColor : slide.img}
                className="swiper-ekyc-banner"
                alt=""
              />
            </a>
          </SwiperSlide>
        ))}
      </Swiper>
    ),
    [listSlide],
  );
  return (
    <section className="ekyc-banner">
      <div className="ekyc-banner-slide">
        <div className="container-child">{BuildSlide}</div>
      </div>
    </section>
  );
}

export default RowBanner;
