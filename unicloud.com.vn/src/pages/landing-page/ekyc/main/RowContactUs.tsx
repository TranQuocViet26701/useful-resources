import FormContact from '@src/components/formContact';
import * as React from 'react';

import { useTranslation } from 'react-i18next';

function RowContactUs() {
  const { t, ready } = useTranslation();
  return (
    <section className="ekyc-contact-us" id="contact">
      <div className="container-child ekyc-contact-us-wrap">
        <h2 className="ekyc-service-detail text-center">{ready && t('ekyc.contactus.title')}</h2>
        <span className="ekyc-service-detail text-center">
          {ready && t('ekyc.contactus.subtitle')}
        </span>
      </div>
      <div className="ekyc-contact-tag">
        <FormContact />
      </div>
    </section>
  );
}

export default RowContactUs;
