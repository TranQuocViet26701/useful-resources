import ModalAdvise from '@components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import useWindowSize from '@src/hooks/useWindowsSize';
import i18next from 'i18next';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Container, Dropdown, Image, Nav, Navbar, Offcanvas } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { NavLink, useLocation, useNavigate } from 'react-router-dom';
import './navbar-ekyc.scss';
import logoColor from '/assets/image/svg/logo-scroll.svg';

interface MenuModel {
  id: number;
  title: string;
  link: string;
  isActive: boolean;
  elementId: string;
}

interface NavbarChildProps {
  id: string;
  children: JSX.Element | JSX.Element[];
  isMobile: boolean;
  show: boolean;
  onShow: () => void;
  onHide: () => void;
}

const convertHash = (hash: string) => {
  const hashNew = `#${hash}`;
  return hashNew;
};

const NavbarChild = ({ id, isMobile, show, onShow, onHide, children }: NavbarChildProps) => {
  if (!isMobile) {
    return <Navbar.Collapse id={id}>{children}</Navbar.Collapse>;
  }

  const location = useLocation();

  useEffect(() => {
    onHide();
  }, [location]);

  return (
    <Navbar.Offcanvas show={show} onShow={onShow} onHide={onHide} id={id}>
      <Offcanvas.Header closeButton>
        <Offcanvas.Title id="offcanvasNavbarLabel">
          <Navbar.Brand href="/">
            <img
              src={logoColor}
              height="64"
              className="d-inline-block align-top"
              alt="Unicloud Logo"
            />
          </Navbar.Brand>
        </Offcanvas.Title>
      </Offcanvas.Header>
      {children}
    </Navbar.Offcanvas>
  );
};

const NavBarEkyc = React.memo(() => {
  const navigate = useNavigate();
  const [showPopup, setShowPopup] = useState(false);

  const location = useLocation();
  const size = useWindowSize();
  const { t, ready, i18n } = useTranslation();
  const [show, setShow] = useState(false);
  const onShow = () => setShow(true);

  const onHide = () => setShow(false);

  const isMobile = useMemo(() => {
    if (size.width && size.width >= 992) {
      return false;
    }
    return true;
  }, [size.width]);

  const changeLanguage = useCallback(
    (lng: string | null) => {
      if (lng && lng !== i18n.language) {
        i18n.changeLanguage(lng);
        const { pathname: path, search, hash } = location;
        navigate(`/${lng}${path.slice(3)}${search}${hash}`, { replace: true });
      }
    },
    [i18n.language, location],
  );

  const languageElement = useMemo(() => {
    const { languages, language } = i18n;
    const sortedLanguages = languages && [...languages];

    const flagPath = (locale: string) => `/flag-${locale}.svg`;
    return (
      <Dropdown className={`d-flex ${!isMobile ? '' : 'flex-column'}`} onSelect={changeLanguage}>
        <Dropdown.Toggle
          variant="none"
          id="dropdown-basic"
          className="box-shadow-unset btn-search-home btn-lang ms-auto"
        >
          {language?.toUpperCase()} &nbsp;&nbsp;&nbsp;
          <Image src={flagPath(language)} alt="icon flag" />
        </Dropdown.Toggle>
        <Dropdown.Menu align="end">
          {sortedLanguages
            ?.sort((a: string, b: string) => b.localeCompare(a))
            .map((locale) => {
              const fixedT = i18next.getFixedT(locale, 'translation', 'navbar.navLink');
              return (
                <Dropdown.Item eventKey={locale} key={locale} active={locale === language}>
                  <Image src={flagPath(locale)} alt="" />
                  <span className="text-dropdown-lang">
                    {ready && fixedT(language === 'en' ? 'languageEn' : 'languageVi').toString()}
                  </span>
                </Dropdown.Item>
              );
            })}
        </Dropdown.Menu>
      </Dropdown>
    );
  }, [isMobile, changeLanguage, i18n.language, ready]);

  const listMenu: MenuModel[] = [
    {
      id: 5,
      title: 'navbar.ekyc.home',
      link: '/',
      isActive: true,
      elementId: '',
    },
    {
      id: 0,
      title: 'navbar.ekyc.introduce',
      link: '',
      isActive: true,
      elementId: 'introduce',
    },
    {
      id: 1,
      title: 'navbar.ekyc.feature',
      link: '',
      isActive: false,
      elementId: 'feature',
    },
    {
      id: 2,
      title: 'navbar.ekyc.integration',
      link: '',
      isActive: false,
      elementId: 'integration',
    },
    {
      id: 3,
      title: 'navbar.ekyc.support',
      link: '',
      isActive: false,
      elementId: 'support',
    },
    {
      id: 4,
      title: 'navbar.ekyc.contact',
      link: '',
      isActive: false,
      elementId: 'contact',
    },
  ];

  const navItems = useMemo(() => {
    return (
      <>
        <Nav className="mx-auto navbar-bizzone-wrap" navbarScroll key="nav-item">
          {listMenu.map((menu) =>
            menu.link ? (
              <NavLink key={menu.id} className="nav-link-bizzone nav-link-ekyc" to={menu.link}>
                {ready && t(`${menu.title}`)}
              </NavLink>
            ) : (
              <a
                key={menu.id}
                className={`nav-link-bizzone nav-link-ekyc ${
                  location.hash === convertHash(menu.elementId) ? 'active' : null
                }`}
                href={`#${menu.elementId}`}
              >
                {ready && t(`${menu.title}`)}
              </a>
            ),
          )}
        </Nav>
        <Nav bsPrefix="custom-action-nav" key="nav-actions">
          {!isMobile && (
            <button type="button" className="btn-cost" onClick={() => setShowPopup(true)}>
              <span>{ready && t('ekyc.navbar.button')}</span>
            </button>
          )}
        </Nav>
        <Nav bsPrefix="custom-action-nav" key="nav-actions">
          {languageElement}
        </Nav>
      </>
    );
  }, [listMenu, location.hash]);

  return (
    <Navbar
      as="header"
      role="banner"
      expand="lg"
      className="background-sticky box-shadow-sticky-on"
      fixed="top"
      collapseOnSelect
      id="nav-bar-id"
    >
      <Container className="container-child menu-desktop">
        <Navbar.Brand href="/">
          <img
            src={logoColor}
            height="64"
            className="logo-menu d-inline-block align-top"
            alt="Unicloud Logo"
            id="logo-menu"
          />
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="offcanvasNavbar"
          bsPrefix="custom-navbar-toggle"
          onClick={onShow}
        />
        <NavbarChild
          id="offcanvasNavbar"
          isMobile={isMobile}
          show={show}
          onShow={onShow}
          onHide={onHide}
        >
          <Offcanvas.Body style={{ flexGrow: 1, overflowY: 'visible' }} className="navbar-bizzone">
            {navItems}
          </Offcanvas.Body>
        </NavbarChild>
      </Container>
      <ModalAdvise show={showPopup} onHide={() => setShowPopup(false)} />
    </Navbar>
  );
});

export default NavBarEkyc;
