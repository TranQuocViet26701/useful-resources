import * as React from 'react';
import Footer from '@src/layouts/footer/Footer';
import imgRight from '/assets/image/landingPage/ekyc/img-animation.png';
import SEO from '@src/seo/seo';
import { useLocation } from 'react-router-dom';

import './style.scss';
import NavBarEkyc from './navbar-ekyc/navbar-ekyc';
import RowBanner from './main/RowBanner';
import RowService from './main/RowService';
import RowBannerService from './main/RowBannerService';
import RowCommonApplication from './main/RowCommonApplication';
import RowReadyLearn from './main/RowReadyLearn';
import RowContactUs from './main/RowContactUs';
import RowHeader from './main/RowHeader';

function EkycPage() {
  const location = useLocation();
  return (
    <>
      <SEO
        title="Định danh điện tử | eKYC"
        description="eKYC được phát triển bởi Unicloud mang lại trải nghiệm xác thực nhanh chóng và dễ dàng."
        imgthumbs={`https://unicloud.com.vn${imgRight}`}
        url={location.pathname}
      />
      <NavBarEkyc />
      <main className="ekyc-page">
        <RowHeader />
        <RowBanner />
        <RowService />
        <RowBannerService />
        <RowCommonApplication />
        <RowReadyLearn />
        <RowContactUs />
      </main>
      <Footer />
    </>
  );
}

export default EkycPage;
