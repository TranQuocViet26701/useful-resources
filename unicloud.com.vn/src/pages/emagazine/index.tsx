import React, { useEffect, useState } from 'react';
import QueryPostEMagazine from '@src/query/queryPostEmagazine';
import SEO from '@src/seo/seo';
import { Link, Navigate } from 'react-router-dom';
import logoEma from '/assets/image/navbar/emagazine.png';
import iconBack from 'assets/image/svg/arrow-left-white-slide-digital.svg';
import { createGlobalStyle } from 'styled-components';
import './style.scss';

interface GroupSeoModel {
  title?: string;
  description?: string;
  link?: string;
  imagethumbs?: {
    sourceUrl: string;
    altText: string;
  };
  keywords?: string;
  stylecss?: string;
}

interface GroupNewModel {
  stylecss: string;
}

function EmagazineDetail() {
  const { data, loading, error } = QueryPostEMagazine();
  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    if (typeof window !== undefined) {
      if (window.innerWidth > 768) {
        setIsMobile(true);
      }
    }
  }, [isMobile]);
  if (loading) return <div>...</div>;
  if (error) return <Navigate to="/coming-soon" />;
  if (!data?.post) return <Navigate to="/coming-soon" />;
  const groupSeo: GroupSeoModel = data?.post?.groupseo;
  const groupNew: GroupNewModel = data?.post?.groupnews;

  const { title, description, link, imagethumbs, keywords } = groupSeo;

  const { stylecss } = groupNew;

  const GlobalStyles = createGlobalStyle`
  ${stylecss?.replace(/(\r\n|\n|\r)/gm, '')}
`;

  return (
    <>
      {title && description && link && imagethumbs && (
        <SEO
          title={title}
          description={description}
          imgthumbs={imagethumbs?.sourceUrl}
          url={link}
          keywords={keywords}
        />
      )}
      <GlobalStyles />
      <header className="menu-top-ema">
        <Link className="btn-back" to="/news">
          <img src={iconBack} alt="icon back" />
        </Link>
        <img className="img-top-ema" src={logoEma} alt="logo emagazine" />
      </header>
      <main className="emagazine-body">
        <div
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{
            __html: isMobile ? data?.post?.content : data?.post?.groupnews?.postmobile,
          }}
          className="emagazine-details"
        />
      </main>
    </>
  );
}

export default EmagazineDetail;
