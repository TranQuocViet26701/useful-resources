import * as React from 'react';
import imgComingSoon from '/assets/image/png/img-coming-soon.png';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import SEO from '@src/seo/seo';
import CustomNav from '../../layouts/navbar/Navbar';
import Footer from '../../layouts/footer/Footer';
import '../not-found/style.scss';
import './style.scss';

function NotFound() {
  const navigate = useNavigate();
  const { t } = useTranslation();
  return (
    <>
      <SEO title="Coming soon" />
      <CustomNav />
      <section className="container-child not-found">
        <img src={imgComingSoon} alt="not found" className="img-not-found" />
        <h3>{t('comingSoon.title')}</h3>
        <span className="desc-404 desc-coming-soon">{t('comingSoon.description')}</span>
        <button type="button" className="btn-home" onClick={() => navigate('/')}>
          <span>{t('comingSoon.home')}</span>
        </button>
      </section>
      <Footer />
    </>
  );
}

export default NotFound;
