import React from 'react';
import {
  // Accordion,
  Row,
  Col,
  Container,
  Form,
  FormControl,
  InputGroup,
  Image,
} from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import axios from 'axios';
import { useLocation } from 'react-router-dom';
import iconName from '/assets/image/svg/icon-name-contact-form.svg';
import iconTelephone from '/assets/image/svg/icon-telephone-contact-form.svg';
import iconEmail from '/assets/image/svg/icon-email-contact-form.svg';
import iconBusiness from '/assets/image/svg/icon-business.svg';
import iconSupport from '/assets/image/svg/icon-support.svg';
import iconMedia from '/assets/image/svg/icon-media.svg';
import '../Contact.scss';

function RowContact() {
  const { t, ready } = useTranslation();
  const location = useLocation();
  const googleSheetAPI =
    'https://script.google.com/macros/s/AKfycbxTlQD1WgGBuFKoKVjf6tiUGERX6DHHhxJfywGZ6R4xuNEurMdCdW4fbRoZwBW4jK6M/exec';
  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm();
  const onSubmit = (data: any) => {
    const formData = new FormData();
    formData.append('name', data.fullName);
    formData.append('phone', data.telephone);
    formData.append('content', data.content);
    formData.append('email', data.email);
    formData.append('boolean', 'true');
    formData.append('list', '6xdCd892x7gSZoG7768926aeLA');
    formData.append('subform', 'yes');

    const googleSheetFormData = new FormData();
    googleSheetFormData.append('fullName', data.fullName);
    googleSheetFormData.append('email', data.email);
    googleSheetFormData.append('telephone', data.telephone);
    googleSheetFormData.append('company', '');
    googleSheetFormData.append('content', data.content);
    googleSheetFormData.append('timestamp', new Date().toLocaleDateString().substring(0, 10));
    googleSheetFormData.append('linkedBy', location.pathname);

    axios
      .post('/subscribe', formData)
      .then(() => {
        // console.log('response: ', response.data);
        // console.log('response.status: ', response.status);
        // console.log('response.data: ', response.data);
      })
      .catch(() => {
        // console.error('Something went wrong!', error);
        // alert('Lỗi hệ thống.');
      });

    axios
      .post(googleSheetAPI, googleSheetFormData)
      .then(() => {
        // console.log('response: ', response.data);
        // console.log('response.status: ', response.status);
        // console.log('response: ', response.data);
        //
      })
      .catch(() => {
        // console.error('Something went wrong!', error);
        // alert('Lỗi hệ thống.');
      });

    reset();
  };
  return (
    <div className="back-ground-contact">
      <Container className="max-width-1180 padding-left-right">
        <Row className="gy-5 gx-5">
          <Col lg={6} className="col-item-digital-smart-city">
            <h3 className="title-category-news">{ready && t('contact.titleLeft')}</h3>
            <div className="div-under-category" />
            <div className="benefits-accordion help-list">
              <div className="help-item">
                <div className="help-item-img">
                  <Image src={iconBusiness} style={{ width: 60, height: 60 }} />
                </div>
                <div className="help-item-content">
                  <h4 className="help-item-title">{ready && t('contact.helpItem1')}</h4>
                  <span className="help-item-mail">sales@unicloud.com.vn</span>
                </div>
              </div>
              <div className="help-item">
                <div className="help-item-img">
                  <Image src={iconSupport} style={{ width: 60, height: 60 }} />
                </div>
                <div className="help-item-content">
                  <h4 className="help-item-title">{ready && t('contact.helpItem2')}</h4>
                  <span className="help-item-mail">supports@unicloud.com.vn</span>
                </div>
              </div>
              <div className="help-item">
                <div className="help-item-img">
                  <Image src={iconMedia} style={{ width: 60, height: 60 }} />
                </div>
                <div className="help-item-content">
                  <h4 className="help-item-title">{ready && t('contact.helpItem3')}</h4>
                  <span className="help-item-mail">media@unicloud.com.vn</span>
                </div>
              </div>
            </div>
          </Col>
          <Col lg={6} className="col-item-digital-smart-city order-first order-lg-last">
            <h3 className="title-category-news">{ready && t('contact.titleRight')}</h3>
            <div className="div-under-category" />
            <Form onSubmit={handleSubmit(onSubmit)} onReset={reset} className="form-contact">
              <Form.Group controlId="validationCustomFullName">
                <InputGroup className="mb-3">
                  <InputGroup.Text className="group-text-contact">
                    <img src={iconName} alt="icon-name" />
                  </InputGroup.Text>
                  <Controller
                    control={control}
                    name="fullName"
                    defaultValue=""
                    rules={{ required: 'Chưa nhập họ tên' }}
                    render={({ field: { onChange, value, ref } }) => (
                      <FormControl
                        onChange={onChange}
                        value={value}
                        ref={ref}
                        isInvalid={errors.fullName}
                        className="fullName"
                        placeholder={ready ? t('contact.fullName') : ''}
                        aria-label="fullName"
                        aria-describedby="fullName"
                        autoComplete="off"
                        // required
                      />
                    )}
                  />
                  <Form.Control.Feedback type="invalid" className="mx-3">
                    {Object.keys(errors).length !== 0 && errors.fullName?.type === 'required' && (
                      <span style={{ color: 'red' }}>{errors.fullName?.message}</span>
                    )}
                  </Form.Control.Feedback>
                </InputGroup>
              </Form.Group>
              <Form.Group controlId="validationCustomTelephone">
                <InputGroup className="mb-3">
                  <InputGroup.Text className="group-text-contact">
                    <img src={iconTelephone} alt="icon-telephone" />
                  </InputGroup.Text>
                  <Controller
                    control={control}
                    name="telephone"
                    defaultValue=""
                    rules={{
                      required: 'Chưa nhập số điện thoại',
                      pattern: {
                        value: /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/,
                        message: 'Số điện thoại không đúng',
                      },
                    }}
                    render={({ field: { onChange, value, ref } }) => (
                      <FormControl
                        onChange={onChange}
                        value={value}
                        ref={ref}
                        isInvalid={errors.telephone}
                        className="telephone"
                        placeholder={ready ? t('contact.telephone') : ''}
                        aria-label="telephone"
                        aria-describedby="telephone"
                        autoComplete="off"
                        // required
                      />
                    )}
                  />
                  <Form.Control.Feedback type="invalid" className="mx-3">
                    {Object.keys(errors).length !== 0 && errors.telephone?.type === 'pattern' && (
                      <span>{errors.telephone?.message}</span>
                    )}
                    {Object.keys(errors).length !== 0 && errors.telephone?.type === 'required' && (
                      <span>{errors.telephone?.message}</span>
                    )}
                  </Form.Control.Feedback>
                </InputGroup>
              </Form.Group>
              <Form.Group controlId="validationCustomEmail">
                <InputGroup className="mb-3">
                  <InputGroup.Text className="group-text-contact">
                    <img src={iconEmail} alt="icon-email" />
                  </InputGroup.Text>
                  <Controller
                    control={control}
                    name="email"
                    defaultValue=""
                    rules={{
                      required: 'Chưa nhập Email',
                      pattern: {
                        value: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                        message: 'Vui lòng nhập đúng Email',
                      },
                    }}
                    render={({ field: { onChange, value, ref } }) => (
                      <FormControl
                        onChange={onChange}
                        value={value}
                        ref={ref}
                        isInvalid={errors.email}
                        className="email"
                        placeholder={ready ? t('contact.email') : ''}
                        aria-label="email"
                        aria-describedby="email"
                        style={{ backgroundColor: '#f3f9ff' }}
                        autoComplete="off"
                        // required
                      />
                    )}
                  />
                  <Form.Control.Feedback type="invalid" className="mx-3">
                    {Object.keys(errors).length !== 0 && errors.email?.type === 'pattern' && (
                      <span>{errors.email?.message}</span>
                    )}
                    {Object.keys(errors).length !== 0 && errors.email?.type === 'required' && (
                      <span>{errors.email?.message}</span>
                    )}
                  </Form.Control.Feedback>
                </InputGroup>
              </Form.Group>
              <Form.Group className="mb-3" controlId="validationCustomTextarea">
                <Controller
                  control={control}
                  name="content"
                  defaultValue=""
                  render={({ field: { onChange, value, ref } }) => (
                    <Form.Control
                      onChange={onChange}
                      value={value}
                      ref={ref}
                      isInvalid={errors.content}
                      className="content"
                      as="textarea"
                      rows={12}
                      placeholder={ready ? t('contact.content') : ''}
                      autoComplete="off"
                    />
                  )}
                />
              </Form.Group>
              <Controller
                control={control}
                name="boolean"
                defaultValue=""
                render={() => <input type="hidden" name="boolean" value="true" />}
              />
              <Controller
                control={control}
                name="hidden2"
                defaultValue=""
                render={() => (
                  <input type="hidden" name="list" value="6xdCd892x7gSZoG7768926aeLA" />
                )}
              />

              <Controller
                control={control}
                name="hidden3"
                defaultValue=""
                render={() => <input type="hidden" name="subform" value="yes" />}
              />
              <button className="btn-submit-contact" type="submit">
                {ready && t('contact.submit')}
              </button>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default RowContact;
