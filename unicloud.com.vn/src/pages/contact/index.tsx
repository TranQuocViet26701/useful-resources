import React from 'react';
import { useLocation } from 'react-router-dom';
import SEO from '@src/seo/seo';
import { Container } from 'react-bootstrap';
import RowContact from './main/RowContact';
import RowTopContact from './main/RowTopContact';

function Contact() {
  const location = useLocation();
  return (
    <main>
      <SEO
        title="Liên hệ | Unicloud Group"
        description="Liên hệ với chúng tôi | Unicloud Group"
        url={location.pathname}
      />
      <Container className="max-width-100 px-0" fluid>
        <RowTopContact />
        <RowContact />
      </Container>
    </main>
  );
}
export default Contact;
