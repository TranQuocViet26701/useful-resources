import React, { useEffect } from 'react';
import ModalVideo from '@src/components/modal/modal-video';
import iconPlay from '@assets/image/aboutUs/img-play-btn.png';

// function BuildArrowCarousel(props: { funcPrev: Function; funcNext: Function }) {
//   const { funcPrev, funcNext } = props;

//   return (
//     <div className="d-flex align-items-center carousel-cus navigation-cus">
//       <div
//         className="navigation-cus-left"
//         onClick={() => funcPrev()}
//         onKeyDown={() => funcPrev()}
//         tabIndex={0}
//         role="button"
//       />
//       <div
//         className="navigation-cus-right"
//         tabIndex={0}
//         role="button"
//         onClick={() => funcNext()}
//         onKeyDown={() => funcNext()}
//       />
//     </div>
//   );
// }

const getPos = (current: number, active: number) => {
  const diff = current - active;

  if (Math.abs(current - active) > 1) {
    return -current;
  }

  return diff;
};

const update = (ele: Element[], newActive: HTMLElement) => {
  const newActivePos: any = newActive.dataset.pos;

  const current = ele.find((elem) => (elem as HTMLElement).dataset.pos === '0') as Element;
  const prev = ele.find((elem) => (elem as HTMLElement).dataset.pos === '-1');
  const next = ele.find((elem) => (elem as HTMLElement).dataset.pos === '1');
  current.classList.remove('carousel-about__item_active');

  [current, prev, next].forEach((item) => {
    const itemPos: any = (item as HTMLElement).dataset.pos;

    (item as HTMLElement).dataset.pos = getPos(itemPos, newActivePos).toString();
  });
};

function RowSlideFive() {
  //   const [carouselItemsState, setCarouselItemsState] = useState<Element[]>();

  //   const handlePrev = useCallback(() => {
  //     const prev = carouselItemsState?.find(
  //       (elem) => (elem as HTMLElement).dataset.pos === '-1',
  //     ) as HTMLElement;
  //     prev.click();
  //   }, [carouselItemsState]);

  //   const handleNext = useCallback(() => {
  //     const next = carouselItemsState?.find(
  //       (elem) => (elem as HTMLElement).dataset.pos === '1',
  //     ) as HTMLElement;
  //     next.click();
  //   }, [carouselItemsState]);

  useEffect(() => {
    const carouselList = document.querySelector('.carousel-about');
    const carouselItems = document.querySelectorAll('.carousel-about__item');
    const elements = Array.from(carouselItems);
    // setCarouselItemsState(elements);
    carouselList?.addEventListener('click', (event) => {
      const newActive = event.target as HTMLElement;
      if (newActive.getAttribute('data-pos')) {
        const isItem = newActive.closest('.carousel-about__item');
        if (!isItem || newActive.classList.contains('carousel-about__item_active')) {
          return;
        }
        update(elements, newActive);
      }
    });
  }, []);
  const [modalShow, setModalShow] = React.useState(false);
  const urlVideo =
    'https://data.unicloud.com.vn/wp-content/uploads/2022/04/nhamay-1643637711958829413607-a0a60.mp4';

  return (
    <section className="row section-four-about g-0">
      <div className="carousel-about">
        <div className="background-slider-element-five" />
        <ul className="carousel-about__list">
          <li className="carousel-about__item" data-pos="-1" />
          <div className="carousel-about__item" data-video="0" data-pos="0" data-index="0">
            <button className="btn-play" type="button" onClick={() => setModalShow(true)}>
              <img src={iconPlay} alt="" />
            </button>
          </div>
          <li className="carousel-about__item" data-pos="1" />
        </ul>
        {/* <BuildArrowCarousel funcPrev={handlePrev} funcNext={handleNext} /> */}
      </div>
      <ModalVideo show={modalShow} onHide={() => setModalShow(false)} urlvideo={urlVideo} />
    </section>
  );
}

export default RowSlideFive;
