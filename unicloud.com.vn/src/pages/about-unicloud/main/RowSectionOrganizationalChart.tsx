import React from 'react';
import { Container } from 'react-bootstrap';
import iconZoom from '/assets/image/svg/icon-zoom.svg';
import mapDiagram from '/assets/image/aboutUs/image-diagram-company.png';
import mapDiagramEn from '/assets/image/aboutUs/image-diagram-company-en.png';
import { useTranslation } from 'react-i18next';

function RowSectionOrganizationalChart() {
  const { t, ready, i18n } = useTranslation();
  return (
    <section className="organization-chart">
      <Container className="container-child organization-chart-wrapper">
        <h3 className="title mr-bottom-14">
          {ready && t('aboutUs.RowSectionOrganizationalChart.title')}
        </h3>
        <p className="description">
          {ready && t('aboutUs.RowSectionOrganizationalChart.content')}
          <br />
        </p>

        <div className="map-company">
          <img src={iconZoom} className="icon-zoom" alt="" />
          <img src={i18n.language === 'en' ? mapDiagramEn : mapDiagram} alt="sơ đồ công ty" />
        </div>
      </Container>
    </section>
  );
}

export default RowSectionOrganizationalChart;
