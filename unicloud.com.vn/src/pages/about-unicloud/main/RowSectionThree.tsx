import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import iconFly from '/assets/image/svg/icon-fly-blue.svg';
import iconValue from '/assets/image/svg/icon-value-blue.svg';
import iconSearch from '/assets/image/svg/icon-search-blue.svg';
import SectionPartnership from '@src/components/partnership/partnership';

function RowSectionValue() {
  const { t, ready } = useTranslation();
  return (
    <section>
      <div className="partnership-stick-mobile">
        <SectionPartnership background="#ffffff" />
      </div>
      <div className="core-value ">
        <div className="partner-about container-child">
          <SectionPartnership background="#ffffff" />
        </div>
        <Container className="container-child">
          <h3 className="text-white fw-bold text-center title-value-core">
            {ready && t('aboutUs.RowSectionValue.title')}
          </h3>
          <Row className="core-value-group align-items-center g-0 gx-lg-3 gy-md-5 gy-3">
            <Col lg={4} sm={12} className="core-value-group__item align-self-start">
              <Row className="g-0">
                <Col lg={12} sm={5}>
                  <div className="d-flex align-self-start justify-content-center ">
                    <span className="id-value-core">
                      {ready && t('aboutUs.RowSectionValue.01')}
                    </span>
                    <img src={iconFly} alt="" className="icon-background-white" />
                  </div>
                </Col>
                <Col lg={12} sm={7}>
                  <div className="core-value-group__content">
                    <h4 className="text-white fw-bold">
                      {ready && t('aboutUs.RowSectionValue.01.title')}
                    </h4>
                    <p className="fs-5 text-white">
                      <strong>{ready && t('aboutUs.RowSectionValue.subtitle')}</strong>
                      {ready && t('aboutUs.RowSectionValue.01.content')}
                    </p>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col lg={4} sm={12} className="core-value-group__item align-self-start">
              <Row className="g-0">
                <Col lg={12} sm={5}>
                  <div className="d-flex align-self-start justify-content-center">
                    <span className="id-value-core">
                      {ready && t('aboutUs.RowSectionValue.02')}
                    </span>
                    <img src={iconValue} alt="" className="icon-background-white" />
                  </div>
                </Col>
                <Col lg={12} sm={7}>
                  <div className="core-value-group__content">
                    <h4 className="text-white fw-bold">
                      {ready && t('aboutUs.RowSectionValue.02.title')}
                    </h4>
                    <p className="fs-5 text-white">
                      <strong>{ready && t('aboutUs.RowSectionValue.subtitle')}</strong>
                      {ready && t('aboutUs.RowSectionValue.02.content')}
                    </p>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col lg={4} sm={12} className="core-value-group__item align-self-start">
              <Row className="g-0">
                <Col lg={12} sm={5}>
                  <div className="d-flex align-self-start justify-content-center">
                    <span className="id-value-core">
                      {ready && t('aboutUs.RowSectionValue.03')}
                    </span>
                    <img src={iconSearch} alt="" className="icon-background-white" />
                  </div>
                </Col>
                <Col lg={12} sm={7}>
                  <div className="core-value-group__content">
                    <h4 className="text-white fw-bold">
                      {ready && t('aboutUs.RowSectionValue.03.title')}
                    </h4>
                    <p className="fs-5 text-white">
                      <strong>{ready && t('aboutUs.RowSectionValue.subtitle')}</strong>
                      {ready && t('aboutUs.RowSectionValue.03.content')}
                    </p>
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
    </section>
  );
}

export default RowSectionValue;
