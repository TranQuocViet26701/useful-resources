import React from 'react';
import { Col, Row } from 'react-bootstrap';
import img1 from '/assets/image/aboutUs/images-1.png';
import img2 from '/assets/image/aboutUs/images-2.png';
import img3 from '/assets/image/aboutUs/images-3.png';
import img4 from '/assets/image/aboutUs/images-4.png';
import imgTopLeft from '/assets/image/aboutUs/bg-stick-top-left.png';
import imgBottomRight from '/assets/image/aboutUs/bg-stick-bottom-right.png';
import { useTranslation } from 'react-i18next';

function RowSectionSix() {
  const { t, ready } = useTranslation();
  return (
    <section className="product-strategy">
      <img src={imgTopLeft} className="img-stick-top-left" alt="" />
      <img src={imgBottomRight} className="img-stick-bottom-right" alt="" />
      <div className="product-strategy-wrap container-child ">
        <h3 className="title mr-bottom-14">{ready && t('aboutUs.RowSectionSix.title1')}</h3>
        <p className="description max-width-674 mb-4">
          {ready && t('aboutUs.RowSectionSix.content1')}
        </p>
        <Row className="group-strategy g-0">
          <Col sm={12} lg={6} className="group-strategy-img">
            <img src={img1} alt="" />
          </Col>
          <Col sm={12} lg={6} className="group-strategy-description">
            <h2 className="fs-48">{ready && t('aboutUs.RowSectionSix.title2')}</h2>
            <p>{ready && t('aboutUs.RowSectionSix.content2')}</p>
          </Col>
        </Row>
        <Row className="group-strategy g-0">
          <Col sm={12} lg={6} className="group-strategy-img">
            <img src={img2} alt="" />
          </Col>
          <Col sm={12} lg={6} className="group-strategy-description">
            <h2 className="fs-48">{ready && t('aboutUs.RowSectionSix.title3')}</h2>
            <p>{ready && t('aboutUs.RowSectionSix.content3')}</p>
          </Col>
        </Row>
        <Row className="group-strategy g-0">
          <Col sm={12} lg={6} className="group-strategy-img">
            <img src={img3} alt="" />
          </Col>
          <Col sm={12} lg={6} className="group-strategy-description">
            <h2 className="fs-48">{ready && t('aboutUs.RowSectionSix.title4')}</h2>
            <p>{ready && t('aboutUs.RowSectionSix.content4')}</p>
          </Col>
        </Row>
        <Row className="group-strategy g-0">
          <Col sm={12} lg={6} className="group-strategy-img">
            <img src={img4} alt="" />
          </Col>
          <Col sm={12} lg={6} className="group-strategy-description">
            <h2 className="fs-48">{ready && t('aboutUs.RowSectionSix.title5')}</h2>
            <p>{ready && t('aboutUs.RowSectionSix.content5')}</p>
          </Col>
        </Row>
      </div>
    </section>
  );
}
export default RowSectionSix;
