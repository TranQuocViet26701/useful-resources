import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import imgElementLeft from '/assets/image/aboutUs/img-left-about.png';
import imgStick from '/assets/image/aboutUs/img-left-bottom.png';
import logoWhite from '/assets/image/logo/logo-uni-white.png';
import Breadcrumb from '@src/components/breadcrumb/Breadcrumb';

function RowDetailAboutUs() {
  const { t, ready } = useTranslation();
  const breadcrumbs: any[] = [
    {
      title: 'navbar.navLink.home',
      url: '/',
    },
    {
      title: 'navbar.navLink.aboutUs',
      url: '/about-us',
    },
  ];
  return (
    <section className="container-wrap about-detail">
      <Container className="container-child">
        <Breadcrumb listBreadCrumb={breadcrumbs} />
        <div className="text-unicloud">
          <img src={logoWhite} alt="" width="100%" />
        </div>
        <div className="group-item__img">
          <div className="item__img-bottom">
            <img src={imgStick} alt="" className="img-stick" />
            <Row className="img-bottom-content d-flex gx-0 gx-lg-3">
              <div className="bottom-content_left">
                <img src={imgElementLeft} alt="" />
              </div>
              <Col xl={8} lg={7} md={6} sm={12} className="bottom-content_right">
                <h3 className="text-gradient-origan fw-bold ">
                  {ready && t('aboutUs.RowDetailAboutUs.title1')}
                </h3>
                <div className="hr-cus" />
                <p className="description-about margin-bottom-52">
                  {ready && t('aboutUs.RowDetailAboutUs.content1')}
                </p>
                <h3 className="text-gradient-origan fw-bold fs-3">
                  {ready && t('aboutUs.RowDetailAboutUs.title2')}
                </h3>
                <div className="hr-cus" />
                <p className="description-about">
                  {ready && t('aboutUs.RowDetailAboutUs.content2')}
                </p>
              </Col>
            </Row>
          </div>
        </div>
      </Container>
    </section>
  );
}

export default RowDetailAboutUs;
