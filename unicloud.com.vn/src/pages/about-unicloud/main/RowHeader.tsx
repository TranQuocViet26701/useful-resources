import * as React from 'react';
import { useTranslation } from 'react-i18next';

function RowHeader() {
  const { t, ready } = useTranslation();
  return (
    <section>
      <div className="background-header-about-us background-image">
        <article className="popup-header text-center">
          <div className="btn-about background-gradient border-radius-8 ">
            <span className="text-white fw-normal">
              {ready && t('aboutUs.RowHeader.header.subTitle')}
            </span>
          </div>
          <h2 className="text-white my-3">{ready && t('aboutUs.RowHeader.header.title1')}</h2>
          <h3 className="text-white">{ready && t('aboutUs.RowHeader.header.title2')}</h3>
          <p className="fs-16 description-popup text-white my-3">
            {ready && t('aboutUs.RowHeader.header.content')}
          </p>
        </article>
        <div className="tangle-bottom-header background-image" />
      </div>
    </section>
  );
}

export default RowHeader;
