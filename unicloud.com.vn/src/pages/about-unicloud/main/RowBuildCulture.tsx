import * as React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import imgRight from '/assets/image/aboutUs/section-culture.png';
import iconDone from '/assets/image/svg/icon-done.svg';
import iconHelp from '/assets/image/svg/icon-help.svg';

function RowBuildCulture() {
  const { t, ready } = useTranslation();
  return (
    <section className="section-culture">
      <Container className="container-child">
        <Row className="culture-wrap gy-xl-0 gy-3 gy-md-5 justify-content-center">
          <Col md={12} lg={6} className="culture-wrap-left">
            <h3 className="text-gradient-origan fs-2 fw-bold">
              {ready && t('aboutUs.RowBuildCulture.title')}
            </h3>

            <div className="group-step-culture">
              <div className="d-flex justify-content-start">
                <img src={iconHelp} alt="" className="icon-culture" />
                <div className="step-culture-content">
                  <h4 className="fw-bold">{ready && t('aboutUs.RowBuildCulture.question1')}</h4>
                  <p className="fs-5 culture-content-desc">
                    {ready && t('aboutUs.RowBuildCulture.content1')}
                  </p>
                </div>
              </div>
              <div className="d-flex justify-content-start mt-4">
                <img src={iconDone} alt="" className="icon-culture" />
                <div className="step-culture-content">
                  <h4 className="fw-bold">{ready && t('aboutUs.RowBuildCulture.question2')}</h4>
                  <p className="fs-5 culture-content-desc">
                    {ready && t('aboutUs.RowBuildCulture.content2')}
                  </p>
                </div>
              </div>
            </div>
            <article className="quote-cus">
              <p className="text-gradient-origan fs-5">
                {ready && t('aboutUs.RowBuildCulture.sumary')}
              </p>
            </article>
          </Col>
          <Col md={12} lg={6} className="culture-wrap-right d-flex justify-content-center">
            <img src={imgRight} alt="" />
          </Col>
        </Row>
      </Container>
    </section>
  );
}

export default RowBuildCulture;
