import React from 'react';
import { useLocation } from 'react-router-dom';
import SEO from '@src/seo/seo';
import RowTechnology from '@components/technology/RowTechnology';
import RowBuildCulture from './main/RowBuildCulture';
import RowDetailAboutUs from './main/RowDetailAboutUs';
import RowHeader from './main/RowHeader';

import RowSectionOrganizationalChart from './main/RowSectionOrganizationalChart';
import RowSectionSix from './main/RowSectionSix';
import RowSectionValue from './main/RowSectionThree';
import RowSlideFive from './main/RowSlideFive';

import './style.scss';

function AboutUs() {
  const location = useLocation();
  return (
    <div className="about-unicloud">
      <SEO
        title="Về Unicloud"
        description="Unicloud Group nghiên cứu, phát triển, và cung cấp các sản phẩm,
        giải pháp công nghệ quan trọng cho hầu hết các lĩnh vực “xương sống” của nền kinh tế."
        url={location.pathname}
      />
      <RowHeader />
      <RowDetailAboutUs />
      <RowSectionValue />
      <RowBuildCulture />
      <RowSlideFive />
      <RowSectionSix />
      <RowSectionOrganizationalChart />
      <RowTechnology />
    </div>
  );
}

export default AboutUs;
