import SEO from '@src/seo/seo';
import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';
import './style.scss';

function PolicyTerm() {
  const location = useLocation();
  return (
    <section className="page-policy-terms container-child">
      <SEO
        title="Chính sách bảo mật thông tin"
        description="Chính sách bảo mật thông tin"
        url={location.pathname}
      />
      <Row className="row-main-post-details d-flex justify-content-center">
        <Row className="max-width-1180 padding-left-right-register-account">
          <Col className="px-0">
            <Row>
              <Col>
                <h3 className="title-privacy-details">Chính sách bảo mật thông tin</h3>
                <p className="details-post-main">
                  <b>Unicloud Group </b>
                  luôn hiểu rằng Quý khách hàng rất quan tâm đến việc những thông tin cá nhân mà Quý
                  khách đã tin cậy cung cấp cho chúng tôi được bảo mật và sử dụng ra sao. Unicloud
                  Group cam kết rằng những thông tin này sẽ được chúng tôi nỗ lực tối đa để bảo mật.
                  Unicloud Group đảm bảo sẽ sử dụng thông tin khách hàng một cách hợp lý, có cân
                  nhắc để không ngừng cải thiện chất lượng dịch vụ và đem lại cho Quý khách hàng
                  những trải nghiệm thú vị khi mua hàng tại website của chúng tôi.
                </p>
                <h3 className="privacy-title-number">1. Mục đích và phạm vi thu thập thông tin</h3>
                <ul className="ul-privacy">
                  <li className="details-post-main">
                    Để đăng ký sử dụng các dịch vụ phần mềm của Unicloud Group, Quý khách hàng phải
                    đăng ký tài khoản và cung cấp một số thông tin như: Tên doanh nghiệp, địa chỉ,
                    mã số thuế, người liên hệ, số điện thoại, email và một số thông tin khác. Phần
                    thủ tục đăng ký này giúp Unicloud Group xác định thông tin chính xác của Quý
                    khách hàng nhằm cung cấp quyền sử dụng các sản phẩm, dịch vụ phần mềm. Ngoài ra,
                    những thông tin này sẽ giúp Unicloud Group có thể liên hệ để phục vụ Quý khách
                    hàng trong suốt quá trình sử dụng sản phẩm, dịch vụ của Unicloud Group.
                  </li>
                  <li className="details-post-main">
                    Ngoài những thông tin bắt buộc phải cung cấp được đánh dấu *, Quý khách hành có
                    thể không cần cung cấp các thông tin không bắt buộc khác. Tuy nhiên, những thông
                    tin này sẽ rất hữu ích cho Unicloud Group khi thực hiện các dịch vụ chăm sóc và
                    hỗ trợ sau bán hàng, vì thế Quý khách hàng nên điền đầy đủ tất cả những thông
                    tin được yêu cầu trong quá trình tạo tài khoản, đăng ký mua các sản phẩm, dịch
                    vụ của Unicloud Group.
                  </li>
                </ul>
                <h3 className="privacy-title-number">2. Phạm vi sử dụng thông tin</h3>
                <span className="details-post-main">
                  Unicloud Group cam kết sẽ chỉ sử dụng thông tin của Quý khách hàng để :
                </span>
                <ul className="ul-privacy">
                  <li className="details-post-main">
                    Quản lý và cấp quyền sử dụng các sản phẩm, dịch vụ phần mềm mà Unicloud Group
                    cung cấp cho khách hàng.
                  </li>
                  <li className="details-post-main">
                    Liên lạc, gửi thông báo cho khách hàng khi sản phẩm, dịch vụ có sự cập nhật,
                    thay đổi.
                  </li>
                  <li className="details-post-main">
                    Gửi thông báo mời khách hàng đăng ký nâng cấp sản phẩm hoặc gia hạn dịch vụ khi
                    khách hàng sắp hết hạn sử dụng dịch vụ.
                  </li>
                  <li className="details-post-main">
                    Thực hiện các hoạt động chăm sóc, hỗ trợ sau bán hàng.
                  </li>
                  <li className="details-post-main">Giải quyết các khiếu nại, tranh chấp.</li>
                  <li className="details-post-main">
                    Unicloud Group cam kết không chia sẻ thông tin khách hàng cho bất kỳ bên thứ 3
                    nào ngoại trừ khi có yêu cầu của cơ quan có thẩm quyền của nhà nước.
                  </li>
                </ul>
                <h3 className="privacy-title-number">3. Thời gian lưu trữ thông tin</h3>
                <p className="details-post-main">
                  Thông tin của Quý khách hàng là dữ liệu đầu vào quan trọng để Unicloud Group cung
                  cấp và quản lý quyền sử dụng các sản phẩm, dịch vụ phần mềm, vì thế thông tin
                  khách hàng sẽ được lưu trữ trong suốt quá trình hoạt động của Unicloud Group.
                </p>
                <h3 className="privacy-title-number">4. Địa chỉ của đơn vị thu thập thông tin</h3>
                <p className="details-post-main">
                  Địa chỉ của Unicloud Group là: Block 5, Lô I-3B-1, Đường N6, Khu Công Nghệ Cao,
                  Phường Tân Phú, Thành phố Thủ Đức, Thành phố Hồ Chí Minh.
                </p>
                <h3 className="privacy-title-number">
                  5. Cam kết bảo mật thông tin cá nhân khách hàng
                </h3>
                <ul className="ul-privacy">
                  <li className="details-post-main">
                    Unicloud Group cam kết luôn nỗ lực sử dụng những biện pháp tốt nhất để bảo mật
                    thông tin của khách hàng nhằm đảm bảo những thông tin này không bị đánh cắp,
                    tiết lộ ngoài ý muốn.
                  </li>
                  <li className="details-post-main">
                    Unicloud Group cam kết không chia sẻ, bán hoặc cho thuê thông tin của khách hàng
                    với bất kỳ bên thứ ba nào.
                  </li>
                </ul>
                <div className="div-under-post-detail" />
              </Col>
            </Row>
          </Col>
        </Row>
      </Row>
    </section>
  );
}

export default PolicyTerm;
