import React from 'react';
import { Container } from 'react-bootstrap';
import QueryPostNewDetail from '@src/query/queryPostNewDetail';
import QueryPostNews from '@src/query/queryPostNew';
import { Navigate } from 'react-router-dom';
import RowLatestAndRelatedNews from '@src/components/latest-and-related-news/RowLatestAndRelatedNews';
import SEO from '@src/seo/seo';
import RowTopNewsDetail from './main/RowTopNewsDetail';
import RowNewsDetail from './main/RowNewsDetail';

import './NewsDetail.scss';

interface GroupSeoModel {
  title?: string;
  description?: string;
  link?: string;
  imagethumbs?: {
    sourceUrl: string;
    altText: string;
  };
  keywords?: string;
}

function NewsDetail() {
  const { data: postData, loading: loadingPost, error: errorPost } = QueryPostNewDetail();

  const { data, loading, error } = QueryPostNews();

  if (loading || loadingPost) return <div>...</div>;
  if (error || errorPost) return <Navigate to="/coming-soon" />;
  if (!data.posts.nodes[0]) return <Navigate to="/coming-soon" />;
  const dataNews = data?.posts;

  function filterNews(list: any[]) {
    let listNew: any[] = [];
    if (list.length > 0) {
      listNew = list.filter((value) => value.postId !== postData?.postBy?.postId);
      listNew = listNew?.filter((value) => value?.groupnews?.isdelete === null);
    }
    return listNew;
  }

  const groupSeo: GroupSeoModel = postData?.postBy?.groupseo;
  const { title, description, link, imagethumbs, keywords } = groupSeo;

  return (
    <>
      {title && description && link && imagethumbs && (
        <SEO
          title={title}
          description={description}
          url={link}
          imgthumbs={imagethumbs?.sourceUrl}
          keywords={keywords}
        />
      )}
      <main>
        <Container className="max-width-100 px-0" fluid>
          <RowTopNewsDetail data={postData?.postBy} />
          <RowNewsDetail data={postData?.postBy} dataNews={dataNews} />
          <RowLatestAndRelatedNews data={filterNews(dataNews?.nodes)} title="TIN TỨC LIÊN QUAN" />
        </Container>
      </main>
    </>
  );
}
export default NewsDetail;
