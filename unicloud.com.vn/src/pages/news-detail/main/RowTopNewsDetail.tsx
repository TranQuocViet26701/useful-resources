import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Image } from 'react-bootstrap';
import iconHomeNews from '/assets/image/news/icon-home-news.svg';

function RowTopNewsDetail(props: { data: any }) {
  const { data } = props;
  return (
    <Row className="back-ground-top-news-detail px-0 d-flex justify-content-center">
      <Row className="max-width-1300 padding-left-right">
        <Col className="">
          <div className="div-news-detail">
            <Image src={iconHomeNews} className="img-fuid icon-home-news " />
            <Link to="/" className="title-go-back-news">
              Trang Chủ
            </Link>
            <span className="middle-news">/</span>
            <Link to="/news" className="sub-title-news">
              Bài Viết
            </Link>
            <span className="middle-news">/</span>
            <Link to={data?.groupnews?.link || '/#'} className="sub-title-news">
              {data?.groupnews?.title}
            </Link>
          </div>
        </Col>
      </Row>
    </Row>
  );
}
export default RowTopNewsDetail;
