import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Row, Col, Image, Container, Form, InputGroup, FormControl } from 'react-bootstrap';
import iconBlue from '/assets/image/svg/icon-blue.svg';
import iconSearch from '/assets/image/svg/icon-search-news-detail.svg';
import calendarNewsPage from '/assets/image/svg/calendar-news-page.svg';
import banner from '/assets/image/news-detail/banner-news-detail.png';
import queryUtil from '@src/query/queryUtil';

function RowNews(props: { data: any; dataNews: any }) {
  const { data, dataNews } = props;

  function filterNews(list: any[]) {
    let listNew: any[] = [];

    if (list.length > 0) {
      listNew = list.filter((value) => value?.postId !== data?.postId);
      listNew = listNew.filter((value) => value?.groupnews?.isdelete === null);
    }
    return listNew;
  }

  return (
    <div className="back-ground-news-detail">
      <Container className="max-width-1300 px-0">
        <Row className="gx-4 padding-left-right row-news-detail">
          <Col lg={8} className="">
            <div className="div-img-news-detail">
              <Link to="/" style={{ marginBottom: '20px' }}>
                <h2 className="title-news-detail">{data?.groupnews?.title}</h2>
              </Link>
              <div className="row-category-post">
                <Link to="/news" className="category-post-details">
                  Tin tức
                </Link>
                <Image src={iconBlue} className="icon-blue-post-details" />
                <Link to="/#" className="category-post-details">
                  {queryUtil.GetCategoriesNew(data?.groupnews?.category)}
                </Link>
                {/* <Image src={iconBlue} className="icon-blue-post-details" /> */}
                {/* <Link to="/" className="category-post-details">
                  Danh mục 03
                </Link> */}
              </div>
              <div style={{ position: 'relative' }}>
                <Image
                  src={data?.groupnews?.image?.sourceUrl}
                  alt={data?.groupnews?.image?.alt}
                  className="img-fluid img-news-detail"
                  style={{ marginBottom: '40px' }}
                />
                <div className="div-time-news-detail">
                  <p className="date-news-detail">
                    {queryUtil.convertDate(data?.date, 'date') > 10
                      ? queryUtil.convertDate(data?.date, 'date')
                      : `0${queryUtil.convertDate(data?.date, 'date')}`}
                  </p>
                  <p className="month-year-news-detail">
                    {queryUtil.convertDate(data?.date, 'month') > 10
                      ? queryUtil.convertDate(data?.date, 'month')
                      : `0${queryUtil.convertDate(data?.date, 'month')}`}
                    , {queryUtil.convertDate(data?.date, 'year')}
                  </p>
                </div>
              </div>
              <div dangerouslySetInnerHTML={{ __html: data?.content }} className="new-details" />
            </div>
          </Col>
          <Col lg={4} className="">
            <div className="div-search-news-detail">
              <Row className="row-btn-slide-latest justify-content-between">
                <h3 className="title-news-side-bar">TÌM KIẾM</h3>
              </Row>
              <div className="div-under-news-side-bar" />
              <Form className="form-search-news-detail">
                <Form.Group>
                  <InputGroup className="">
                    <InputGroup.Text className="group-text-contact">
                      <img src={iconSearch} alt="icon-name" />
                    </InputGroup.Text>
                    <FormControl
                      className="fullName"
                      placeholder="Nhập từ khóa..."
                      aria-label="fullName"
                      aria-describedby="fullName"
                      required
                    />
                  </InputGroup>
                </Form.Group>
              </Form>
            </div>
            <div className="div-recent-posts">
              <Row className="row-btn-slide-latest justify-content-between">
                <h3 className="title-news-side-bar">BÀI ĐĂNG GẦN ĐÂY</h3>
              </Row>
              <div className="div-under-news-side-bar" />
              {filterNews(dataNews?.nodes)
                ?.slice(0, 5)
                .map((post: any) => (
                  <div key={post.postId}>
                    <Row className="gx-3 ">
                      <Col xs={3} sm={3} lg={4}>
                        <Image
                          src={post?.groupnews?.image?.sourceUrl}
                          className="img-fluid img-news-side-bar"
                        />
                      </Col>
                      <Col xs={9} sm={9} lg={8}>
                        <NavLink
                          to={post?.groupnews?.link || '/coming-soon'}
                          className="sub-news-side-bar"
                        >
                          {post?.groupnews?.title}
                        </NavLink>
                        <div>
                          <Image src={calendarNewsPage} />
                          <span className="date-time-news-main-sub">
                            &nbsp;
                            {queryUtil.convertDate(post.date, 'date') > 10
                              ? queryUtil.convertDate(post.date, 'date')
                              : `0${queryUtil.convertDate(post.date, 'date')}`}
                            &nbsp;tháng&nbsp;
                            {queryUtil.convertDate(post.date, 'month') > 10
                              ? queryUtil.convertDate(post.date, 'month')
                              : `0${queryUtil.convertDate(post.date, 'month')}`}
                            ,&nbsp;{queryUtil.convertDate(post.date, 'year')}
                          </span>
                        </div>
                      </Col>
                    </Row>
                    <div className="div-under-news-detail-sub" />
                  </div>
                ))}
            </div>

            <div className="div-banner text-center">
              <Image src={banner} className="img-fluid img-banner-news-detail" />
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default RowNews;
