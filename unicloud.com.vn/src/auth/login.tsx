import { gql } from 'graphql-tag';
import { useMutation } from '@apollo/client/react/hooks/useMutation';

const LOGIN_USER = gql`
  mutation LoginUser($username: username, $password: password) {
    login(input: { clientMutationId: "uniqueId", username: $username, password: $password }) {
      authToken
      user {
        id
        name
      }
    }
  }
`;

function Login() {
  const [LoginUsers, { loading }] = useMutation(LOGIN_USER, {
    variables: { username: 'root@gmail.com', password: 'Aqswde123@@' },
  });
  LoginUsers();
  return { loading };
}

export default Login;
