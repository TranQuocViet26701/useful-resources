import React from 'react';
import fetch from 'cross-fetch';
import { gql } from 'graphql-tag';

import './i18n';
import { Location } from 'react-router-dom';
import { StaticRouter } from 'react-router-dom/server.js';
import { HelmetProvider } from 'react-helmet-async';
import { ApolloClient } from '@apollo/client/index';
import { createHttpLink } from '@apollo/client/link/http/createHttpLink';
import { InMemoryCache } from '@apollo/client/cache/inmemory/inMemoryCache';
import { ApolloProvider } from '@apollo/client/react/context/ApolloProvider';
import { renderToStringWithData } from '@apollo/client/react/ssr';
import { setContext } from '@apollo/client/link/context';

import App from './App';

const LOGIN_USER = gql`
  mutation LoginUser($username: string = "root@gmail.com", $password: string = "Aqswde123@@") {
    login(input: { clientMutationId: "uniqueId", username: $username, password: $password }) {
      authToken
      user {
        id
        name
      }
    }
  }
`;

const authLink = (token: string) =>
  setContext((_, { headers }) => {
    return {
      headers: {
        ...headers,
        authorization: `Bearer ${token}`,
      },
    };
  });

const httpLink = createHttpLink({
  uri: 'https://data.unicloud.com.vn/graphql',
  fetch,
});

const getClient = (isTest: boolean) => {
  const client = new ApolloClient({
    ssrMode: true,
    link: httpLink,
    cache: new InMemoryCache(),
    credentials: 'include',
  });
  if (isTest) {
    client
      .mutate({
        mutation: LOGIN_USER,
        variables: {},
      })
      .then((value) => {
        const token = value?.data?.login?.authToken;
        if (token) client.setLink(authLink(token).concat(httpLink));
      });
  }
  return client;
};

function render(url: string | Partial<Location>) {
  return renderToStringWithData(
    <ApolloProvider client={getClient(false)}>
      <HelmetProvider>
        <StaticRouter location={url}>
          <App />
        </StaticRouter>
      </HelmetProvider>
    </ApolloProvider>,
  );
}

export default render;
