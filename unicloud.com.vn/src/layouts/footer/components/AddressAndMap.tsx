import React, { useCallback, useMemo, useState } from 'react';
import { Col, Row, Container } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import SectionMap from '@src/components/map/map';

import iconBgPin from '/assets/image/svg/icon-bg-pin.svg';
import iconBgEmail from '/assets/image/svg/icon-bg-mail.svg';
import iconBgPhone from '/assets/image/svg/icon-bg-phone.svg';

const listContact = [
  {
    key: 'aboutUs.address.headquarters',
    urlIcon: iconBgPin,
    position: { lat: 21.0300541, lng: 105.7786784 },
  },
  {
    key: 'aboutUs.address.factory',
    urlIcon: iconBgPin,
    position: { lat: 10.849997413384632, lng: 106.80553665690692 },
  },
  { key: 'aboutUs.address.email', urlIcon: iconBgEmail },
  { key: 'aboutUs.address.hotline', urlIcon: iconBgPhone },
];

export default function AddressAndMap() {
  const { t } = useTranslation();

  const [position, setPosition] = useState(listContact[0].position);

  const [map, setMap] = useState<any>();

  const MAP_ZOOM = 15;

  const onSelectContact = useCallback(
    (contact: typeof listContact[0]) => {
      if (contact.position && map) {
        setPosition(contact.position);
        map.setView(contact.position, map.getZoom());
      }
    },
    [map],
  );

  const onMapCreated = useCallback(
    (_map: any) => {
      setMap(_map);
      _map.setView(listContact[0].position!, MAP_ZOOM);
    },
    [map],
  );

  const BuildMap = useMemo(
    () => <SectionMap position={position} whenCreated={onMapCreated} />,
    [position],
  );

  return (
    <Container className="map-detail container-child">
      <Row className="g-lg-5 g-sm-3">
        <Col sm={12} lg={6}>
          {BuildMap}
        </Col>
        <Col sm={12} lg={6}>
          <h3 className="fs-4 fw-bold text-gradient-origan map-detail-address-footer">
            {t('footer.address')}
          </h3>
          <div className="group-contact">
            {listContact.map((contact) => (
              <div
                key={contact.key}
                className="group-contact-wrap d-flex align-items-center mb-3"
                role="button"
                onClick={() => onSelectContact(contact)}
                aria-hidden="true"
              >
                <img
                  src={contact.urlIcon}
                  alt=""
                  width={48}
                  className={`${contact.key ? 'align-self-start' : ''}`}
                />
                <div className="contact-wrap-item">
                  <h5 className="fw-bold fs-18 title-contact">{t(`${contact.key}.title`)}</h5>
                  <p className="title-contact fs-6">{t(`${contact.key}.description`)}</p>
                </div>
              </div>
            ))}
          </div>
        </Col>
      </Row>
    </Container>
  );
}
