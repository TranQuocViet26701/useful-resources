import React, { useEffect } from 'react';
import { Col, Container, Row, Image } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@apollo/client/react/hooks/useQuery';
import { gql } from 'graphql-tag';

import RowTechnology from '@components/technology/RowTechnology';
import AddressAndMap from './components/AddressAndMap';

import logoWhite from '/assets/image/svg/logo-white.svg';
import iconFb from '/assets/image/svg/icon-fb.svg';
import iconYtn from '/assets/image/svg/icon-youtube.svg';
import iconPhone from '/assets/image/svg/icon-linkedin.svg';
import iconApple from '/assets/image/homePage/icon-down-apple.png';
import iconChPlay from '/assets/image/homePage/icon-down-chplay.png';
import iconUpTop from '/assets/image/svg/icon-up-top-main.svg';

import './footer.scss';

export const dataPost = gql`
  query NewQuery {
    page(id: "cG9zdDoy") {
      social {
        linkedin
        linkfacebook
        linkyoutube
      }
    }
  }
`;

const BuildLogo = () => <img src={logoWhite} alt="logo Unicloud" width={144} height="auto" />;

function BuildInformation() {
  const { t, ready } = useTranslation();
  const { data } = useQuery(dataPost);

  let linkedin;
  let linkfacebook;
  let linkyoutube;
  if (data) {
    linkedin = data?.page?.social?.linkedin;
    linkfacebook = data?.page?.social?.linkfacebook;
    linkyoutube = data?.page?.social?.linkyoutube;
  }
  return (
    <div className="footer-info">
      <BuildLogo />
      <p className="text-white fs-14 my-4">{ready && t('footer.description')}</p>
      <h4 className="fs-6 fw-bold text-white mb-3">{ready && t('footer.connectWithUs')}</h4>
      <div className="group-icon-social d-flex justify-content-center">
        <a href={linkfacebook} target="_blank" rel="noopener noreferrer">
          <img src={iconFb} alt="facebook" />
        </a>
        <a href={linkyoutube} target="_blank" rel="noopener noreferrer">
          <img src={iconYtn} alt="youtube" />
        </a>
        <a href={linkedin} target="_blank" rel="noopener noreferrer">
          <img src={iconPhone} alt="phone" />
        </a>
      </div>
    </div>
  );
}

function BuildAboutUs(props: { listArray: any[]; title: string }) {
  const { listArray, title } = props;
  return (
    <div className="footer-about-us">
      <h4 className="text-white fw-bold fs-4">{title}</h4>
      <ul>
        {listArray?.map((about) => (
          <li key={about.id}>
            <a
              className="item-about text-white "
              href={about.url}
              target={about.url.includes('https://') ? '_blank' : '_self'}
              rel="noreferrer"
            >
              {about.title}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
}

function BuildApps() {
  const { t, ready } = useTranslation();
  return (
    <div className="footer-apps">
      <h4 className="text-white fw-bold fs-4">{ready && t('footer.applications')}</h4>
      <img className="my-3" src={iconApple} alt="" />
      <img className="my-3" src={iconChPlay} alt="" />

      {/* <img src={qrCode} alt="" /> */}
    </div>
  );
}
const scrollTop = () => {
  window.scrollTo({ top: 0, behavior: 'smooth' });
};
function Footer() {
  const { t, ready } = useTranslation();

  const listAbout: any[] = [
    { id: 0, title: ready ? t('footer.nav.aboutUs') : '', url: '/about-us' },
    { id: 1, title: ready ? t('footer.nav.ecosystem') : '', url: '/ecosystem' },
    { id: 2, title: ready ? t('footer.nav.news') : '', url: '/news' },
    { id: 3, title: ready ? t('footer.nav.career') : '', url: 'https://career.unicloud.com.vn' },
    { id: 4, title: ready ? t('footer.nav.support') : '', url: '/contact-us' },
  ];

  const listHref: any[] = [
    { id: 0, title: ready ? t('footer.link.termOfUse') : '', url: '/policy-terms' },
    // { id: 1, title: t('footer.link.privacy'), url: '#' },
    // { id: 2, title: t('footer.link.nda'), url: '#' },
    // { id: 3, title: t('footer.link.personalPolicy'), url: '#' },
    // { id: 4, title: t('footer.link.report'), url: '#' },
  ];

  useEffect(() => {
    const funcScroll = () => {
      const iconUpTop2 = document.getElementById('div-icon-up-top');
      const windowHeightScroll = window.scrollY;
      if (window !== undefined && iconUpTop2) {
        if (windowHeightScroll >= 40) {
          iconUpTop2.style.display = 'block';
        } else {
          iconUpTop2.style.display = 'none';
        }
      }
    };
    window.addEventListener('scroll', funcScroll);
    return () => {
      window.removeEventListener('scroll', funcScroll);
    };
  }, []);

  const image =
    'https://images.dmca.com/Badges/dmca_protected_sml_120t.png?ID=0c2432ce-c662-4a0b-9acd-498d22292907';

  return (
    <section>
      {ready && <RowTechnology />}
      <footer className="footer">
        {ready && <AddressAndMap />}
        <div className="bg-footer">
          <div className="background-parent">
            <Container className="footer-wrap container-child">
              <Row className="g-0 gy-md-5">
                <Col sm={12} md={12} lg={4} className="d-flex justify-content-center">
                  <BuildInformation />
                </Col>
                <Col sm={6} lg={2}>
                  <BuildAboutUs
                    listArray={listAbout}
                    title={ready ? t('footer.aboutUnicloud') : ''}
                  />
                </Col>
                <Col sm={6} lg={3}>
                  <BuildAboutUs listArray={listHref} title={ready ? t('footer.links') : ''} />
                  <a
                    href="//www.dmca.com/Protection/Status.aspx?ID=0c2432ce-c662-4a0b-9acd-498d22292907"
                    title="DMCA.com Protection Status"
                    className="dmca-badge"
                  >
                    <img src={image} alt="DMCA.com Protection Status" />
                  </a>
                </Col>
                <Col sm={12} md={12} lg={3} className="d-flex justify-content-center text-center">
                  <BuildApps />
                </Col>
              </Row>
            </Container>
          </div>
          <section>
            <div className="copy-right text-white">
              <span>© UnicloudGroup 2022. All Rights Reserved.</span>
            </div>
          </section>
        </div>
      </footer>
      <button type="button" onClick={scrollTop} className="div-icon-up-top" id="div-icon-up-top">
        <Image src={iconUpTop} className="img-fluid img-icon-up-top" />
      </button>
    </section>
  );
}

export default Footer;
