import queryUtil from '@src/query/queryUtil';
import React, { useEffect, useMemo, useState } from 'react';
import { Accordion, Nav } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router-dom';
import CustomLink from './CustomLink';

function NavbarMobile(props: {
  menuFintech: any[];
  menuDigital: any[];
  menuSmartCity: any[];
  menuVr: any[];
  ecosystemUrl: string;
}) {
  const { menuFintech, menuDigital, menuSmartCity, menuVr, ecosystemUrl } = props;
  const { t } = useTranslation();

  const location = useLocation();

  const [eventKeyActive, setEventKey] = useState<string>('');
  const [eventKeyChild, setEventKeyChild] = useState<string>();
  const [activeChildCurrent, setActiveChildCurrent] = useState<string>('');
  const [activeClassCurrent, setActiveClassCurrent] = useState<string>('');
  const [isPathEcosystem, setIsPathEcosystem] = useState<boolean>(false);
  const [TypePathCurrent, setTypePashCurrent] = useState<string>('');

  const onToggleChild = (key: string) => {
    setActiveChildCurrent(key);
    if (activeChildCurrent === key) {
      setEventKeyChild('');
      return;
    }
    switch (key) {
      case '1':
        setEventKeyChild(key);
        setActiveClassCurrent(key);
        break;
      case '2':
        setEventKeyChild(key);
        setActiveClassCurrent(key);
        break;
      case '3':
        setEventKeyChild(key);
        setActiveClassCurrent(key);
        break;
      case '4':
        setEventKeyChild(key);
        setActiveClassCurrent(key);

        break;
      default:
        break;
    }
  };

  useEffect(() => {
    if (location.pathname.indexOf('/ecosystem') > -1) {
      setIsPathEcosystem(true);
      setEventKey('1');
    } else {
      setEventKey('');
    }

    if (location.pathname.indexOf('/digital-banking-platform/') > -1) {
      onToggleChild('1');
      setTypePashCurrent('1');
    } else if (location.pathname.indexOf('/digital-transformation/') > -1) {
      onToggleChild('3');
      setTypePashCurrent('3');
    } else if (location.pathname.indexOf('/smart-city/') > -1) {
      onToggleChild('2');
      setTypePashCurrent('2');
    } else if (location.pathname.indexOf('/vr/') > -1) {
      onToggleChild('4');
      setTypePashCurrent('4');
    }
  }, [location.pathname]);

  const onToggleHeader = () => {
    if (eventKeyActive === '1') {
      setEventKey('');
    } else {
      setEventKey('1');
    }
  };

  const MenuDropdown = useMemo(() => {
    return (
      <Accordion flush className="" activeKey={eventKeyActive}>
        <Accordion.Item eventKey="1">
          <Accordion.Header className="header-parent" onClick={onToggleHeader}>
            <Nav.Link
              className="link-accordion"
              as={CustomLink}
              to={ecosystemUrl || '/coming-soon'}
              bsPrefix="custom-nav-link"
              id="ecosystem-menu"
            >
              {t('navbar.navLink.ecosystem')}
            </Nav.Link>
          </Accordion.Header>
          <Accordion.Body className="navbar-accordion-child">
            <Accordion flush activeKey={eventKeyChild}>
              <Accordion.Item eventKey="1" className="navbar-accordion-child-item">
                <Accordion.Header
                  onClick={() => onToggleChild('1')}
                  className={
                    (activeClassCurrent === '1' && isPathEcosystem && TypePathCurrent === '1') ||
                    TypePathCurrent === '1'
                      ? 'is-active-toggle'
                      : ''
                  }
                >
                  Digital Banking Platform
                </Accordion.Header>
                <Accordion.Body>
                  <Nav>
                    {menuFintech.map((menu) =>
                      queryUtil.isLinkInternal(menu?.formpost?.grouplinkdetail?.link) ? (
                        <a
                          key={menu?.formpost?.grouplinkdetail?.title}
                          href={menu?.formpost?.grouplinkdetail?.link}
                          target="_blank"
                          rel="noreferrer"
                          className="custom-nav-link"
                        >
                          {menu?.formpost?.grouplinkdetail?.title}
                        </a>
                      ) : (
                        <Nav.Link
                          as={CustomLink}
                          key={menu?.formpost?.grouplinkdetail?.title}
                          to={menu?.formpost?.grouplinkdetail?.link || '/coming-soon'}
                          bsPrefix="custom-nav-link"
                        >
                          {menu?.formpost?.grouplinkdetail?.title}
                        </Nav.Link>
                      ),
                    )}
                  </Nav>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="3" className="navbar-accordion-child-item">
                <Accordion.Header
                  onClick={() => onToggleChild('3')}
                  className={
                    (activeClassCurrent === '3' && isPathEcosystem && TypePathCurrent === '2') ||
                    TypePathCurrent === '3'
                      ? 'is-active-toggle'
                      : ''
                  }
                >
                  Digital Transformation
                </Accordion.Header>
                <Accordion.Body>
                  <Nav>
                    {menuDigital.map((menu) =>
                      queryUtil.isLinkInternal(menu?.formpost?.grouplinkdetail?.link) ? (
                        <a
                          href={menu?.formpost?.grouplinkdetail?.link}
                          target="_blank"
                          rel="noreferrer"
                          className="custom-nav-link"
                        >
                          {menu?.formpost?.grouplinkdetail?.title}
                        </a>
                      ) : (
                        <Nav.Link
                          as={CustomLink}
                          key={menu?.formpost?.grouplinkdetail?.title}
                          to={menu?.formpost?.grouplinkdetail?.link || '/coming-soon'}
                          bsPrefix="custom-nav-link"
                        >
                          {menu?.formpost?.grouplinkdetail?.title}
                        </Nav.Link>
                      ),
                    )}
                    {menuDigital.length <= 0 && (
                      <span className="not-found-link">Không tìm thấy dữ liệu</span>
                    )}
                  </Nav>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="2" className="navbar-accordion-child-item">
                <Accordion.Header
                  onClick={() => onToggleChild('2')}
                  className={
                    (activeClassCurrent === '2' && isPathEcosystem && TypePathCurrent === '2') ||
                    TypePathCurrent === '2'
                      ? 'is-active-toggle'
                      : ''
                  }
                >
                  Smart City
                </Accordion.Header>
                <Accordion.Body>
                  <Nav>
                    {menuSmartCity.map((menu) =>
                      queryUtil.isLinkInternal(menu?.formpost?.grouplinkdetail?.link) ? (
                        <a
                          href={menu?.formpost?.grouplinkdetail?.link}
                          target="_blank"
                          rel="noreferrer"
                          className="custom-nav-link"
                        >
                          {menu?.formpost?.grouplinkdetail?.title}
                        </a>
                      ) : (
                        <Nav.Link
                          as={CustomLink}
                          key={menu?.formpost?.grouplinkdetail?.title}
                          to={menu?.formpost?.grouplinkdetail?.link || '/coming-soon'}
                          bsPrefix="custom-nav-link"
                        >
                          {menu?.formpost?.grouplinkdetail?.title}
                        </Nav.Link>
                      ),
                    )}
                    {menuSmartCity.length <= 0 && (
                      <span className="not-found-link">Không tìm thấy dữ liệu</span>
                    )}
                  </Nav>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="4" className="navbar-accordion-child-item">
                <Accordion.Header
                  onClick={() => onToggleChild('4')}
                  className={
                    (activeClassCurrent === '4' && isPathEcosystem && TypePathCurrent === '4') ||
                    TypePathCurrent === '4'
                      ? 'is-active-toggle'
                      : ''
                  }
                >
                  Virtual Reality (VR)
                </Accordion.Header>
                <Accordion.Body>
                  <Nav>
                    {menuVr.map((menu) =>
                      queryUtil.isLinkInternal(menu?.formpost?.grouplinkdetail?.link) ? (
                        <a
                          href={menu?.formpost?.grouplinkdetail?.link || '/coming-soon'}
                          target="_blank"
                          rel="noreferrer"
                          className="custom-nav-link"
                        >
                          {menu?.formpost?.grouplinkdetail?.title}
                        </a>
                      ) : (
                        <Nav.Link
                          as={CustomLink}
                          key={menu?.formpost?.grouplinkdetail?.title}
                          to={menu?.formpost?.grouplinkdetail?.link || '/coming-soon'}
                          bsPrefix="custom-nav-link"
                        >
                          {menu?.formpost?.grouplinkdetail?.title}
                        </Nav.Link>
                      ),
                    )}
                    {menuVr.length <= 0 && (
                      <span className="not-found-link">Không tìm thấy dữ liệu</span>
                    )}
                  </Nav>
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    );
  }, [eventKeyActive, eventKeyChild]);

  return <div className="navbar-mobile">{MenuDropdown}</div>;
}

export default NavbarMobile;
