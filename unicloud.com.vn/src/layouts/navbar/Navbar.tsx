import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Col, Container, Dropdown, Image, Nav, Navbar, Offcanvas, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { useLocation, useNavigate } from 'react-router-dom';

import useWindowSize from '@src/hooks/useWindowsSize';

import logoScroll from '/assets/image/svg/logo-scroll.svg';
import logoWhite from '/assets/image/svg/logo-white.svg';
// import searchBtn from '/assets/image/svg/search-btn.svg';
// import searchBtnScroll from '/assets/image/svg/icon-search-btn-scroll.svg';
import QueryPostDetail from '@src/query/queryPostDetail';
import queryUtil from '@src/query/queryUtil';
import i18next from 'i18next';
import CustomLink from './CustomLink';
import NavbarMobile from './navbar-mobile';
import './navbar.scss';
import iconBgEmail from '/assets/image/svg/icon-bg-mail.svg';
import iconBgPhone from '/assets/image/svg/icon-bg-phone.svg';

interface PostModel {
  indexActive: number;
  title: string;
  desc: string;
  listItem: any[];
  link: string;
}

const NavbarChild = React.memo(
  (props: {
    id: string;
    children: JSX.Element | JSX.Element[];
    isMobile: boolean;
    show: boolean;
    onShow: () => void;
    onHide: () => void;
  }) => {
    const { id, isMobile, show, onShow, onHide } = props;

    if (!isMobile) {
      return <Navbar.Collapse id={id}>{props.children}</Navbar.Collapse>;
    }

    const location = useLocation();

    useEffect(() => {
      onHide();
    }, [location]);

    return (
      <Navbar.Offcanvas show={show} onShow={onShow} onHide={onHide} id={id}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title id="offcanvasNavbarLabel">
            <Navbar.Brand href="/">
              <img
                src={logoScroll}
                height="64"
                className="d-inline-block align-top"
                alt="Unicloud Logo"
              />
            </Navbar.Brand>
          </Offcanvas.Title>
        </Offcanvas.Header>
        {props.children}
      </Navbar.Offcanvas>
    );
  },
);

// check navbar is not transparent
function isNotTransparent(pathname: string) {
  const pathnameNotTransparentList = ['/news/', '/policy-terms', '/coming-soon', '/not-found'];

  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < pathnameNotTransparentList.length; i++) {
    if (pathname.includes(pathnameNotTransparentList[i])) return true;
  }

  return false;
}

const CustomNav = React.memo(() => {
  const { t, ready, i18n } = useTranslation();
  const { data: dataPost, loading, error } = QueryPostDetail();
  const [postFintech, setPostFintech] = useState<any[]>([]);
  const [postDigital, setPostDigital] = useState<any[]>([]);
  const [postSmartCity, setPostSmartCity] = useState<any[]>([]);
  const [postVr, setPostVr] = useState<any[]>([]);
  const navigate = useNavigate();

  const [menuActive, setMenuActive] = useState<PostModel>({
    indexActive: 1,
    title: t('navbar.ecosystem.banking.title'),
    desc: t('navbar.ecosystem.banking.subtitle'),
    listItem: [],
    link: '/neobank',
  });
  const size = useWindowSize();
  const isMobile = useMemo(() => {
    if (size.width && size.width >= 992) {
      return false;
    }
    return true;
  }, [size.width]);

  function filterMenuByType(list: any[], count: number) {
    let listNew: any[] = [];

    const arrBanking = [
      t('navbar.ecosystem.banking.array.item1').toString(),
      t('navbar.ecosystem.banking.array.item2').toString(),
      t('navbar.ecosystem.banking.array.item3').toString(),
    ];

    if (count > 0 && window.innerWidth > 768) {
      for (let index = 1; index <= count; index += 1) {
        const listFilter: any[] = [];
        const objNew = { title: arrBanking[0], listChild: listFilter };
        objNew.title = arrBanking[index - 1];

        for (let j = 0; j < list.length; j += 1) {
          const ele = list[j];
          if (ele?.formpost?.grouplinkdetail?.typechild === index) {
            objNew.listChild.push(ele);
          }
        }
        listNew.push(objNew);
      }
    } else {
      listNew = list;
    }
    return listNew;
  }

  function filterSortByIndex(list: any[]) {
    let listNew: any[] = [];
    listNew = list.sort(
      (a, b) =>
        parseFloat(a?.formpost?.grouplinkdetail?.index) -
        parseFloat(b?.formpost?.grouplinkdetail?.index),
    );
    return listNew;
  }

  function filterDeletedSubMenu(list: any[]) {
    let listNew: any[] = [];
    if (list.length > 0) {
      listNew = list.filter((value) => !value.formpost?.isdeleted);
      listNew = filterSortByIndex(listNew);
    }

    return listNew;
  }

  function FilterEcosystemByType(typeEcosystem: string, arrayPosts: any[]) {
    let filterPostCurrent = arrayPosts.filter(
      (item) => item.formpost.typeecosystem === typeEcosystem,
    );
    if (typeEcosystem === '1') {
      filterPostCurrent = filterMenuByType(filterPostCurrent, 3);
    } else {
      filterPostCurrent = filterDeletedSubMenu(filterPostCurrent);
    }

    return filterPostCurrent;
  }

  const [ecosystemUrl, setEcosystemUrl] = useState<string>('/ecosystem');

  useEffect(() => {
    if (!loading && !error && dataPost?.posts?.nodes) {
      const dataCurrent: any[] = dataPost?.posts?.nodes;
      if (dataCurrent.length > 0) {
        setPostFintech(FilterEcosystemByType('1', dataCurrent));
        setPostDigital(FilterEcosystemByType('2', dataCurrent));
        setPostSmartCity(FilterEcosystemByType('3', dataCurrent));
        setPostVr(FilterEcosystemByType('4', dataCurrent));
        setMenuActive({
          indexActive: 1,
          title: t('navbar.ecosystem.banking.title'),
          desc: t('navbar.ecosystem.banking.subtitle'),
          listItem: FilterEcosystemByType('1', dataCurrent),
          link: '/neobank',
        });
        setEcosystemUrl('/ecosystem');
      }
    } else {
      setEcosystemUrl('/not-found');
    }
  }, [loading, i18n.language, ready]);
  const [show, setShow] = useState(false);
  const onShow = () => setShow(true);

  const onHide = () => setShow(false);

  const [logoNav, setLogoNav] = useState(logoWhite);
  //   const [iconSearch, setIconSearch] = useState(searchBtn);
  const [stickyClass, setStickyClass] = useState('background-transparent');

  //	xử lý menu news
  const location = useLocation();
  const { pathname } = location;

  const isComingSoon = pathname.includes('/coming-soon');

  useEffect(() => {
    if (isNotTransparent(pathname)) {
      setLogoNav(logoScroll);
      setStickyClass('background-sticky box-shadow-sticky-on');
    }
  }, [pathname]);

  // xử lý sticky menu
  const stickNavbar = useCallback(() => {
    if (isNotTransparent(pathname)) return;

    if (typeof window !== 'undefined') {
      const windowHeightScroll = window.scrollY;
      if (windowHeightScroll > 20) {
        setLogoNav(logoScroll);
        setStickyClass('background-sticky box-shadow-sticky-on');
        // setIconSearch(searchBtnScroll);
      } else {
        setLogoNav(logoWhite);
        setStickyClass('background-transparent');
        // setIconSearch(searchBtn);
      }
    }
  }, [pathname]);

  useEffect(() => {
    window.addEventListener('scroll', stickNavbar);
    return () => {
      window.removeEventListener('scroll', stickNavbar);
    };
  }, []);

  const changeLanguage = useCallback(
    (lng: string | null) => {
      if (lng && lng !== i18n.language) {
        i18n.changeLanguage(lng);
        const { pathname: path, search, hash } = location;
        navigate(`/${lng}${path.slice(3)}${search}${hash}`, { replace: true });
      }
    },
    [i18n.language, location],
  );

  const languageElement = useMemo(() => {
    const { languages, language } = i18n;

    const sortedLanguages = languages && [...languages];

    const flagPath = (locale: string) => `/flag-${locale}.svg`;
    return (
      <Dropdown className={`d-flex ${!isMobile ? '' : 'flex-column'}`} onSelect={changeLanguage}>
        <Dropdown.Toggle
          variant="none"
          id="dropdown-basic"
          className="box-shadow-unset btn-search-home btn-lang ms-auto"
        >
          {language?.toUpperCase()} &nbsp;&nbsp;&nbsp;
          <Image src={flagPath(language)} alt="icon flag" />
        </Dropdown.Toggle>

        <Dropdown.Menu align="end">
          {sortedLanguages
            ?.sort((a: string, b: string) => b.localeCompare(a))
            .map((locale) => {
              const fixedT = i18next.getFixedT(locale, 'translation', 'navbar.navLink');
              return (
                <Dropdown.Item eventKey={locale} key={locale} active={locale === language}>
                  <Image src={flagPath(locale)} alt="" />
                  <span className="text-dropdown-lang">
                    {ready && fixedT(language === 'en' ? 'languageEn' : 'languageVi').toString()}
                  </span>
                </Dropdown.Item>
              );
            })}
        </Dropdown.Menu>
      </Dropdown>
    );
  }, [isMobile, changeLanguage, i18n.language, ready]);

  function hoverMenu(event: any) {
    const data = event.target.getAttribute('data-value');

    if (data) {
      switch (data) {
        case '1':
          setMenuActive({
            indexActive: 1,
            title: t('navbar.ecosystem.banking.title'),
            desc: t('navbar.ecosystem.banking.subtitle'),
            listItem: postFintech,
            link: '/neobank',
          });
          break;
        case '2':
          setMenuActive({
            title: t('navbar.ecosystem.smartcity.title'),
            indexActive: 2,
            desc: t('navbar.ecosystem.smartcity.subtitle'),
            listItem: postSmartCity,
            link: '/coming-soon',
          });
          break;
        case '3':
          setMenuActive({
            ...menuActive,
            indexActive: 3,
            title: t('navbar.ecosystem.transformation.title'),
            desc: t('navbar.ecosystem.transformation.subtitle'),
            listItem: postDigital,
            link: '/coming-soon',
          });
          break;
        case '4':
          setMenuActive({
            ...menuActive,
            title: t('navbar.ecosystem.vr.title'),
            indexActive: 4,
            desc: t('navbar.ecosystem.vr.subtitle'),
            listItem: postVr,
            link: 'https://univr.vn/',
          });
          break;
        default:
          break;
      }
    }
  }

  const BuildMenuChild = useMemo(
    () => (
      <div className="menu-child" id="ecosystem-menu-child">
        <div className="arrow-up-menu" />
        <div className="menu-child-wrap">
          <div className="child-wrap-left">
            <Row
              className={`wrap-left-item ${menuActive.indexActive === 1 ? 'is-hover-menu' : ''}`}
              data-value="1"
              id="menu-id-left"
              onMouseOver={(e) => hoverMenu(e)}
            >
              <span>Digital Banking Platform</span>
            </Row>
            <Row
              className={`wrap-left-item ${menuActive.indexActive === 3 ? 'is-hover-menu' : ''}`}
              data-value="3"
              id="menu-id-left"
              onMouseOver={(e) => hoverMenu(e)}
            >
              <span>Digital Transformation</span>
            </Row>
            <Row
              className={`wrap-left-item ${menuActive.indexActive === 2 ? 'is-hover-menu' : ''}`}
              data-value="2"
              id="menu-id-left"
              onMouseOver={(e) => hoverMenu(e)}
            >
              <span>Smart City</span>
            </Row>
            <Row
              className={`wrap-left-item ${menuActive.indexActive === 4 ? 'is-hover-menu' : ''}`}
              data-value="4"
              id="menu-id-left"
              onMouseOver={(e) => hoverMenu(e)}
            >
              <span>Virtual Reality (VR)</span>
            </Row>
          </div>
          <Col className="child-wrap-right">
            <CustomLink to={menuActive.link}>
              <h3>{menuActive.title}</h3>
            </CustomLink>
            <p className="child-wrap-right_desc">
              {menuActive.desc}
              <a
                href={menuActive.link}
                target="_blank"
                rel="noopener noreferrer"
                className="detail-link-menu"
              >
                ({t('navbar.ecosystem.button')})
              </a>
            </p>

            {menuActive.indexActive !== 1 && (
              <ul
                className={
                  menuActive?.listItem?.length > 6 ? 'menu-child-length10' : 'menu-child-length6'
                }
              >
                {menuActive?.listItem.map((item) => (
                  <li
                    key={item?.formpost?.grouplinkdetail?.title}
                    className="menu-right-item"
                    id="menu-id-right"
                  >
                    <div>
                      {queryUtil.isLinkInternal(item?.formpost?.grouplinkdetail?.link) ? (
                        <a
                          href={item?.formpost?.grouplinkdetail?.link}
                          target="_blank"
                          rel="noreferrer"
                        >
                          <h4>{item?.formpost?.grouplinkdetail?.title}</h4>
                        </a>
                      ) : (
                        <Nav.Link as={CustomLink} to={item?.formpost?.grouplinkdetail?.link}>
                          <h4>{item?.formpost?.grouplinkdetail?.title}</h4>
                        </Nav.Link>
                      )}

                      <span>
                        {i18n.language === 'vi'
                          ? item?.formpost?.grouplinkdetail?.description
                          : item?.formpost?.grouplinkdetail?.descriptionen}
                      </span>
                    </div>
                  </li>
                ))}
              </ul>
            )}
            {!isMobile && menuActive.indexActive === 1 && (
              <ul className="menu-child-length10">
                {menuActive?.listItem.map((item) => {
                  return (
                    <div key={item.title || item?.formpost?.grouplinkdetail?.title}>
                      <h4 className="title-menu-sub">{item.title}</h4>
                      {item?.listChild?.map((child: any) => (
                        <li
                          key={child?.formpost?.grouplinkdetail?.title}
                          className="menu-right-item"
                          id="menu-id-right"
                        >
                          <div>
                            {queryUtil.isLinkInternal(child?.formpost.grouplinkdetail.link) ? (
                              <a
                                href={child?.formpost?.grouplinkdetail?.link}
                                target="_blank"
                                rel="noreferrer"
                              >
                                <h4>{child?.formpost?.grouplinkdetail?.title}</h4>
                              </a>
                            ) : (
                              <Nav.Link as={CustomLink} to={child?.formpost?.grouplinkdetail?.link}>
                                <h4>{child?.formpost?.grouplinkdetail?.title}</h4>
                              </Nav.Link>
                            )}

                            <span>
                              {i18n.language === 'vi'
                                ? child?.formpost?.grouplinkdetail?.description
                                : child?.formpost?.grouplinkdetail?.descriptionen}
                            </span>
                          </div>
                        </li>
                      ))}
                    </div>
                  );
                })}
              </ul>
            )}
            {filterDeletedSubMenu(menuActive.listItem).length <= 0 && (
              <span className="fst-italic">Không có dữ liệu...</span>
            )}
          </Col>
        </div>
        <div className="divide-bottom" />
      </div>
    ),
    [menuActive, i18n.language, ready],
  );

  const ecosystemMouseEvent = useCallback(
    (event: Event) => {
      const ecosystemId = document.getElementById('ecosystem-menu');
      const ecosystemChildId = document.getElementById('ecosystem-menu-child');

      if (!ecosystemChildId || !ecosystemId || isMobile) return;
      if (event.type === 'mousemove') {
        ecosystemChildId.style.display = 'block';
        ecosystemChildId.style.pointerEvents = ' all';
        ecosystemId.style.fontWeight = 'bold';
        window.setTimeout(() => {
          ecosystemChildId.style.opacity = '1';
        }, 200);
      } else if (event.type === 'mouseleave' || event.type === 'scroll') {
        ecosystemChildId.style.display = 'none';
        ecosystemChildId.style.pointerEvents = 'none';
        ecosystemId.style.fontWeight = '500';
        window.setTimeout(() => {
          ecosystemChildId!.style!.opacity = '0';
        }, 200);
      }
    },
    [isMobile],
  );

  const navItems = useMemo(() => {
    const items = [
      <Nav className="mx-auto" navbarScroll key="nav-item">
        {ready && (
          <>
            <Nav.Link as={CustomLink} to="/" bsPrefix="custom-nav-link">
              {t('navbar.navLink.home')}
            </Nav.Link>
            <Nav.Link as={CustomLink} to="/about-us" bsPrefix="custom-nav-link">
              {t('navbar.navLink.aboutUs')}
            </Nav.Link>
            {!isComingSoon && !isMobile && menuActive.listItem.length > 0 && (
              // <Nav.Link as={NavLink} to={ecosystemUrl} bsPrefix="custom-nav-link" id="ecosystem-menu">
              //   {t('navbar.navLink.ecosystem')}
              // </Nav.Link>
              <div className="custom-nav-link" id="ecosystem-menu">
                {t('navbar.navLink.ecosystem')}
              </div>
            )}
            {!isComingSoon && !isMobile && menuActive.listItem.length <= 0 ? (
              <span className="custom-nav-link" id="ecosystem-menu">
                {t('navbar.navLink.ecosystem')}
              </span>
            ) : null}
            {isMobile && (
              <NavbarMobile
                menuFintech={postFintech}
                menuDigital={postDigital}
                menuSmartCity={postSmartCity}
                menuVr={postVr}
                ecosystemUrl={ecosystemUrl}
              />
            )}
            <Nav.Link as={CustomLink} to="/news" bsPrefix="custom-nav-link">
              {t('navbar.navLink.news')}
            </Nav.Link>
            <a
              href="https://blog.unicloud.com.vn/"
              rel="noreferrer"
              target="_blank"
              className="custom-nav-link"
            >
              BLOG
            </a>
            <Nav.Link
              as="a"
              href="https://career.unicloud.com.vn"
              target="_blank"
              bsPrefix="custom-nav-link"
              active={false}
              data-inactive="true"
            >
              {t('navbar.navLink.career')}
            </Nav.Link>
            <Nav.Link as={CustomLink} to="/contact-us" bsPrefix="custom-nav-link">
              {t('navbar.navLink.contact')}
            </Nav.Link>
          </>
        )}

        {isMobile && (
          <>
            <br />
            <div className="group-contact-wrap d-flex align-items-center mb-3 mt-3">
              <img src={iconBgEmail} alt="" width={48} className="align-self-start" />
              <div className="contact-wrap-item">
                <p className="title-contact fs-6">info@unicloud.com.vn</p>
              </div>
            </div>
            <div className="group-contact-wrap d-flex align-items-center mb-3">
              <img src={iconBgPhone} alt="" width={48} className="align-self-start" />
              <div className="contact-wrap-item">
                <p className="title-contact fs-6">02473039999</p>
              </div>
            </div>
          </>
        )}
      </Nav>,
      <Nav bsPrefix="custom-action-nav" key="nav-actions">
        {languageElement}
      </Nav>,
    ];
    if (isMobile) return items.reverse();
    return items;
  }, [isMobile, t, languageElement, postFintech]);

  useEffect(() => {
    const ecosystemId = document.getElementById('ecosystem-menu');
    const ecosystemChildId = document.getElementById('ecosystem-menu-child');
    ecosystemId?.addEventListener('mousemove', ecosystemMouseEvent);
    ecosystemId?.addEventListener('mouseleave', ecosystemMouseEvent);
    ecosystemChildId?.addEventListener('mousemove', ecosystemMouseEvent);
    ecosystemChildId?.addEventListener('mouseleave', ecosystemMouseEvent);
    window.addEventListener('scroll', ecosystemMouseEvent);

    return function cleanup() {
      ecosystemId?.removeEventListener('mousemove', ecosystemMouseEvent);
      ecosystemId?.removeEventListener('mousemove', ecosystemMouseEvent);
      ecosystemChildId?.removeEventListener('mouseleave', ecosystemMouseEvent);
      ecosystemChildId?.removeEventListener('mousemove', ecosystemMouseEvent);
      window.removeEventListener('scroll', ecosystemMouseEvent);
    };
  }, [navItems, ecosystemMouseEvent]);

  return (
    <Navbar
      as="header"
      role="banner"
      expand="lg"
      className={stickyClass}
      fixed="top"
      collapseOnSelect
      id="nav-bar-id"
    >
      <Container className="max-width-1600 menu-desktop">
        <Navbar.Brand href="/">
          <img
            src={logoNav}
            height="64"
            className="logo-menu d-inline-block align-top"
            alt="Unicloud Logo"
            id="logo-menu"
          />
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="offcanvasNavbar"
          bsPrefix="custom-navbar-toggle"
          onClick={onShow}
        />
        <NavbarChild
          id="offcanvasNavbar"
          isMobile={isMobile}
          show={show}
          onShow={onShow}
          onHide={onHide}
        >
          <Offcanvas.Body style={{ flexGrow: 1, overflowY: 'visible' }} className="navbar-bizzone">
            {navItems}
          </Offcanvas.Body>
        </NavbarChild>
      </Container>
      {BuildMenuChild}
    </Navbar>
  );
});

export default CustomNav;
