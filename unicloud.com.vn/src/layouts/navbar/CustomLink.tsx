/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { useTranslation } from 'react-i18next';
import { LinkProps, NavLink } from 'react-router-dom';

export default function CustomLink({ children, to, ...props }: LinkProps) {
  const { i18n } = useTranslation();
  const currentLng = i18n.language;

  return (
    <NavLink to={`/${currentLng}${to}`} {...props}>
      {children}
    </NavLink>
  );
}
