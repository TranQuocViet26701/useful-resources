import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import * as ApolloClientExport from '@apollo/client';

import App from './App';

import '@assets/scss/style.scss';

const { ApolloClient, InMemoryCache, ApolloProvider } = ApolloClientExport;

const client = new ApolloClient({
  uri: 'https://data.unicloud.com.vn/graphql',
  cache: new InMemoryCache(),
});
ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <Router>
        <App />
      </Router>
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);
