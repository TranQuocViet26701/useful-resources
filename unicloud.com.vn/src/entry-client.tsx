import React from 'react';
import ReactDOM from 'react-dom';
import { gql } from 'graphql-tag';
import { HelmetProvider } from 'react-helmet-async';
import { BrowserRouter as Router } from 'react-router-dom';
import { ApolloClient } from '@apollo/client/index';

import { createHttpLink } from '@apollo/client/link/http/createHttpLink';
import { ApolloProvider } from '@apollo/client/react/context/ApolloProvider';
import { InMemoryCache } from '@apollo/client/cache/inmemory/inMemoryCache';
import { setContext } from '@apollo/client/link/context';
import App from './App';
import './i18n';

const LOGIN_USER = gql`
  mutation LoginUser($username: string = "root@gmail.com", $password: string = "Aqswde123@@") {
    login(input: { clientMutationId: "uniqueId", username: $username, password: $password }) {
      authToken
      user {
        id
        name
      }
    }
  }
`;

const httpLink = createHttpLink({
  uri: 'https://data.unicloud.com.vn/graphql',
});

const authLink = (token: string) =>
  setContext((_, { headers }) => {
    return {
      headers: {
        ...headers,
        authorization: `Bearer ${token}`,
      },
    };
  });
const getClient = (isTest: boolean) => {
  const client = new ApolloClient({
    ssrMode: true,
    link: httpLink,
    cache: new InMemoryCache(),
    credentials: 'include',
  });
  if (isTest) {
    client
      .mutate({
        mutation: LOGIN_USER,
        variables: {
          username: 'root@gmail.com',
          password: 'Aqswde123@@',
        },
      })
      .then((value) => {
        const token = value?.data?.login?.authToken;
        if (token) client.setLink(authLink(token).concat(httpLink));
      });
  }
  return client;
};

ReactDOM.hydrate(
  <HelmetProvider>
    <ApolloProvider client={getClient(false)}>
      <Router>
        <App />
      </Router>
    </ApolloProvider>
  </HelmetProvider>,
  document.getElementById('root'),
);
