/* eslint-disable react/react-in-jsx-scope */
import React from 'react';
import AboutUs from '@pages/about-unicloud';
import ComingSoon from '@pages/coming-soon';
import Contact from '@pages/contact';
import HomePage from '@pages/home/HomePage';
import BizzoneCloudPage from '@pages/landing-page/bizzone-cloud';
import DigitalBanking from '@pages/landing-page/digital-banking';
import EkycPage from '@pages/landing-page/ekyc';
import News from '@pages/news';
import NotFound from '@pages/not-found';
import PolicyTerm from '@pages/policy/ policy-terms';

export const routerPageRouteList = [
  {
    path: '/',
    element: <HomePage />,
  },
  {
    path: '/about-us',
    element: <AboutUs />,
  },
  {
    path: '/news',
    element: <News />,
  },
  {
    path: '/contact-us',
    element: <Contact />,
  },
  {
    path: '/policy-terms',
    element: <PolicyTerm />,
  },
];

export const appRouteList = [
  {
    path: '/neobank',
    element: <DigitalBanking />,
  },
  {
    path: '/bizzone-hr',
    element: <BizzoneCloudPage />,
  },
  {
    path: '/ekyc',
    element: <EkycPage />,
  },
  {
    path: '/not-found',
    element: <NotFound />,
  },
  {
    path: '/coming-soon',
    element: <ComingSoon />,
  },
];
