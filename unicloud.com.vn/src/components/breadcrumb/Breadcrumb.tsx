import React from 'react';
import { Link } from 'react-router-dom';

import iconHome from '/assets/image/svg/icon-home.svg';

import './Breadcrumb.scss';
import { useTranslation } from 'react-i18next';

interface BreadcrumbI {
  title: string;
  url: string;
}

type BreadcrumbProps = {
  listBreadCrumb: Array<BreadcrumbI>;
};

function Breadcrumb(props: BreadcrumbProps) {
  const { t } = useTranslation();
  const { listBreadCrumb } = props;
  return (
    <div className="breadcrumb">
      <nav aria-label="breadcrumb">
        <ol className="breadcrumb">
          {listBreadCrumb.map((breadcrumb, index) => (
            <Link
              key={breadcrumb.title}
              to={breadcrumb.url || '/coming-soon'}
              className={`breadcrumb-item ${index === listBreadCrumb.length - 1 ? 'active' : ''}`}
              aria-current="page"
            >
              {index === 0 ? (
                <img src={iconHome} width={14} alt="" style={{ marginRight: '10px' }} />
              ) : (
                ''
              )}{' '}
              {t(breadcrumb.title)}
            </Link>
          ))}
        </ol>
      </nav>
    </div>
  );
}

export default Breadcrumb;
