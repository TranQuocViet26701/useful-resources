import React from 'react';
import { Row, Col, Image } from 'react-bootstrap';
import logoSunshine from '/assets/image/homePage/logo-sunshine-home-main.svg';
import logoSmartConstruction from '/assets/image/homePage/logo-smart-construction-main.svg';
import logoKSFinance from '/assets/image/homePage/logo-ks-finance-main.svg';
import logoMediaPartner from '/assets/image/homePage/logo-media-partner-main.svg';
import logoKienLongBank from '/assets/image/homePage/logo-kienlong-bank.svg';
import iconHover from '/assets/image/homePage/icon-hover-partnership-logo.svg';

import './partnership.scss';

import { useTranslation } from 'react-i18next';

import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Autoplay } from 'swiper';

function SectionPartnership(props: { background: string }) {
  const { background } = props;

  const { t } = useTranslation();

  return (
    <article>
      <Row
        className="background-partnership partnership-component"
        style={{ background: `${background}` }}
      >
        <div className="back-ground-partnership back-ground-partnership-component ">
          <div className="max-width-1180 padding-left-right-partnership">
            <div className="row-btn-partnership">
              <button
                type="button"
                className="btn-arrow-left btn-arrow-partnership pre-slide-partnership"
              />
              <button
                type="button"
                className="btn-arrow-right btn-arrow-partnership next-slide-partnership"
              />
              <h2 className="title title-partnership-1 title-partnership-custom">
                {t('component.partnership.title')}
              </h2>
            </div>
            <div className="row-slide-partnership">
              <div className="px-0 col-slide-partnership">
                <Swiper
                  modules={[Navigation, Autoplay]}
                  spaceBetween={16}
                  slidesPerView={4}
                  navigation={{
                    prevEl: '.pre-slide-partnership',
                    nextEl: '.next-slide-partnership',
                  }}
                  className="swiper-partnership"
                  autoplay={{ delay: 3000 }}
                  breakpoints={{
                    320: {
                      slidesPerView: 1.65,
                      spaceBetween: 16,
                    },
                    550: {
                      slidesPerView: 2,
                    },
                    661: {
                      slidesPerView: 3,
                    },
                    992: {
                      slidesPerView: 4,
                    },
                  }}
                >
                  <SwiperSlide>
                    <a
                      href="https://ssh.vn/en/"
                      rel="noreferrer"
                      target="_blank"
                      className="a-link-logo"
                    >
                      <div className="div-logo-partnership">
                        <Image
                          src={logoSunshine}
                          className="logo-slide-partnership img-fluid"
                          alt=""
                        />
                      </div>
                      <div className="overplay-logo-partnership">
                        <Image src={iconHover} className="icon-overplay-logo img-fluid" alt="" />
                        <div className="text-overplay-logo">
                          {t('component.partnership.title2')}
                        </div>
                      </div>
                    </a>
                  </SwiperSlide>
                  <SwiperSlide>
                    <a
                      href="https://scgr.vn/"
                      rel="noreferrer"
                      target="_blank"
                      className="a-link-logo"
                    >
                      <div className="div-logo-partnership">
                        <Image
                          src={logoSmartConstruction}
                          className="logo-slide-partnership img-fluid"
                          alt=""
                        />
                      </div>
                      <div className="overplay-logo-partnership">
                        <Image src={iconHover} className="icon-overplay-logo" alt="" />
                        <div className="text-overplay-logo">
                          {t('component.partnership.title3')}
                        </div>
                      </div>
                    </a>
                  </SwiperSlide>
                  <SwiperSlide>
                    <a
                      href="https://ksfinance.vn/"
                      rel="noreferrer"
                      target="_blank"
                      className="a-link-logo"
                    >
                      <div className="div-logo-partnership">
                        <Image
                          src={logoKSFinance}
                          className="logo-slide-partnership img-fluid"
                          alt=""
                        />
                      </div>
                      <div className="overplay-logo-partnership">
                        <Image src={iconHover} className="icon-overplay-logo" alt="" />
                        <div className="text-overplay-logo">
                          {t('component.partnership.title4')}
                        </div>
                      </div>
                    </a>
                  </SwiperSlide>
                  <SwiperSlide>
                    <a
                      href="https://ode.vn/"
                      rel="noreferrer"
                      target="_blank"
                      className="a-link-logo"
                    >
                      <div className="div-logo-partnership">
                        <Image
                          src={logoMediaPartner}
                          className="logo-slide-partnership img-fluid"
                          alt=""
                        />
                      </div>
                      <div className="overplay-logo-partnership">
                        <Image src={iconHover} className="icon-overplay-logo" alt="" />
                        <div className="text-overplay-logo">
                          {t('component.partnership.title5')}
                        </div>
                      </div>
                    </a>
                  </SwiperSlide>
                  <SwiperSlide>
                    <a
                      href="https://kienlongbank.com/"
                      target="_blank"
                      rel="noreferrer"
                      className="a-link-logo"
                    >
                      <div className="div-logo-partnership">
                        <Image
                          src={logoKienLongBank}
                          className="logo-slide-partnership img-fluid"
                          alt=""
                        />
                      </div>
                      <div className="overplay-logo-partnership">
                        <Image src={iconHover} className="icon-overplay-logo" alt="" />
                        <div className="text-overplay-logo">
                          {t('component.partnership.title6')}
                        </div>
                      </div>
                    </a>
                  </SwiperSlide>
                </Swiper>
              </div>
            </div>
            <Col className="col-mobile-btn">
              <button
                type="button"
                className="btn-arrow-left btn-arrow-partnership-mobile pre-slide-partnership"
              />
              <button
                type="button"
                className="btn-arrow-right btn-arrow-partnership next-slide-partnership"
              />
            </Col>
          </div>
        </div>
      </Row>
    </article>
  );
}

export default SectionPartnership;
