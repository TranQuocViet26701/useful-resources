import React, { useMemo, useState } from 'react';
import { Row, Col, Image, Container } from 'react-bootstrap';
import useWindowSize from '@src/hooks/useWindowsSize';
import { useTranslation } from 'react-i18next';
import './RowTechnology.scss';
import QueryPostNews from '@src/query/queryPostNew';
import { Link, NavLink } from 'react-router-dom';
import queryUtil from '@src/query/queryUtil';

import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination, Autoplay } from 'swiper';

function filterNews(list: any[]) {
  let listNew: any[] = [];

  if (list.length > 0) {
    listNew = list.filter((value) => value?.groupnews?.isdelete === null);
  }
  return listNew;
}

function RowTechnology() {
  const size = useWindowSize();
  const [countSlice, setCountSlice] = useState(68);

  const isMobile = useMemo(() => {
    if (size.width && size.width >= 992) {
      setCountSlice(68);
      return false;
    }
    setCountSlice(50);
    return true;
  }, [size.width]);

  const { t, ready } = useTranslation();
  const { data, loading } = QueryPostNews();

  if (loading) return <div>...</div>;

  return (
    <div className="back-ground-technology">
      <Container className="max-width-1180 padding-left-right-technology">
        <h2 className="title title-technology-1">{ready && t('homepage.technologyNews.title')}</h2>
        <Row className="row-btn-slide-technology justify-content-between mb-xs-0">
          <Col className="px-0 g-0 gx-3">
            <button
              type="button"
              className="btn-arrow-left btn-arrow-technology pre-slide-technology me-3"
            />
            <button type="button" className="btn-arrow-right next-slide-technology" />
          </Col>
          <Col />
          <Col className="px-0 col-xem-them">
            <NavLink to="/news">
              <button type="button" className="btn-view-more-technology">
                {ready && t('homepage.createFuture.viewMore')}
              </button>
            </NavLink>
          </Col>
        </Row>
        <Row>
          <Swiper
            modules={[Pagination, Autoplay]}
            spaceBetween={40}
            slidesPerView={3}
            navigation={{
              prevEl: '.pre-slide-technology',
              nextEl: '.next-slide-technology',
            }}
            pagination={isMobile ? false : { clickable: true }}
            className="swiper-technology"
            autoplay={{ delay: 6000 }}
            breakpoints={{
              320: {
                slidesPerView: 1.5,
                spaceBetween: 16,
              },
              550: {
                slidesPerView: 2,
                spaceBetween: 16,
              },
              861: {
                slidesPerView: 3,
                spaceBetween: 16,
              },
            }}
          >
            {data &&
              filterNews(data?.posts?.nodes).map((post: any) => (
                <SwiperSlide key={post?.postId}>
                  <Link to={post?.groupnews?.link || '/coming-soon'}>
                    <div className="row-item-slide-latest-news">
                      <div className="div-img-slide-news">
                        <Image
                          src={post?.groupnews?.image?.sourceUrl}
                          className="img-fluid img-latest-news"
                        />
                      </div>
                      <div className="latest-news-content">
                        <div className="div-time-latest-news">
                          <p className="date-latest-news">
                            {queryUtil.convertDate(post?.date, 'date') > 10
                              ? queryUtil.convertDate(post?.date, 'date')
                              : `0${queryUtil.convertDate(post?.date, 'date')}`}
                          </p>
                          <p className="month-year-latest-news">
                            {queryUtil.convertDate(post?.date, 'month') > 10
                              ? queryUtil.convertDate(post?.date, 'month')
                              : `0${queryUtil.convertDate(post?.date, 'month')}`}
                            , {queryUtil.convertDate(post?.date, 'year')}
                          </p>
                        </div>
                        <div className="div-detail-latest-news">
                          <p className="author-latest-news">
                            Đăng bởi: <span className="text-author-latest-news">Unicloud</span>
                          </p>
                          <p className="detail-latest-news">
                            {post?.groupnews?.title?.substring(0, countSlice)}
                            {post?.groupnews?.title?.length > countSlice ? '...' : ''}
                          </p>
                        </div>
                      </div>
                    </div>
                  </Link>
                </SwiperSlide>
              ))}
          </Swiper>
        </Row>
      </Container>
    </div>
  );
}
export default RowTechnology;
