import React from 'react';
import { Row, Col, Image, Container } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Autoplay } from 'swiper';
import { Link } from 'react-router-dom';
import '../../pages/news/News.scss';
import queryUtil from '@src/query/queryUtil';

function RowLatestAndRelatedNews(props: { title: string; data: any }) {
  const { title, data } = props;

  function filterNews(list: any[]) {
    let listNew: any[] = [];
    const newDate = new Date();

    if (list.length > 0) {
      listNew = list.filter(
        (value) => value?.groupnews?.isdelete === null || !value?.groupnews?.link,
      );
      listNew = listNew.filter((value) => queryUtil.createDate(value.date) <= newDate);
    }

    return listNew;
  }
  return (
    <div className="back-ground-latest-news">
      <Container className="max-width-1300 padding-left-right-latest-news">
        <Row className="row-btn-slide-latest justify-content-between">
          <Col>
            <h3 className="title-category-news">{title}</h3>
          </Col>
          <Col className="d-flex justify-content-end col-arrow-latest-news">
            <button
              type="button"
              className="btn-arrow-left btn-arrow-latest-news pre-slide-latest-news me-4"
            />
            <button
              type="button"
              className="btn-arrow-right btn-arrow-latest-news next-slide-latest-news"
            />
          </Col>
        </Row>
        <div className="div-under-category" />
        <Row>
          <Swiper
            modules={[Navigation, Autoplay]}
            spaceBetween={16}
            slidesPerView={4}
            navigation={{
              prevEl: '.pre-slide-latest-news',
              nextEl: '.next-slide-latest-news',
            }}
            className="swiper-latest-news"
            // autoplay={{ delay: 6000 }}
            breakpoints={{
              320: {
                slidesPerView: 1.2,
                spaceBetween: 16,
              },
              450: {
                slidesPerView: 1.5,
                spaceBetween: 16,
              },
              550: {
                slidesPerView: 2,
                spaceBetween: 16,
              },
              861: {
                slidesPerView: 3,
                spaceBetween: 16,
              },
              992: {
                slidesPerView: 4,
                spaceBetween: 16,
              },
            }}
          >
            {data?.length > 0 &&
              filterNews(data).map((post: any) => (
                <SwiperSlide key={post?.postId}>
                  <Link to={post?.groupnews?.link || '/coming-soon'}>
                    <div className="row-item-slide-latest-news">
                      <div className="div-img-slide-news">
                        <Image
                          src={post?.groupnews?.image?.sourceUrl}
                          className="img-fluid img-latest-news"
                        />
                      </div>
                      <div className="latest-news-content">
                        <div className="div-time-latest-news">
                          <p className="date-latest-news">
                            {queryUtil.convertDate(post?.date, 'date') > 10
                              ? queryUtil.convertDate(post?.date, 'date')
                              : `0${queryUtil.convertDate(post?.date, 'date')}`}
                          </p>
                          <p className="month-year-latest-news">
                            {queryUtil.convertDate(post?.date, 'month') > 10
                              ? queryUtil.convertDate(post?.date, 'month')
                              : `0${queryUtil.convertDate(post?.date, 'month')}`}
                            , {queryUtil.convertDate(post?.date, 'year')}
                          </p>
                        </div>
                        <div className="div-detail-latest-news">
                          <p className="author-latest-news">
                            Đăng bởi: <span className="text-author-latest-news">Unicloud</span>
                          </p>
                          <p className="detail-latest-news">
                            {post?.groupnews?.title?.substring(0, 62)}
                            {post?.groupnews?.title?.length > 62 ? '...' : ''}
                          </p>
                        </div>
                      </div>
                    </div>
                  </Link>
                </SwiperSlide>
              ))}
          </Swiper>
        </Row>
      </Container>
    </div>
  );
}

RowLatestAndRelatedNews.prototype = {
  title: '',
  data: null,
};

export default RowLatestAndRelatedNews;
