import React, { useCallback, useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import iconLocation from '/assets/image/svg/icon-pin.svg';

interface PositionProps {
  lat: number;
  lng: number;
}

function SectionMap(props: { position?: PositionProps; whenCreated?: Function }) {
  const { position, whenCreated } = props;

  const [isDimout, setIsDimout] = useState(false);

  useEffect(() => {
    if (typeof window !== undefined) {
      setIsDimout(true);
    } else {
      setIsDimout(false);
    }
  }, []);

  const [map, setMap] = useState<any>(null);
  const [L, setL] = useState<any>(null);

  const handelCallBack = useCallback(async () => {
    await import('react-leaflet').then((value) => setMap(value));
    await import('leaflet').then((value) => setL(value));
  }, []);

  useEffect(() => {
    handelCallBack();
  }, []);

  const BuildMap = useMemo(() => {
    if (isDimout && L && map && position && whenCreated) {
      const iconMap = L.icon({
        iconUrl: iconLocation,
        iconSize: [25, 35], // size of the icon
        iconAnchor: [10, 30], // point of the icon which will correspond to marker's location
        popupAnchor: [-3, -76],
        shadowSize: [68, 95], // the same for the shadow
        shadowAnchor: [22, 94],
      });
      return (
        <map.MapContainer
          center={position}
          zoom={15}
          scrollWheelZoom={false}
          style={{ width: '100%', height: 362 }}
          preferCanvas
          whenCreated={whenCreated}
        >
          <map.TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {position && <map.Marker position={position} icon={iconMap} />}
        </map.MapContainer>
      );
    }
    return <div>...</div>;
  }, [isDimout, position, map, L]);
  return (
    <article>
      <div className="map">{BuildMap}</div>
    </article>
  );
}

SectionMap.defaultProps = {
  position: {},
  whenCreated: () => {},
};

SectionMap.prototype = {
  position: PropTypes.object,
  whenCreated: PropTypes.func,
};
export default SectionMap;
