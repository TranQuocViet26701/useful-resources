import React from 'react';
import { Row, Col, Container, FloatingLabel, Form, FormControl } from 'react-bootstrap';
import axios from 'axios';
import { Controller, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router-dom';
import './style.scss';

function FormContact() {
  const { t } = useTranslation();

  const location = useLocation();

  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm({ mode: 'onChange' });

  const onSubmit = (data: any) => {
    // console.log('data', data);
    const formData = new FormData();
    formData.append('name', data.fullName);
    formData.append('phone', data.telephone);
    formData.append('content', data.content);
    formData.append('email', data.email);
    formData.append('boolean', 'true');
    formData.append('list', '6xdCd892x7gSZoG7768926aeLA');
    formData.append('subform', 'yes');

    const googleSheetFormData = new FormData();
    googleSheetFormData.append('fullName', data.fullName);
    googleSheetFormData.append('email', data.email);
    googleSheetFormData.append('telephone', data.telephone);
    googleSheetFormData.append('company', data.company);
    googleSheetFormData.append('content', data.content);
    googleSheetFormData.append('timestamp', new Date().toLocaleDateString().substring(0, 10));
    googleSheetFormData.append('linkedBy', location.pathname);

    axios
      .post('/subscribe', formData)
      .then(() => {
        // console.log('response: ', response.data);
        // console.log('response.status: ', response.status);
        // console.log('response.data: ', response.data);
      })
      .catch(() => {});

    axios
      .post(
        'https://sheet.best/api/sheets/db2c6577-b6f0-4ad7-b78d-d45c7034faaa',
        googleSheetFormData,
      )
      .then(() => {
        // console.log('response: ', response.data);
        // console.log('response.status: ', response.status);
        // console.log('response.data: ', response.data);
      })
      .catch(() => {
        // alert('Lỗi hệ thống.');
      });

    reset();
  };
  return (
    <Container className="max-width-1180 padding-left-right form-contact-wrap">
      <div className="row-form-contact-us">
        <Form className="form-contact px-0" onSubmit={handleSubmit(onSubmit)}>
          <Row className="gx-4">
            <Col xs={12} md={6}>
              <FloatingLabel controlId="floatingTextarea" label="" className="mb-5">
                <Controller
                  control={control}
                  name="fullName"
                  defaultValue=""
                  rules={{ required: true }}
                  render={({
                    field: { onChange, value, ref },
                    fieldState: { invalid, isDirty },
                  }) => (
                    <FormControl
                      onChange={onChange}
                      value={value}
                      ref={ref}
                      isInvalid={errors.fullName}
                      isValid={isDirty && !invalid}
                      className="fullName"
                      placeholder={t('digitalBankingLandingPage.contact.fullName')}
                      aria-label="fullName"
                      aria-describedby="fullName"
                      autoComplete="off"
                    />
                  )}
                />

                <Form.Control.Feedback type="invalid">
                  {Object.keys(errors).length !== 0 && errors.fullName?.type === 'required' && (
                    <span>{t('digitalBankingLandingPage.contact.fullNameError')}</span>
                  )}
                </Form.Control.Feedback>
                <Form.Label>
                  {t('digitalBankingLandingPage.contact.fullName')}{' '}
                  <span style={{ color: 'red' }}> *</span>
                </Form.Label>
              </FloatingLabel>
            </Col>
            <Col xs={12} md={6}>
              <FloatingLabel controlId="floatingInput" label="" className="mb-5">
                <Controller
                  control={control}
                  name="telephone"
                  defaultValue=""
                  rules={{
                    required: true,
                    pattern: {
                      value: /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/,
                      message: '',
                    },
                  }}
                  render={({
                    field: { onChange, value, ref },
                    fieldState: { invalid, isDirty },
                  }) => (
                    <FormControl
                      onChange={onChange}
                      value={value}
                      ref={ref}
                      isInvalid={errors.telephone}
                      isValid={isDirty && !invalid}
                      className="telephone"
                      placeholder={t('digitalBankingLandingPage.contact.telephone')}
                      aria-label="telephone"
                      aria-describedby="telephone"
                      autoComplete="off"
                    />
                  )}
                />
                <Form.Control.Feedback type="invalid">
                  {Object.keys(errors).length !== 0 && errors.telephone?.type === 'pattern' && (
                    <span>{t('digitalBankingLandingPage.contact.telephoneErrorValid')}</span>
                  )}
                  {Object.keys(errors).length !== 0 && errors.telephone?.type === 'required' && (
                    <span>{t('digitalBankingLandingPage.contact.telephoneError')}</span>
                  )}
                </Form.Control.Feedback>
                <Form.Label>
                  {t('digitalBankingLandingPage.contact.telephone')}
                  <span style={{ color: 'red' }}> *</span>
                </Form.Label>
              </FloatingLabel>
            </Col>
          </Row>
          <Row className="gx-4">
            <Col xs={12} md={6}>
              <FloatingLabel controlId="floatingTextarea" label="" className="mb-5">
                <Controller
                  control={control}
                  name="email"
                  defaultValue=""
                  rules={{
                    required: true,
                    pattern: {
                      value: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                      message: '',
                    },
                  }}
                  render={({
                    field: { onChange, value, ref },
                    fieldState: { invalid, isDirty },
                  }) => {
                    return (
                      <FormControl
                        onChange={onChange}
                        value={value}
                        ref={ref}
                        isInvalid={errors.email}
                        isValid={isDirty && !invalid}
                        className="email"
                        placeholder={t('digitalBankingLandingPage.contact.email')}
                        aria-label="email"
                        aria-describedby="email"
                        autoComplete="nope"
                      />
                    );
                  }}
                />

                <Form.Control.Feedback type="invalid">
                  {Object.keys(errors).length !== 0 && errors.email?.type === 'pattern' && (
                    <span>{t('digitalBankingLandingPage.contact.emailErrorValid')}</span>
                  )}
                  {Object.keys(errors).length !== 0 && errors.email?.type === 'required' && (
                    <span>{t('digitalBankingLandingPage.contact.emailError')}</span>
                  )}
                </Form.Control.Feedback>
                <Form.Label>
                  {t('digitalBankingLandingPage.contact.email')}{' '}
                  <span style={{ color: 'red' }}> *</span>
                </Form.Label>
              </FloatingLabel>
            </Col>
            <Col xs={12} md={6}>
              <FloatingLabel controlId="floatingTextarea" label="" className="mb-5">
                <Controller
                  control={control}
                  name="company"
                  defaultValue=""
                  rules={{ required: true }}
                  render={({
                    field: { onChange, value, ref },
                    fieldState: { invalid, isDirty },
                  }) => (
                    <FormControl
                      onChange={onChange}
                      value={value}
                      ref={ref}
                      isInvalid={errors.company}
                      isValid={isDirty && !invalid}
                      className="company"
                      placeholder={t('digitalBankingLandingPage.contact.company')}
                      aria-label="company"
                      aria-describedby="company"
                      autoComplete="off"
                    />
                  )}
                />

                <Form.Control.Feedback type="invalid">
                  {Object.keys(errors).length !== 0 && errors.company?.type === 'required' && (
                    <span>{t('digitalBankingLandingPage.contact.companyError')}</span>
                  )}
                </Form.Control.Feedback>
                <Form.Label>
                  {t('digitalBankingLandingPage.contact.company')}{' '}
                  <span style={{ color: 'red' }}> *</span>
                </Form.Label>
              </FloatingLabel>
            </Col>
          </Row>
          <Row>
            <Form.Group className="mb-4" controlId="exampleForm.ControlTextarea1">
              <Controller
                control={control}
                name="content"
                defaultValue=""
                //   rules={{ required: 'Chưa nhập tên công ty' }}
                render={({ field: { onChange, value, ref }, fieldState: { invalid, isDirty } }) => (
                  <Form.Control
                    onChange={onChange}
                    value={value}
                    ref={ref}
                    isInvalid={errors.content}
                    isValid={isDirty && !invalid}
                    className="content"
                    as="textarea"
                    rows={9}
                    placeholder={t('digitalBankingLandingPage.contact.content')}
                    autoComplete="off"
                  />
                )}
              />
            </Form.Group>
          </Row>
          <Row className="justify-content-center">
            <button type="submit" className="btn-consult-now">
              {t('digitalBankingLandingPage.contact.button')}
            </button>
          </Row>
        </Form>
      </div>
    </Container>
  );
}

export default FormContact;
