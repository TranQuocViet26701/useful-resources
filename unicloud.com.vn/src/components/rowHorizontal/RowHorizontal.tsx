import React from 'react';
import imgLeft from '/assets/image/ecosystem/product/img-360.jpeg';
import './style.scss';

function RowHorizontal(props: any) {
  const { reverse, title, subTitle, description, image, link } = props;
  return (
    <div reverse={reverse} className="row-horizontal container-child">
      <div className="px-0 row-horizontal-left" style={reverse ? { order: '1' } : undefined}>
        <img src={image || imgLeft} alt="" className="img-product" />
      </div>
      <div className="px-0 row-horizontal-right">
        <h3>{title}</h3>
        <h6>{subTitle}</h6>
        {description &&
          description.map((desc: any) => (
            <p className="product-desc" key={desc.num}>
              {desc.desc}
            </p>
          ))}
        <a href={link} target="_blank" rel="noopener noreferrer" className="btn-detail">
          <span>Chi tiết</span>
        </a>
      </div>
    </div>
  );
}

export default RowHorizontal;
