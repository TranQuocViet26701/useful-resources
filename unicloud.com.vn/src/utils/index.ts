// eslint-disable-next-line import/prefer-default-export
export const handleLocaleRedirect = () => {
  if (typeof window !== 'undefined') {
    if (window?.localStorage.getItem('i18nextLng'))
      return window?.localStorage.getItem('i18nextLng') as string;

    // detect browser language
    if (window?.navigator.language) return window?.navigator.language as string;
  }
  return 'vi';
};
