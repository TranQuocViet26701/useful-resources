/* eslint-disable @typescript-eslint/naming-convention */
import { useQuery } from '@apollo/client/react/hooks/useQuery';
import { gql } from 'graphql-tag';
import { useLocation } from 'react-router-dom';
// import { second } from '@apollo/client/utilities/graphql/fragments';

export const queryEMagazine = gql`
  query queryEMagazine($id: ID = "", $idTypes: PostIdType = SLUG) {
    post(id: $id, idType: $idTypes) {
      content(format: RENDERED)
      groupseo {
        ...Post_GroupseoFragment
      }
      groupnews {
        ...Post_GroupnewsFragment
        image {
          ...MediaItemFragment
        }
        imagethumbs {
          ...MediaItemFragment
        }
      }
    }
  }

  fragment Post_GroupseoFragment on Post_Groupseo {
    description
    imagethumbs {
      altText
      sourceUrl
    }
    title
    link
    keywords
  }

  fragment Post_GroupnewsFragment on Post_Groupnews {
    title
    link
    isdelete
    index
    descriptionsort
    category
    stylecss
    postmobile
  }

  fragment MediaItemFragment on MediaItem {
    sourceUrl
    altText
  }
`;

function QueryPostEMagazine() {
  const location = useLocation();
  const urlQuery = location.pathname.slice(11, location.pathname.length);

  const { data, loading, error } = useQuery(queryEMagazine, {
    variables: { id: urlQuery, idTypes: 'SLUG' },
  });
  return { data, loading, error };
}

export default QueryPostEMagazine;
