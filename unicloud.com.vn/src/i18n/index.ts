import i18n, { ReadCallback } from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import resourcesToBackend from 'i18next-resources-to-backend';
import { initReactI18next } from 'react-i18next';

import translation from './translations';

const loadResource = (language: string, namespace: string, callback: ReadCallback) => {
  let fetch;
  if (language === 'vi') {
    fetch = translation(import.meta.glob('./translations/*/vi-VN.json'));
  } else if (language === 'en') {
    fetch = translation(import.meta.glob('./translations/*/en-US.json'));
  }
  if (!fetch) {
    callback(new Error(`${language} not supported`), null);
    return;
  }
  fetch
    .then((resources) => {
      callback(null, resources);
    })
    .catch((error) => {
      callback(error, null);
    });
};

i18n
  .use(resourcesToBackend(loadResource))
  .use(initReactI18next)
  .use(LanguageDetector)
  .init({
    fallbackLng: ['vi', 'en'],
    defaultNS: 'translations',
    partialBundledLanguages: true,
    supportedLngs: ['en', 'vi'],
    interpolation: {
      escapeValue: false,
    },
    debug: false,
    react: {
      useSuspense: false,
    },
    detection: {
      order: ['path', 'cookie', 'localStorage', 'subdomain', 'navigator'],
      caches: ['localStorage', 'cookie'],
    },
  });

export default i18n;
