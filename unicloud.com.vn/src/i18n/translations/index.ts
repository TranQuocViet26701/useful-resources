export default async (data: ReturnType<ImportMeta['glob']>) => {
  let translate = {};
  // eslint-disable-next-line no-restricted-syntax
  for await (const iterator of Object.values(data)) {
    const mod = (await iterator()) as any;
    translate = { ...translate, ...mod.default };
  }
  return translate;
};
