/* eslint-disable react/require-default-props */
/* eslint-disable @typescript-eslint/no-unused-vars */
import * as React from 'react';
import { Helmet } from 'react-helmet';
import { useLocation } from 'react-router-dom';

interface SeoModel {
  title: string;
  description?: string;
  url?: string;
  imgthumbs?: string;
  keywords?: string;
}

function SEO(props: SeoModel) {
  const { title, description, url, imgthumbs, keywords } = props;
  const siteTitle = title || 'Unicloud Group';
  const siteUrl = `https://unicloud.com.vn${url}` || 'https://unicloud.com.vn/';
  const siteImg = imgthumbs || 'https://unicloud.com.vn/meta-data.jpg';
  const siteDescription = description || '';

  return (
    <Helmet>
      <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
      <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
      <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
      <link rel="manifest" href="/site.webmanifest" />
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <html lang="en" />
      <title>{siteTitle}</title>
      <meta name="title" content={siteTitle} data-react data-react-helmet="true" />
      <meta name="description" content={siteDescription} data-react-helmet="true" />
      <meta name="image" content={siteImg} data-react-helmet="true" />
      <meta name="keywords" content={keywords?.toString()} data-react-helmet="true" />

      <link rel="canonical" href={siteUrl} data-react-helmet="true" />

      <meta property="og:type" content="website" data-react-helmet="true" />
      <meta property="og:url" content={siteUrl} data-react-helmet="true" />
      <meta property="og:title" content={siteTitle} data-react-helmet="true" />
      <meta property="og:description" content={siteDescription} data-react-helmet="true" />
      <meta property="og:image" content={siteImg} data-react-helmet="true" />

      <meta property="twitter:card" content="summary_large_image" data-react-helmet="true" />
      <meta property="twitter:url" content={siteUrl} data-react-helmet="true" />
      <meta property="twitter:title" content={siteTitle} data-react-helmet="true" />
      <meta property="twitter:description" content={siteDescription} data-react-helmet="true" />
      <meta property="twitter:image" content={siteImg} data-react-helmet="true" />
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/8.1.4/swiper-bundle.css"
        integrity="sha512-KBVE5XtR1mD+xG5KJdK4mNAvbqLXeD6fbzeSNGLWRQcOdD6sStIZJIUUIN+ng8glwOzQ2x2uRCWkYkqSQSbGvg=="
        crossOrigin="anonymous"
        referrerPolicy="no-referrer"
      />
    </Helmet>
  );
}

export default SEO;
