import React from 'react';

const usePostContext = React.createContext<any>({});

export const PostContextProvider = usePostContext.Provider;
export const PostContextConsumer = usePostContext.Consumer;

export default usePostContext;
