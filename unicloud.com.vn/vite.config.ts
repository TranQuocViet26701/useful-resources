import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import tsConfigPaths from 'vite-tsconfig-paths';
import graphql from '@rollup/plugin-graphql';
import * as path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react({
      jsxRuntime: 'classic',
      fastRefresh: true,
    }),
    tsConfigPaths(),
    graphql(),
  ],

  build: {
    ssrManifest: true,
    rollupOptions: {
      output: {
        entryFileNames: `[name].js`,
        format: 'esm',
      },
    },
  },
});
