import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import * as render from './dist/server/entry-server.js';

const filename = fileURLToPath(import.meta.url);
const dirname = path.dirname(filename);
const toAbsolute = (p) => path.resolve(dirname, p);

const template = fs.readFileSync(toAbsolute('dist/static/index.html'), 'utf-8');

const routesToPrerender = fs.readdirSync(toAbsolute('src/pages')).map((file) => {
  const name = file.replace(/\.jsx$/, '').toLowerCase();
  return name === 'home' ? `/` : `/${name}`;
});

(async () => {
  routesToPrerender.forEach(async (url) => {
    const appHtml = await render.default(url);

    const html = template.replace(`<!--app-html-->`, appHtml);

    const filePath = `dist/static${url === '/' ? '/index' : url}.html`;
    fs.writeFileSync(toAbsolute(filePath), html);
  });
})();
