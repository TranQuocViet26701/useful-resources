### 18/06/2022

- Update Navbar for mobile
- Can change language now (some text may be not translated yet)

### 21/03/2022

- update hero banner
- add page news

### 23/03/2022

- Responsive _About Us_ page
- Responsive footer
- Link technology news slide to footer

### 24/03/2022

- Update Homepage follow new UI

## 31/03/2022

- Add page news and page contact
- Update ecosystem page follow new UI

## 01/04/2022

-Add page news detail

## 05/04/2022

-Update page news detail


## 07/04/2022

-Fix attribute image : alt=""
