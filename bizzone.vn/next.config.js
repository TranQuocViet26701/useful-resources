const path = require('path');
const webpack = require('webpack');
module.exports = {
  images: {
    loader: 'imgix',
    path: 'https://bizzone.vn/',
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  env: {},
  eslint: {
    // Warning: This allows production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  assetPrefix: '',
  module: {
    rules: [
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
      {
        test: /\.svg$/,
        use: [{ loader: 'svg-url-loader' }],
      },
    ],
  },
  exportPathMap: async function (defaultPathMap, { dev, dir, outDir, distDir, buildId }) {
    return {
      '/': { page: '/usee' },
      //   '/404': { page: '/404' },
      //   '/univr360': { page: '/univr360' },
      //   '/univrmobile': { page: '/univrmobile'},
      //   '/univrgame': { page: '/univrgame'},
      //   '/univrtouch': { page: '/univrtouch'},
      //   '/policy': { page: '/policy' },
      //   '/univr360/greeniconic': { page: '/greeniconic' },
      //   '/univr360/greeniconic/utilities': { page: '/greeniconic-utilities' },
      //   '/univr360/greeniconic/house': { page: '/greeniconic-house' },
      //   '/univr360/goldenriver': { page: '/goldenriver' },
      //   '/univr360/goldenriver/house': { page: '/goldenriver-house' },
      //   '/univr360/goldenriver/utilities': { page: '/goldenriver-utilities' },
    };
  },
};
