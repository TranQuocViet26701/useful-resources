import { gql, useQuery } from '@apollo/client';

export const dataPost = gql`
  query QueryDataPost($categoryName: String = "Post Ecosystem") {
    posts(
      where: { categoryName: $categoryName, orderby: { field: TITLE, order: ASC } }
      first: 40
    ) {
      nodes {
        formpost {
          grouplinkdetail {
            description
            fieldGroupName
            link
            title
            index
            typechild
            image {
              altText
              sourceUrl
            }
          }
          typeecosystem
          isdeleted
        }
      }
    }
  }
`;

function QueryPostDetail() {
  const { data, loading, error } = useQuery(dataPost);
  return { data, loading, error };
}

export default QueryPostDetail;
