import { gql, useQuery } from '@apollo/client';

export const queryNews = gql`
  query NewQuery {
    posts(where: { categoryName: "news" }) {
      nodes {
        groupnews {
          author
          category
          descriptionsort
          isdelete
          title
          image {
            altText
            sourceUrl
          }
          link
          index
        }
        content(format: RENDERED)
        date
        postId
      }
    }
  }
`;

function QueryPostNews() {
  const { data, loading, error } = useQuery(queryNews);
  return { data, loading, error };
}

export default QueryPostNews;
