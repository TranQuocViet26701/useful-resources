import { gql, useQuery } from '@apollo/client';
import { useRouter } from 'next/router';

export const queryNews = gql`
  query NewQuery($slug: String = "") {
    postBy(slug: $slug) {
      groupnews {
        author
        category
        descriptionsort
        isdelete
        title
        image {
          altText
          sourceUrl
        }
        link
        imagethumbs {
          altText
          sourceUrl
        }
      }
      groupseo {
        description
        imagethumbs {
          altText
          sourceUrl
        }
        title
        link
        keywords
      }
      content(format: RENDERED)
      date
      postId
    }
  }
`;

function QueryPostNewDetail() {
  const { pathname } = useRouter();

  const urlCurrent = pathname.substring(6, pathname.length);
  const { data, loading, error } = useQuery(queryNews, {
    variables: { slug: urlCurrent },
  });

  return { data, loading, error };
}

export default QueryPostNewDetail;
