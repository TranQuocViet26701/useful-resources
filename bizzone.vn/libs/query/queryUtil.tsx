const GetCategoriesNew = (type: any) => {
  let category: string = '';
  switch (type) {
    case '1':
      category = 'Digital banking platform';
      break;
    case '2':
      category = 'Digital transformation';
      break;
    case '3':
      category = 'Smart City';
      break;
    case '4':
      category = 'Virtual Reality';
      break;

    default:
      break;
  }
  return category;
};

const convertDate = (date: string, filterName: string) => {
  let value: number = 0;
  const dateCurrent = new Date(date);

  switch (filterName) {
    case 'date':
      value = dateCurrent.getDate();
      break;
    case 'month':
      value = dateCurrent.getMonth() + 1;
      break;
    case 'year':
      value = dateCurrent.getFullYear();
      break;
    default:
      break;
  }

  return value;
};

const createDate = (date: string) => {
  const dateCurrent = new Date(date);

  return dateCurrent;
};

const isLinkInternal = (text: string) => {
  const tmp = document.createElement('a');
  tmp.href = text;
  return tmp.host !== window.location.host;
};

const capitalizeFirstLetter = (text: string) => {
  return text && text[0].toUpperCase() + text.slice(1);
};

// eslint-disable-next-line import/no-anonymous-default-export
export default { GetCategoriesNew, convertDate, isLinkInternal, capitalizeFirstLetter, createDate };
