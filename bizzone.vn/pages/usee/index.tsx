import React from 'react';
import { HomepageBizzoneShop } from '../../components/BizzoneShop';

export default function BizzoneShop() {
  return (
    <>
      <HomepageBizzoneShop />
    </>
  );
}
