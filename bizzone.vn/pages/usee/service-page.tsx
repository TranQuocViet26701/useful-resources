import * as React from 'react';
import { ServicepageBizzoneShop } from '../../components/BizzoneShop';

export default function Servicepage() {
  return (
    <>
      <ServicepageBizzoneShop />
    </>
  );
}
