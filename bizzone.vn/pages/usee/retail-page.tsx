import * as React from 'react';
import { RetailpageBizzoneShop } from '../../components/BizzoneShop';

export default function Retailpage() {
  return (
    <>
      <RetailpageBizzoneShop />
    </>
  );
}
