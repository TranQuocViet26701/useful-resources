import { ApolloClient, InMemoryCache } from "@apollo/client";

const client = new ApolloClient({
    uri: "https://data.unicloud.com.vn/graphql",
    cache: new InMemoryCache(),
});

export default client;