import React from 'react';
import { Image, Modal } from 'react-bootstrap';
import styles from './ModalSendSuccess.module.scss';

interface ModalSendSuccessProps {
  show: boolean;
  onCloseModal: (show: boolean) => void;
}

export default function ModalSendSuccess({
  show,
  onCloseModal,
}: ModalSendSuccessProps) {
  const handleCloseModal = () => {
    onCloseModal(false);
  };

  return (
    <Modal
      show={show}
      backdrop="static"
      keyboard
      className={styles['div-modal-pricing-page-bizzone-shop']}
      centered
    >
      <Modal.Body>
        <div
          className={styles['div-send-success-modal-pricing-page-bizzone-shop']}
        >
          <Image
            src="/images/svg/bizzone-shop/icon-success-modal-pricing-page.svg"
            style={{ maxWidth: '286px', maxHeight: '286px' }}
            alt=""
          />
          <h3
            className={styles['title-success-modal-pricing-page-bizzone-shop']}
          >
            Gửi thông tin thành công!
          </h3>
          <p
            className={
              styles['sub-title-success-modal-pricing-page-bizzone-shop']
            }
          >
            Cảm ơn bạn đã quan tâm đến các gói giải pháp của Usee! Bộ
            phận tư vấn của chúng tôi sẽ liên hệ với bạn trong khoảng thời gian
            sớm nhất.
          </p>
        </div>
        {/* Button close modal */}
        <button
          type="button"
          className={styles['btn-close-modal']}
          onClick={handleCloseModal}
        >
          <Image src="/images/svg/bizzone-shop/btn-close-modal.svg" alt="" />
        </button>
      </Modal.Body>
    </Modal>
  );
}
