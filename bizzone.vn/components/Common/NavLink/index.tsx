import Link from 'next/link';
import { useRouter } from 'next/router';

interface NavLinkProps {
  href: string;
  exact?: boolean;
  children: React.ReactNode;
  className: string;
}

export default function NavLink({ href, exact = false, children, className }: NavLinkProps) {
  const { pathname } = useRouter();
  const isActive = exact ? pathname === href : pathname.startsWith(href);

  if (isActive) {
    className += ' active';
  }

  return (
    <Link href={href}>
      <a className={className}>{children}</a>
    </Link>
  );
}
