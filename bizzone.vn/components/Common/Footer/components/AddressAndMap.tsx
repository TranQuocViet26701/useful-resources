import { LatLngExpression, Map } from 'leaflet';
import dynamic from 'next/dynamic';
import React, { useCallback, useState } from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import styles from '../styles/Footer.module.scss';

const SectionMap = dynamic(() => import('../../Map'), {
  ssr: false,
});

const listContact = [
  {
    key: 'aboutUs.address.headquarters',
    title: 'Trụ sở chính',
    description: 'Tầng 10, Toà nhà Sunshine Center, Số 16 Phạm Hùng, Hà Nội',
    urlIcon: '/images/svg/icon-bg-pin.svg',
    position: { lat: 21.0300541, lng: 105.7786784 },
  },
  {
    key: 'aboutUs.address.factory',
    title: 'Chi nhánh TP.Hồ Chí Minh',
    description:
      'Block 5, Lô I-3B-1, Đường N6, Khu Công Nghệ Cao, Phường Tân Phú, Thành phố Thủ Đức, Thành phố Hồ Chí Minh',
    urlIcon: '/images/svg/icon-bg-pin.svg',
    position: { lat: 10.85023, lng: 106.80562 },
  },
  {
    key: 'aboutUs.address.email',
    title: 'Email',
    description: 'info@unicloud.com.vn',
    urlIcon: '/images/svg/icon-bg-mail.svg',
  },
  {
    key: 'aboutUs.address.hotline',
    title: 'Hotline',
    description: '19006054',
    urlIcon: '/images/svg/icon-bg-phone.svg',
  },
];

export default function AddressAndMap() {
  const [position, setPosition] = useState(listContact[0].position);
  const [map, setMap] = useState<Map>();

  const MAP_ZOOM = 20;

  const onSelectContact = useCallback(
    (contact: typeof listContact[0]) => {
      if (contact.position && map) {
        setPosition(contact.position);
        map.setView(contact.position, map.getZoom());
      }
    },
    [map],
  );

  const onMapCreated = useCallback(
    (_map: Map) => {
      setMap(_map);
      _map.setView(listContact[0].position!, MAP_ZOOM);
    },
    [],
  );

  return (
    <Container className={`${styles['map-detail']} ${styles['container-child']}`}>
      <Row className="g-lg-5 g-sm-3">
        <Col sm={12} lg={6}>
          <SectionMap position={position} whenCreated={onMapCreated} />
        </Col>
        <Col sm={12} lg={6}>
          <h3
            className={`${styles['map-detail-address-footer']} fs-4 fw-bold text-gradient-origan`}
          >
            Kết nối với chúng tôi
          </h3>
          <div className={styles['group-contact']}>
            {listContact.map((contact) => (
              <div
                key={contact.key}
                className={`${styles['group-contact-wrap']} d-flex align-items-center mb-3`}
                role="button"
                onClick={() => onSelectContact(contact)}
                aria-hidden="true"
              >
                <Image
                  src={contact.urlIcon}
                  alt=""
                  width={48}
                  className={`${contact.key ? 'align-self-start' : ''}`}
                />
                <div className={styles['contact-wrap-item']}>
                  <h5 className={`${styles['title-contact']} fw-bold fs-18`}>{contact.title}</h5>
                  <p className={`${styles['title-contact']} fs-6`}>{contact.description}</p>
                </div>
              </div>
            ))}
          </div>
        </Col>
      </Row>
    </Container>
  );
}
