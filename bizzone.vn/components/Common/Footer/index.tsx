import { gql, useQuery } from '@apollo/client';
import { useEffect } from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import AddressAndMap from './components/AddressAndMap';
import styles from './styles/Footer.module.scss';

export const dataPost = gql`
  query NewQuery {
    page(id: "cG9zdDoy") {
      social {
        linkedin
        linkfacebook
        linkyoutube
      }
    }
  }
`;

const BuildLogo = () => (
  <Image src="/images/svg/logo-white.svg" alt="logo Unicloud" width={144} height="auto" />
);

function BuildInformation() {
  const { data } = useQuery(dataPost);

  let linkedin;
  let linkfacebook;
  let linkyoutube;
  if (data) {
    linkedin = data?.page?.social?.linkedin;
    linkfacebook = data?.page?.social?.linkfacebook;
    linkyoutube = data?.page?.social?.linkyoutube;
  }

  return (
    <div className={styles['footer-info']}>
      <BuildLogo />
      <p className="text-white fs-14 my-4">
        Unicloud tiên phong chuyển đổi số nâng tầm giá trị chất xám con người Việt Nam, vươn xa thế
        giới.
      </p>
      <h4 className="fs-6 fw-bold text-white mb-3">Kết nối với chúng tôi</h4>
      <div className={`${styles['group-icon-social']} d-flex justify-content-center`}>
        <a href={linkfacebook} target="_blank" rel="noopener noreferrer">
          <Image src="/images/svg/icon-fb.svg" alt="facebook" />
        </a>
        <a href={linkyoutube} target="_blank" rel="noopener noreferrer">
          <Image src="/images/svg/icon-youtube.svg" alt="youtube" />
        </a>
        <a href={linkedin} target="_blank" rel="noopener noreferrer">
          <Image src="/images/svg/icon-linkedin.svg" alt="phone" />
        </a>
      </div>
    </div>
  );
}

function BuildAboutUs(props: { listArray: any[]; title: string }) {
  const { listArray, title } = props;
  return (
    <div className={styles['footer-about-us']}>
      <h4 className="text-white fw-bold fs-4">{title}</h4>
      <ul>
        {listArray?.map((about) => (
          <li key={about.id}>
            <a
              className={`${styles['item-about']} text-white`}
              href={about.url}
              target={about.url.includes('https://') ? '_blank' : '_self'}
              rel="noreferrer"
            >
              {about.title}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
}

function BuildApps() {
  return (
    <div className={styles['footer-apps']}>
      <h4 className="text-white fw-bold fs-4">Kho ứng dụng</h4>
      <Image className="my-3" src="/images/homePage/icon-down-apple.png" alt="" />
      <Image className="my-3" src="/images/homePage/icon-down-chplay.png" alt="" />
    </div>
  );
}
const scrollTop = () => {
  window.scrollTo({ top: 0, behavior: 'smooth' });
};
function Footer() {
  const listAbout: any[] = [
    { id: 0, title: 'Giới thiệu', url: '/about-us' },
    { id: 1, title: 'Hệ sinh thái', url: '/ecosystem' },
    { id: 2, title: 'Tin công nghệ', url: '/news' },
    {
      id: 3,
      title: 'Tuyển dụng',
      url: 'https://career.unicloud.com.vn',
    },
    { id: 4, title: 'Liên hệ', url: '/contact-us' },
  ];

  const listHref: any[] = [{ id: 0, title: 'Điều khoản sử dụng', url: '/policy-terms' }];

  useEffect(() => {
    const funcScroll = () => {
      const iconUpTop2 = document.getElementById('div-icon-up-top');
      const windowHeightScroll = window.scrollY;
      if (window !== undefined && iconUpTop2) {
        if (windowHeightScroll >= 40) {
          iconUpTop2.style.display = 'block';
        } else {
          iconUpTop2.style.display = 'none';
        }
      }
    };
    window.addEventListener('scroll', funcScroll);
    return () => {
      window.removeEventListener('scroll', funcScroll);
    };
  }, []);

  return (
    <section>
      <footer className={styles['footer']}>
        <AddressAndMap />
        <div className={styles['bg-footer']}>
          <div className={styles['background-parent']}>
            <Container className={`${styles['footer-wrap']} ${styles['container-child']}`}>
              <Row className="g-0 gy-md-5">
                <Col sm={12} md={12} lg={4} className="d-flex justify-content-center">
                  <BuildInformation />
                </Col>
                <Col sm={6} lg={2}>
                  <BuildAboutUs listArray={listAbout} title="Về Unicloud" />
                </Col>
                <Col sm={6} lg={3}>
                  <BuildAboutUs listArray={listHref} title="Liên kết" />
                </Col>
                <Col sm={12} md={12} lg={3} className="d-flex justify-content-center text-center">
                  <BuildApps />
                </Col>
              </Row>
            </Container>
          </div>
          <section>
            <div className={`${styles['copy-right']} text-white`}>
              <span>© UnicloudGroup 2022. All Rights Reserved.</span>
            </div>
          </section>
        </div>
      </footer>
      <button
        type="button"
        onClick={scrollTop}
        className={styles['div-icon-up-top']}
        id="div-icon-up-top"
      >
        <Image
          src="/images/svg/icon-up-top-main.svg"
          className={`img-fluid ${styles['img-icon-up-top']}`}
          alt=""
        />
      </button>
    </section>
  );
}

export default Footer;
