import React, { useState, useEffect, FC, createElement, Fragment, ReactNode } from 'react';

interface Props {
  children: ReactNode;
}

/** React component that renders its children client-side only / after first mount */
const ClientOnly = ({ children }: Props) => {
  const hasMounted = useClientOnly();

  if (!hasMounted) {
    return null;
  }

  // eslint-disable-next-line react/no-children-prop
  return createElement(Fragment, { children });
};

/** React hook that returns true if the component has mounted client-side */
const useClientOnly = () => {
  const [hasMounted, setHasMounted] = useState(false);

  useEffect(() => {
    setHasMounted(true);
  }, []);

  return hasMounted;
};

export default ClientOnly;
