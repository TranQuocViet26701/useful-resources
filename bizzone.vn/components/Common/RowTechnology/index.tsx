import { useMemo, useState } from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';

// import useWindowSize from '@src/hooks/useWindowsSize';

import styles from './RowTechnology.module.scss';

import Link from 'next/link';
import SwiperCore, { Autoplay, Navigation, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import useWindowSize from '../../../hooks/useWindowsSize';
import QueryPostNews from '../../../libs/query/queryPostNew';
import queryUtil from '../../../libs/query/queryUtil';

import t from './vi-VN.json';

SwiperCore.use([Navigation]);

function filterNews(list: any[]) {
  let listNew: any[] = [];

  if (list.length > 0) {
    listNew = list.filter((value) => value.groupnews.isdelete === null);
  }
  return listNew;
}

function RowTechnology() {
  const size = useWindowSize();
  const [countSlice, setCountSlice] = useState(68);

  const isMobile = useMemo(() => {
    if (size.width && size.width >= 992) {
      setCountSlice(68);
      return false;
    }
    setCountSlice(50);
    return true;
  }, [size.width]);

  // const { t } = useTranslation();
  const { data, loading } = QueryPostNews();

  if (loading) return <div>...</div>;

  return (
    <div className={styles['back-ground-technology']}>
      <Container className={`max-width-1180 ${styles['padding-left-right-technology']}`}>
        <h2 className={`${styles['title-technology-1']} ${styles['title']}`}>
          {t['homepage.technologyNews.title']}
        </h2>
        <Row className={`${styles['row-btn-slide-technology']} justify-content-between mb-xs-0`}>
          <Col className="px-0 g-0 gx-3">
            <button
              type="button"
              className={`pre-slide-technology ${styles['btn-arrow-left']} ${styles['btn-arrow-technology']} me-3`}
            />
            <button
              type="button"
              className={`next-slide-technology ${styles['btn-arrow-right']}`}
            />
          </Col>
          <Col />
          <Col className="px-0 col-xem-them">
            <Link href="/news">
              <a>
                <button type="button" className={styles['btn-view-more-technology']}>
                  {t['homepage.createFuture.viewMore']}
                </button>
              </a>
            </Link>
          </Col>
        </Row>
        <Row>
          <Swiper
            modules={[Pagination, Autoplay]}
            spaceBetween={40}
            slidesPerView={3}
            navigation={{
              prevEl: '.pre-slide-technology',
              nextEl: '.next-slide-technology',
            }}
            pagination={isMobile ? false : { clickable: true }}
            className={styles['swiper-technology']}
            autoplay={{ delay: 6000 }}
            breakpoints={{
              320: {
                slidesPerView: 1.5,
                spaceBetween: 16,
              },
              550: {
                slidesPerView: 2,
                spaceBetween: 16,
              },
              861: {
                slidesPerView: 3,
                spaceBetween: 16,
              },
            }}
          >
            {data &&
              filterNews(data?.posts?.nodes).map((post: any) => (
                <SwiperSlide key={post.postId}>
                  <Link href={post?.groupnews.link}>
                    <a>
                      <div className={styles['row-item-slide-latest-news']}>
                        <div className={styles['div-img-slide-news']}>
                          <Image
                            src={post?.groupnews?.image?.sourceUrl}
                            className={`${styles['img-latest-news']} img-fluid`}
                            alt=""
                          />
                        </div>
                        <div className={styles['latest-news-content']}>
                          <div className={styles['div-time-latest-news']}>
                            <p className={styles['date-latest-news']}>
                              {queryUtil.convertDate(post.date, 'date') > 10
                                ? queryUtil.convertDate(post.date, 'date')
                                : `0${queryUtil.convertDate(post.date, 'date')}`}
                            </p>
                            <p className={styles['month-year-latest-news']}>
                              {queryUtil.convertDate(post.date, 'month') > 10
                                ? queryUtil.convertDate(post.date, 'month')
                                : `0${queryUtil.convertDate(post.date, 'month')}`}
                              , {queryUtil.convertDate(post.date, 'year')}
                            </p>
                          </div>
                          <div className={styles['div-detail-latest-news']}>
                            <p className={styles['author-latest-news']}>
                              Đăng bởi:{' '}
                              <span className={styles['text-author-latest-news']}>Unicloud</span>
                            </p>
                            <p className={styles['detail-latest-news']}>
                              {post?.groupnews?.title?.substring(0, countSlice)}
                              {post?.groupnews?.title?.length > countSlice ? '...' : ''}
                            </p>
                          </div>
                        </div>
                      </div>
                    </a>
                  </Link>
                </SwiperSlide>
              ))}
          </Swiper>
        </Row>
      </Container>
    </div>
  );
}
export default RowTechnology;
