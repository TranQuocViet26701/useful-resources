import * as React from 'react';
import { Row, Form, FloatingLabel, Modal, FormControl } from 'react-bootstrap';
import axios from 'axios';
import styles from './ModalAdviseDigitalBanking.module.scss';
import { Controller, useForm } from 'react-hook-form';
// import { useTranslation } from 'react-i18next';
import { useRouter } from 'next/router';
// import { useLocation } from 'react-router-dom';

import t from '../vi-VN.json';

function ModalAdviseDigitalBanking(props: any) {
  // const { t } = useTranslation();
  const location = useRouter();

  const googleSheetAPI =
    'https://script.google.com/macros/s/AKfycbxTlQD1WgGBuFKoKVjf6tiUGERX6DHHhxJfywGZ6R4xuNEurMdCdW4fbRoZwBW4jK6M/exec';

  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm({ mode: 'onChange' });

  const onSubmit = (data: any) => {
    const formData = new FormData();
    formData.append('name', data.fullName);
    formData.append('phone', data.telephone);
    formData.append('email', data.email);
    formData.append('content', `Công ty : ${data.company}`);
    formData.append('boolean', 'true');
    formData.append('list', '6xdCd892x7gSZoG7768926aeLA');
    formData.append('subform', 'yes');

    const googleSheetFormData = new FormData();
    googleSheetFormData.append('fullName', data.fullName);
    googleSheetFormData.append('email', data.email);
    googleSheetFormData.append('telephone', data.telephone);
    googleSheetFormData.append('company', data.company);
    googleSheetFormData.append('content', '');
    googleSheetFormData.append('timestamp', new Date().toLocaleDateString().substring(0, 10));
    googleSheetFormData.append('linkedBy', location.pathname);

    axios
      .post('/subscribe', formData)
      .then(() => {
        // console.log('response: ', response.data);
        // console.log('response.status: ', response.status);
        // console.log('response.data: ', response.data);
      })
      .catch(() => {
        // console.error('Something went wrong!', error);
      });

    axios
      .post(googleSheetAPI, googleSheetFormData)
      .then(() => {
        // console.log('response: ', response.data);
        // console.log('response.status: ', response.status);
        // console.log('response: ', response.data);
        //
      })
      .catch(() => {
        // console.error('Something went wrong!', error);
        // alert('Lỗi hệ thống.');
      });
    // console.log('data', formData);
    reset();
  };
  return (
    <Modal
      /* eslint-disable-next-line react/jsx-props-no-spreading */
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className={styles['modal-digital-banking']}
    >
      <Modal.Header closeButton />
      <Modal.Body>
        <Form
          className={`${styles['form-contact-digital-digital-popup']} px-0`}
          onSubmit={handleSubmit(onSubmit)}
        >
          <Row className="gx-4">
            <FloatingLabel controlId="floatingTextarea" label="" className="mb-4">
              <Controller
                control={control}
                name="fullName"
                defaultValue=""
                rules={{ required: true }}
                render={({ field: { onChange, value, ref }, fieldState: { invalid, isDirty } }) => (
                  <FormControl
                    onChange={onChange}
                    value={value}
                    ref={ref}
                    isInvalid={errors.fullName}
                    isValid={isDirty && !invalid}
                    placeholder={t['digitalBankingLandingPage.contact.fullName']}
                    aria-label="fullName"
                    aria-describedby="fullName"
                    autoComplete="off"
                  />
                )}
              />

              <Form.Control.Feedback type="invalid">
                {Object.keys(errors).length !== 0 && errors.fullName?.type === 'required' && (
                  <span>{t['digitalBankingLandingPage.contact.fullNameError']}</span>
                )}
              </Form.Control.Feedback>
              <Form.Label>
                {t['digitalBankingLandingPage.contact.fullName']}{' '}
                <span style={{ color: 'red' }}>*</span>
              </Form.Label>
            </FloatingLabel>
          </Row>
          <Row>
            <FloatingLabel controlId="floatingTextarea" label="" className="mb-4">
              <Controller
                control={control}
                name="telephone"
                defaultValue=""
                rules={{
                  required: true,
                  pattern: {
                    value: /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/,
                    message: `${t['digitalBankingLandingPage.contact.telephoneErrorValid']}`,
                  },
                }}
                render={({ field: { onChange, value, ref }, fieldState: { invalid, isDirty } }) => (
                  <FormControl
                    onChange={onChange}
                    value={value}
                    ref={ref}
                    isInvalid={errors.telephone}
                    isValid={isDirty && !invalid}
                    placeholder={t['digitalBankingLandingPage.contact.telephone']}
                    aria-label="telephone"
                    aria-describedby="telephone"
                    autoComplete="off"
                  />
                )}
              />
              <Form.Control.Feedback type="invalid">
                {Object.keys(errors).length !== 0 && errors.telephone?.type === 'pattern' && (
                  <span>{t['digitalBankingLandingPage.contact.telephoneErrorValid']}</span>
                )}
                {Object.keys(errors).length !== 0 && errors.telephone?.type === 'required' && (
                  <span>{t['digitalBankingLandingPage.contact.telephoneError']}</span>
                )}
              </Form.Control.Feedback>
              <Form.Label>
                {t['digitalBankingLandingPage.contact.telephone']}{' '}
                <span style={{ color: 'red' }}>*</span>
              </Form.Label>
            </FloatingLabel>
          </Row>
          <Row className="gx-4">
            <FloatingLabel controlId="floatingTextarea" label="" className="mb-4">
              <Controller
                control={control}
                name="email"
                defaultValue=""
                rules={{
                  required: true,
                  pattern: {
                    value: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                    message: `${t['digitalBankingLandingPage.contact.emailErrorValid']}`,
                  },
                }}
                render={({ field: { onChange, value, ref }, fieldState: { invalid, isDirty } }) => {
                  return (
                    <FormControl
                      onChange={onChange}
                      value={value}
                      ref={ref}
                      isInvalid={errors.email}
                      isValid={isDirty && !invalid}
                      placeholder={t['digitalBankingLandingPage.contact.email']}
                      aria-label="email"
                      aria-describedby="email"
                      autoComplete="nope"
                    />
                  );
                }}
              />

              <Form.Control.Feedback type="invalid">
                {Object.keys(errors).length !== 0 && errors.email?.type === 'pattern' && (
                  <span>{t['digitalBankingLandingPage.contact.emailErrorValid']}</span>
                )}
                {Object.keys(errors).length !== 0 && errors.email?.type === 'required' && (
                  <span>{t['digitalBankingLandingPage.contact.emailError']}</span>
                )}
              </Form.Control.Feedback>
              <Form.Label>
                {t['digitalBankingLandingPage.contact.email']}{' '}
                <span style={{ color: 'red' }}>*</span>
              </Form.Label>
            </FloatingLabel>
          </Row>
          <Row>
            <FloatingLabel controlId="floatingTextarea" label="" className="mb-4">
              <Controller
                control={control}
                name="company"
                defaultValue=""
                rules={{ required: true }}
                render={({ field: { onChange, value, ref }, fieldState: { invalid, isDirty } }) => (
                  <FormControl
                    onChange={onChange}
                    value={value}
                    ref={ref}
                    isInvalid={errors.company}
                    isValid={isDirty && !invalid}
                    placeholder={t['digitalBankingLandingPage.contact.company']}
                    aria-label="company"
                    aria-describedby="company"
                    autoComplete="off"
                  />
                )}
              />

              <Form.Control.Feedback type="invalid">
                {Object.keys(errors).length !== 0 && errors.company?.type === 'required' && (
                  <span>{t['digitalBankingLandingPage.contact.companyError']}</span>
                )}
              </Form.Control.Feedback>
              <Form.Label>
                {t['digitalBankingLandingPage.contact.company']}{' '}
                <span style={{ color: 'red' }}>*</span>
              </Form.Label>
            </FloatingLabel>
          </Row>
          <div className="justify-content-center">
            <button type="submit" className={styles['btn-consult-now-digital-popup']}>
              {t['digitalBankingLandingPage.getAdvice']}
            </button>
          </div>
        </Form>
      </Modal.Body>
    </Modal>
  );
}

export default ModalAdviseDigitalBanking;
