import * as React from 'react';
import { Modal } from 'react-bootstrap';
import ReactPlayer from 'react-player';
import styles from './modalVideo.module.scss';

function ModalVideo(props: any) {
  const { urlvideo } = props;

  return (
    <Modal
      /* eslint-disable-next-line react/jsx-props-no-spreading */
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      className={styles['modal-video']}
    >
      <ReactPlayer
        //   ref={ref}
        className={styles['iframe-youtube-sliving']}
        url={urlvideo}
        controls
        playing
        width={1180}
        // onStart=
      />
    </Modal>
  );
}

export default ModalVideo;
