import L, { LatLngExpression, LatLngLiteral } from 'leaflet';
import React from 'react';
import { MapContainer, MapContainerProps, Marker, TileLayer, Tooltip } from 'react-leaflet';

const positionList = {
  headquarters: { lat: 21.0300541, lng: 105.7786784 },
  factory: { lat: 10.85023, lng: 106.80562 },
};

type Props = {
  position?: LatLngLiteral;
  whenCreated?: MapContainerProps['whenCreated'];
};

function SectionMap({ position, whenCreated }: Props) {
  const iconMap = L.icon({
    iconUrl: '/images/icon/location-pin.svg',
    iconSize: [35, 35], // size of the icon
    iconAnchor: [22, 35], // point of the icon which will correspond to marker's location
    popupAnchor: [-3, -76],
    shadowSize: [68, 95], // the same for the shadow
    shadowAnchor: [22, 94],
  });

  return (
    <article>
      <div className="map">
        <MapContainer
          center={position}
          zoom={20}
          scrollWheelZoom={false}
          style={{ width: '100%', height: 362, zIndex: '1' }}
          preferCanvas
          whenCreated={whenCreated}
        >
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {position && (
            <Marker position={position} icon={iconMap}>
              <Tooltip direction="top" offset={[-5, -20]} opacity={1} permanent>
                {position.lat === positionList.headquarters.lat &&
                position.lng === positionList.headquarters.lng
                  ? 'Sunshine Center'
                  : 'Unicloud Smart Factory'}
              </Tooltip>
            </Marker>
          )}
        </MapContainer>
      </div>
    </article>
  );
}

SectionMap.defaultProps = {
  position: undefined,
  whenCreated: undefined,
};

export default SectionMap;
