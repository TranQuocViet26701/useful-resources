import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

import styles from '../../styles/Pricingpage.module.scss';

function RowTopPricingPage() {
  return (
    <div className={styles['bg-top-pricing-bizzone-shop']}>
      <Container className="max-width-1180 px-0">
        <Row className="gx-5 padding-left-right">
          <Col md={6}>
            <h2 className={styles['title-top-pricing-bizzone-shop']}>
              Bảng giá phần mềm quản lý bán hàng
            </h2>
            <p className={styles['sub-title-top-pricing-bizzone-shop']}>
              Hãy lựa chọn gói giải pháp phù hợp với nhu cầu của bạn
            </p>
          </Col>
          <Col md={6} />
        </Row>
      </Container>
    </div>
  );
}
export default RowTopPricingPage;
