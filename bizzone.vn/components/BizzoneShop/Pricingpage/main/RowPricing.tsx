import React, { useState } from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import { Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { ModalFormContact, ModalSendSuccess } from '../../../Common';
import styles from '../../styles/Pricingpage.module.scss';

const baseSolutionList = [
  {
    id: '1',
    title: 'Cơ bản 1',
    pricePerYear: 'Đang cập nhật',
    desc: 'Dành cho cửa hàng nhỏ mới kinh doanh',
    features: [
      'Quản lý tồn kho',
      'Quản lý doanh thu',
      'Quản lý nhân viên',
      'Quản lý giao dịch',
      'Quản lý khách hàng',
    ],
  },
  {
    id: '2',
    title: 'Cơ bản 2',
    pricePerYear: 'Đang cập nhật',
    desc: 'Dành cho cửa hàng nhỏ mới kinh doanh',
    features: [
      'Quản lý tồn kho',
      'Quản lý doanh thu',
      'Quản lý nhân viên',
      'Quản lý giao dịch',
      'Quản lý khách hàng',
    ],
  },
  {
    id: '3',
    title: 'Cơ bản 3',
    pricePerYear: 'Đang cập nhật',
    desc: 'Dành cho cửa hàng nhỏ mới kinh doanh',
    features: [
      'Quản lý tồn kho',
      'Quản lý doanh thu',
      'Quản lý nhân viên',
      'Quản lý giao dịch',
      'Quản lý khách hàng',
    ],
  },
];

const enhancedSolutionList = [
  {
    id: '4',
    title: 'Nâng cao 1',
    pricePerYear: 'Đang cập nhật',
    desc: 'Dành cho cửa hàng nhỏ mới kinh doanh',
    features: [
      'Quản lý tồn kho',
      'Quản lý doanh thu',
      'Quản lý nhân viên',
      'Quản lý giao dịch',
      'Quản lý khách hàng',
    ],
  },
  {
    id: '5',
    title: 'Nâng cao 2',
    pricePerYear: 'Đang cập nhật',
    desc: 'Dành cho cửa hàng nhỏ mới kinh doanh',
    features: [
      'Quản lý tồn kho',
      'Quản lý doanh thu',
      'Quản lý nhân viên',
      'Quản lý giao dịch',
      'Quản lý khách hàng',
    ],
  },
  {
    id: '6',
    title: 'Nâng cao 3',
    pricePerYear: 'Đang cập nhật',
    desc: 'Dành cho cửa hàng nhỏ mới kinh doanh',
    features: [
      'Quản lý tồn kho',
      'Quản lý doanh thu',
      'Quản lý nhân viên',
      'Quản lý giao dịch',
      'Quản lý khách hàng',
    ],
  },
];

const preniumSolutionList = [
  {
    id: '7',
    title: 'Cao cấp 1',
    pricePerYear: 'Đang cập nhật',
    desc: 'Dành cho cửa hàng nhỏ mới kinh doanh',
    features: [
      'Quản lý tồn kho',
      'Quản lý doanh thu',
      'Quản lý nhân viên',
      'Quản lý giao dịch',
      'Quản lý khách hàng',
    ],
  },
  {
    id: '8',
    title: 'Cao cấp 2',
    pricePerYear: 'Đang cập nhật',
    desc: 'Dành cho cửa hàng nhỏ mới kinh doanh',
    features: [
      'Quản lý tồn kho',
      'Quản lý doanh thu',
      'Quản lý nhân viên',
      'Quản lý giao dịch',
      'Quản lý khách hàng',
    ],
  },
  {
    id: '9',
    title: 'Cao cấp 3',
    pricePerYear: 'Đang cập nhật',
    desc: 'Dành cho cửa hàng nhỏ mới kinh doanh',
    features: [
      'Quản lý tồn kho',
      'Quản lý doanh thu',
      'Quản lý nhân viên',
      'Quản lý giao dịch',
      'Quản lý khách hàng',
    ],
  },
];

const googleSheetAPI =
  'https://script.google.com/macros/s/AKfycbxTlQD1WgGBuFKoKVjf6tiUGERX6DHHhxJfywGZ6R4xuNEurMdCdW4fbRoZwBW4jK6M/exec';

function RowPricing() {
  const [showFormModal, setShowFormModal] = useState(false);
  const [showSucessModal, setShowSucessModal] = useState(false);
  const [selectedPackage, setSelectedPackage] = useState('');

  const handleSetFormContact = (show: boolean) => {
    setShowFormModal(show);
  };

  const handleSetSuccessModal = (show: boolean) => {
    setShowSucessModal(show);
  };

  const handleChoosePackage = (packageId: string) => {
    setSelectedPackage(packageId);
    setShowFormModal(true);
  };

  return (
    <>
      <div className={styles['back-ground-pricing-bizzone-shop']}>
        <Container className="max-width-1180 px-0">
          <div className="padding-left-right">
            <div className={styles['div-wrapper-row-pricing']}>
              <h2 className={styles['title-pricing-bizzone-shop']}>Phiên bản cơ bản</h2>
              <div className={styles['div-under-pricing-page']} />
              <Swiper
                pagination={{
                  dynamicBullets: true,
                }}
                modules={[Pagination]}
                className={`${styles['swiper-pricing-page-bizzone-shop']} mt-5 d-lg-none`}
              >
                {baseSolutionList.map((s) => (
                  <SwiperSlide key={s.id}>
                    <div className={styles['div-wrapper-pricing-bizzone-shop']}>
                      <h3 className={styles['title-pricing-bizzone-shop-2']}>
                        {s.title.toUpperCase()}
                      </h3>
                      <p className={styles['sub-title-pricing-bizzone-shop-1']}>
                        <span>
                          {s.pricePerYear}
                          {/* <sup>đ</sup> */}
                        </span>
                        {/* /năm */}
                      </p>
                      <p className={styles['sub-title-pricing-bizzone-shop-2']}>{s.desc}</p>
                      <div className={styles['div-under-pricing-page-2']} />
                      <div className={styles['div-wrapper-benefis-pricing']}>
                        {s.features.map((f) => (
                          <p key={f}>
                            <Image
                              src="/images/svg/icon-tich-v-pricing-page-bizzone-shop.svg"
                              alt=""
                            />
                            <span>{f}</span>
                          </p>
                        ))}
                      </div>
                      <button
                        type="button"
                        className={styles['btn-choose-package']}
                        onClick={() => handleChoosePackage(s.id)}
                      >
                        <span>Chọn gói</span>
                      </button>
                    </div>
                  </SwiperSlide>
                ))}
              </Swiper>
              <Row className="gx-4 gy-4 mt-5">
                {baseSolutionList.map((s) => (
                  <Col key={s.id} lg={4} className="d-none d-lg-block">
                    <div className={styles['div-wrapper-pricing-bizzone-shop']}>
                      <h3 className={styles['title-pricing-bizzone-shop-2']}>
                        {s.title.toUpperCase()}
                      </h3>
                      <p className={styles['sub-title-pricing-bizzone-shop-1']}>
                        <span>
                          {s.pricePerYear}
                          {/* <sup>đ</sup> */}
                        </span>
                        {/* /năm */}
                      </p>
                      <p className={styles['sub-title-pricing-bizzone-shop-2']}>{s.desc}</p>
                      <div className={styles['div-under-pricing-page-2']} />
                      <div className={styles['div-wrapper-benefis-pricing']}>
                        {s.features.map((f) => (
                          <p key={f}>
                            <Image
                              src="/images/svg/icon-tich-v-pricing-page-bizzone-shop.svg"
                              alt=""
                            />
                            <span>{f}</span>
                          </p>
                        ))}
                      </div>
                      <button
                        type="button"
                        className={styles['btn-choose-package']}
                        onClick={() => handleChoosePackage(s.id)}
                      >
                        <span>Chọn gói</span>
                      </button>
                    </div>
                  </Col>
                ))}
              </Row>
            </div>
            <div className={styles['div-wrapper-row-pricing']}>
              <h2 className={styles['title-pricing-bizzone-shop']}>Phiên bản nâng cao</h2>
              <div className={styles['div-under-pricing-page']} />
              <Swiper
                pagination={{
                  dynamicBullets: true,
                }}
                modules={[Pagination]}
                className={`${styles['swiper-pricing-page-bizzone-shop']} mt-5 d-lg-none`}
              >
                {enhancedSolutionList.map((s) => (
                  <SwiperSlide key={s.id}>
                    <div className={styles['div-wrapper-pricing-bizzone-shop']}>
                      <h3 className={styles['title-pricing-bizzone-shop-2']}>
                        {s.title.toUpperCase()}
                      </h3>
                      <p className={styles['sub-title-pricing-bizzone-shop-1']}>
                        <span>
                          {s.pricePerYear}
                          {/* <sup>đ</sup> */}
                        </span>
                        {/* /năm */}
                      </p>
                      <p className={styles['sub-title-pricing-bizzone-shop-2']}>{s.desc}</p>
                      <div className={styles['div-under-pricing-page-2']} />
                      <div className={styles['div-wrapper-benefis-pricing']}>
                        {s.features.map((f) => (
                          <p key={f}>
                            <Image
                              src="/images/svg/icon-tich-v-pricing-page-bizzone-shop.svg"
                              alt=""
                            />
                            <span>{f}</span>
                          </p>
                        ))}
                      </div>
                      <button
                        type="button"
                        className={styles['btn-choose-package']}
                        onClick={() => handleChoosePackage(s.id)}
                      >
                        <span>Chọn gói</span>
                      </button>
                    </div>
                  </SwiperSlide>
                ))}
              </Swiper>
              <Row className="gx-4 gy-4 mt-5">
                {enhancedSolutionList.map((s) => (
                  <Col key={s.id} lg={4} className="d-none d-lg-block">
                    <div className={styles['div-wrapper-pricing-bizzone-shop']}>
                      <h3 className={styles['title-pricing-bizzone-shop-2']}>
                        {s.title.toUpperCase()}
                      </h3>
                      <p className={styles['sub-title-pricing-bizzone-shop-1']}>
                        <span>
                          {s.pricePerYear}
                          {/* <sup>đ</sup> */}
                        </span>
                        {/* /năm */}
                      </p>
                      <p className={styles['sub-title-pricing-bizzone-shop-2']}>{s.desc}</p>
                      <div className={styles['div-under-pricing-page-2']} />
                      <div className={styles['div-wrapper-benefis-pricing']}>
                        {s.features.map((f) => (
                          <p key={f}>
                            <Image
                              src="/images/svg/icon-tich-v-pricing-page-bizzone-shop.svg"
                              alt=""
                            />
                            <span>{f}</span>
                          </p>
                        ))}
                      </div>
                      <button
                        type="button"
                        className={styles['btn-choose-package']}
                        onClick={() => handleChoosePackage(s.id)}
                      >
                        <span>Chọn gói</span>
                      </button>
                    </div>
                  </Col>
                ))}
              </Row>
            </div>
            <div>
              <h2 className={styles['title-pricing-bizzone-shop']}>Phiên bản cao cấp</h2>
              <div className={styles['div-under-pricing-page']} />
              <Swiper
                pagination={{
                  dynamicBullets: true,
                }}
                modules={[Pagination]}
                className={`${styles['swiper-pricing-page-bizzone-shop']} mt-5 d-lg-none`}
              >
                {preniumSolutionList.map((s) => (
                  <SwiperSlide key={s.id}>
                    <div className={styles['div-wrapper-pricing-bizzone-shop']}>
                      <h3 className={styles['title-pricing-bizzone-shop-2']}>
                        {s.title.toUpperCase()}
                      </h3>
                      <p className={styles['sub-title-pricing-bizzone-shop-1']}>
                        <span>
                          {s.pricePerYear}
                          {/* <sup>đ</sup> */}
                        </span>
                        {/* /năm */}
                      </p>
                      <p className={styles['sub-title-pricing-bizzone-shop-2']}>{s.desc}</p>
                      <div className={styles['div-under-pricing-page-2']} />
                      <div className={styles['div-wrapper-benefis-pricing']}>
                        {s.features.map((f) => (
                          <p key={f}>
                            <Image
                              src="/images/svg/icon-tich-v-pricing-page-bizzone-shop.svg"
                              alt=""
                            />
                            <span>{f}</span>
                          </p>
                        ))}
                      </div>
                      <button
                        type="button"
                        className={styles['btn-choose-package']}
                        onClick={() => handleChoosePackage(s.id)}
                      >
                        <span>Chọn gói</span>
                      </button>
                    </div>
                  </SwiperSlide>
                ))}
              </Swiper>
              <Row className="gx-4 gy-4 mt-5">
                {preniumSolutionList.map((s) => (
                  <Col key={s.id} lg={4} className="d-none d-lg-block">
                    <div className={styles['div-wrapper-pricing-bizzone-shop']}>
                      <h3 className={styles['title-pricing-bizzone-shop-2']}>
                        {s.title.toUpperCase()}
                      </h3>
                      <p className={styles['sub-title-pricing-bizzone-shop-1']}>
                        <span>
                          {s.pricePerYear}
                          {/* <sup>đ</sup> */}
                        </span>
                        {/* /năm */}
                      </p>
                      <p className={styles['sub-title-pricing-bizzone-shop-2']}>{s.desc}</p>
                      <div className={styles['div-under-pricing-page-2']} />
                      <div className={styles['div-wrapper-benefis-pricing']}>
                        {s.features.map((f) => (
                          <p key={f}>
                            <Image
                              src="/images/svg/icon-tich-v-pricing-page-bizzone-shop.svg"
                              alt=""
                            />
                            <span>{f}</span>
                          </p>
                        ))}
                      </div>
                      <button
                        type="button"
                        className={styles['btn-choose-package']}
                        onClick={() => handleChoosePackage(s.id)}
                      >
                        <span>Chọn gói</span>
                      </button>
                    </div>
                  </Col>
                ))}
              </Row>
            </div>
          </div>
        </Container>
      </div>

      {/* Show form modal */}
      <ModalFormContact
        show={showFormModal}
        option={selectedPackage}
        handleSetFormContact={handleSetFormContact}
        handleSetSuccessModal={handleSetSuccessModal}
      />

      {/* Show success modal */}
      <ModalSendSuccess show={showSucessModal} onCloseModal={handleSetSuccessModal} />
    </>
  );
}
export default RowPricing;
