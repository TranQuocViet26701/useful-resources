import Head from 'next/head';
import React from 'react';
import { Container } from 'react-bootstrap';
import LayoutBizzoneShop from '../LayoutBizzoneShop';
import { RowPricing, RowQuestion, RowTopPricingPage } from './main';

function PricingPage() {
  return (
    <>
      <Head>
        <title>Bảng giá | Usee</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="title" content="Bảng giá | Usee" />
        <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png" />
        <meta name="title" content="Bảng giá | Usee - Bảng giá phần mềm quản lý bán hàng" />
        <meta
          name="description"
          content="Bảng giá | Usee - Hãy lựa chọn gói giải pháp phù hợp với nhu cầu của bạn"
        />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://bizzone.vn/" />
        <meta property="og:title" content="Bảng giá | Usee - Bảng giá phần mềm quản lý bán hàng" />
        <meta
          property="og:description"
          content="Bảng giá | Usee - Hãy lựa chọn gói giải pháp phù hợp với nhu cầu của bạn"
        />
        <meta property="og:image" content="https://bizzone.vn/meta-data.png" />

        {/* Twitter */}
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://bizzone.vn/" />
        <meta
          property="twitter:title"
          content="Bảng giá | Usee - Bảng giá phần mềm quản lý bán hàng"
        />
        <meta
          property="twitter:description"
          content="Bảng giá | Usee - Hãy lựa chọn gói giải pháp phù hợp với nhu cầu của bạn"
        />
        <meta property="twitter:image" content="https://bizzone.vn/meta-data.png" />
      </Head>
      <LayoutBizzoneShop>
        <main>
          <Container className="max-width-100 px-0" fluid>
            <RowTopPricingPage />
            <RowPricing />
            <RowQuestion />
          </Container>
        </main>
      </LayoutBizzoneShop>
    </>
  );
}
export default PricingPage;
