import Link from 'next/link';
import React from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import styles from '../../styles/RetailPage.module.scss';

function RowPosSolution() {
  return (
    <div className={styles['wrapper-pos-solution-retail-page-bizzone-shop']}>
      <Container className="max-width-1180 px-0">
        <Row className="gx-5 padding-left-right">
          <Col lg={6} className="d-flex justify-content-center">
            <Image
              src="/images/bizzone-shop/img-pos-retail-page-bizzone-shop.png"
              className="img-fluid"
              alt=""
            />
          </Col>
          <Col lg={6} className="d-flex flex-column py-2 justify-content-center">
            <h2 className={styles['title-pos-solution-retail-page-bizzone-shop']}>
              Bán hàng chuyên nghiệp hơn khi tích hợp máy POS
            </h2>
            <p className={styles['sub-title-pos-solution-retail-page-bizzone-shop']}>
              Giải pháp phần mềm quản lý bán hàng của chúng tôi đi kèm với máy POS màn hình cảm ứng,
              máy in bill/ hoá đơn, máy quét mã vạch, đầu đọc thẻ tín dụng và ngăn kéo đựng tiền tạo
              nên một hệ thống bán hàng hoàn chỉnh.
            </p>
            <Link href="/usee/pricing-page">
              <a className="d-flex">
                <button className="btn-bizzone-shop-1" type="button" style={{ width: '200px' }}>
                  <span>Chọn gói giải pháp</span>
                </button>
              </a>
            </Link>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default RowPosSolution;
