import Link from 'next/link';
import React from 'react';
import { Col,Container,Row } from 'react-bootstrap';
import styles from '../../styles/RetailPage.module.scss';

function RowBottomRetailPage() {
  return (
    <div className={styles['bg-bottom-retail-bizzone-shop']}>
      <Container className="max-width-1180 px-0">
        <Row className="gx-5 padding-left-right">
          <Col md={8}>
            <h2 className={styles['title-bottom-retail-page-bizzone-shop']}>
              Bán hàng đa kênh hiệu quả, đa dạng lĩnh vực cùng Usee
            </h2>
            <p className={styles['sub-title-bottom-retail-page-bizzone-shop']}>
              Khám phá giải pháp bán hàng POS từ Usee giúp chuyển đổi
              doanh thu kinh doanh bán lẻ của doanh nghiệp bạn tốt hơn.
            </p>
            <div className="d-flex justify-content-center justify-content-md-start">
              <Link href="/usee/pricing-page">
                <a>
                  <button className="btn-bizzone-shop-1" type="button">
                    <span>Chọn gói giải pháp</span>
                  </button>
                </a>
              </Link>
              <button className="btn-bizzone-shop-2" type="button">
                <span>Dùng thử</span>
              </button>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default RowBottomRetailPage;
