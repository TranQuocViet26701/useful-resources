import React from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import styles from '../../styles/RetailPage.module.scss';

function RowGoodFeature() {
  return (
    <div>
      <Container className="max-width-1180 px-0">
        <Row className="padding-left-right" style={{ marginBottom: '40px' }}>
          <Col>
            <h2 className={styles['title-good-feature-bizzone-shop']}>
              Những tính năng mà một hệ thống POS bán lẻ tốt cần có
            </h2>
          </Col>
        </Row>
        <Row>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature1-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Quản lý & báo cáo tồn kho thông minh theo thời gian thực
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Báo cáo hàng tồn kho thấp dựa trên tỷ lệ bán hàng hiện tại, đưa ra phân tích hàng hoá
              tồn kho để dự báo nhu cầu đặt hàng, điều chỉnh giải pháp kinh doanh phù hợp.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature2-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Cung cấp tính năng phân tích - báo cáo nhanh chóng & chính xác
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Cung cấp báo cáo theo thời gian thực về hiệu suất bán hàng, tình trạng tồn kho, năng
              suất của nhân viên và doanh thu theo từng chi nhánh.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature3-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Hỗ trợ đa dạng phương thức & thiết bị thanh toán
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Đa dạng quy trình thanh toán ngoài dùng tiền mặt, khách hàng có thể thanh toán nhanh
              chóng bằng hình thức quẹt thẻ, chuyển khoản an toàn và tiện lợi.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature4-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Gợi ý thiết lập chính sách giá & chương trình khuyến mãi
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Linh hoạt áp dụng các mức giá khác nhau theo chương trình khuyến mãi (voucher, giảm
              giá, tích điểm, thẻ thành viên...) theo từng mốc thời gian, chi nhánh.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature5-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Quản lý các chi nhánh một cách đồng bộ & thông minh
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Khách hàng dễ kiểm tra sản phẩm tồn kho có sẵn, chọn mua hàng ở chi nhánh gần nhất.
              Đồng bộ hóa dữ liệu, theo dõi và cải thiện hiệu suất của từng cửa hàng.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature6-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Tăng trải nghiệm mua hàng & duy trì lượng khách hàng thân thiết
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Thông báo hoá đơn, khuyến mãi và chăm sóc hậu mãi cho khách hàng chu đáo với tính năng
              gửi Email, SMS, nhắn tin qua Facebook, Zalo nhanh chóng.
            </p>
          </Col>
        </Row>
        <Row className="justify-content-center" style={{ marginTop: '40px' }}>
          <button
            className="btn-bizzone-shop-2"
            type="button"
            style={{
              margin: 'auto',
              width: '200px',
              boxShadow:
                '0px 6px 14px -6px rgba(24, 39, 75, 0.12), 0px 10px 32px -4px rgba(24, 39, 75, 0.1)',
            }}
          >
            <span>Dùng thử</span>
          </button>
        </Row>
      </Container>
    </div>
  );
}
export default RowGoodFeature;
