import Link from 'next/link';
import React from 'react';
import { Accordion, Col, Container, Row } from 'react-bootstrap';
import styles from '../../styles/RetailPage.module.scss';

function RowQuestion() {
  return (
    <div className={styles['bg-question-bizzone-shop']}>
      <Container className="max-width-1180 px-0">
        <div className="padding-left-right">
          <h2 className={styles['title-question-retail-page-bizzone-shop']}>
            Một số câu hỏi thường gặp
          </h2>
          <p className={styles['sub-title-question-retail-page-bizzone-shop']}>
            Bạn có thắc mắc. Usee sẵn sàng trả lời.
          </p>
          <div className={styles['question-accordion-pricing']}>
            <Accordion defaultActiveKey="2">
              <Row className="justify-content-center">
                <Col md={8}>
                  <Accordion.Item eventKey="0">
                    <Accordion.Header>
                      Phí khởi tạo có bao gồm trong giá gói dịch vụ không?
                    </Accordion.Header>
                    <Accordion.Body>
                      Giá gói dịch vụ chỉ bao gồm các giải pháp quản lý bán hàng cần thiết, chưa bao
                      gồm chi phí khởi tạo. Tuy nhiên, phí khởi tạo cũng có thể linh hoạt theo từng
                      loại gói dịch khác nhau hoặc miễn phí khởi tạo khi khách hàng đăng ký dùng
                      dịch vụ lâu dài. Phí này chỉ cần chi một lần duy nhất, không tốn chi phí duy
                      trì cũng như không có phí phát sinh.
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="1">
                    <Accordion.Header>
                      Tôi được dùng thử dịch vụ trong vòng bao lâu?
                    </Accordion.Header>
                    <Accordion.Body>
                      Lựa chọn gói dịch vụ phù hợp với nhu cầu kinh doanh của bạn và bắt đầu dùng
                      thử miễn phí trong khoảng 7 ngày. Bạn sẽ sử dụng được tất cả các tính năng
                      theo từng gói mà không bị giới hạn bất cứ chức năng nào. Đội ngũ chuyên viên
                      tư vấn của Usee sẵn sàng đồng hành cùng bạn trong suốt thời gian sử dụng để
                      giải đáp các thắc mắc và hỗ trợ bạn mua gói khi có nhu cầu.
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="2">
                    <Accordion.Header>
                      Khi hết thời gian sử dụng dịch vụ, tôi muốn gia hạn dịch vụ thì chi phí như
                      thế nào?
                    </Accordion.Header>
                    <Accordion.Body>
                      Tùy vào thời điểm bạn gia hạn mà chi phí gia hạn gói dịch vụ sẽ có sự chênh
                      lệch khác nhau. Nếu bạn gia hạn sớm thì càng được chiết khấu cao, nghĩa là chi
                      phí gia hạn sẽ được giảm nhiều hơn. Bạn có thể chủ động gia hạn trên hệ thống
                      hoặc liên hệ nhân viên Usee hỗ trợ gia hạn dịch vụ nếu có những yêu cầu thay
                      đổi sau thời gian sử dụng.
                    </Accordion.Body>
                  </Accordion.Item>
                  <Accordion.Item eventKey="3">
                    <Accordion.Header>
                      Tôi phải làm gì nếu muốn mua gói dịch vụ chính thức sau khi đã dùng thử?
                    </Accordion.Header>
                    <Accordion.Body>
                      Trong quá trình dùng thử miễn phí, khi có nhu cầu mua gói dịch vụ chính thức
                      bạn có thể liên hệ trực tiếp qua hotline: 19006054 để được tư vấn hỗ trợ. Đội
                      ngũ chăm sóc khách hàng của Usee luôn nhiệt tình giải đáp thắc mắc và yêu cầu
                      của bạn tất cả các ngày trong tuần, hướng dẫn bạn sử dụng phần mềm cách tối ưu
                      và hiệu quả nhất.
                    </Accordion.Body>
                  </Accordion.Item>
                </Col>
              </Row>
            </Accordion>
          </div>
          <p className={styles['link-more-advice']}>
            <span>Bạn cần thêm thông tin? </span>{' '}
            <Link href="/">
              <a> Liên hệ tư vấn ngay!</a>
            </Link>
          </p>
        </div>
      </Container>
    </div>
  );
}
export default RowQuestion;
