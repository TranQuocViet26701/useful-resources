import Head from 'next/head';
import React from 'react';
import { Container } from 'react-bootstrap';
import LayoutBizzoneShop from '../LayoutBizzoneShop';
import {
  RowBottomRetailPage,
  RowBusinessItem,
  RowGoodFeature,
  RowPosSolution,
  RowQuestion,
  RowTopRetailPage,
} from './main';

function RetailPage() {
  return (
    <>
      <Head>
        <title>Bán lẻ | Usee</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="title" content="Bán lẻ | Usee" />
        <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png" />
        <meta
          name="title"
          content="Bán lẻ | Usee - Chuẩn hóa bán hàng cho kinh doanh bán lẻ cùng Usee"
        />
        <meta
          name="description"
          content="Bán lẻ | Usee - Tăng doanh thu và tăng trải nghiệm khách hàng với giải pháp bán lẻ đa kênh linh hoạt, đơn giản hóa quản lý tồn kho với chi phí hiệu quả."
        />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://bizzone.vn/" />
        <meta
          property="og:title"
          content="Bán lẻ | Usee - Chuẩn hóa bán hàng cho kinh doanh bán lẻ cùng Usee"
        />
        <meta
          property="og:description"
          content="Bán lẻ | Usee - Tăng doanh thu và tăng trải nghiệm khách hàng với giải pháp bán lẻ đa kênh linh hoạt, đơn giản hóa quản lý tồn kho với chi phí hiệu quả."
        />
        <meta property="og:image" content="https://bizzone.vn/meta-data.png" />

        {/* Twitter */}
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://bizzone.vn/" />
        <meta
          property="twitter:title"
          content="Bán lẻ | Usee - Chuẩn hóa bán hàng cho kinh doanh bán lẻ cùng Usee"
        />
        <meta
          property="twitter:description"
          content="Bán lẻ | Usee - Tăng doanh thu và tăng trải nghiệm khách hàng với giải pháp bán lẻ đa kênh linh hoạt, đơn giản hóa quản lý tồn kho với chi phí hiệu quả."
        />
        <meta property="twitter:image" content="https://bizzone.vn/meta-data.png" />
      </Head>
      <LayoutBizzoneShop>
        <main>
          <Container className="max-width-100 px-0" fluid>
            <RowTopRetailPage />
            <RowGoodFeature />
            <RowBusinessItem />
            <RowPosSolution />
            <RowBottomRetailPage />
            <RowQuestion />
          </Container>
        </main>
      </LayoutBizzoneShop>
    </>
  );
}

export default RetailPage;
