import Head from 'next/head';
import React from 'react';
import { Container } from 'react-bootstrap';
import LayoutBizzoneShop from '../LayoutBizzoneShop';
import {
  RowBenefit,
  RowBusinessIntegration,
  RowContactUs,
  RowManagementSolution,
  RowPartnership,
  RowTopBizzoneShop,
} from './main';

function Homepage() {
  //   const pathname = window.location.href;
  //   const final = pathname.substring(pathname.indexOf('bizzone-shop#') + 13, pathname.length);
  //   console.log('first path', pathname);
  //   console.log('num', pathname.indexOf('bizzone-shop#'));
  //   console.log('path', final);

  //   const handleClickScroll = () => {
  //     const Element = document.getElementById(final);
  //     Element?.scrollIntoView({ behavior: 'smooth' });
  //   };
  //   useEffect(() => {
  //     setTimeout(handleClickScroll, 700);
  //   }, []);
  return (
    <>
      <Head>
        <title>Usee</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="title" content="Usee" />
        <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png" />
        <meta name="title" content="Usee - Tăng sức mạnh bán hàng đa kênh cùng Usee" />
        <meta
          name="description"
          content="Usee - Giải pháp kinh doanh bán lẻ hàng đầu giúp doanh nghiệp tiếp thị đa kênh hiệu quả, quản lý tồn kho thông minh và tối ưu hoá các thủ tục thanh toán."
        />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://bizzone.vn/" />
        <meta property="og:title" content="Usee - Tăng sức mạnh bán hàng đa kênh cùng Usee" />
        <meta
          property="og:description"
          content="Usee - Giải pháp kinh doanh bán lẻ hàng đầu giúp doanh nghiệp tiếp thị đa kênh hiệu quả, quản lý tồn kho thông minh và tối ưu hoá các thủ tục thanh toán."
        />
        <meta property="og:image" content="https://bizzone.vn/meta-data.png" />

        {/* Twitter */}
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://bizzone.vn/" />
        <meta property="twitter:title" content="Usee - Tăng sức mạnh bán hàng đa kênh cùng Usee" />
        <meta
          property="twitter:description"
          content="Usee - Giải pháp kinh doanh bán lẻ hàng đầu giúp doanh nghiệp tiếp thị đa kênh hiệu quả, quản lý tồn kho thông minh và tối ưu hoá các thủ tục thanh toán."
        />
        <meta property="twitter:image" content="https://bizzone.vn/meta-data.png" />
      </Head>
      <LayoutBizzoneShop>
        <main>
          <Container className="max-width-100 px-0" fluid>
            <RowTopBizzoneShop />
            <RowBenefit />
            <RowManagementSolution />
            <RowBusinessIntegration />
            <RowPartnership />
            <RowContactUs />
          </Container>
        </main>
      </LayoutBizzoneShop>
    </>
  );
}
export default Homepage;
