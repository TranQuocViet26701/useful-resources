import Link from 'next/link';
import React, { useState } from 'react';
import { Accordion, Col, Container, Image, Row } from 'react-bootstrap';
import styles from '../../styles/Homepage.module.scss';

function AccordionBusinessIntegration() {
  return (
    <div className={`${styles['div-business-integration-bizzone-shop']} d-md-none`}>
      <Accordion>
        <Accordion.Item eventKey="0">
          <Accordion.Header>Bán lẻ</Accordion.Header>
          <Accordion.Body>
            <Row className={`${styles['row-img-retail-business']} gy-4`}>
              <Col xs={5}>
                <Image
                  src="/images/bizzone-shop/img-retail-bizzone-shop-1.png"
                  className={`${styles['img-retail-1-bizzone-shop']} img-fluid`}
                  alt=""
                />
              </Col>
              <Col xs={7}>
                <Image
                  src="/images/bizzone-shop/img-retail-bizzone-shop-2.png"
                  alt=""
                  className={`${styles['img-retail-2-bizzone-shop']} img-fluid`}
                />
              </Col>
              <Col xs={12}>
                <Image
                  src="/images/bizzone-shop/img-retail-bizzone-shop-3.png"
                  alt=""
                  className={`${styles['img-retail-3-bizzone-shop']} img-fluid`}
                />
              </Col>
            </Row>
            <p className={styles['description-business-intergration']}>
              Lĩnh vực bán lẻ tích hợp POS giúp quản lý bán hàng đa kênh thông minh từ khâu lên hóa
              đơn, thanh toán, quản lý đơn hàng và theo dõi hiệu suất bán hàng toàn diện.
            </p>
            <Link href="/usee/retail-page">
              <a className={styles['link-business-intergration']}>
                <span>
                  Tìm hiểu thêm
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 25"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M13 5.65967L20 12.6597L13 19.6597M4 12.6597H20H4Z"
                      stroke="#2391FD"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </span>
              </a>
            </Link>
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="1">
          <Accordion.Header>F&B</Accordion.Header>
          <Accordion.Body>
            <Row className={`${styles['row-img-retail-business']} gy-4`}>
              <Col xs={5}>
                <Image
                  src="/images/bizzone-shop/img-fb-bizzone-shop-1.png"
                  alt=""
                  className={`${styles['img-retail-1-bizzone-shop']} img-fluid`}
                />
              </Col>
              <Col xs={7}>
                <Image
                  src="/images/bizzone-shop/img-fb-bizzone-shop-2.png"
                  alt=""
                  className={`${styles['img-retail-2-bizzone-shop']} img-fluid`}
                />
              </Col>
              <Col xs={12}>
                <Image
                  src="/images/bizzone-shop/img-fb-bizzone-shop-3.png"
                  alt=""
                  className={`${styles['img-retail-3-bizzone-shop']} img-fluid`}
                />
              </Col>
            </Row>
            <p className={styles['description-business-intergration']}>
              Phần mềm POS của Usee giúp quản lý quy trình order, thanh toán, đồng bộ hệ thống order
              giữa nhân viên và nhà bếp, thu ngân và quản lý kho nguyên vật liệu hiệu quả.
            </p>
            <Link href="/usee/f&b-page">
              <a className={styles['link-business-intergration']}>
                <span>
                  Tìm hiểu thêm
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 25"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M13 5.65967L20 12.6597L13 19.6597M4 12.6597H20H4Z"
                      stroke="#2391FD"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </span>
              </a>
            </Link>
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="2">
          <Accordion.Header>Dịch vụ</Accordion.Header>
          <Accordion.Body>
            <Row className={`${styles['row-img-retail-business']} gy-4`}>
              <Col xs={5}>
                <Image
                  src="/images/bizzone-shop/img-service-bizzone-shop-1.png"
                  alt=""
                  className={`${styles['img-retail-1-bizzone-shop']} img-fluid`}
                />
              </Col>
              <Col xs={7}>
                <Image
                  src="/images/bizzone-shop/img-service-bizzone-shop-2.png"
                  alt=""
                  className={`${styles['img-retail-2-bizzone-shop']} img-fluid`}
                />
              </Col>
              <Col xs={12}>
                <Image
                  src="/images/bizzone-shop/img-service-bizzone-shop-3.png"
                  alt=""
                  className={`${styles['img-retail-3-bizzone-shop']} img-fluid`}
                />
              </Col>
            </Row>
            <p className={styles['description-business-intergration']}>
              Usee hỗ trợ các ngành dịch vụ như tài chính, bảo hiểm, logistic, du lịch, bưu chính -
              viễn thông trong công tác thanh toán, thu phí và các giao dịch đặc thù khác.
            </p>
            <Link href="/usee/service-page">
              <a className={styles['link-business-intergration']}>
                <span>
                  Tìm hiểu thêm
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 25"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M13 5.65967L20 12.6597L13 19.6597M4 12.6597H20H4Z"
                      stroke="#2391FD"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </span>
              </a>
            </Link>
          </Accordion.Body>
        </Accordion.Item>
        <Accordion.Item eventKey="3">
          <Accordion.Header>Ngành nghề khác</Accordion.Header>
          <Accordion.Body>
            <Row className={`${styles['row-img-retail-business']} gy-4`}>
              <Col xs={5}>
                <Image
                  src="/images/bizzone-shop/img-other-bizzone-shop-1.png"
                  alt=""
                  className={`${styles['img-retail-1-bizzone-shop']} img-fluid`}
                />
              </Col>
              <Col xs={7}>
                <Image
                  src="/images/bizzone-shop/img-other-bizzone-shop-2.png"
                  alt=""
                  className={`${styles['img-retail-2-bizzone-shop']} img-fluid`}
                />
              </Col>
              <Col xs={12}>
                <Image
                  src="/images/bizzone-shop/img-other-bizzone-shop-3.png"
                  alt=""
                  className={`${styles['img-retail-3-bizzone-shop']} img-fluid`}
                />
              </Col>
            </Row>
            <p className={styles['description-business-intergration']}>
              Phần mềm POS của Usee được ứng dụng trong đa dạng ngành nghề, giúp doanh nghiệp tối ưu
              về mặt thời gian và chi phí vận hành kinh doanh.
            </p>
            <Link href="/usee/other-field-page">
              <a className={styles['link-business-intergration']}>
                <span>
                  Tìm hiểu thêm
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 25"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M13 5.65967L20 12.6597L13 19.6597M4 12.6597H20H4Z"
                      stroke="#2391FD"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </span>
              </a>
            </Link>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    </div>
  );
}

function RowBusinessIntegration() {
  const [solutionTable, setSolutionTable] = useState(1);

  return (
    <div
      className={`${styles['back-ground-business-bizzone-shop']} ${styles['scroll-margin-top-section']}`}
      id="business"
    >
      <Container className="max-width-1180 px-0">
        <div className="padding-left-right">
          <h2 className={styles['title-solution-bizzone-shop']}>
            Tích hợp cho mọi loại hình và quy mô kinh doanh
          </h2>
          <Row>
            <Col md={5} className="d-none d-md-block">
              <Link href="/usee/retail-page">
                <a
                  className={`${styles['link-business-intergration']} ${
                    solutionTable === 1 ? `${styles['active-hover-business-integration']}` : ''
                  }`}
                >
                  <span id="bizzone-shop-retail" onMouseOver={() => setSolutionTable(1)}>
                    Bán lẻ
                    <svg
                      width="24"
                      height="25"
                      viewBox="0 0 24 25"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M13 5.65967L20 12.6597L13 19.6597M4 12.6597H20H4Z"
                        stroke="#76809B"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      />
                    </svg>
                  </span>
                </a>
              </Link>
              <p className={styles['description-business-intergration']}>
                Lĩnh vực bán lẻ tích hợp POS giúp quản lý bán hàng đa kênh thông minh từ khâu lên
                hóa đơn, thanh toán, quản lý đơn hàng và theo dõi hiệu suất bán hàng toàn diện.
              </p>
              <Link href="/usee/f&b-page">
                <a
                  className={`${styles['link-business-intergration']} ${
                    solutionTable === 2 ? `${styles['active-hover-business-integration']}` : ''
                  }`}
                >
                  <span id="bizzone-shop-f-and-b" onMouseOver={() => setSolutionTable(2)}>
                    F&B
                    <svg
                      width="24"
                      height="25"
                      viewBox="0 0 24 25"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M13 5.65967L20 12.6597L13 19.6597M4 12.6597H20H4Z"
                        stroke="#76809B"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      />
                    </svg>
                  </span>
                </a>
              </Link>
              <p className={styles['description-business-intergration']}>
                Phần mềm POS của Usee giúp quản lý quy trình order, thanh toán, đồng bộ hệ thống
                order giữa nhân viên và nhà bếp, thu ngân và quản lý kho nguyên vật liệu hiệu quả.
              </p>
              <Link href="/usee/service-page">
                <a
                  className={`${styles['link-business-intergration']} ${
                    solutionTable === 3 ? `${styles['active-hover-business-integration']}` : ''
                  }`}
                >
                  <span id="bizzone-shop-service" onMouseOver={() => setSolutionTable(3)}>
                    Dịch vụ
                    <svg
                      width="24"
                      height="25"
                      viewBox="0 0 24 25"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M13 5.65967L20 12.6597L13 19.6597M4 12.6597H20H4Z"
                        stroke="#76809B"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      />
                    </svg>
                  </span>
                </a>
              </Link>
              <p className={styles['description-business-intergration']}>
                Usee hỗ trợ các ngành dịch vụ như tài chính, bảo hiểm, logistic, du lịch, bưu chính
                - viễn thông trong công tác thanh toán, thu phí và các giao dịch đặc thù khác.
              </p>
              <Link href="/usee/other-field-page">
                <a
                  className={`${styles['link-business-intergration']} ${
                    solutionTable === 4 ? `${styles['active-hover-business-integration']}` : ''
                  }`}
                >
                  <span id="bizzone-shop-other" onMouseOver={() => setSolutionTable(4)}>
                    Ngành nghề khác
                    <svg
                      width="24"
                      height="25"
                      viewBox="0 0 24 25"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M13 5.65967L20 12.6597L13 19.6597M4 12.6597H20H4Z"
                        stroke="#76809B"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      />
                    </svg>
                  </span>
                </a>
              </Link>
              <p className={styles['description-business-intergration']}>
                Phần mềm POS của Usee được ứng dụng trong đa dạng ngành nghề, giúp doanh nghiệp tối
                ưu về mặt thời gian và chi phí vận hành kinh doanh.
              </p>
            </Col>
            <Col
              md={7}
              className={
                solutionTable === 1
                  ? `${styles['solution-bizzon-shop-show-1']} d-none d-md-block gx-5`
                  : 'd-none'
              }
            >
              <Row className={`${styles['row-img-retail-business']} gy-4`}>
                <Col xs={5}>
                  <Image
                    src="/images/bizzone-shop/img-retail-bizzone-shop-1.png"
                    className={`${styles['img-retail-1-bizzone-shop']} img-fluid`}
                    alt=""
                  />
                </Col>
                <Col xs={7}>
                  <Image
                    src="/images/bizzone-shop/img-retail-bizzone-shop-2.png"
                    alt=""
                    className={`${styles['img-retail-2-bizzone-shop']} img-fluid`}
                  />
                </Col>
                <Col xs={12}>
                  <Image
                    src="/images/bizzone-shop/img-retail-bizzone-shop-3.png"
                    alt=""
                    className={`${styles['img-retail-3-bizzone-shop']} img-fluid`}
                  />
                </Col>
              </Row>
            </Col>
            <Col
              md={7}
              className={
                solutionTable === 2
                  ? `${styles['solution-bizzon-shop-show-2']} d-none d-md-block gx-5`
                  : 'd-none'
              }
            >
              <Row className={`${styles['row-img-retail-business']} gy-4`}>
                <Col xs={5}>
                  <Image
                    src="/images/bizzone-shop/img-fb-bizzone-shop-1.png"
                    alt=""
                    className={`${styles['img-retail-1-bizzone-shop']} img-fluid`}
                  />
                </Col>
                <Col xs={7}>
                  <Image
                    src="/images/bizzone-shop/img-fb-bizzone-shop-2.png"
                    alt=""
                    className={`${styles['img-retail-2-bizzone-shop']} img-fluid`}
                  />
                </Col>
                <Col xs={12}>
                  <Image
                    src="/images/bizzone-shop/img-fb-bizzone-shop-3.png"
                    alt=""
                    className={`${styles['img-retail-3-bizzone-shop']} img-fluid`}
                  />
                </Col>
              </Row>
            </Col>
            <Col
              md={7}
              className={
                solutionTable === 3
                  ? `${styles['solution-bizzon-shop-show-3']} d-none d-md-block gx-5`
                  : 'd-none'
              }
            >
              <Row className={`${styles['row-img-retail-business']} gy-4`}>
                <Col xs={5}>
                  <Image
                    src="/images/bizzone-shop/img-service-bizzone-shop-1.png"
                    alt=""
                    className={`${styles['img-retail-1-bizzone-shop']} img-fluid`}
                  />
                </Col>
                <Col xs={7}>
                  <Image
                    src="/images/bizzone-shop/img-service-bizzone-shop-2.png"
                    alt=""
                    className={`${styles['img-retail-2-bizzone-shop']} img-fluid`}
                  />
                </Col>
                <Col xs={12}>
                  <Image
                    src="/images/bizzone-shop/img-service-bizzone-shop-3.png"
                    alt=""
                    className={`${styles['img-retail-3-bizzone-shop']} img-fluid`}
                  />
                </Col>
              </Row>
            </Col>
            <Col
              md={7}
              className={
                solutionTable === 4
                  ? `${styles['solution-bizzon-shop-show-4']} d-none d-md-block gx-5`
                  : 'd-none'
              }
            >
              <Row className={`${styles['row-img-retail-business']} gy-4`}>
                <Col xs={5}>
                  <Image
                    src="/images/bizzone-shop/img-other-bizzone-shop-1.png"
                    alt=""
                    className={`${styles['img-retail-1-bizzone-shop']} img-fluid`}
                  />
                </Col>
                <Col xs={7}>
                  <Image
                    src="/images/bizzone-shop/img-other-bizzone-shop-2.png"
                    alt=""
                    className={`${styles['img-retail-2-bizzone-shop']} img-fluid`}
                  />
                </Col>
                <Col xs={12}>
                  <Image
                    src="/images/bizzone-shop/img-other-bizzone-shop-3.png"
                    alt=""
                    className={`${styles['img-retail-3-bizzone-shop']} img-fluid`}
                  />
                </Col>
              </Row>
            </Col>
            {/* Responsive mobile */}
            <AccordionBusinessIntegration />
          </Row>
        </div>
      </Container>
    </div>
  );
}
export default RowBusinessIntegration;
