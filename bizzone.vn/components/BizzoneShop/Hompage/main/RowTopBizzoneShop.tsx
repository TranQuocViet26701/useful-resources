import React from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import styles from '../../styles/Homepage.module.scss';

function RowTopBizzoneShop() {
  return (
    <div className={styles['back-ground-top-bizzone-shop']}>
      <Container className="max-width-1180 px-0">
        <Row className="gx-5 padding-left-right">
          <Col md={6} className="pb-3">
            <h2 className={styles['title-top-bizzone-shop']}>
              Tăng sức mạnh bán hàng đa kênh cùng Usee
            </h2>
            <p className={styles['sub-title-top-bizzone-shop']}>
              Giải pháp kinh doanh bán lẻ hàng đầu giúp doanh nghiệp tiếp thị đa
              kênh hiệu quả, quản lý tồn kho thông minh và tối ưu hoá các thủ
              tục thanh toán.
            </p>
            <div className="d-flex">
              <a href="#contact-bizzone-shop">
                <button className={styles['btn-bizzone-shop-1']} type="button">
                  <span>Liên hệ tư vấn</span>
                </button>
              </a>
              <button
                className={styles['btn-bizzone-shop-2']}
                type="button"
                style={{ marginLeft: 16 }}
              >
                <span>Dùng thử</span>
              </button>
            </div>
          </Col>
          <Col md={6} className="d-flex align-items-center">
            <Image
              src="/images/bizzone-shop/img-top-bizzone-shop.png"
              className="img-fluid"
              alt=""
            />
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default RowTopBizzoneShop;
