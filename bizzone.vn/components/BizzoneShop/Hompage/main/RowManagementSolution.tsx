import React, { useState } from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import styles from '../../styles/Homepage.module.scss';

interface ImageMobileManagementSolutionProps {
  solutionTable: number;
}

function ImageMobileManagementSolution({ solutionTable }: ImageMobileManagementSolutionProps) {
  return (
    <>
      {solutionTable === 1 && (
        <div className={`${styles['img-mobile-solution-bizzone-shop']} d-md-none`}>
          <Image
            src="/images/bizzone-shop/img-inventory-manager.png"
            className="img-fluid"
            alt=""
          />
        </div>
      )}
      {solutionTable === 2 && (
        <div className={`${styles['img-mobile-solution-bizzone-shop']} d-md-none`}>
          <Image
            src="/images/bizzone-shop/img-sale-bizzone-shop.png"
            className="img-fluid"
            alt=""
          />
        </div>
      )}
      {solutionTable === 3 && (
        <div className={`${styles['img-mobile-solution-bizzone-shop']} d-md-none`}>
          <Image
            src="/images/bizzone-shop/img-partner-bizzone-shop.png"
            className="img-fluid"
            alt=""
          />
        </div>
      )}
      {solutionTable === 4 && (
        <div className={`${styles['img-mobile-solution-bizzone-shop']} d-md-none`}>
          <Image
            src="/images/bizzone-shop/img-report-bizzone-shop.png"
            className="img-fluid"
            alt=""
          />
        </div>
      )}
      {solutionTable === 5 && (
        <div className={`${styles['img-mobile-solution-bizzone-shop']} d-md-none`}>
          <Image
            src="/images/bizzone-shop/img-staff-bizzone-shop.png"
            className="img-fluid"
            alt=""
          />
        </div>
      )}
    </>
  );
}

function RowManagementSolution() {
  const [solutionTable, setSolutionTable] = useState(1);

  const handleSlidesSolutionTable = (numSolutionTable: number) => {
    const intNumSolutionTable = numSolutionTable;
    setSolutionTable(intNumSolutionTable);
  };

  return (
    <div
      className={`${styles['back-ground-solution-bizzone-shop']} ${styles['scroll-margin-top-section']}`}
      id="feature"
    >
      <Container className="max-width-1180 px-0">
        <div className="padding-left-right">
          <h2 className={styles['title-solution-bizzone-shop']}>
            Giải pháp quản lý bán hàng toàn diện cho cửa hàng
          </h2>
          <ImageMobileManagementSolution solutionTable={solutionTable} />
          <div className={styles['div-solution-bizzone-shop']}>
            <div>
              <ul className={styles['ul-solution-bizzone-shop']}>
                <li
                  className={solutionTable === 1 ? styles['active-li-solution-bizzone-shop'] : ''}
                >
                  <button type="button" onClick={() => handleSlidesSolutionTable(1)}>
                    Quản lý kho
                  </button>
                </li>
                <li
                  className={solutionTable === 2 ? styles['active-li-solution-bizzone-shop'] : ''}
                >
                  <button type="button" onClick={() => handleSlidesSolutionTable(2)}>
                    Bán hàng
                  </button>
                </li>
                <li
                  className={solutionTable === 3 ? styles['active-li-solution-bizzone-shop'] : ''}
                >
                  <button type="button" onClick={() => handleSlidesSolutionTable(3)}>
                    Đối tác
                  </button>
                </li>
                <li
                  className={solutionTable === 4 ? styles['active-li-solution-bizzone-shop'] : ''}
                >
                  <button type="button" onClick={() => handleSlidesSolutionTable(4)}>
                    Báo cáo
                  </button>
                </li>
                <li
                  className={solutionTable === 5 ? styles['active-li-solution-bizzone-shop'] : ''}
                >
                  <button type="button" onClick={() => handleSlidesSolutionTable(5)}>
                    Nhân viên
                  </button>
                </li>
              </ul>
            </div>
            <Row
              className={
                solutionTable === 1 ? `${styles['solution-bizzon-shop-show-1']} gx-5` : 'd-none'
              }
            >
              <Col md={6}>
                <h2 className={styles['title-2-solution-bizzone-shop']}>
                  Quản lý tồn kho thông minh giúp giảm rủi ro và tăng lợi nhuận
                </h2>
                <Row className="gx-1">
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Tạo và quản lý đơn đặt hàng nhanh chóng, tự động cập nhật thông tin đơn hàng
                      và đưa ra các số liệu theo dõi hàng tồn kho hiệu quả.
                    </p>
                  </Col>
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Thực hiện các phân tích và báo cáo hàng tồn kho chi tiết dựa trên các số liệu
                      thu thập được, giúp doanh nghiệp có cái nhìn trực quan và chính xác nhất.
                    </p>
                  </Col>
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Đưa ra dự báo nhu cầu hàng hoá dựa trên hiệu suất đơn đặt hàng hoặc kỳ khuyến
                      mãi, từ đó đề xuất các mặt hàng giảm giá, đưa ra gợi ý mặt hàng bán chạy hoặc
                      tồn kho.
                    </p>
                  </Col>
                </Row>
                <div className={styles['div-button-solution-bizzone-shop']}>
                  <button className={styles['btn-bizzone-shop-2']} type="button">
                    <span>Dùng thử</span>
                  </button>
                </div>
              </Col>
              <Col md={6} className="d-none d-md-block">
                <Image
                  src="/images/bizzone-shop/img-inventory-manager.png"
                  className="img-fluid"
                  alt=""
                />
              </Col>
            </Row>

            <Row
              className={
                solutionTable === 2 ? `${styles['solution-bizzon-shop-show-2']} gx-5` : 'd-none'
              }
            >
              <Col md={6}>
                <h2 className={styles['title-2-solution-bizzone-shop']}>
                  Hỗ trợ bán đa kênh vượt trội mang đến trải nghiệm mua sắm mượt mà
                </h2>
                <Row className="gx-1">
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Khách hàng mua hàng trực tuyến, nhận hàng tại cửa hàng cách thuận tiện. Nhờ đó
                      mang khách hàng mua sắm online đến cửa hàng để tăng tính trải nghiệm sản phẩm.
                    </p>
                  </Col>
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Khách hàng mua sắm tại cửa hàng và chọn hình thức giao hàng về nhà, giúp giải
                      quyết được vấn đề hàng tồn kho tại cửa hàng hết hoặc số lượng có hạn.
                    </p>
                  </Col>
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Hỗ trợ nhà bán quản lý và hoàn thành các đơn giao hàng đến khách hàng khi đặt
                      hàng trực tuyến qua hệ thống POS của Usee.
                    </p>
                  </Col>
                </Row>
                <div className={styles['div-button-solution-bizzone-shop']}>
                  <button className={styles['btn-bizzone-shop-2']} type="button">
                    <span>Dùng thử</span>
                  </button>
                </div>
              </Col>
              <Col md={6} className="d-none d-md-block">
                <Image
                  src="/images/bizzone-shop/img-sale-bizzone-shop.png"
                  className="img-fluid"
                  alt=""
                />
              </Col>
            </Row>

            <Row
              className={
                solutionTable === 3 ? `${styles['solution-bizzon-shop-show-3']} gx-5` : 'd-none'
              }
            >
              <Col md={6}>
                <h2 className={styles['title-2-solution-bizzone-shop']}>
                  Tăng cường và giữ chân khách hàng thân thiết về lâu dài
                </h2>
                <Row className="gx-1">
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Tạo và lưu trữ hồ sơ khách hàng phong phú, dễ dàng theo dõi thông tin, lịch sử
                      mua hàng để đưa ra giải pháp tiếp thị giữ chân khách hàng hiệu quả.
                    </p>
                  </Col>
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Đồng bộ hoá thông tin khách hàng dễ dàng và nhanh chóng ngay cả khi khách hàng
                      mua sắm trực tuyến hoặc tại cửa hàng.
                    </p>
                  </Col>
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Liên hệ và chăm sóc khách hàng từ POS của Usee thông qua SMS, Email hoặc số
                      điện thoại để duy trì khách hàng thân thiết.
                    </p>
                  </Col>
                </Row>
                <div className={styles['div-button-solution-bizzone-shop']}>
                  <button className={styles['btn-bizzone-shop-2']} type="button">
                    <span>Dùng thử</span>
                  </button>
                </div>
              </Col>
              <Col md={6} className="d-none d-md-block">
                <Image
                  src="/images/bizzone-shop/img-partner-bizzone-shop.png"
                  className="img-fluid"
                  alt=""
                />
              </Col>
            </Row>

            <Row
              className={
                solutionTable === 4 ? `${styles['solution-bizzon-shop-show-4']} gx-5` : 'd-none'
              }
            >
              <Col md={6}>
                <h2 className={styles['title-2-solution-bizzone-shop']}>
                  Thực hiện các báo cáo và phân tích trực quan, toàn diện
                </h2>
                <Row className="gx-1">
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Phân tích và báo cáo số lượng và phần trăm hàng tồn kho để kịp thời đưa ra các
                      giải pháp bán hàng online và cửa hàng.
                    </p>
                  </Col>
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Báo cáo các thông tin chi tiết về số lượng sản phẩm, hiệu suất của nhân viên
                      và doanh số trong ngày hoặc theo tuần, tháng.
                    </p>
                  </Col>
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Theo dõi báo cáo các chương trình khuyến mãi để doanh nghiệp có thể đưa ra các
                      đề xuất bán hàng thời điểm tiếp theo.
                    </p>
                  </Col>
                </Row>
                <div className={styles['div-button-solution-bizzone-shop']}>
                  <button className={styles['btn-bizzone-shop-2']} type="button">
                    <span>Dùng thử</span>
                  </button>
                </div>
              </Col>
              <Col md={6} className="d-none d-md-block">
                <Image
                  src="/images/bizzone-shop/img-report-bizzone-shop.png"
                  className="img-fluid"
                  alt=""
                />
              </Col>
            </Row>

            <Row
              className={
                solutionTable === 5 ? `${styles['solution-bizzon-shop-show-5']} gx-5` : 'd-none'
              }
            >
              <Col md={6}>
                <h2 className={styles['title-2-solution-bizzone-shop']}>
                  Hỗ trợ quản lý nhân viên hiệu quả, không giới hạn
                </h2>
                <Row className="gx-1">
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Thêm quyền truy cập và quản trị cho nhân viên theo từng đối tượng, vai trò của
                      mỗi người.
                    </p>
                  </Col>
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Dễ dàng theo dõi và tính toán doanh số bán hàng của nhân viên theo từng thời
                      điểm để đưa ra mức thưởng hoa hồng phù hợp.
                    </p>
                  </Col>
                  <Col xs={1} className="text-center">
                    <Image src="/images/svg/icon-tick-v-solution-bizzone-shop.svg" alt="" />
                  </Col>
                  <Col xs={11}>
                    <p>
                      Tiết kiệm được nhiều thời gian trong việc giám sát và theo dõi hiệu suất của
                      nhân viên mọi lúc, mọi nơi.
                    </p>
                  </Col>
                </Row>
                <div className={styles['div-button-solution-bizzone-shop']}>
                  <button className={styles['btn-bizzone-shop-2']} type="button">
                    <span>Dùng thử</span>
                  </button>
                </div>
              </Col>
              <Col md={6} className="d-none d-md-block">
                <Image
                  src="/images/bizzone-shop/img-staff-bizzone-shop.png"
                  className="img-fluid"
                  alt=""
                />
              </Col>
            </Row>
          </div>
        </div>
      </Container>
    </div>
  );
}
export default RowManagementSolution;
