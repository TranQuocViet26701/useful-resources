import axios from 'axios';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { Col, Container, Form, FormControl, Image, Row } from 'react-bootstrap';
import { Controller, useForm } from 'react-hook-form';
import ModalSendSuccess from '../../../Common/ModalSendSuccess';
import styles from '../../styles/Homepage.module.scss';

const googleSheetAPI =
  'https://script.google.com/macros/s/AKfycbxTlQD1WgGBuFKoKVjf6tiUGERX6DHHhxJfywGZ6R4xuNEurMdCdW4fbRoZwBW4jK6M/exec';

function RowContactUs() {
  const {
    handleSubmit,
    control,
    reset,
    formState: { errors, dirtyFields },
  } = useForm({ mode: 'onChange' });
  const router = useRouter();

  const [showSucessModal, setShowSucessModal] = useState(false);
  const [modalSending, setModalSending] = useState(false);

  const handleCloseModal = () => {
    setShowSucessModal(false);
  };

  const onSubmit = async (data: any) => {
    setModalSending(true);
    const googleSheetFormData = new FormData();
    googleSheetFormData.append('fullName', data.fullName);
    googleSheetFormData.append('email', data.email);
    googleSheetFormData.append('telephone', data.telephone);
    googleSheetFormData.append('company', '');
    googleSheetFormData.append('content', data.content);
    googleSheetFormData.append('timestamp', new Date().toLocaleDateString().substring(0, 10));
    googleSheetFormData.append('linkedBy', router.pathname);
    googleSheetFormData.append('selectedPackage', data.selectedPackage);

    try {
      const res = await axios.post(googleSheetAPI, googleSheetFormData);
      console.log('Respone google sheet: ', res);
      reset();
      setShowSucessModal(true);
      setModalSending(false);
    } catch (err) {
      console.log('Something went wrong!', err);
      reset();
      alert('Lỗi hệ thống.');
      setModalSending(false);
    }
  };

  return (
    <div
      className={`${styles['back-ground-contact-bizzone-shop']} ${styles['scroll-margin-top-section']}`}
      id="contact"
    >
      <Container className="max-width-1180 px-0">
        <div className="padding-left-right">
          <h2 className={styles['title-partnership-bizzone-shop']}>Hãy liên hệ với chúng tôi</h2>
          <div className={styles['div-contact-bizzone-shop']}>
            <Row className="gx-5">
              <Col md={5}>
                <div className={styles['col-contact-left']}>
                  <h3 className={styles['title-contact-bizzone-shop']}>
                    Quản lý dễ dàng hơn với Usee{' '}
                  </h3>
                  <p className={styles['detail-contact-bizzone-shop']}>Hỗ trợ tư vấn : 1900 6054</p>
                </div>
              </Col>
              <Col md={7}>
                <h3 className={styles['title-contact-2-bizzone-shop']}>Thông tin của bạn</h3>
                <Form
                  className={styles['form-contact-bizzone-shop']}
                  onReset={reset}
                  onSubmit={handleSubmit(onSubmit)}
                >
                  <Form.Group className="mb-4" controlId="formBasicFullName">
                    <Form.Label>
                      Họ và tên <span style={{ color: 'red' }}>*</span>
                    </Form.Label>
                    <Controller
                      control={control}
                      name="fullName"
                      defaultValue=""
                      rules={{ required: true }}
                      render={({
                        field: { onChange, value, ref },
                        fieldState: { invalid, isDirty },
                      }) => (
                        <FormControl
                          onChange={onChange}
                          value={value}
                          ref={ref}
                          isInvalid={errors.fullName}
                          isValid={isDirty && !invalid}
                          placeholder="Nhập tên của bạn"
                          aria-label="fullName"
                          aria-describedby="fullName"
                          autoComplete="off"
                        />
                      )}
                    />
                    <Form.Control.Feedback>
                      {Object.keys(dirtyFields).length !== 0 &&
                        dirtyFields.fullName &&
                        !errors.fullName && (
                          <span>
                            <Image src="/images/svg/icon-check-circle.svg" alt="" />
                            Họ và tên hợp lệ
                          </span>
                        )}
                    </Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">
                      {Object.keys(errors).length !== 0 && errors.fullName?.type === 'required' && (
                        <span>
                          <Image src="/images/svg/icon-invalid.svg" alt="" />
                          Vui lòng nhập họ tên
                        </span>
                      )}
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Form.Group className="mb-4" controlId="formBasicEmail">
                    <Form.Label>
                      Email <span style={{ color: 'red' }}>*</span>
                    </Form.Label>
                    <Controller
                      control={control}
                      name="email"
                      defaultValue=""
                      rules={{
                        required: 'Vui lòng nhập Email',
                        pattern: {
                          value: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                          message: 'Vui lòng nhập đúng Email',
                        },
                      }}
                      render={({
                        field: { onChange, value, ref },
                        fieldState: { invalid, isDirty },
                      }) => (
                        <FormControl
                          onChange={onChange}
                          value={value}
                          ref={ref}
                          isInvalid={errors.email}
                          isValid={isDirty && !invalid}
                          placeholder="Nhập email của bạn"
                          aria-label="email"
                          aria-describedby="email"
                          autoComplete="off"
                        />
                      )}
                    />
                    <Form.Control.Feedback>
                      {Object.keys(dirtyFields).length !== 0 && dirtyFields.email && !errors.email && (
                        <span>
                          <Image src="/images/svg/icon-check-circle.svg" alt="" />
                          Email hợp lệ
                        </span>
                      )}
                    </Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">
                      {Object.keys(errors).length !== 0 && errors.email?.type === 'pattern' && (
                        <span>
                          <Image src="/images/svg/icon-invalid.svg" alt="" />{' '}
                          {errors.email?.message}
                        </span>
                      )}
                      {Object.keys(errors).length !== 0 && errors.email?.type === 'required' && (
                        <span>
                          <Image src="/images/svg/icon-invalid.svg" alt="" />
                          {errors.email?.message}
                        </span>
                      )}
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Form.Group className="mb-4" controlId="formBasicTelephone">
                    <Form.Label>
                      Số điện thoại <span style={{ color: 'red' }}>*</span>
                    </Form.Label>
                    <Controller
                      control={control}
                      name="telephone"
                      defaultValue=""
                      rules={{
                        required: 'Vui lòng nhập số điện thoại',
                        pattern: {
                          value: /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/,
                          message: 'Số điện thoại không đúng',
                        },
                      }}
                      render={({
                        field: { onChange, value, ref },
                        fieldState: { invalid, isDirty },
                      }) => (
                        <FormControl
                          onChange={onChange}
                          value={value}
                          ref={ref}
                          isInvalid={errors.telephone}
                          isValid={isDirty && !invalid}
                          placeholder="Nhập số điện thoại của bạn"
                          aria-label="telephone"
                          aria-describedby="telephone"
                          autoComplete="off"
                        />
                      )}
                    />
                    <Form.Control.Feedback>
                      {Object.keys(dirtyFields).length !== 0 &&
                        dirtyFields.telephone &&
                        !errors.telephone && (
                          <span>
                            <Image src="/images/svg/icon-check-circle.svg" alt="" />
                            Số điện thoại hợp lệ
                          </span>
                        )}
                    </Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">
                      {Object.keys(errors).length !== 0 && errors.telephone?.type === 'pattern' && (
                        <span>
                          <Image src="/images/svg/icon-invalid.svg" alt="" />{' '}
                          {errors.telephone?.message}
                        </span>
                      )}
                      {Object.keys(errors).length !== 0 && errors.telephone?.type === 'required' && (
                        <span>
                          <Image src="/images/svg/icon-invalid.svg" alt="" />{' '}
                          {errors.telephone?.message}
                        </span>
                      )}
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Form.Group className="mb-4" controlId="formBasicAddress">
                    <Form.Label>Địa điểm</Form.Label>
                    <Controller
                      control={control}
                      name="address"
                      defaultValue=""
                      render={({
                        field: { onChange, value, ref },
                        fieldState: { invalid, isDirty },
                      }) => (
                        <Form.Select
                          aria-label="Default select example"
                          onChange={onChange}
                          value={value}
                          ref={ref}
                          isInvalid={errors.address}
                          isValid={isDirty && !invalid}
                        >
                          <option value="" disabled>
                            Tỉnh/Thành phố
                          </option>
                          <option value="1">Hà Nội</option>
                          <option value="2">Đà Nẵng</option>
                          <option value="3">TP Hồ Chí Minh</option>
                        </Form.Select>
                      )}
                    />
                    <Form.Control.Feedback>
                      {Object.keys(dirtyFields).length !== 0 &&
                        dirtyFields.address &&
                        !errors.address && (
                          <span>
                            <Image src="/images/svg/icon-check-circle.svg" alt="" />
                            Địa điểm hợp lệ
                          </span>
                        )}
                    </Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">
                      {Object.keys(errors).length !== 0 && errors.address?.type === 'required' && (
                        <span>
                          <Image src="/images/svg/icon-invalid.svg" alt="" />
                          Vui lòng chọn địa điểm
                        </span>
                      )}
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Form.Group className="mb-4" controlId="formBasicContent">
                    <Form.Label>Nội dung của bạn</Form.Label>
                    <Controller
                      control={control}
                      name="content"
                      defaultValue=""
                      render={({
                        field: { onChange, value, ref },
                        fieldState: { invalid, isDirty },
                      }) => (
                        <FormControl
                          onChange={onChange}
                          value={value}
                          ref={ref}
                          isInvalid={errors.content}
                          isValid={isDirty && !invalid}
                          placeholder="Sản phẩm, lĩnh vực kinh doanh,..."
                          aria-label="content"
                          aria-describedby="content"
                          autoComplete="off"
                          type="textarea"
                          as="textarea"
                          rows={3}
                        />
                      )}
                    />
                    <Form.Control.Feedback>
                      {Object.keys(dirtyFields).length !== 0 &&
                        dirtyFields.content &&
                        !errors.content && (
                          <span>
                            <Image src="/images/svg/icon-check-circle.svg" alt="" />
                            Nội dung hợp lệ
                          </span>
                        )}
                    </Form.Control.Feedback>
                    <Form.Control.Feedback type="invalid">
                      {Object.keys(errors).length !== 0 && errors.content?.type === 'required' && (
                        <span>
                          <Image src="/images/svg/icon-invalid.svg" alt="" /> Vui lòng nhập nội dung
                        </span>
                      )}
                    </Form.Control.Feedback>
                  </Form.Group>

                  <div className="text-center">
                    <button
                      className={
                        modalSending
                          ? `${styles['btn-bizzone-shop-2']} ${styles['btn-loading']}`
                          : `${styles['btn-bizzone-shop-2']}`
                      }
                      type="submit"
                      disabled={modalSending}
                    >
                      <span>{modalSending ? 'ĐANG GỬI...' : 'NHẬN TƯ VẤN'}</span>
                    </button>
                  </div>
                </Form>
              </Col>
            </Row>
          </div>
        </div>
      </Container>

      {/* Show success modal */}
      <div className={styles['div-modal-pricing-page-bizzone-shop']}>
        <ModalSendSuccess show={showSucessModal} onCloseModal={handleCloseModal} />
      </div>
    </div>
  );
}
export default RowContactUs;
