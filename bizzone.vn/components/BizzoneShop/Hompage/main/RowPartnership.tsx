import React from 'react';
import { Container,Image } from 'react-bootstrap';
import styles from '../../styles/Homepage.module.scss';


function RowPartnership() {
  return (
    <div className={styles['back-ground-partnership-bizzone-shop']}>
      <Container className="max-width-1180 px-0">
        <div className="padding-left-right">
          <h2 className={styles['title-partnership-bizzone-shop']}>
            Được tin dùng bởi các doanh nghiệp
          </h2>
          <div className={styles['row-logo-partnership-bizzone-shop']}>
            <div
              className={styles['row-content-logo-partnership-bizzone-shop']}
            >
              <div>
                <Image
                  src="/images/svg/logo-smart-construction-main.svg"
                  className="img-fluid"
                  alt=""
                />
              </div>
              <div>
                <Image
                  src="/images/svg/logo-ks-finance-main.svg"
                  className="img-fluid"
                  alt=""
                />
              </div>
              <div>
                <Image
                  src="/images/svg/logo-media-partner-main.svg"
                  className="img-fluid"
                  alt=""
                />
              </div>
              <div>
                <Image
                  src="/images/svg/logo-sunshine-home-main.svg"
                  className="img-fluid"
                  alt=""
                />
              </div>
              <div>
                <Image
                  src="/images/svg/logo-sunshine-smart.svg"
                  className="img-fluid"
                  alt=""
                />
              </div>
              <div>
                <Image
                  src="/images/svg/logo-kienlong-bank.svg"
                  className="img-fluid"
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
}
export default RowPartnership;
