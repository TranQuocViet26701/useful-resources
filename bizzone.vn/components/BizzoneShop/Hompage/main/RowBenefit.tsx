import React from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import styles from '../../styles/Homepage.module.scss';

function RowBenefit() {
  return (
    <div className={styles['back-ground-benefis-bizzone-shop']}>
      <Container className="max-width-1180 px-0">
        <Row className="padding-left-right">
          <Col md={4} style={{ padding: '0 20px' }}>
            <div className={styles['div-icon-bizzone-shop']}>
              <Image
                src="/images/svg/icon-tick-v-bizzone-shop.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h3 className={styles['title-benefis-bizzone-shop']}>Dễ sử dụng</h3>
            <p className={styles['sub-title-benefis-bizzone-shop']}>
              Giao diện thân thiện dễ dàng thao tác, tùy chỉnh POS của Usee
              theo nhu cầu bán online và cửa hàng thuận tiện nhất.
            </p>
          </Col>
          <Col md={4} style={{ padding: '0 20px' }}>
            <div className={styles['div-icon-bizzone-shop']}>
              <Image
                src="/images/svg/icon-industry-bizzone-shop.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h3 className={styles['title-benefis-bizzone-shop']}>
              Tích hợp đa dạng ngành
            </h3>
            <p className={styles['sub-title-benefis-bizzone-shop']}>
              Giải pháp bán hàng đa kênh tích hợp đa ngành, phù hợp với mọi loại
              hình và quy mô kinh doanh của doanh nghiệp.
            </p>
          </Col>
          <Col md={4} style={{ padding: '0 20px' }}>
            <div className={styles['div-icon-bizzone-shop']}>
              <Image
                src="/images/svg/icon-save-bizzone-shop.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h3 className={styles['title-benefis-bizzone-shop']}>
              Tiết kiệm chi phí
            </h3>
            <p className={styles['sub-title-benefis-bizzone-shop']}>
              Rút ngắn thời gian thực hiện các thủ tục thanh toán, quản lý nhân
              viên, đồng nhất dữ liệu quản lý tồn kho giúp tiết kiệm chi phí vận
              hành.
            </p>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
export default RowBenefit;
