export {default as HomepageBizzoneShop} from './Hompage'
export {default as PricingpageBizzoneShop} from './Pricingpage'
export {default as RetailpageBizzoneShop} from './Retailpage'
export {default as FBpageBizzoneShop} from './F&Bpage'
export {default as ServicepageBizzoneShop} from './Servicepage'
export {default as OtherFieldpageBizzoneShop} from './OtherFieldpage'
