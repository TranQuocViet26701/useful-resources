/* eslint-disable max-len */
import React, { useState } from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import styles2 from '../../styles/FoodBeveragePage.module.scss';
import styles from '../../styles/RetailPage.module.scss';

const businessList = [
  {
    name: 'Nhà hàng',
    content:
      'Tiết kiệm thời gian phục vụ thông qua tính năng gọi món, tối ưu quy trình order, phục vụ, thu ngân, bar/bếp giúp hoạt động vận hành kinh doanh suôn sẻ hơn. Hỗ trợ đa dạng phương thức thanh toán, giảm thất thoát khâu quản lý nguyên vật liệu và lên báo cáo - phân tích bán hàng hiệu quả.',
  },
  {
    name: 'Quán Cafe',
    content:
      'Tiết kiệm thời gian phục vụ thông qua tính năng gọi món, tối ưu quy trình order, phục vụ, thu ngân, bar/bếp giúp hoạt động vận hành kinh doanh suôn sẻ hơn. Hỗ trợ đa dạng phương thức thanh toán, giảm thất thoát khâu quản lý nguyên vật liệu và lên báo cáo - phân tích bán hàng hiệu quả.',
  },
  {
    name: 'Quán Bar',
    content:
      'Tiết kiệm thời gian phục vụ thông qua tính năng gọi món, tối ưu quy trình order, phục vụ, thu ngân, bar/bếp giúp hoạt động vận hành kinh doanh suôn sẻ hơn. Hỗ trợ đa dạng phương thức thanh toán, giảm thất thoát khâu quản lý nguyên vật liệu và lên báo cáo - phân tích bán hàng hiệu quả.',
  },
  {
    name: 'Karaoke',
    content:
      'Tiết kiệm thời gian phục vụ thông qua tính năng gọi món, tối ưu quy trình order, phục vụ, thu ngân, bar/bếp giúp hoạt động vận hành kinh doanh suôn sẻ hơn. Hỗ trợ đa dạng phương thức thanh toán, giảm thất thoát khâu quản lý nguyên vật liệu và lên báo cáo - phân tích bán hàng hiệu quả.',
  },
];

interface RowBusinessFieldProps {
  businessId: number;
  handleClickChangeBusiness: (businessId: number) => void;
}

function RowBusinessField({ businessId, handleClickChangeBusiness }: RowBusinessFieldProps) {
  const handleBackToAll = () => {
    if (!handleClickChangeBusiness) return;
    handleClickChangeBusiness(-1);
  };

  return (
    <>
      <Row className="justify-content-center" style={{ width: '100%', marginTop: '10px' }}>
        <Col xs="auto">
          <Image
            src="/images/svg/arrow-left-black.svg"
            fluid
            style={{
              cursor: 'pointer',
              marginRight: '23px',
            }}
            onClick={handleBackToAll}
            alt=""
          />
          <button className="btn-bizzone-shop-4" type="button" style={{ maxWidth: '267px' }}>
            <span>{businessList[businessId].name}</span>
          </button>
        </Col>
      </Row>
      <p className={styles['sub-title-business-item-retail-page']}>
        {businessList[businessId].content}
      </p>
    </>
  );
}

function RowBusinessItem() {
  const [businessId, setBusinessId] = useState(-1);

  const handleClickChangeBusiness = (numBusiness: number) => {
    const intNumBusiness = numBusiness;
    setBusinessId(intNumBusiness);
  };

  return (
    <div
      className={`${styles['bg-business-item-retail-bizzone-shop']} ${styles2['bg-business-item-food-beverage-page-bizzone-shop']}`}
    >
      <Container className="max-width-1180 px-0">
        <div className={styles['wrapper-business-item-retail-page-bizzone-shop']}>
          <h2 className={styles['title-business-item-retail-bizzone-shop']}>
            Mặt hàng bạn kinh doanh là gì?
          </h2>
          {businessId === -1 ? (
            <Row style={{ width: '100%', margin: '0' }}>
              {businessList.map((business, index) => (
                <Col
                  key={business.name}
                  md={6}
                  className={`d-flex justify-content-center justify-content-md-${
                    index % 2 === 0 ? 'end' : 'start'
                  }`}
                >
                  <button
                    className="btn-bizzone-shop-3"
                    type="button"
                    style={{ maxWidth: '410px', width: '100%' }}
                    onClick={() => handleClickChangeBusiness(index)}
                  >
                    <span>{businessList[index].name}</span>
                  </button>
                </Col>
              ))}
            </Row>
          ) : (
            <RowBusinessField
              businessId={businessId}
              handleClickChangeBusiness={handleClickChangeBusiness}
            />
          )}
        </div>
      </Container>
    </div>
  );
}
export default RowBusinessItem;
