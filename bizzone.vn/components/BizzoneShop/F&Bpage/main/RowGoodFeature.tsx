import React from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import styles from '../../styles/RetailPage.module.scss';

function RowGoodFeature() {
  return (
    <div>
      <Container className="max-width-1180 px-0">
        <Row className="padding-left-right" style={{ marginBottom: '40px' }}>
          <Col>
            <h2 className={styles['title-good-feature-bizzone-shop']}>
              Những tính năng mà một hệ thống POS F&B tốt cần có
            </h2>
          </Col>
        </Row>
        <Row>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature1-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Quản lý kho với đa dạng mặt hàng một cách dễ dàng
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Hàng hóa được sắp xếp theo danh mục, quản lý bằng mã vạch/mã số riêng. Phân loại chi
              tiết từng sản phẩm theo thương hiệu, màu sắc, kích thước, chủng loại...
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature2-food-beverage-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Quy lý quy trình Phục vụ - Thu ngân - Bếp chuẩn xác, thuận lợi
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Theo dõi hoạt động, thao tác thông báo giữa các bên phục vụ, thu ngân, quầy bar/bếp
              thuận tiện và kịp thời.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature3-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Thanh toán nhanh giảm thiểu thất thoát
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Hỗ trợ thu ngân thanh toán nhanh trong giờ cao điểm, giảm thiểu nhầm lẫn. Tất cả thao
              tác của nhân viên đều được lưu trữ rõ ràng, dễ dàng tra cứu khi cần thiết.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature4-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Thiết lập chính sách giá & chương trình khuyến mãi linh hoạt
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Thiết lập các chương trình khuyến mãi (Voucher/ Giảm giá/ Tích điểm/ Thẻ thành
              viên...) Linh hoạt áp dụng các mức giá khác nhau, thời gian, chi nhánh
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature5-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Quản lý các chi nhánh một cách dễ dàng & đồng bộ
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Hàng hóa được sắp xếp theo danh mục, quản lý bằng mã vạch/mã số riêng. Phân loại chi
              tiết từng sản phẩm theo thương hiệu, màu sắc, kích thước, chủng loại...
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature6-food-beverage-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Đặt bàn, đặt chỗ từ xa một cách dễ dàng
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Phần mềm hỗ trợ quản lý phòng bàn chuyên nghiệp, kiểm tra nhanh bàn nào còn trống, bàn
              nào đã có khách... hoặc đặt bàn sẵn theo nhu cầu của khách.
            </p>
          </Col>
        </Row>
        <Row className="justify-content-center" style={{ marginTop: '40px' }}>
          <button
            className="btn-bizzone-shop-2"
            type="button"
            style={{
              margin: 'auto',
              width: '200px',
              boxShadow:
                '0px 6px 14px -6px rgba(24, 39, 75, 0.12), 0px 10px 32px -4px rgba(24, 39, 75, 0.1)',
            }}
          >
            <span>Dùng thử</span>
          </button>
        </Row>
      </Container>
    </div>
  );
}
export default RowGoodFeature;
