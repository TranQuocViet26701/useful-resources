import Head from 'next/head';
import React from 'react';
import { Container } from 'react-bootstrap';
import LayoutBizzoneShop from '../LayoutBizzoneShop';
import { RowBottomRetailPage, RowPosSolution } from '../Retailpage/main';
import { RowBusinessItem, RowGoodFeature, RowQuestion, RowTopFoodBeveragePage } from './main';

function FBPage() {
  return (
    <>
      <Head>
        <title>F&B | Usee</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="title" content="F&B | Usee" />
        <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png" />
        <meta name="title" content="F&B | Usee - Chuẩn hóa bán hàng cho kinh doanh F&B cùng Usee" />
        <meta
          name="description"
          content="F&B | Usee - Quản lý kinh doanh F&B hiệu quả giúp tối ưu quy trình order - thu ngân - bếp, tiết kiệm tối đa thời gian phục vụ và hỗ trợ thanh toán thuận lợi."
        />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://bizzone.vn/" />
        <meta
          property="og:title"
          content="F&B | Usee - Chuẩn hóa bán hàng cho kinh doanh F&B cùng Usee"
        />
        <meta
          property="og:description"
          content="F&B | Usee - Quản lý kinh doanh F&B hiệu quả giúp tối ưu quy trình order - thu ngân - bếp, tiết kiệm tối đa thời gian phục vụ và hỗ trợ thanh toán thuận lợi."
        />
        <meta property="og:image" content="https://bizzone.vn/meta-data.png" />

        {/* Twitter */}
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://bizzone.vn/" />
        <meta
          property="twitter:title"
          content="F&B | Usee - Chuẩn hóa bán hàng cho kinh doanh F&B cùng Usee"
        />
        <meta
          property="twitter:description"
          content="F&B | Usee - Quản lý kinh doanh F&B hiệu quả giúp tối ưu quy trình order - thu ngân - bếp, tiết kiệm tối đa thời gian phục vụ và hỗ trợ thanh toán thuận lợi."
        />
        <meta property="twitter:image" content="https://bizzone.vn/meta-data.png" />
      </Head>
      <LayoutBizzoneShop>
        <main>
          <Container className="max-width-100 px-0" fluid>
            <RowTopFoodBeveragePage />
            <RowGoodFeature />
            <RowBusinessItem />
            <RowPosSolution />
            <RowBottomRetailPage />
            <RowQuestion />
          </Container>
        </main>
      </LayoutBizzoneShop>
    </>
  );
}

export default FBPage;
