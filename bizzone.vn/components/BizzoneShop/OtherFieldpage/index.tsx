import Head from 'next/head';
import React from 'react';
import { Container } from 'react-bootstrap';
import LayoutBizzoneShop from '../LayoutBizzoneShop';
import { RowBottomRetailPage, RowPosSolution } from '../Retailpage/main';
import { RowBusinessItem, RowGoodFeature, RowQuestion, RowTopOtherFieldPage } from './main';

function OtherFieldPage() {
  return (
    <>
      <Head>
        <title>Ngành nghề khác | Usee</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="title" content="Ngành nghề khác | Usee" />
        <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png" />
        <meta
          name="title"
          content="Ngành nghề khác | Usee - Chuẩn hóa bán hàng các Ngành nghề khác"
        />
        <meta
          name="description"
          content="Ngành nghề khác | Usee - Giải pháp giúp doanh nghiệp tiết kiệm thời gian, chi phí khâu quản lý bán hàng, thúc đẩy hoạt động kinh doanh hiệu quả ở đa dạng lĩnh vực - ngành nghề."
        />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://bizzone.vn/" />
        <meta
          property="og:title"
          content="Ngành nghề khác | Usee - Chuẩn hóa bán hàng các Ngành nghề khác"
        />
        <meta
          property="og:description"
          content="Ngành nghề khác | Usee - Giải pháp giúp doanh nghiệp tiết kiệm thời gian, chi phí khâu quản lý bán hàng, thúc đẩy hoạt động kinh doanh hiệu quả ở đa dạng lĩnh vực - ngành nghề."
        />
        <meta property="og:image" content="https://bizzone.vn/meta-data.png" />

        {/* Twitter */}
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://bizzone.vn/" />
        <meta
          property="twitter:title"
          content="Ngành nghề khác | Usee - Chuẩn hóa bán hàng các Ngành nghề khác"
        />
        <meta
          property="twitter:description"
          content="Ngành nghề khác | Usee - Giải pháp giúp doanh nghiệp tiết kiệm thời gian, chi phí khâu quản lý bán hàng, thúc đẩy hoạt động kinh doanh hiệu quả ở đa dạng lĩnh vực - ngành nghề."
        />
        <meta property="twitter:image" content="https://bizzone.vn/meta-data.png" />
      </Head>
      <LayoutBizzoneShop>
        <main>
          <Container className="max-width-100 px-0" fluid>
            <RowTopOtherFieldPage />
            <RowGoodFeature />
            <RowBusinessItem />
            <RowPosSolution />
            <RowBottomRetailPage />
            <RowQuestion />
          </Container>
        </main>
      </LayoutBizzoneShop>
    </>
  );
}

export default OtherFieldPage;
