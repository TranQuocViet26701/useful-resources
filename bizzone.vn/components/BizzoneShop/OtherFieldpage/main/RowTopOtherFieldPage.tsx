import React, { useState } from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import { ModalFormContact, ModalSendSuccess } from '../../../Common';
import styles2 from '../../styles/OtherFieldPage.module.scss';
import styles from '../../styles/RetailPage.module.scss';

function RowTopOtherFieldPage() {
  const [showFormModal, setShowFormModal] = useState(false);
  const [showSucessModal, setShowSucessModal] = useState(false);

  const handleSetFormContact = (show: boolean) => {
    setShowFormModal(show);
  };

  const handleSetSuccessModal = (show: boolean) => {
    setShowSucessModal(show);
  };

  return (
    <div
      className={`${styles['bg-top-retail-bizzone-shop']} ${styles2['bg-top-other-field-page-bizzone-shop']}`}
    >
      <Container className="max-width-1180 px-0">
        <Row className="gx-5 padding-left-right">
          <Col md={7} className="mb-4 mb-md-5">
            <h2 className={styles['title-top-retail-page-bizzone-shop']}>
              Chuẩn hóa bán hàng các Ngành nghề khác
            </h2>
            <p className={styles['sub-title-top-retail-page-bizzone-shop']}>
              Giải pháp giúp doanh nghiệp tiết kiệm thời gian, chi phí khâu quản lý bán hàng, thúc
              đẩy hoạt động kinh doanh hiệu quả ở đa dạng lĩnh vực - ngành nghề.
            </p>
            <div className="d-flex justify-content-center justify-content-md-start">
              <button
                className="btn-bizzone-shop-1"
                type="button"
                onClick={() => setShowFormModal(true)}
              >
                <span>Liên hệ tư vấn</span>
              </button>
              <button className="btn-bizzone-shop-2" type="button" style={{ marginLeft: 16 }}>
                <span>Dùng thử</span>
              </button>
            </div>
          </Col>
        </Row>
      </Container>
      <Image
        src="/images/bizzone-shop/img-top-mobile-other-field-page-bizzone-shop.png"
        className={styles['img-top-retail-bizzone-shop']}
        alt=""
      />

      {/* Show form modal */}
      <ModalFormContact
        show={showFormModal}
        option=""
        handleSetFormContact={handleSetFormContact}
        handleSetSuccessModal={handleSetSuccessModal}
      />

      {/* Show success modal */}
      <ModalSendSuccess show={showSucessModal} onCloseModal={handleSetSuccessModal} />
    </div>
  );
}
export default RowTopOtherFieldPage;
