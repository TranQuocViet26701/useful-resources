/* eslint-disable max-len */
import React, { useState } from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import styles from '../../styles/RetailPage.module.scss';
import styles2 from '../../styles/ServicePage.module.scss';

const businessList = [
  {
    name: 'Cửa hàng thú cưng',
    content:
      'Linh hoạt thời gian và tiết kiệm tối đa chi phí với giải pháp POS cho các nhà bán lẻ. Usee giúp quản lý tồn kho, đồng bộ dữ liệu trên hệ thống bán hàng đa kênh, hỗ trợ chăm sóc khách hàng và quản lý nhân viên hiệu quả. Nhờ đó doanh nghiệp có thể đưa ra kế hoạch kinh doanh kịp thời.',
  },
  {
    name: 'Bán hàng kết hợp cho thuê',
    content:
      'Linh hoạt thời gian và tiết kiệm tối đa chi phí với giải pháp POS cho các nhà bán lẻ. Usee giúp quản lý tồn kho, đồng bộ dữ liệu trên hệ thống bán hàng đa kênh, hỗ trợ chăm sóc khách hàng và quản lý nhân viên hiệu quả. Nhờ đó doanh nghiệp có thể đưa ra kế hoạch kinh doanh kịp thời.',
  },
];

interface RowBusinessFieldProps {
  businessId: number;
  handleClickChangeBusiness: (businessId: number) => void;
}

function RowBusinessField({ businessId, handleClickChangeBusiness }: RowBusinessFieldProps) {
  const handleBackToAll = () => {
    if (!handleClickChangeBusiness) return;
    handleClickChangeBusiness(-1);
  };

  return (
    <>
      <Row className="justify-content-center" style={{ width: '100%', marginTop: '10px' }}>
        <Col xs="auto">
          <Image
            src="/images/svg/arrow-left-black.svg"
            fluid
            style={{
              cursor: 'pointer',
              marginRight: '23px',
            }}
            onClick={handleBackToAll}
            alt=""
          />
          <button className="btn-bizzone-shop-4" type="button" style={{ maxWidth: '267px' }}>
            <span>{businessList[businessId].name}</span>
          </button>
        </Col>
      </Row>
      <p className={styles['sub-title-business-item-retail-page']}>
        {businessList[businessId].content}
      </p>
    </>
  );
}

function RowBusinessItem() {
  const [businessId, setBusinessId] = useState(-1);

  const handleClickChangeBusiness = (numBusiness: number) => {
    const intNumBusiness = numBusiness;
    setBusinessId(intNumBusiness);
  };

  return (
    <div
      className={`${styles['bg-business-item-retail-bizzone-shop']} ${styles2['bg-business-item-other-field-bizzone-shop']}`}
    >
      <Container className="max-width-1180 px-0">
        <div className={styles['wrapper-business-item-retail-page-bizzone-shop']}>
          <h2 className={styles['title-business-item-retail-bizzone-shop']}>Bạn kinh doanh gì?</h2>
          {businessId === -1 ? (
            <Row style={{ width: '100%', margin: '0' }}>
              {businessList.map((business, index) => (
                <Col
                  key={business.name}
                  md={6}
                  className={`d-flex justify-content-center justify-content-md-${
                    index % 2 === 0 ? 'end' : 'start'
                  }`}
                >
                  <button
                    className="btn-bizzone-shop-3"
                    type="button"
                    style={{ maxWidth: '410px', width: '100%' }}
                    onClick={() => handleClickChangeBusiness(index)}
                  >
                    <span>{businessList[index].name}</span>
                  </button>
                </Col>
              ))}
            </Row>
          ) : (
            <RowBusinessField
              businessId={businessId}
              handleClickChangeBusiness={handleClickChangeBusiness}
            />
          )}
        </div>
      </Container>
    </div>
  );
}
export default RowBusinessItem;
