import React from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import styles from '../../styles/RetailPage.module.scss';

function RowGoodFeature() {
  return (
    <div>
      <Container className="max-width-1180 px-0">
        <Row className="padding-left-right" style={{ marginBottom: '40px' }}>
          <Col>
            <h2 className={styles['title-good-feature-bizzone-shop']}>
              Những tính năng mà một hệ thống POS tốt cần có
            </h2>
          </Col>
        </Row>
        <Row>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature1-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Quản lý danh mục hàng hoá đa dạng một cách dễ dàng
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Hàng hóa được phân loại chi tiết theo thương hiệu, màu sắc, kích thước, chủng loại và
              sắp xếp theo danh mục, quản lý bằng mã vạch/ mã số riêng.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature2-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Tính năng phân tích - báo cáo mạnh mẽ & chính xác
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Thực hiện báo cáo tồn kho, báo cáo chi tiết mỗi ngày về doanh số, sản phẩm và hiệu
              suất của nhân viên bán hàng để quyết định hành động kinh doanh sắp tới.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature3-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Linh hoạt các phương thức thanh toán tiện lợi
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Đảm bảo quy trình thanh toán linh hoạt và an toàn thông qua hình thức chuyển khoản,
              quẹt thẻ nhanh chóng bên cạnh cách dùng tiền mặt.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature4-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Thiết lập giá & chương trình khuyến mãi linh hoạt
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Linh hoạt áp dụng các mức giá khác nhau theo chương trình khuyến mãi (voucher, giảm
              giá, tích điểm, thẻ thành viên...) theo từng mốc thời gian, chi nhánh.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature5-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Quản lý các chi nhánh một cách dễ dàng & đồng bộ
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Đồng bộ thông tin và dữ liệu từ các chi nhánh theo thời gian thực, theo dõi và cải
              thiện hiệu suất cửa hàng để đưa ra kế hoạch kinh doanh phù hợp.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature6-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Tăng trải nghiệm mua hàng & cải thiện dịch vụ khách hàng
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Thông báo hoá đơn, khuyến mãi và chăm sóc hậu mãi cho khách hàng chu đáo với tính năng
              gửi Email, SMS, nhắn tin qua Facebook, Zalo nhanh chóng.
            </p>
          </Col>
        </Row>
        <Row className="justify-content-center" style={{ marginTop: '40px' }}>
          <button
            className="btn-bizzone-shop-2"
            type="button"
            style={{
              margin: 'auto',
              width: '200px',
              boxShadow:
                '0px 6px 14px -6px rgba(24, 39, 75, 0.12), 0px 10px 32px -4px rgba(24, 39, 75, 0.1)',
            }}
          >
            <span>Dùng thử</span>
          </button>
        </Row>
      </Container>
    </div>
  );
}
export default RowGoodFeature;
