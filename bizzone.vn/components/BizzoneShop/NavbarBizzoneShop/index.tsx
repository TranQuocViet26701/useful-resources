import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useMemo, useState } from 'react';
import { Container, Image, Nav, Navbar, Offcanvas } from 'react-bootstrap';
import styles from './NavbarBizzoneShop.module.scss';

interface MenuModel {
  id: number;
  title: string;
  link: string;
  elementId: string;
  valueCheck: string;
}

const listMenu: MenuModel[] = [
  {
    id: 0,
    title: 'Trang chủ',
    link: '/usee',
    elementId: '',
    valueCheck: 'usee',
  },
  {
    id: 1,
    title: 'Tính năng',
    link: '/usee',
    elementId: 'feature',
    valueCheck: 'feature',
  },
  {
    id: 2,
    title: 'Bảng giá',
    link: '/usee/pricing-page',
    elementId: '',
    valueCheck: 'pricing-page',
  },
  {
    id: 3,
    title: 'Ngành hàng',
    link: '/usee',
    elementId: 'business',
    valueCheck: 'business',
  },
  {
    id: 4,
    title: 'Liên hệ',
    link: '/usee',
    elementId: 'contact',
    valueCheck: 'contact',
  },
];

const NavbarBizzoneShop = () => {
  const [show, setShow] = useState(false);

  const onShow = () => setShow(true);

  const onHide = () => setShow(false);

  const location = useRouter().asPath;

  const [activeNav, setActiveNav] = useState(() => {
    let value = location.split('/usee')[1];
    // if have # => get hash value else get page name
    if (value.includes('#')) {
      value = value.split('#')[1];
    } else if (value.includes('/')) {
      if (value.split('/')[1] !== 'pricing-page') value = 'business';
      else value = 'pricing-page';
    } else {
      value = 'usee';
    }

    return value;
  });

  // useEffect(() => {
  //   const handleValueCheck = () => {
  //     let value = location.split('/usee')[1];
  //     // if have # => get hash value else get page name
  //     if (value.includes('#')) {
  //       value = value.split('#')[1];
  //     } else if (value.includes('/')) {
  //       if (value.split('/')[1] !== 'pricing-page') value = 'business';
  //       else value = 'pricing-page';
  //     } else {
  //       value = 'usee';
  //     }

  //     setActiveNav(value);
  //   };

  //   handleValueCheck();
  // }, [location]);

  const handleClickScroll = (id: string) => {
    const Element = document.getElementById(id);
    Element?.scrollIntoView({ behavior: 'smooth' });
    setActiveNav(id);
  };

  const navItems = useMemo(() => {
    return (
      <>
        <Nav className="mx-auto" navbarScroll key="nav-item">
          {listMenu.map((menu) => {
            return (
              <Link
                key={menu.id}
                href={menu.elementId ? `/usee#${menu.elementId}` : menu.link}
                passHref
              >
                <Nav.Link
                  className={`${styles['custom-link-bizzone-shop']} ${
                    activeNav === menu.valueCheck ? 'active' : ''
                  }`}
                  onClick={() => handleClickScroll(menu.elementId)}
                >
                  {menu.title}
                </Nav.Link>
              </Link>
            );
          })}
        </Nav>
        <Nav>
          <Link href="" passHref>
            <Nav.Link>
              <button
                type="button"
                className={styles['btn-trial-bizzone-shop']}
                style={{ marginRight: 12 }}
              >
                <span>DÙNG THỬ</span>
              </button>
            </Nav.Link>
          </Link>
        </Nav>
      </>
    );
  }, [activeNav]);

  return (
    <Navbar
      as="header"
      role="banner"
      expand="lg"
      className={styles['nav-bizzone-shop']}
      fixed="top"
      collapseOnSelect
      id="nav-bar-id"
    >
      <Container className={styles['menu-desktop']}>
        <Navbar.Brand href="/usee">
          <Image
            src="/images/svg/logo-usee.svg"
            height={64}
            className={`${styles['logo-menu']} d-inline-block align-top`}
            alt="Usee Logo"
            id="logo-menu"
          />
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="offcanvasNavbar"
          className={styles['custom-navbar-toggle']}
          onClick={onShow}
        />
        <Navbar.Offcanvas show={show ? 1 : 0} onShow={onShow} onHide={onHide} id="offcanvasNavbar">
          <Offcanvas.Header closeButton className={styles['offcanvas-bizzone-shop']}>
            <Offcanvas.Title id="offcanvasNavbarLabel">
              <Navbar.Brand href="/usee">
                <Image
                  src="/images/svg/logo-bizzone-shop.svg"
                  height={64}
                  className={`${styles['logo-menu']} d-inline-block align-top`}
                  alt="Usee Logo"
                  id="logo-menu"
                />
              </Navbar.Brand>
            </Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body style={{ flexGrow: 1 }} className={styles['offcanvas-bizzone-shop']}>
            {navItems}
          </Offcanvas.Body>
        </Navbar.Offcanvas>
      </Container>
    </Navbar>
  );
};

export default NavbarBizzoneShop;
