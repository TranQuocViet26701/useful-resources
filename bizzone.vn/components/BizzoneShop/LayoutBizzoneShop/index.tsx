import dynamic from 'next/dynamic';
import React from 'react';
import { Footer } from '../../Common';

const NavbarBizzoneShop = dynamic(() => import('../NavbarBizzoneShop'), {
  ssr: false,
});

export interface LayoutBizzoneShopProps {
  children: JSX.Element | JSX.Element[];
}

export default function LayoutBizzoneShop({ children }: LayoutBizzoneShopProps) {
  return (
    <>
      <NavbarBizzoneShop />
      {children}
      <Footer />
    </>
  );
}
