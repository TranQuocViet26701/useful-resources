import React from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import styles from '../../styles/RetailPage.module.scss';

function RowGoodFeature() {
  return (
    <div>
      <Container className="max-width-1180 px-0">
        <Row className="padding-left-right" style={{ marginBottom: '40px' }}>
          <Col>
            <h2 className={styles['title-good-feature-bizzone-shop']}>
              Những tính năng mà một POS dịch vụ tốt cần có
            </h2>
          </Col>
        </Row>
        <Row>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature6-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Chương trình chăm sóc khách hàng chuyên nghiệp & chu đáo
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Lưu trữ thông tin khách hàng, tra cứu và phân tích dữ liệu khách hàng để phục vụ chăm
              sóc hậu mãi chu đáo với tính năng gửi Email, SMS, nhắn tin qua Facebook, Zalo hiệu
              quả.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature2-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Phân tích & báo cáo kinh doanh chi tiết, rõ ràng
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Thực hiện báo cáo tồn kho, báo cáo chi tiết mỗi ngày về doanh số, sản phẩm và hiệu
              suất của nhân viên bán hàng để quyết định hành động kinh doanh sắp tới.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature3-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Hỗ trợ đa dạng phương thức thanh toán tiện lợi
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Giảm thiểu tình trạng thanh toán chậm, thất thoát và nhầm lẫn vào giờ cao điểm. Giúp
              lưu trữ thông tin giao dịch và hỗ trợ thu ngân tra cứu khi cần thiết.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature4-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Thiết lập giá & chương trình khuyến mãi linh hoạt
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Linh hoạt áp dụng các mức giá khác nhau phù hợp trong thời gian khuyến mãi, hỗ trợ lên
              kế hoạch tặng voucher, giảm giá, tích điểm thành viên,... cho khách hàng.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature5-retail-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Quản lý các chi nhánh cách dễ dàng & đồng bộ
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Đồng bộ thông tin và dữ liệu từ các chi nhánh theo thời gian thực, theo dõi và cải
              thiện hiệu suất từng cửa hàng để đưa ra kế hoạch kinh doanh phù hợp.
            </p>
          </Col>
          <Col md={4} className={styles['pos-good-feature-bizzone-shop-item']}>
            <div className={styles['div-icon-pos-good-feature-bizzone-shop-item']}>
              <Image
                src="/images/svg/bizzone-shop/icon-pos-good-feature6-food-beverage-page.svg"
                className="img-fluid"
                alt=""
              />
            </div>
            <h4 className={styles['title-pos-good-feature-bizzone-shop-item']}>
              Quản lý lịch hẹn với khách hàng tiện lợi & chuyên nghiệp
            </h4>
            <p className={styles['sub-title-pos-good-feature-bizzone-shop-item']}>
              Lưu trữ thông tin dữ liệu khách hàng, dễ dàng tìm kiếm và lên lịch hỗ trợ, tư vấn với
              khách hàng, hỗ trợ điều chỉnh lịch hẹn khi có nhu cầu.
            </p>
          </Col>
        </Row>
        <Row className="justify-content-center" style={{ marginTop: '40px' }}>
          <button
            className="btn-bizzone-shop-2"
            type="button"
            style={{
              margin: 'auto',
              width: '200px',
              boxShadow:
                '0px 6px 14px -6px rgba(24, 39, 75, 0.12), 0px 10px 32px -4px rgba(24, 39, 75, 0.1)',
            }}
          >
            <span>Dùng thử</span>
          </button>
        </Row>
      </Container>
    </div>
  );
}
export default RowGoodFeature;
