import Head from 'next/head';
import React from 'react';
import { Container } from 'react-bootstrap';
import LayoutBizzoneShop from '../LayoutBizzoneShop';
import { RowBottomRetailPage, RowPosSolution } from '../Retailpage/main';
import { RowBusinessItem, RowGoodFeature, RowQuestion, RowTopServicePage } from './main';

function ServicePage() {
  return (
    <>
      <Head>
        <title>Dịch vụ | Usee</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="title" content="Dịch vụ | Usee" />
        <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png" />
        <meta name="title" content="Dịch vụ | Usee - Chuẩn hóa cho kinh doanh Dịch vụ cùng Usee" />
        <meta
          name="description"
          content="Dịch vụ | Usee - Linh hoạt chi phí kinh doanh và tối ưu hoạt động khuyến mãi, chăm sóc khách hàng hiệu quả với giải pháp kinh doanh dịch vụ thông minh và chuyên nghiệp."
        />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://bizzone.vn/" />
        <meta
          property="og:title"
          content="Dịch vụ | Usee - Chuẩn hóa cho kinh doanh Dịch vụ cùng Usee"
        />
        <meta
          property="og:description"
          content="Dịch vụ | Usee - Linh hoạt chi phí kinh doanh và tối ưu hoạt động khuyến mãi, chăm sóc khách hàng hiệu quả với giải pháp kinh doanh dịch vụ thông minh và chuyên nghiệp."
        />
        <meta property="og:image" content="https://bizzone.vn/meta-data.png" />

        {/* Twitter */}
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://bizzone.vn/" />
        <meta
          property="twitter:title"
          content="Dịch vụ | Usee - Chuẩn hóa cho kinh doanh Dịch vụ cùng Usee"
        />
        <meta
          property="twitter:description"
          content="Dịch vụ | Usee - Linh hoạt chi phí kinh doanh và tối ưu hoạt động khuyến mãi, chăm sóc khách hàng hiệu quả với giải pháp kinh doanh dịch vụ thông minh và chuyên nghiệp."
        />
        <meta property="twitter:image" content="https://bizzone.vn/meta-data.png" />
      </Head>
      <LayoutBizzoneShop>
        <main>
          <Container className="max-width-100 px-0" fluid>
            <RowTopServicePage />
            <RowGoodFeature />
            <RowBusinessItem />
            <RowPosSolution />
            <RowBottomRetailPage />
            <RowQuestion />
          </Container>
        </main>
      </LayoutBizzoneShop>
    </>
  );
}

export default ServicePage;
