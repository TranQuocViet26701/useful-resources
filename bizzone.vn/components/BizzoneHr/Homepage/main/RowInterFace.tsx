import * as React from 'react';
import { Image } from 'react-bootstrap';
// import '/images/landingPage/bizzone-enterprise/slide-interface-1.png' from '/images/landingPage/bizzone-enterprise/slide-interface-1.png';
// import '/images/landingPage/bizzone-enterprise/slide-interface-2.png' from '/images/landingPage/bizzone-enterprise/slide-interface-2.png';
// import '/images/landingPage/bizzone-enterprise/slide-interface-3.png' from '/images/landingPage/bizzone-enterprise/slide-interface-3.png';
// import '/images/landingPage/bizzone-enterprise/slide-interface-4.png' from '/images/landingPage/bizzone-enterprise/slide-interface-4.png';
// import '/images/landingPage/bizzone-enterprise/slide-interface-5.png' from '/images/landingPage/bizzone-enterprise/slide-interface-5.png';
// import '/images/landingPage/bizzone-enterprise/slide-interface-6.png' from '/images/landingPage/bizzone-enterprise/slide-interface-6.png';

import SwiperCore, { Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
// import { useTranslation } from 'react-i18next';
import t from '../../vi-VN.json';

import styles from '../../styles/BizzoneHr.module.scss';

SwiperCore.use([Navigation]);

const listSlide: any[] = [
  {
    id: 1,
    img: '/images/landingPage/bizzone-enterprise/slide-interface-1.png',
  },
  {
    id: 2,
    img: '/images/landingPage/bizzone-enterprise/slide-interface-2.png',
  },
  {
    id: 3,
    img: '/images/landingPage/bizzone-enterprise/slide-interface-3.png',
  },
  {
    id: 4,
    img: '/images/landingPage/bizzone-enterprise/slide-interface-4.png',
  },
  {
    id: 5,
    img: '/images/landingPage/bizzone-enterprise/slide-interface-5.png',
  },
  {
    id: 6,
    img: '/images/landingPage/bizzone-enterprise/slide-interface-6.png',
  },
];

function RowInterFace() {
  // const { t } = useTranslation();

  return (
    <section className={styles['bizzone-interface']}>
      <div className={`${styles['bizzone-interface-wrap']} container-child`}>
        <h3 className={`${styles['bizzone-process-detail']} text-center`}>
          {t['bizzoneLandingPage.rowInterFace.title']}
        </h3>
        <div className={styles['divider-gradient']} />
        <div className={`${styles['bizzone-interface-slide']} ${styles['template-grid-3']}`}>
          <div className={`prev ${styles['prev-slide']}`}>
            <svg
              width="24"
              height="25"
              viewBox="0 0 24 25"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M14.4297 5.97192L20.4997 12.0419L14.4297 18.1119"
                stroke="white"
                strokeWidth="1.5"
                strokeMiterlimit="10"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M3.49976 12.0419H20.3298"
                stroke="white"
                strokeWidth="1.5"
                strokeMiterlimit="10"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </div>
          <Swiper
            navigation={{
              prevEl: '.prev',
              nextEl: '.next',
            }}
            className={styles['swiper-interface-img-wrapper']}
            slidesPerView={1}
            spaceBetween={30}
          >
            {listSlide.map((slide) => (
              <SwiperSlide key={slide.id}>
                <Image src={slide.img} className={styles['swiper-interface-img']} alt="" />
              </SwiperSlide>
            ))}
          </Swiper>
          <div className={`next ${styles['next-slide']}`}>
            <svg
              width="24"
              height="25"
              viewBox="0 0 24 25"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M14.4297 5.97192L20.4997 12.0419L14.4297 18.1119"
                stroke="white"
                strokeWidth="1.5"
                strokeMiterlimit="10"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M3.49976 12.0419H20.3298"
                stroke="white"
                strokeWidth="1.5"
                strokeMiterlimit="10"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
          </div>
        </div>
      </div>
    </section>
  );
}

export default RowInterFace;
