import Link from 'next/link';
import React from 'react';
// import imgRight from '/images/landingPage/bizzone-enterprise/img-bizzone.png';
import { Accordion, Image } from 'react-bootstrap';
import t from '../../vi-VN.json';

import styles from '../../styles/BizzoneHr.module.scss';
// import { useQuery } from '@apollo/client/react/hooks/useQuery';

// import imgCloudDownload from '/images/svg/icon-cloud-download.svg';
// import { Link } from 'react-router-dom';

// import { useTranslation } from 'react-i18next';
// import { gql } from 'graphql-tag';

// const queryData = gql`
//   query NewQuery($title: String = "Link Download Bizzone Enterprise") {
//     pages(where: { title: $title }) {
//       nodes {
//         linkdownload {
//           link {
//             mediaItemUrl
//           }
//         }
//         title
//       }
//     }
//   }
// `;

interface SupportModel {
  id: number;
  title: string;
  desc: string[];
}

function RowSupportCustomer() {
  // const { data } = useQuery(queryData, {
  //   variables: { title: 'Link Download Bizzone Enterprise' },
  // });

  // const { t } = useTranslation();

  const linkDownload = '/unicloud-digital-banking-platformV1.4.pdf';

  const listSupport: SupportModel[] = [
    {
      id: 0,
      title: t['bizzoneLandingPage.rowSupportCustomer.title1'],
      desc: [
        t['bizzoneLandingPage.rowSupportCustomer.title1.desc1'],
        t['bizzoneLandingPage.rowSupportCustomer.title1.desc2'],
        t['bizzoneLandingPage.rowSupportCustomer.title1.desc3'],
        t['bizzoneLandingPage.rowSupportCustomer.title1.desc4'],
      ],
    },
    {
      id: 1,
      title: t['bizzoneLandingPage.rowSupportCustomer.title2'],
      desc: [
        t['bizzoneLandingPage.rowSupportCustomer.title2.desc1'],
        t['bizzoneLandingPage.rowSupportCustomer.title2.desc2'],
        t['bizzoneLandingPage.rowSupportCustomer.title2.desc3'],
        t['bizzoneLandingPage.rowSupportCustomer.title2.desc4'],
      ],
    },
    {
      id: 2,
      title: t['bizzoneLandingPage.rowSupportCustomer.title3'],
      desc: [t['bizzoneLandingPage.rowSupportCustomer.title3.desc1']],
    },
    {
      id: 3,
      title: t['bizzoneLandingPage.rowSupportCustomer.title4'],
      desc: [t['bizzoneLandingPage.rowSupportCustomer.title4.desc1']],
    },
  ];

  const handleClickDownload = () => {
    window.open(linkDownload);
  };
  return (
    <section
      className={`${styles['bizzone-support']} ${styles['scroll-margin-top-section']}`}
      id="support"
    >
      <div className={`${styles['bizzone-support-wrap']} container-child`}>
        <div className={styles['template-grid-2']}>
          <div className={styles['bizzone-support-left']}>
            <div>
              <h3 className={`${styles['bizzone-process-detail']} text-start`}>
                {t['bizzoneLandingPage.rowSupportCustomer.bannerTitle']}
              </h3>
              <div className={styles['divider-blue-normal']} />
            </div>
            <Accordion defaultActiveKey="0" flush>
              {listSupport.map((listItem, index) => (
                <Accordion.Item key={listItem.id} eventKey={index.toString()}>
                  <Accordion.Header>{listItem.title}</Accordion.Header>
                  <Accordion.Body>
                    <ul className={styles['accordion-list-item']}>
                      {listItem?.desc.map((value) => (
                        <li key={value} className="">
                          <span className={styles['accordion-desc']}>{value}</span>
                        </li>
                      ))}
                    </ul>

                    {index === listSupport.length - 1 && (
                      <div className={styles['accordion-btn-container']}>
                        <Link href="/unicloud-digital-banking-platformV1.4.pdf">
                          <a target="_blank" download onClick={handleClickDownload}>
                            <button
                              className={styles['accordion-desc-btn']}
                              type="submit"
                              onClick={handleClickDownload}
                            >
                              {t['bizzoneLandingPage.rowSupportCustomer.downloadBtn']}
                              <Image
                                src="/images/svg/icon-cloud-download.svg"
                                className={styles['accordion-desc-icon']}
                                alt=""
                              />
                            </button>
                          </a>
                        </Link>
                      </div>
                    )}
                  </Accordion.Body>
                </Accordion.Item>
              ))}
            </Accordion>
          </div>
          <div className={styles['bizzone-support-right']}>
            <Image src="/images/landingPage/bizzone-enterprise/img-bizzone.png" alt="bizzone" />
          </div>
        </div>
      </div>
    </section>
  );
}

export default RowSupportCustomer;
