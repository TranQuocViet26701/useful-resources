import React from 'react';
import { Image } from 'react-bootstrap';
// import '/images/landingPage/bizzone-enterprise/icon/icon-search.svg' from '/images/landingPage/bizzone-enterprise/icon/icon-search.svg';
// import '/images/landingPage/bizzone-enterprise/icon/icon-security.svg' from '/images/landingPage/bizzone-enterprise/icon/icon-security.svg';
// import '/images/landingPage/bizzone-enterprise/icon/icon-group-user.svg' from '/images/landingPage/bizzone-enterprise/icon/icon-group-user.svg';
// import '/images/landingPage/bizzone-enterprise/icon/icon-cost.svg' from '/images/landingPage/bizzone-enterprise/icon/icon-cost.svg';
// import '/images/landingPage/bizzone-enterprise/icon/icon-exactly.svg' from '/images/landingPage/bizzone-enterprise/icon/icon-exactly.svg';
// import '/images/landingPage/bizzone-enterprise/icon/icon-admin.svg' from '/images/landingPage/bizzone-enterprise/icon/icon-admin.svg';

// import { useTranslation } from 'react-i18next';

import t from '../../vi-VN.json';

import styles from '../../styles/BizzoneHr.module.scss';

interface BenefitModel {
  id: number;
  title: string;
  icon: string;
  desc: string;
}

const listBenefit: BenefitModel[] = [
  {
    id: 0,
    title: t['bizzoneLandingPage.rowBenefit.title1'],
    icon: '/images/landingPage/bizzone-enterprise/icon/icon-search.svg',
    desc: t['bizzoneLandingPage.rowBenefit.desc1'],
  },
  {
    id: 1,
    title: t['bizzoneLandingPage.rowBenefit.title2'],
    icon: '/images/landingPage/bizzone-enterprise/icon/icon-security.svg',
    desc: t['bizzoneLandingPage.rowBenefit.desc2'],
  },
  {
    id: 2,
    title: t['bizzoneLandingPage.rowBenefit.title3'],
    icon: '/images/landingPage/bizzone-enterprise/icon/icon-group-user.svg',
    desc: t['bizzoneLandingPage.rowBenefit.desc3'],
  },
  {
    id: 3,
    title: t['bizzoneLandingPage.rowBenefit.title4'],
    icon: '/images/landingPage/bizzone-enterprise/icon/icon-cost.svg',
    desc: t['bizzoneLandingPage.rowBenefit.desc4'],
  },
  {
    id: 4,
    title: t['bizzoneLandingPage.rowBenefit.title5'],
    icon: '/images/landingPage/bizzone-enterprise/icon/icon-exactly.svg',
    desc: t['bizzoneLandingPage.rowBenefit.desc5'],
  },
  {
    id: 5,
    title: t['bizzoneLandingPage.rowBenefit.title6'],
    icon: '/images/landingPage/bizzone-enterprise/icon/icon-admin.svg',
    desc: t['bizzoneLandingPage.rowBenefit.desc6'],
  },
];

function RowBenefit() {
  // const { t } = useTranslation();

  return (
    <section
      className={`${styles['bizzone-benefit']} container-child ${styles['scroll-margin-top-section']}`}
      id="benefit"
    >
      <h2 className={styles['bizzone']}>{t['bizzoneLandingPage.rowBenefit.bannerTitle']}</h2>
      <div className={styles['divider-blue']} />

      <div className={styles['benefit-cards']}>
        {listBenefit.map((benefit) => (
          <div key={benefit.id} className={styles['benefit-cards-item']}>
            <Image
              className={styles['benefit-card']}
              src={benefit.icon}
              alt="icon benefit"
              width={100}
            />
            <h4 className={styles['benefit-card']}>{benefit.title}</h4>
            <p className={styles['benefit-card']}>{benefit.desc}</p>
          </div>
        ))}
      </div>
    </section>
  );
}

export default RowBenefit;
