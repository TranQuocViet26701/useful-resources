import React from 'react';
import imgLaptop1 from '/images/landingPage/bizzone-enterprise/img-laptop-first.png';
import imgLeft from '/images/landingPage/bizzone-enterprise/bg-left.png';

import imgLaptop2 from '/images/landingPage/bizzone-enterprise/img-laptop-second.png';
import imgRight from '/images/landingPage/bizzone-enterprise/bg-right.png';

import imgImac1 from '/images/landingPage/bizzone-enterprise/img-imac-first.png';
import imgImacLeft from '/images/landingPage/bizzone-enterprise/bg-imac.png';
// import { useTranslation } from 'react-i18next';
// import ModalAdvise from '@components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';

import imgLaptop3 from '/images/landingPage/bizzone-enterprise/img-macbook-pro.png';
import imgLaptop3Right from '/images/landingPage/bizzone-enterprise/bg-macbook-pro.png';
import { Image } from 'react-bootstrap';
import t from '../../vi-VN.json';
import ModalAdviseDigitalBanking from '../../../Common/Modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import styles from '../../styles/BizzoneHr.module.scss';

function RowFeatureDetail() {
  const [modalShow, setModalShow] = React.useState(false);
  // const { t } = useTranslation();

  return (
    <section className={styles['bizzone-feature-detail']}>
      <div className={`${styles['feature-detail-wrap']} ${styles['template-grid-2-auto']}`}>
        <div className={styles['feature-left']}>
          <Image
            className={styles['stick-bg']}
            src="/images/landingPage/bizzone-enterprise/bg-left.png"
            alt=""
          />
          <Image
            className={styles['stick-img']}
            src="/images/landingPage/bizzone-enterprise/img-laptop-first.png"
            alt="laptop"
          />
        </div>
        <div className={styles['feature-right']}>
          <h2 className={styles['service-detail']}>
            {t['bizzoneLandingPage.rowFeatureDetail.title1']}
          </h2>
          <span className={styles['detail-item']}>
            {t['bizzoneLandingPage.rowFeatureDetail.subTitle1']}
          </span>
          <button
            className={`${styles['btn-advise']} ${styles['margin-top-54']}`}
            type="button"
            onClick={() => setModalShow(true)}
          >
            <span>{t['bizzoneLandingPage.rowFeatureDetail.contactBtn']}</span>
          </button>
        </div>
      </div>
      <div
        className={`${styles['template-grid-2-auto']} ${styles['feature-detail-wrap']} ${styles['feature-detail-wrap-revert']}`}
      >
        <div className={styles['feature-left']}>
          <Image
            className={styles['stick-bg']}
            src="/images/landingPage/bizzone-enterprise/bg-right.png"
            alt=""
          />
          <Image
            className={styles['stick-img']}
            src="/images/landingPage/bizzone-enterprise/img-laptop-second.png"
            alt="laptop"
          />
        </div>
        <div className={styles['feature-right']}>
          <h2 className={styles['service-detail']}>
            {t['bizzoneLandingPage.rowFeatureDetail.title2']}
          </h2>
          <span className={styles['detail-item']}>
            {t['bizzoneLandingPage.rowFeatureDetail.subTitle2']}
          </span>
          <button
            className={`${styles['btn-advise']} ${styles['margin-top-54']}`}
            type="button"
            onClick={() => setModalShow(true)}
          >
            <span>{t['bizzoneLandingPage.rowFeatureDetail.contactBtn']}</span>
          </button>
        </div>
      </div>

      <div className={`${styles['feature-detail-wrap']} ${styles['template-grid-2-auto']}`}>
        <div className={styles['imac-feature-left']}>
          <Image
            className={styles['stick-bg']}
            src="/images/landingPage/bizzone-enterprise/bg-imac.png"
            alt=""
          />
          <Image
            className={styles['stick-img']}
            src="/images/landingPage/bizzone-enterprise/img-imac-first.png"
            alt="imac"
          />
        </div>
        <div className={styles['feature-right']}>
          <h2 className={styles['service-detail']}>
            {t['bizzoneLandingPage.rowFeatureDetail.title3']}
          </h2>
          <span className={styles['detail-item']}>
            {t['bizzoneLandingPage.rowFeatureDetail.subTitle3']}
          </span>
          <button
            className={`${styles['btn-advise']} ${styles['margin-top-54']}`}
            type="button"
            onClick={() => setModalShow(true)}
          >
            <span>{t['bizzoneLandingPage.rowFeatureDetail.contactBtn']}</span>
          </button>
        </div>
      </div>

      <div
        className={`${styles['template-grid-2-auto']} ${styles['feature-detail-wrap']} ${styles['feature-detail-wrap-revert']}`}
      >
        <div className={styles['macbook-feature-left']}>
          <Image
            className={styles['stick-bg']}
            src="/images/landingPage/bizzone-enterprise/bg-macbook-pro.png"
            alt=""
          />
          <Image
            className={styles['stick-img']}
            src="/images/landingPage/bizzone-enterprise/img-macbook-pro.png"
            alt="laptop3"
          />
        </div>
        <div className={styles['feature-right']}>
          <h2 className={styles['service-detail']}>
            {t['bizzoneLandingPage.rowFeatureDetail.title4']}
          </h2>
          <span className={styles['detail-item']}>
            {t['bizzoneLandingPage.rowFeatureDetail.subTitle4']}
          </span>
          <button
            className={`${styles['btn-advise']} ${styles['margin-top-54']}`}
            type="button"
            onClick={() => setModalShow(true)}
          >
            <span>{t['bizzoneLandingPage.rowFeatureDetail.contactBtn']}</span>
          </button>
        </div>
      </div>
      <ModalAdviseDigitalBanking show={modalShow} onHide={() => setModalShow(false)} />
    </section>
  );
}

export default RowFeatureDetail;
