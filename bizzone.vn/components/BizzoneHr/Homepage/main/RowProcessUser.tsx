import React from 'react';
import imgRight from '/images/landingPage/bizzone-enterprise/img-horizontal.png';

// import '/images/svg/icon-survey.svg' from '/images/svg/icon-survey.svg';
// import '/images/svg/icon-call-center.svg' from '/images/svg/icon-call-center.svg';
// import '/images/svg/icon-agreement.svg' from '/images/svg/icon-agreement.svg';
// import '/images/svg/icon-research.svg' from '/images/svg/icon-research.svg';
// import '/images/svg/icon-approve.svg' from '/images/svg/icon-approve.svg';

// import { useTranslation } from 'react-i18next';
import { Image } from 'react-bootstrap';

import t from '../../vi-VN.json';

import styles from '../../styles/BizzoneHr.module.scss';

const listProcess: ProcessModel[] = [
  {
    id: 1,
    title: t['bizzoneLandingPage.rowProcessUser.title1'],
    desc: [
      t['bizzoneLandingPage.rowProcessUser.title1.desc1'],
      t['bizzoneLandingPage.rowProcessUser.title1.desc2'],
      t['bizzoneLandingPage.rowProcessUser.title1.desc3'],
      t['bizzoneLandingPage.rowProcessUser.title1.desc4'],
    ],
    icon: '/images/svg/icon-survey.svg',
    alt: 'icon survey',
  },
  {
    id: 2,
    title: t['bizzoneLandingPage.rowProcessUser.title2'],
    desc: [
      t['bizzoneLandingPage.rowProcessUser.title2.desc1'],
      t['bizzoneLandingPage.rowProcessUser.title2.desc2'],
      t['bizzoneLandingPage.rowProcessUser.title2.desc3'],
    ],
    icon: '/images/svg/icon-call-center.svg',
    alt: 'icon call center',
  },
  {
    id: 3,
    title: t['bizzoneLandingPage.rowProcessUser.title3'],
    desc: [
      t['bizzoneLandingPage.rowProcessUser.title3.desc1'],
      t['bizzoneLandingPage.rowProcessUser.title3.desc2'],
      t['bizzoneLandingPage.rowProcessUser.title3.desc3'],
    ],
    icon: '/images/svg/icon-agreement.svg',
    alt: 'icon agreement',
  },
  {
    id: 4,
    title: t['bizzoneLandingPage.rowProcessUser.title4'],
    desc: [
      t['bizzoneLandingPage.rowProcessUser.title4.desc1'],
      t['bizzoneLandingPage.rowProcessUser.title4.desc2'],
      t['bizzoneLandingPage.rowProcessUser.title4.desc3'],
      t['bizzoneLandingPage.rowProcessUser.title4.desc4'],
    ],
    icon: '/images/svg/icon-research.svg',
    alt: 'icon research',
  },
  {
    id: 5,
    title: t['bizzoneLandingPage.rowProcessUser.title5'],
    desc: [
      t['bizzoneLandingPage.rowProcessUser.title5.desc1'],
      t['bizzoneLandingPage.rowProcessUser.title5.desc2'],
      t['bizzoneLandingPage.rowProcessUser.title5.desc3'],
    ],
    icon: '/images/svg/icon-approve.svg',
    alt: 'icon approve',
  },
];

interface ProcessModel {
  id: number;
  title: string;
  desc: string[];
  icon: string;
  alt: string;
}

function RowProcessUser() {
  // const { t } = useTranslation();

  return (
    <section className={styles['bizzone-process']}>
      <div className={`${styles['bizzone-process-wrap']} container-child`}>
        <h3 className={`${styles['bizzone-process-detail']} ${styles['padding-20']} text-center`}>
          {t['bizzoneLandingPage.rowProcessUser.bannerTitle']}
        </h3>
        <div className={styles['divider-gradient']} />
        <div className={`${styles['process-wrap-detail']} ${styles['template-grid-2']}`}>
          <div className={styles['process-left']}>
            {listProcess.map((process) => (
              <div
                className={`${styles['process-item']} ${styles['template-grid-4']} ${
                  process.id % 2 === 0 ? `${styles['margin-left-auto']}` : ''
                }`}
                key={process.id}
              >
                <h3 className={styles['bizzone-process-detail']}>0{process.id}</h3>
                <h4 className={`${styles['bizzone-process-detail']} d-flex align-items-center`}>
                  {process.title}
                </h4>
                <Image src={process.icon} width={64} alt={process.alt} />
                <ul className={styles['bizzone-process-detail']}>
                  {process.desc.map((item) => (
                    <li key={item} className={styles['bizzone-process-detail']}>
                      {item}
                    </li>
                  ))}
                </ul>
              </div>
            ))}
          </div>
          <div className={styles['process-right']}>
            <Image
              src="/images/landingPage/bizzone-enterprise/img-horizontal.png"
              alt="Process user"
            />
          </div>
        </div>
      </div>
    </section>
  );
}

export default RowProcessUser;
