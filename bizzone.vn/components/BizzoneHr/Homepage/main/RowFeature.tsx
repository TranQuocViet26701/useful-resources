import React from 'react';
import { Image } from 'react-bootstrap';
import ModalAdviseDigitalBanking from '../../../Common/Modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import RowBanking from './RowBanking';
// import { useTranslation } from 'react-i18next';
// import ModalAdvise from '@components/modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import RowFeatureDetail from './RowFeatureDetail';

// import imgRight from '/images/landingPage/bizzone-enterprise/img-illutration.png';
import t from '../../vi-VN.json';
import styles from '../../styles/BizzoneHr.module.scss';

interface ServiceItemModel {
  id: number;
  desc: string;
}

const listService: ServiceItemModel[] = [
  {
    id: 0,
    desc: t['bizzoneLandingPage.rowFeature.listItem1'],
  },
  {
    id: 1,
    desc: t['bizzoneLandingPage.rowFeature.listItem2'],
  },
  {
    id: 2,
    desc: t['bizzoneLandingPage.rowFeature.listItem3'],
  },
  {
    id: 3,
    desc: t['bizzoneLandingPage.rowFeature.listItem4'],
  },
  {
    id: 4,
    desc: t['bizzoneLandingPage.rowFeature.listItem5'],
  },
  {
    id: 5,
    desc: t['bizzoneLandingPage.rowFeature.listItem6'],
  },
];

function RowFeature() {
  const [modalShow, setModalShow] = React.useState(false);

  // const { t } = useTranslation();

  return (
    <section
      className={`${styles['bizzone-feature']} ${styles['scroll-margin-top-section']}`}
      id="feature"
    >
      <div className={styles['bizzone-feature-detail']}>
        <h1 className={styles['feature-detail']}> {t['bizzoneLandingPage.rowFeature.title']}</h1>
      </div>
      <div className={`${styles['bizzone-feature-service']} container-child`}>
        <div className={`${styles['feature-service-wrap']} ${styles['template-grid-2']}`}>
          <div className={styles['service-wrap_left']}>
            <h2 className={styles['service-detail']}>
              {t['bizzoneLandingPage.rowFeature.subTitle']}
            </h2>
            <ul className={styles['service-detail-list']}>
              {listService.map((service) => (
                <li key={service.id} className={styles['detail-list-item']}>
                  <span className={styles['detail-item']}>{service.desc}</span>
                </li>
              ))}
            </ul>
            <button
              className={`${styles['btn-advise']} ${styles['margin-top-54']}`}
              type="button"
              onClick={() => setModalShow(true)}
            >
              <span>{t['bizzoneLandingPage.rowFeature.contactBtn']}</span>
            </button>
          </div>
          <div className={styles['service-wrap_right']}>
            <div className={styles['wrap_right__image']}>
              <Image
                src="/images/landingPage/bizzone-enterprise/img-illutration.png"
                alt="feature"
                loading="eager"
              />
            </div>
          </div>
        </div>
      </div>
      <RowBanking />
      <RowFeatureDetail />
      <ModalAdviseDigitalBanking show={modalShow} onHide={() => setModalShow(false)} />
    </section>
  );
}

export default RowFeature;
