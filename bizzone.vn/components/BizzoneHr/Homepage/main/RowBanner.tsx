import React from 'react';

// import { useTranslation } from 'react-i18next';
import t from '../../vi-VN.json';
import styles from '../../styles/BizzoneHr.module.scss';

interface SolutionModel {
  id: number;
  title: string;
  desc: string;
}

const listSolution: SolutionModel[] = [
  {
    id: 0,
    title: '99%',
    desc: t['bizzoneLandingPage.rowBanner.desc1'],
  },
  {
    id: 1,
    title: '500%',
    desc: t['bizzoneLandingPage.rowBanner.desc2'],
  },
  {
    id: 2,
    title: '200+',
    desc: t['bizzoneLandingPage.rowBanner.desc3'],
  },
  {
    id: 3,
    title: '300+',
    desc: t['bizzoneLandingPage.rowBanner.desc4'],
  },
];

function RowBanner() {
  // const { t } = useTranslation();

  return (
    <article className={styles['bizzone-banner']} id="banner">
      <div className={`${styles['bizzone-banner-wrap']} container-child`}>
        <h2 className={styles['bizzone-banner-detail']}>
          {t['bizzoneLandingPage.rowBanner.title']}
        </h2>
        <div className={styles['divider-white']} />
        <div className={styles['bizzone-group-solution']}>
          {listSolution.map((solution) => (
            <div className={styles['group-solution-item']} key={solution.id}>
              <h1 className={styles['bizzone-detail']}>{solution.title}</h1>
              <p className={styles['bizzone-detail']}>{solution.desc}</p>
            </div>
          ))}
        </div>
      </div>
    </article>
  );
}

export default RowBanner;
