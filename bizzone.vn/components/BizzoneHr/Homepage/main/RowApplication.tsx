import React, { useMemo } from 'react';

import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay } from 'swiper';

// import { useTranslation } from 'react-i18next';
import { Image } from 'react-bootstrap';
import styles from '../../styles/BizzoneHr.module.scss';

import t from '../../vi-VN.json';

const listSlide: any[] = [
  {
    id: 1,
    img: '/images/landingPage/logo/logo-klb-color.svg',
  },
  {
    id: 2,
    img: '/images/landingPage/logo/logo-ode-color.svg',
  },
  {
    id: 3,
    img: '/images/landingPage/logo/logo-ksf-color.svg',
  },
  {
    id: 4,
    img: '/images/landingPage/logo/logo-sunshine-home-color.svg',
  },
  {
    id: 5,
    img: '/images/landingPage/logo/logo-smart-color.svg',
  },
];

function RowApplication() {
  // const { t } = useTranslation();

  const BuildSlide = useMemo(
    () => (
      <Swiper
        modules={[Autoplay]}
        spaceBetween={25}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        breakpoints={{
          475: {
            slidesPerView: 1,
            spaceBetween: 10,
          },
          640: {
            slidesPerView: 3,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 4,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 5,
            spaceBetween: 50,
          },
        }}
      >
        {listSlide.map((slide) => (
          <SwiperSlide key={slide.id}>
            <Image src={slide.img} className={styles['swiper-partnership-application']} alt="" />
          </SwiperSlide>
        ))}
      </Swiper>
    ),
    [],
  );

  return (
    <section className={styles['bizzone-application']}>
      <div className={`${styles['bizzone-application-wrap']} container-child`}>
        <h3 className={`${styles['bizzone-process-detail']} text-center`}>
          {t['bizzoneLandingPage.rowApplication.title']}
        </h3>
        <div className={styles['divider-gradient']} />
      </div>
      <div className={styles['bizzone-apllication-slide']}>
        <div className="container-child">{BuildSlide}</div>
      </div>
    </section>
  );
}

export default RowApplication;
