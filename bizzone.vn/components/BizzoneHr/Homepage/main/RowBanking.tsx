import React from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
// import { useTranslation } from 'react-i18next';
import { Autoplay } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import ModalAdviseDigitalBanking from '../../../Common/Modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
// import imgSlide1 from '/images/landingPage/bizzone-enterprise/img-slide-row-banking-1.png';
// import imgSlide2 from '/images/landingPage/bizzone-enterprise/img-slide-row-banking-2.png';
// import imgSlide3 from '/images/landingPage/bizzone-enterprise/img-slide-row-banking-3.png';
// import imgSlide4 from '/images/landingPage/bizzone-enterprise/img-slide-row-banking-4.png';
// import imgSlide5 from '/images/landingPage/bizzone-enterprise/img-slide-row-banking-5.png';
// import imgSlide6 from '/images/landingPage/bizzone-enterprise/img-slide-row-banking-6.png';
import t from '../../vi-VN.json';

import styles from '../../styles/BizzoneHr.module.scss';

function RowBanking() {
  const [modalShow, setModalShow] = React.useState(false);
  // const { t } = useTranslation();
  return (
    <div className={styles['row-banking-back-ground']}>
      <Container className="max-width-1180 px-0">
        <Row className="gx-5 padding-left-right">
          <Col xs={12} md={5} className={styles['row-banking-col-left']}>
            <h2 className={styles['row-banking-title-sub']}>Tổng quan các phân hệ chính</h2>

            <p className={styles['row-banking-detail-2']}>
              {t['bizzoneLandingPage.rowBanking.detail']}
            </p>
            <div>
              <button
                type="button"
                className={styles['btn-row-banking']}
                onClick={() => setModalShow(true)}
              >
                {t['bizzoneLandingPage.rowBanking.advise']}
              </button>
            </div>
            <div className={styles['wrapper-mouse-scroll']}>
              <div className="text-center mt-5 mb-3 d-flex justify-content-center">
                <Image
                  src="/images/svg/icon-mouse-scroll.svg"
                  className={`${styles['img-mouse-scroll']} img-fluid`}
                  alt=""
                />
              </div>
              <div className="text-center d-flex justify-content-center">
                <Image src="/images/svg/arrow-left-slide-to-discover.svg" alt="" />
                <span className={styles['text-discover']}>
                  {t['bizzoneLandingPage.rowBanking.textDiscover']}
                </span>
                <Image src="/images/svg/arrow-right-slide-to-discover.svg" alt="" />
              </div>
            </div>
          </Col>
          <Col xs={12} md={7} className={styles['col-slide-row-banking']}>
            <Swiper
              modules={[Autoplay]}
              spaceBetween={20}
              slidesPerView={2.5}
              breakpoints={{
                320: {
                  slidesPerView: 1.3,
                },
                600: {
                  slidesPerView: 1.5,
                },
                768: {
                  slidesPerView: 1.3,
                },
                900: {
                  slidesPerView: 1.6,
                },
                1100: {
                  slidesPerView: 2,
                },
                1300: {
                  slidesPerView: 2.5,
                },
              }}
              className={styles['swiper-row-banking']}
            >
              <SwiperSlide>
                <div className={styles['wrapper-slide-row-banking']}>
                  <h4 className={styles['title-slide-row-banking']}>
                    {t['bizzoneLandingPage.rowBanking.titleSlide']}
                    <br />
                    {t['bizzoneLandingPage.rowBanking.subTitleSlide1']}
                  </h4>
                  <div className="text-center">
                    <Image
                      src="/images/landingPage/bizzone-enterprise/img-slide-row-banking-1.png"
                      className={`${styles['img-slide-row-banking']} img-fluid`}
                      alt=""
                    />
                  </div>
                  <p className={styles['detail-slide-row-banking']}>
                    {t['bizzoneLandingPage.rowBanking.detailSlide1']}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className={styles['wrapper-slide-row-banking']}>
                  <h4 className={styles['title-slide-row-banking']}>
                    {t['bizzoneLandingPage.rowBanking.titleSlide']}
                    <br />
                    {t['bizzoneLandingPage.rowBanking.subTitleSlide2']}
                  </h4>
                  <div className="text-center">
                    <Image
                      src="/images/landingPage/bizzone-enterprise/img-slide-row-banking-2.png"
                      className={`${styles['img-slide-row-banking']} img-fluid`}
                      alt=""
                    />
                  </div>

                  <p className={styles['detail-slide-row-banking']}>
                    {t['bizzoneLandingPage.rowBanking.detailSlide2']}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className={styles['wrapper-slide-row-banking']}>
                  <h2 className={styles['title-slide-row-banking']}>
                    {t['bizzoneLandingPage.rowBanking.titleSlide']}
                    <br /> {t['bizzoneLandingPage.rowBanking.subTitleSlide3']}
                  </h2>
                  <div className="text-center">
                    <Image
                      src="/images/landingPage/bizzone-enterprise/img-slide-row-banking-3.png"
                      className={`${styles['img-slide-row-banking']} img-fluid`}
                      alt=""
                    />
                  </div>
                  <p className={styles['detail-slide-row-banking']}>
                    {t['bizzoneLandingPage.rowBanking.detailSlide3']}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className={styles['wrapper-slide-row-banking']}>
                  <h2 className={styles['title-slide-row-banking']}>
                    {t['bizzoneLandingPage.rowBanking.titleSlide']}
                    <br />
                    {t['bizzoneLandingPage.rowBanking.subTitleSlide4']}
                  </h2>
                  <div className="text-center">
                    <Image
                      src="/images/landingPage/bizzone-enterprise/img-slide-row-banking-4.png"
                      className={`${styles['img-slide-row-banking']} img-fluid`}
                      alt=""
                    />
                  </div>
                  <p className={styles['detail-slide-row-banking']}>
                    {t['bizzoneLandingPage.rowBanking.detailSlide4']}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className={styles['wrapper-slide-row-banking']}>
                  <h2 className={styles['title-slide-row-banking']}>
                    {t['bizzoneLandingPage.rowBanking.titleSlide']}
                    <br /> {t['bizzoneLandingPage.rowBanking.subTitleSlide5']}
                  </h2>
                  <div className="text-center">
                    <Image
                      src="/images/landingPage/bizzone-enterprise/img-slide-row-banking-5.png"
                      className={`${styles['img-slide-row-banking']} img-fluid`}
                      alt=""
                    />
                  </div>
                  <p className={styles['detail-slide-row-banking']}>
                    {t['bizzoneLandingPage.rowBanking.detailSlide5']}
                  </p>
                </div>
              </SwiperSlide>
              <SwiperSlide>
                <div className={styles['wrapper-slide-row-banking']}>
                  <h2 className={styles['title-slide-row-banking']}>
                    {t['bizzoneLandingPage.rowBanking.titleSlide']}
                    <br /> {t['bizzoneLandingPage.rowBanking.subTitleSlide6']}
                  </h2>
                  <div className="text-center">
                    <Image
                      src="/images/landingPage/bizzone-enterprise/img-slide-row-banking-6.png"
                      className={`${styles['img-slide-row-banking']} img-fluid`}
                      alt=""
                    />
                  </div>
                  <p className={styles['detail-slide-row-banking']}>
                    {t['bizzoneLandingPage.rowBanking.detailSlide6']}
                  </p>
                </div>
              </SwiperSlide>
            </Swiper>
          </Col>
        </Row>
        <ModalAdviseDigitalBanking show={modalShow} onHide={() => setModalShow(false)} />
      </Container>
    </div>
  );
}
export default RowBanking;
