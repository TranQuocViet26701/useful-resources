import React from 'react';
import { Image } from 'react-bootstrap';
import ModalAdviseDigitalBanking from '../../../Common/Modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import styles from '../../styles/BizzoneHr.module.scss';
import t from '../../vi-VN.json';

function RowHeader() {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <section id="header">
      <div className={`${styles['row-header-banner']} ${styles['background-image']}`}>
        <div className={styles['row-header-content']}>
          <div className={styles['row-header-left-content']}>
            <h1 className={styles['row-header-title']}>
              {t['bizzoneLandingPage.rowHeader.title']}
            </h1>
            <span className={styles['row-header-subtitle']}>
              {t['bizzoneLandingPage.rowHeader.subTitle']}
            </span>

            <button
              className={styles['btn-advise']}
              type="button"
              onClick={() => setModalShow(true)}
            >
              <span>Tư vấn cho tôi</span>
            </button>
          </div>

          <div className={styles['row-header-right-content']}>
            <Image
              src="/images/homePage/img-bizzone-laptop-landing-page.png"
              alt=""
              className={styles['row-header-laptop-img']}
            />
          </div>
        </div>
      </div>
      <ModalAdviseDigitalBanking show={modalShow} onHide={() => setModalShow(false)} />
    </section>
  );
}
export default RowHeader;
