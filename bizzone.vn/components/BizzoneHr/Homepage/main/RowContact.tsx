import axios from 'axios';
import { useRouter } from 'next/router';
import React from 'react';
import { Card, Col, Form, FormControl, Row } from 'react-bootstrap';
import { Controller, useForm } from 'react-hook-form';
// import { useTranslation } from 'react-i18next';
// import { userouter } from 'react-router-dom';
import t from '../../vi-VN.json';
import styles from '../../styles/BizzoneHr.module.scss';

function RowContact() {
  const {
    handleSubmit,
    control,
    formState: { errors },
    reset,
  } = useForm({ mode: 'onChange' });

  // const { t } = useTranslation();

  interface ContactItemModel {
    id: number;
    desc: string;
  }

  const listContact: ContactItemModel[] = [
    {
      id: 0,
      desc: t['bizzoneLandingPage.rowContact.contactItem1'],
    },
    {
      id: 1,
      desc: t['bizzoneLandingPage.rowContact.contactItem2'],
    },
    {
      id: 2,
      desc: t['bizzoneLandingPage.rowContact.contactItem3'],
    },
    {
      id: 3,
      desc: t['bizzoneLandingPage.rowContact.contactItem4'],
    },
    {
      id: 4,
      desc: t['bizzoneLandingPage.rowContact.contactItem5'],
    },
  ];

  //   interface SelectItemModel {
  //     id: number;
  //     desc: string;
  //   }

  //   const selectListItem: SelectItemModel[] = [
  //     {
  //       id: 0,
  //       desc: t('bizzoneLandingPage.rowContact.listItem1'),
  //     },
  //     {
  //       id: 1,
  //       desc: t('bizzoneLandingPage.rowContact.listItem2'),
  //     },
  //     {
  //       id: 2,
  //       desc: t('bizzoneLandingPage.rowContact.listItem3'),
  //     },
  //     {
  //       id: 3,
  //       desc: t('bizzoneLandingPage.rowContact.listItem4'),
  //     },
  //     {
  //       id: 4,
  //       desc: t('bizzoneLandingPage.rowContact.listItem5'),
  //     },
  //   ];

  const router = useRouter();

  const googleSheetAPI =
    'https://script.google.com/macros/s/AKfycbxTlQD1WgGBuFKoKVjf6tiUGERX6DHHhxJfywGZ6R4xuNEurMdCdW4fbRoZwBW4jK6M/exec';

  const onSubmit = (data: any) => {
    const formData = new FormData();
    formData.append('name', data.fullName);
    formData.append('phone', data.telephone);
    formData.append('content', `Công ty : ${data.company}`);
    formData.append('email', data.email);
    formData.append('boolean', 'true');
    formData.append('list', '6xdCd892x7gSZoG7768926aeLA');

    const googleSheetFormData = new FormData();
    googleSheetFormData.append('fullName', data.fullName);
    googleSheetFormData.append('email', data.email);
    googleSheetFormData.append('telephone', data.telephone);
    googleSheetFormData.append('company', data.company);
    googleSheetFormData.append('content', '');
    googleSheetFormData.append('timestamp', new Date().toLocaleDateString().substring(0, 10));
    googleSheetFormData.append('linkedBy', router.pathname);

    axios
      .post('/subscribe', formData)
      .then(() => {
        // console.log('response: ', response.data);
        // console.log('response.status: ', response.status);
        // console.log('response.data: ', response.data);
      })
      .catch(() => {
        // console.log('Something went wrong!', error);
        // alert('Lỗi hệ thống.');
      });

    axios
      .post(googleSheetAPI, googleSheetFormData)
      .then(() => {
        // console.log('response: ', response.data);
        // console.log('response.status: ', response.status);
        // console.log('response: ', response.data);
        //
      })
      .catch(() => {
        // console.error('Something went wrong!', error);
        // alert('Lỗi hệ thống.');
      });

    reset();
  };

  //   const [isActive, setIsActive] = React.useState(false);

  //   const [isSelected, setIsSelected] = React.useState(selectListItem[0].desc);

  return (
    <section id="contact" className={styles['scroll-margin-top-section']}>
      <div className={`${styles['row-contact-banner']} ${styles['background-image']}`}>
        <div className={styles['row-contact-content']}>
          <div className={styles['row-contact-left-content']}>
            <Card>
              <Card.Body>
                <h1 className={styles['row-contact-title']}>
                  {t['bizzoneLandingPage.rowContact.formTitle']}
                </h1>
                <Form onSubmit={handleSubmit(onSubmit)} className={styles['row-contact-form']}>
                  <Form.Group
                    className={`${styles['row-contact-position-relative']} mb-3`}
                    controlId="formBasicName"
                  >
                    <Form.Label className={styles['row-contact-label']}>
                      {t['bizzoneLandingPage.rowContact.formFullName']}
                    </Form.Label>

                    <Controller
                      control={control}
                      name="fullName"
                      defaultValue=""
                      rules={{
                        required: `${t['bizzoneLandingPage.rowContact.validRequiredFullName']}`,
                      }}
                      render={({
                        field: { onChange, value, ref },
                        fieldState: { invalid, isDirty },
                      }) => (
                        <FormControl
                          onChange={onChange}
                          value={value}
                          ref={ref}
                          isInvalid={errors.fullName}
                          isValid={isDirty && !invalid}
                          placeholder={t['bizzoneLandingPage.rowContact.placeHolderFullName']}
                          aria-label="fullName"
                          aria-describedby="fullName"
                          autoComplete="off"
                        />
                      )}
                    />

                    <Form.Control.Feedback type="invalid" tooltip>
                      {Object.keys(errors).length !== 0 && errors.fullName?.type === 'required' && (
                        <span className={styles['row-contact-form-error']}>
                          {errors.fullName?.message}
                        </span>
                      )}
                    </Form.Control.Feedback>
                  </Form.Group>

                  <Row>
                    <Form.Group
                      as={Col}
                      className={`${styles['row-contact-position-relative']} mb-3`}
                      controlId="formBasicEmail"
                    >
                      <Form.Label className={styles['row-contact-label']}>Email</Form.Label>

                      <Controller
                        control={control}
                        name="email"
                        defaultValue=""
                        rules={{
                          required: `${t['bizzoneLandingPage.rowContact.validRequiredEmail']}`,
                          pattern: {
                            value: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                            message: 'Vui lòng nhập đúng Email',
                          },
                        }}
                        render={({
                          field: { onChange, value, ref },
                          fieldState: { invalid, isDirty },
                        }) => {
                          return (
                            <FormControl
                              onChange={onChange}
                              value={value}
                              ref={ref}
                              isInvalid={errors.email}
                              isValid={isDirty && !invalid}
                              placeholder={t['bizzoneLandingPage.rowContact.placeHolderEmail']}
                              aria-label="email"
                              aria-describedby="email"
                              autoComplete="nope"
                            />
                          );
                        }}
                      />

                      <Form.Control.Feedback type="invalid" tooltip>
                        {Object.keys(errors).length !== 0 && errors.email?.type === 'pattern' && (
                          <span className={styles['row-contact-form-error']}>
                            {errors.email?.message}
                          </span>
                        )}
                        {Object.keys(errors).length !== 0 && errors.email?.type === 'required' && (
                          <span className={styles['row-contact-form-error']}>
                            {errors.email?.message}
                          </span>
                        )}
                      </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group
                      as={Col}
                      className={`${styles['row-contact-position-relative']} mb-3`}
                      controlId="formBasicPhone"
                    >
                      <Form.Label className={styles['row-contact-label']}>
                        {t['bizzoneLandingPage.rowContact.formTelephone']}
                      </Form.Label>

                      <Controller
                        control={control}
                        name="telephone"
                        defaultValue=""
                        rules={{
                          required: `${t['bizzoneLandingPage.rowContact.validRequiredTelephone']}`,
                          pattern: {
                            value: /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/,
                            message: `${t['bizzoneLandingPage.rowContact.validPatternTelephone']}`,
                          },
                        }}
                        render={({
                          field: { onChange, value, ref },
                          fieldState: { invalid, isDirty },
                        }) => (
                          <FormControl
                            onChange={onChange}
                            value={value}
                            ref={ref}
                            isInvalid={errors.telephone}
                            isValid={isDirty && !invalid}
                            placeholder={t['bizzoneLandingPage.rowContact.placeHolderTelephone']}
                            aria-label="telephone"
                            aria-describedby="telephone"
                            autoComplete="off"
                          />
                        )}
                      />
                      <Form.Control.Feedback type="invalid" tooltip>
                        {Object.keys(errors).length !== 0 &&
                          errors.telephone?.type === 'pattern' && (
                            <span className={styles['row-contact-form-error']}>
                              {errors.telephone?.message}
                            </span>
                          )}
                        {Object.keys(errors).length !== 0 &&
                          errors.telephone?.type === 'required' && (
                            <span className={styles['row-contact-form-error']}>
                              {errors.telephone?.message}
                            </span>
                          )}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </Row>

                  <Form.Group
                    className={`${styles['row-contact-position-relative']} mb-3`}
                    controlId="formBasicCompany"
                  >
                    <Form.Label className={styles['row-contact-label']}>
                      {t['bizzoneLandingPage.rowContact.formCompany']}
                    </Form.Label>

                    <Controller
                      control={control}
                      name="company"
                      defaultValue=""
                      rules={{
                        required: `${t['bizzoneLandingPage.rowContact.validRequiredCompany']}`,
                      }}
                      render={({
                        field: { onChange, value, ref },
                        fieldState: { invalid, isDirty },
                      }) => (
                        <FormControl
                          onChange={onChange}
                          value={value}
                          ref={ref}
                          isInvalid={errors.company}
                          isValid={isDirty && !invalid}
                          placeholder={t['bizzoneLandingPage.rowContact.placeHolderCompany']}
                          aria-label="company"
                          aria-describedby="company"
                          autoComplete="off"
                        />
                      )}
                    />

                    <Form.Control.Feedback type="invalid" tooltip>
                      {Object.keys(errors).length !== 0 && errors.company?.type === 'required' && (
                        <span className={styles['row-contact-form-error']}>
                          {errors.company?.message}
                        </span>
                      )}
                    </Form.Control.Feedback>
                  </Form.Group>

                  {/* <Form.Group
                    className={`${styles["row-contact-position-relative"]} mb-3`}
                    controlId="formBasicQuantity"
                  >
                    <Form.Label className={styles["row-contact-label"]}>
                      {t('bizzoneLandingPage.rowContact.formQuantity')}
                    </Form.Label>

                    <div className="contact-form-dropdown">
                      <div
                        className={
                          isActive
                            ? 'contact-form-dropdown-btn contact-form-dropdown-btn-no-radius'
                            : 'contact-form-dropdown-btn'
                        }
                        onClick={() => setIsActive(!isActive)}
                        onKeyPress={() => setIsActive(!isActive)}
                        role="button"
                        tabIndex={0}
                      >
                        {isSelected}
                      </div>
                      {isActive && (
                        <div className="contact-form-dropdown-list ">
                          {selectListItem.map((item, index) => (
                            <option
                              value={item.id}
                              key={item.id}
                              className={
                                index === 0 && isActive
                                  ? 'contact-form-dropdown-item contact-form-dropdown-item-no-border'
                                  : 'contact-form-dropdown-item'
                              }
                              onClick={() => {
                                setIsSelected(item.desc);
                                setIsActive(false);
                              }}
                              onKeyPress={() => {
                                setIsSelected(item.desc);
                                setIsActive(false);
                              }}
                              role="button"
                              tabIndex={0}
                            >
                              {item.desc}
                            </option>
                          ))}
                        </div>
                      )}
                    </div>
                  </Form.Group> */}

                  <button className={styles['btn-submit-row-contact']} type="submit">
                    {t['bizzoneLandingPage.rowContact.formButton']}
                  </button>
                </Form>
              </Card.Body>
            </Card>
          </div>

          <div className={styles['row-contact-right-content']}>
            <h2 className={styles['row-contact-detail']}>
              {t['bizzoneLandingPage.rowContact.contactTitle']}
            </h2>

            <ul className={styles['row-contact-detail-list']}>
              {listContact.map((contact) => (
                <li key={contact.id} className={styles['row-contact-list-item']}>
                  <span className={styles['row-contact-item']}>{contact.desc}</span>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
}
export default RowContact;
