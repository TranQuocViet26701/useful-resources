import Head from 'next/head';
import React from 'react';
import LayoutBizzoneHr from '../LayoutBizzoneHr';
import {
  RowApplication,
  RowBanner,
  RowBenefit,
  RowContact,
  RowFeature,
  RowHeader,
  RowInterFace,
  RowProcessUser,
  RowSupportCustomer,
} from './main';

function Homepage() {
  //   const pathname = window.location.href;
  //   const final = pathname.substring(pathname.indexOf('bizzone-shop#') + 13, pathname.length);
  //   console.log('first path', pathname);
  //   console.log('num', pathname.indexOf('bizzone-shop#'));
  //   console.log('path', final);

  //   const handleClickScroll = () => {
  //     const Element = document.getElementById(final);
  //     Element?.scrollIntoView({ behavior: 'smooth' });
  //   };
  //   useEffect(() => {
  //     setTimeout(handleClickScroll, 700);
  //   }, []);
  return (
    <>
      <Head>
        <title>Bizzone HR</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="title" content="Usee" />
        <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png" />
        <meta name="title" content="Bizzone HR - Giải pháp quản trị hiệu suất toàn diện giúp bạn" />
        <meta
          name="description"
          content="Bizzone HR - Giải pháp phần mềm nhân sự lưu trữ và truy xuất thông tin một cách nhanh chóng, là giải pháp hiệu quả nhất cho nhà quản trị nhân sự chuyên nghiệp"
        />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://bizzone.vn/" />
        <meta
          property="og:title"
          content="Bizzone HR - Giải pháp quản trị hiệu suất toàn diện giúp bạn"
        />
        <meta
          property="og:description"
          content="Bizzone HR - Giải pháp phần mềm nhân sự lưu trữ và truy xuất thông tin một cách nhanh chóng, là giải pháp hiệu quả nhất cho nhà quản trị nhân sự chuyên nghiệp"
        />
        <meta property="og:image" content="https://bizzone.vn/meta-data.png" />

        {/* Twitter */}
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://bizzone.vn/" />
        <meta
          property="twitter:title"
          content="Bizzone HR - Giải pháp quản trị hiệu suất toàn diện giúp bạn"
        />
        <meta
          property="twitter:description"
          content="Bizzone HR - Giải pháp phần mềm nhân sự lưu trữ và truy xuất thông tin một cách nhanh chóng, là giải pháp hiệu quả nhất cho nhà quản trị nhân sự chuyên nghiệp"
        />
        <meta property="twitter:image" content="https://bizzone.vn/meta-data.png" />
      </Head>
      <LayoutBizzoneHr>
        <main className="bizzone-cloud">
          <RowHeader />
          <RowBenefit />
          <RowBanner />
          <RowFeature />
          <RowProcessUser />
          <RowInterFace />
          <RowApplication />
          <RowContact />
          <RowSupportCustomer />
        </main>
      </LayoutBizzoneHr>
    </>
  );
}
export default Homepage;
