import dynamic from 'next/dynamic';
import React from 'react';
import { ClientOnly, Footer, RowTechnology } from '../../Common';

const NavbarBizzoneHr = dynamic(() => import('../NavbarBizzoneHr'), {
  ssr: false,
});

export interface LayoutBizzoneHrProps {
  children: JSX.Element | JSX.Element[];
}

export default function LayoutBizzoneHr({ children }: LayoutBizzoneHrProps) {
  return (
    <>
      <NavbarBizzoneHr />
      {children}
      <footer>
        <section>
          <ClientOnly>
            <RowTechnology />
          </ClientOnly>
          <Footer />
        </section>
      </footer>
    </>
  );
}
