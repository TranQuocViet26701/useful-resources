import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useMemo, useState } from 'react';
import { Container, Image, Nav, Navbar, Offcanvas } from 'react-bootstrap';
import { NavLink } from '../../Common';
import ModalAdviseDigitalBanking from '../../Common/Modal/modal-advice-digital-banking/ModalAdviseDigitalBanking';
import styles from './NavbarBizzoneHr.module.scss';

interface MenuModel {
  id: number;
  title: string;
  link: string;
  elementId: string;
}

const listMenu: MenuModel[] = [
  {
    id: 0,
    title: 'Trang chủ',
    link: '',
    elementId: '',
  },
  {
    id: 1,
    title: 'Lợi ích',
    link: '',
    elementId: 'benefit',
  },
  {
    id: 2,
    title: 'Tính năng',
    link: '',
    elementId: 'feature',
  },
  {
    id: 3,
    title: 'Liên hệ',
    link: '',
    elementId: 'contact',
  },
  {
    id: 4,
    title: 'Hỗ trợ',
    link: '',
    elementId: 'support',
  },
];

const NavBarBizzone = React.memo(function NavbarBizzone() {
  const [showPopup, setShowPopup] = useState(false);

  const [show, setShow] = useState(false);
  const onShow = () => setShow(true);

  const onHide = () => setShow(false);

  const hashUrl = useRouter().asPath.split('#')[1];

  const convertHash = (hash: string) => {
    const hashNew = `#${hash || ''}`;
    return hashNew;
  };

  useEffect(() => {
    const detectHash = () => {
      for (let menu = 0; menu < listMenu.length; menu += 1) {
        const eleMenu = listMenu[menu];

        const el = document.querySelector(`[id='${eleMenu.elementId}']`);

        const observer = new window.IntersectionObserver(([entry]) => {
          if (entry.isIntersecting) {
            const hashNew = `#${eleMenu.elementId}`;
            window.history.pushState({}, '', hashNew);
          }
        });
        if (el) {
          observer.observe(el);
        }
      }
    };

    detectHash();
    return () => {
      detectHash();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const navItems = useMemo(() => {
    return (
      <>
        <Nav className={`${styles['navbar-bizzone-wrap']} mx-auto`} navbarScroll key="nav-item">
          {listMenu.map((menu) => {
            return menu.link ? (
              <NavLink key={menu.id} className={styles['nav-link-bizzone']} href={menu.link}>
                {menu.title}
              </NavLink>
            ) : (
              <Link key={menu.id} href={`${menu.elementId ? `#${menu.elementId}` : ''}`}>
                <a
                  className={`${styles['nav-link-bizzone']} ${
                    convertHash(hashUrl) === convertHash(menu.elementId) ? 'active' : ''
                  } `}
                >
                  {menu.title}
                </a>
              </Link>
            );
          })}
        </Nav>
        <Nav.Link className={styles['custom-nav-link']} key="nav-actions">
          <button type="button" className={styles['btn-cost']} onClick={() => setShowPopup(true)}>
            <span>Nhận tư vấn</span>
          </button>
        </Nav.Link>
      </>
    );
  }, [hashUrl]);

  return (
    <Navbar
      as="header"
      role="banner"
      expand="lg"
      className="background-sticky box-shadow-sticky-on"
      fixed="top"
      collapseOnSelect
      id="nav-bar-id"
    >
      <Container className={`${styles['menu-desktop']} container-child`}>
        <Navbar.Brand href="/bizzone-hr">
          <Image
            src="/images/svg/logo-scroll.svg"
            height="64"
            className={`${styles['logo-menu']} d-inline-block align-top`}
            alt="Unicloud Logo"
            id="logo-menu"
          />
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="offcanvasNavbar"
          className={styles['custom-navbar-toggle']}
          onClick={onShow}
        />
        <Navbar.Offcanvas show={show ? 1 : 0} onShow={onShow} onHide={onHide} id="offcanvasNavbar">
          <Offcanvas.Header closeButton>
            <Offcanvas.Title id="offcanvasNavbarLabel">
              <Navbar.Brand href="/">
                <Image
                  src="/images/svg/logo-scroll.svg"
                  height="64"
                  className="d-inline-block align-top"
                  alt="Unicloud Logo"
                />
              </Navbar.Brand>
            </Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body style={{ flexGrow: 1 }} className={styles['navbar-bizzone']}>
            {navItems}
          </Offcanvas.Body>
        </Navbar.Offcanvas>
      </Container>
      <ModalAdviseDigitalBanking show={showPopup} onHide={() => setShowPopup(false)} />
    </Navbar>
  );
});

export default NavBarBizzone;
