import * as React from 'react';

export interface IRowHeaderProps {
  fullName: string, 
  position: string,
}

export default function RowHeader (props: IRowHeaderProps) {
  return (
    <td>
    <table
      cellPadding={0}
      cellSpacing={0}
      style={{
        padding: '0px!important',
        borderSpacing: 0,
        margin: 0,
        borderCollapse: 'collapse',
      }}
    >
      <tbody>
        <tr>
          <td></td>
        </tr>
        <tr>
          <td style={{ padding: '0px !important' }}>
            <img
              src="https://res.cloudinary.com/dilh6ijxi/image/upload/v1656924129/unicloud-email-signature/logo_vgoxsl.png"
              alt="logo-unicloud"
              width={65}
              height={42}
            />
          </td>
          <td width="30" />
          <td
            style={{
              padding: '0px !important',
              verticalAlign: 'bottom',
            }}
          >
            <table
              cellPadding={0}
              cellSpacing={0}
              style={{
                padding: '0px!important',
                margin: 0,
                borderCollapse: 'collapse',
              }}
            >
              <tbody>
                <tr>
                  <td style={{ padding: '0px !important' }}>
                    <table
                      cellPadding={0}
                      cellSpacing={0}
                      style={{
                        lineHeight: 0,
                        fontSize: 1,
                        padding: '0px!important',
                        borderSpacing: 0,
                        margin: 0,
                        borderCollapse: 'collapse',
                      }}
                    >
                      <tbody>
                        <tr>
                          <td
                            style={{
                              padding: '0px !important',
                              verticalAlign: 'bottom',
                            }}
                          >
                            <table
                              cellPadding={0}
                              cellSpacing={0}
                              style={{
                                padding: '0px!important',
                                borderSpacing: 0,
                                margin: 0,
                                borderCollapse: 'collapse',
                              }}
                            >
                              <tbody>
                                <tr>
                                  <td
                                    style={{
                                      borderCollapse: 'collapse',
                                      padding: '0px !important',
                                    }}
                                  >
                                    <span
                                      style={{
                                        fontSize: '14px',
                                        fontStyle: 'normal',
                                        lineHeight: '100%',
                                        fontWeight: 700,
                                        color: '#FF5B2B',
                                        display: 'inline',
                                        textTransform: 'uppercase',
                                      }}
                                    >
                                      {props.fullName}
                                    </span>
                                  </td>
                                </tr>
                                <tr>
                                  <td
                                    style={{
                                      borderCollapse: 'collapse',
                                      padding: '0px !important',
                                    }}
                                  >
                                    <span
                                      style={{
                                        fontSize: '12px',
                                        fontStyle: 'italic',
                                        lineHeight: '110%',
                                        fontWeight: 400,
                                        color: '#13A9DD',
                                        display: 'block',
                                        mixBlendMode: 'normal',
                                      }}
                                    >
                                      {props.position}
                                    </span>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <img
                                      src="https://res.cloudinary.com/dilh6ijxi/image/upload/v1656924126/unicloud-email-signature/Divider_h4eflh.png"
                                      alt="divider"
                                      height={4}
                                    />
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </td>
  );
}
