import * as React from 'react';

export interface IRowInforRightProps {
  language: 'vi' | 'en';
}

export default function RowInforRight(props: IRowInforRightProps) {
  return (
    <td style={{ verticalAlign: 'top' }}>
      <table
        cellPadding={0}
        cellSpacing={0}
        style={{
          padding: '0px!important',
          borderSpacing: 0,
          margin: 0,
          borderCollapse: 'collapse',
        }}
      >
        <tbody>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td style={{ padding: '0px !important' }}>
              <table
                cellPadding={0}
                cellSpacing={0}
                style={{
                  padding: '0px!important',
                  margin: 0,
                  borderCollapse: 'collapse',
                }}
              >
                <tbody>
                  <tr>
                    <td
                      style={{
                        padding: '0px !important',
                      }}
                    >
                      <table
                        cellPadding={0}
                        cellSpacing={0}
                        style={{
                          lineHeight: 0,
                          fontSize: 1,
                          padding: '0px!important',
                          borderSpacing: 0,
                          margin: 0,
                          borderCollapse: 'collapse',
                        }}
                      >
                        <tbody>
                          {/* row-headquarter */}
                          <tr>
                            <td
                              style={{
                                padding: '0px !important',
                              }}
                            >
                              <table
                                cellPadding={0}
                                cellSpacing={0}
                                style={{
                                  padding: '0px!important',
                                  margin: 0,
                                  borderCollapse: 'collapse',
                                }}
                              >
                                <tbody>
                                  <tr>
                                    <td
                                      style={{
                                        borderCollapse: 'collapse',
                                        padding: '0px !important',
                                        verticalAlign: 'top',
                                      }}
                                    >
                                      <img
                                        src="https://res.cloudinary.com/dilh6ijxi/image/upload/v1656924126/unicloud-email-signature/icon-location_lzy04t.png"
                                        alt="logo-phone"
                                        width="16"
                                        height="16"
                                      />
                                    </td>
                                    <td>
                                      <span
                                        style={{
                                          width: '5px',
                                          display: 'block',
                                        }}
                                      />
                                    </td>
                                    <td>
                                      <table
                                        cellPadding={0}
                                        cellSpacing={0}
                                        style={{
                                          padding: '0px!important',
                                          margin: 0,
                                          borderCollapse: 'collapse',
                                        }}
                                      >
                                        <tbody>
                                          <tr>
                                            <td>
                                              <span
                                                style={{
                                                  fontStyle: 'normal',
                                                  fontWeight: 700,
                                                  fontSize: '12px',
                                                  lineHeight: '100%',
                                                  color: '#000000',
                                                }}
                                              >
                                                {props.language === 'vi'
                                                  ? 'Trụ sở chính Hà Nội'
                                                  : 'Headquarters'}
                                              </span>
                                            </td>
                                          </tr>
                                          <tr
                                            style={{
                                              height: '3px',
                                            }}
                                          />
                                          <tr>
                                            <td>
                                              <a
                                                href={void(0)}
                                                style={{
                                                  fontStyle: 'normal',
                                                  fontWeight: 400,
                                                  fontSize: '12px',
                                                  lineHeight: '120%',
                                                  color: '#000000',
                                                  display: 'block',
                                                  width: 265,
                                                  height: 30,
                                                  whiteSpace: 'pre-wrap',
                                                  textDecoration: 'none',
                                                  pointerEvents: 'none',
                                                  overflow: 'auto'
                                                }}
                                              >
                                                {props.language === 'vi'
                                                  ? `Tầng 10, Toà Sunshine Center, 16 Phạm Hùng, Nam Từ Liêm, Hà Nội`
                                                  : `10th Floor, Sunshine Center Building, 16 Pham Hung, Nam Tu Liem, Ha Noi`}
                                              </a>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <tr style={{ height: '5px' }} />
                          {/* row-branch */}
                          <tr>
                            <td
                              style={{
                                padding: '0px !important',
                              }}
                            >
                              <table
                                cellPadding={0}
                                cellSpacing={0}
                                style={{
                                  padding: '0px!important',
                                  margin: 0,
                                  borderCollapse: 'collapse',
                                }}
                              >
                                <tbody>
                                  <tr>
                                    <td
                                      style={{
                                        borderCollapse: 'collapse',
                                        padding: '0px !important',
                                        verticalAlign: 'top',
                                      }}
                                    >
                                      <img
                                        src="https://res.cloudinary.com/dilh6ijxi/image/upload/v1656924126/unicloud-email-signature/icon-location_lzy04t.png"
                                        alt="logo-phone"
                                        width="16"
                                        height="16"
                                      />
                                    </td>
                                    <td>
                                      <span
                                        style={{
                                          width: '5px',
                                          display: 'block',
                                        }}
                                      />
                                    </td>
                                    <td>
                                      <table
                                        cellPadding={0}
                                        cellSpacing={0}
                                        style={{
                                          padding: '0px!important',
                                          margin: 0,
                                          borderCollapse: 'collapse',
                                        }}
                                      >
                                        <tbody>
                                          <tr>
                                            <td>
                                              <span
                                                style={{
                                                  fontStyle: 'normal',
                                                  fontWeight: 700,
                                                  fontSize: '12px',
                                                  lineHeight: '100%',
                                                  color: '#000000',
                                                }}
                                              >
                                                {props.language === 'vi'
                                                  ? 'Chi nhánh HCM - Trung tâm R&D - Nhà máy'
                                                  : 'HCM Branch - R&D Centre - Factory'}
                                              </span>
                                            </td>
                                          </tr>
                                          <tr
                                            style={{
                                              height: '3px',
                                            }}
                                          />
                                          <tr>
                                            <td>
                                              <a
                                                href={void(0)}
                                                style={{
                                                  fontStyle: 'normal',
                                                  fontWeight: 400,
                                                  fontSize: '12px',
                                                  lineHeight: '120%',
                                                  color: '#000000',
                                                  display: 'block',
                                                  // width: 350,
                                                  height: 30,
                                                  overflow: 'auto',
                                                  whiteSpace: 'pre-wrap',
                                                  textDecoration: 'none',
                                                  pointerEvents: 'none',
                                                }}
                                              >
                                                {props.language === 'vi'
                                                  ? 'Block 5, Lô I-3B-1, Đường N6, Khu Công Nghệ Cao, Thủ Đức, Hồ Chí Minh'
                                                  : 'Block 5, Lot I-3B-1, N6 Str, Hi-Tech Park, Thu Duc, Ho Chi Minh'}
                                              </a>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  );
}


