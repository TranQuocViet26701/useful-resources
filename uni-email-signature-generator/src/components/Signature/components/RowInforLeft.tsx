import * as React from 'react';

export interface IRowInforLeftProps {
  phone: string;
  mail: string;
}

export default function RowInforLeft(props: IRowInforLeftProps) {

  return (
    <td style={{ verticalAlign: 'top' }}>
      <table
        cellPadding={0}
        cellSpacing={0}
        style={{
          padding: '0px!important',
          borderSpacing: 0,
          margin: 0,
          borderCollapse: 'collapse',
        }}
      >
        <tbody>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td style={{ padding: '0px !important' }}>
              <table
                cellPadding={0}
                cellSpacing={0}
                style={{
                  padding: '0px!important',
                  margin: 0,
                  borderCollapse: 'collapse',
                }}
              >
                <tbody>
                  <tr>
                    <td
                      style={{
                        padding: '0px !important',
                      }}
                    >
                      <table
                        cellPadding={0}
                        cellSpacing={0}
                        style={{
                          lineHeight: 0,
                          fontSize: 1,
                          padding: '0px!important',
                          borderSpacing: 0,
                          margin: 0,
                          borderCollapse: 'collapse',
                        }}
                      >
                        <tbody>
                          {/* row-phone */}
                          <tr>
                            <td
                              style={{
                                padding: '0px !important',
                              }}
                            >
                              <table
                                cellPadding={0}
                                cellSpacing={0}
                                style={{
                                  padding: '0px!important',
                                  margin: 0,
                                  borderCollapse: 'collapse',
                                }}
                              >
                                <tbody>
                                  <tr>
                                    <td
                                      style={{
                                        borderCollapse: 'collapse',
                                        padding: '0px !important',
                                      }}
                                    >
                                      <img
                                        src="https://res.cloudinary.com/dilh6ijxi/image/upload/v1656924126/unicloud-email-signature/icon-phone_rdcykh.png"
                                        alt="logo-phone"
                                        width="16"
                                        height="16"
                                      />
                                    </td>
                                    <td>
                                      <span
                                        style={{
                                          width: '5px',
                                          display: 'block',
                                        }}
                                      />
                                    </td>
                                    <td>
                                      <a
                                        href={`tel:${props.phone}`}
                                        style={{
                                          fontSize: '12px',
                                          fontStyle: 'normal',
                                          lineHeight: '14px',
                                          fontWeight: 900,
                                          color: '#000000',
                                          display: 'inline',
                                          // letterSpacing: '1.5px',
                                          width: 'fit-content',
                                          textDecoration: 'unset',
                                          whiteSpace: 'nowrap',
                                        }}
                                      >
                                        {props.phone}
                                      </a>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <tr style={{ height: 10 }} />
                          {/* row-mail */}
                          <tr>
                            <td
                              style={{
                                padding: '0px !important',
                              }}
                            >
                              <table
                                cellPadding={0}
                                cellSpacing={0}
                                style={{
                                  padding: '0px!important',
                                  margin: 0,
                                  borderCollapse: 'collapse',
                                }}
                              >
                                <tbody>
                                  <tr>
                                    <td
                                      style={{
                                        borderCollapse: 'collapse',
                                        padding: '0px !important',
                                      }}
                                    >
                                      <img
                                        src="https://res.cloudinary.com/dilh6ijxi/image/upload/v1656924126/unicloud-email-signature/icon-mail_y6trmi.png"
                                        alt="logo-phone"
                                        width="16"
                                        height="16"
                                      />
                                    </td>
                                    <td>
                                      <span
                                        style={{
                                          width: '5px',
                                          display: 'block',
                                        }}
                                      />
                                    </td>
                                    <td>
                                      <a
                                        href={`mailto:${props.mail}`}
                                        style={{
                                          fontSize: '12px',
                                          fontStyle: 'normal',
                                          lineHeight: '14px',
                                          fontWeight: 700,
                                          color: '#000000',
                                          display: 'inline',
                                          whiteSpace: 'nowrap',
                                          textDecoration: 'unset',
                                          width: 'fit-content',
                                        }}
                                      >
                                        {props.mail}
                                      </a>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <tr style={{ height: 10 }} />
                          {/* row-website */}
                          <tr>
                            <td
                              style={{
                                padding: '0px !important',
                              }}
                            >
                              <table
                                cellPadding={0}
                                cellSpacing={0}
                                style={{
                                  padding: '0px!important',
                                  margin: 0,
                                  borderCollapse: 'collapse',
                                }}
                              >
                                <tbody>
                                  <tr>
                                    <td
                                      style={{
                                        borderCollapse: 'collapse',
                                        padding: '0px !important',
                                      }}
                                    >
                                      <img
                                        src="https://res.cloudinary.com/dilh6ijxi/image/upload/v1656924126/unicloud-email-signature/icon-website_jebmiy.png"
                                        alt="logo-phone"
                                        width="16"
                                        height="16"
                                      />
                                    </td>
                                    <td>
                                      <span
                                        style={{
                                          width: '5px',
                                          display: 'block',
                                        }}
                                      />
                                    </td>
                                    <td>
                                      <a
                                        href="https://unicloud.com.vn/"
                                        style={{
                                          fontSize: '12px',
                                          fontStyle: 'normal',
                                          lineHeight: '14px',
                                          fontWeight: 700,
                                          color: '#000000',
                                          display: 'inline',
                                          textDecoration: 'unset',
                                          width: 'fit-content',
                                          whiteSpace: 'nowrap',
                                        }}
                                      >
                                        www.unicloud.com.vn
                                      </a>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <tr style={{ height: 10 }} />
                          {/* row-hotline */}
                          <tr>
                            <td
                              style={{
                                padding: '0px !important',
                              }}
                            >
                              <table
                                cellPadding={0}
                                cellSpacing={0}
                                style={{
                                  padding: '0px!important',
                                  margin: 0,
                                  borderCollapse: 'collapse',
                                }}
                              >
                                <tbody>
                                  <tr>
                                    <td
                                      style={{
                                        borderCollapse: 'collapse',
                                        padding: '0px !important',
                                      }}
                                    >
                                      <img
                                        src="https://res.cloudinary.com/dilh6ijxi/image/upload/v1656924126/unicloud-email-signature/icon-phone_rdcykh.png"
                                        alt="logo-phone"
                                        width="16"
                                        height="16"
                                      />
                                    </td>
                                    <td>
                                      <span
                                        style={{
                                          width: '5px',
                                          display: 'block',
                                        }}
                                      />
                                    </td>
                                    <td>
                                      <span
                                        style={{
                                          fontSize: '12px',
                                          fontStyle: 'normal',
                                          lineHeight: '16px',
                                          fontWeight: 400,
                                          color: '#FF5B2B',
                                          display: 'inline',
                                        }}
                                      >
                                        {`Hotline: `}
                                        <a
                                          href="tel:19006054"
                                          style={{
                                            fontSize: '12px',
                                            fontWeight: 'bold',
                                            // marginLeft: '5px',
                                            width: 'fit-content',
                                            textDecoration: 'unset',
                                            color: '#FF5B2B',
                                          }}
                                        >
                                          19006054
                                        </a>
                                      </span>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  );
}
