import { createStyles, Grid, makeStyles, Theme } from '@material-ui/core';
import { PhotoSignatureProps } from '../../App';
import { RowHeader, RowInforLeft, RowInforRight } from './components';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    signature: {
      paddingLeft: '37.5px',
      paddingRight: '37.5px',
    },
  }),
);

const Signature = (props: PhotoSignatureProps) => {
  const classes = useStyles();

  return (
    <Grid container justifyContent="flex-start" className={classes.signature}>
      <table
        id="signature"
        cellPadding={0}
        cellSpacing={0}
        style={{
          lineHeight: 0,
          fontSize: 1,
          padding: '0px!important',
          borderSpacing: 0,
          margin: 0,
          borderCollapse: 'collapse',
          width: '50%'
        }}
      >
        <tbody>
          {/* padding-top */}
          <tr>
            <td style={{ height: '32.5px' }}></td>
          </tr>
          <tr>
            {/* padding-left */}
            <td style={{ width: '37.5px' }} />
            {/* main-tabel */}
            <td style={{ padding: '0px !important' }}>
              <table
                cellPadding={0}
                cellSpacing={0}
                style={{
                  padding: '0px!important',
                  borderSpacing: 0,
                  margin: 0,
                  borderCollapse: 'collapse',
                }}
              >
                <tbody>
                  {/* row-1 */}
                  <tr>
                    <RowHeader fullName={props.fullName} position={props.position} />
                  </tr>
                  {/* row-2 */}
                  <tr>
                    <td style={{ height: 15 }} />
                  </tr>
                  {/* row-3 */}
                  <tr>
                    <td style={{ padding: '0px !important' }}>
                      <table
                        cellPadding={0}
                        cellSpacing={0}
                        style={{
                          padding: '0px!important',
                          borderSpacing: 0,
                          margin: 0,
                          borderCollapse: 'collapse',
                        }}
                      >
                        <tbody>
                          <tr>
                            {/* col-1 */}
                            <RowInforLeft phone={props.phone} mail={props.mail} />
                            {/* col-2 */}
                            <td width="10">
                              <div style={{ width: '10px', height: 'auto' }}></div>
                            </td>
                            {/* col-3 */}
                            <RowInforRight language={props.language} />
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
            {/* padding-right */}
            <td style={{ width: '50%' }} />
          </tr>
          {/* padding-bottom */}
          <tr>
            <td style={{ height: '32.5px' }} />
          </tr>
        </tbody>
      </table>
    </Grid>
  );
};

export default Signature;
