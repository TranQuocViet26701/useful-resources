import {
  Button,
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { CheckOutlined, FileCopyOutlined } from '@material-ui/icons';
import React, { useEffect } from 'react';
import './App.css';
import Logo from './assets/logo.svg';
import CircularProgressWithLabel from './components/CircularProgressWithLabel';
import Signature from './components/Signature';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
      },
      '& .label-root': {
        margin: theme.spacing(1),
      },
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'left',
      color: theme.palette.text.secondary,
    },
    centeredImage: {
      display: 'block',
      marginLeft: 'auto',
      marginRight: 'auto',
      width: '150px',
      height: '150px',
    },
    centeredText: {
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      textAlign: 'center',
    },
  }),
);

export interface PhotoSignatureProps {
  fullName: string;
  position: string;
  mail: string;
  phone: string;
  language: 'vi' | 'en';
}

interface State extends PhotoSignatureProps {
  copied: boolean;
}

const initialState: State = {
  fullName: '',
  position: '',
  mail: '',
  phone: '',
  language: 'vi',
  copied: false,
};

function App() {
  const classes = useStyles();
  const [state, setState] = React.useState<State>(initialState);

  useEffect(() => {
    if (window) {
      const itemsLocalStorage = window.localStorage.getItem('items') || '{}';
      const items = JSON.parse(itemsLocalStorage);
      if (items) {
        setState((prevState) => ({
          ...prevState,
          ...items,
        }));
      }
    }
  }, []);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  const handleChangeSelect = (event: React.ChangeEvent<{ value: unknown }>) => {
    setState((prevState) => ({
      ...prevState,
      language: event.target.value as 'vi' | 'en',
    }));
  };

  const enoughData = () => {
    let progress = 100;

    if (state.fullName && state.phone && state.position && state.mail) {
      return (
        <React.Fragment>
          <Signature
            fullName={state.fullName}
            position={state.position}
            mail={state.mail}
            phone={state.phone}
            language={state.language}
          />
          <br />
          <Grid container justifyContent="center">
            <Button
              onClick={copyToClipboard}
              color="primary"
              variant="contained"
              endIcon={state.copied ? <CheckOutlined /> : <FileCopyOutlined />}
            >
              {state.copied ? 'Copied' : 'Copy to clipboard'}
            </Button>
          </Grid>
        </React.Fragment>
      );
    } else {
      Object.entries(state).forEach(([key, value]) => {
        if (['fullName', 'phone', 'position', 'mail'].includes(key)) {
          if (value.length === 0) {
            progress = progress - 25;
          }
        }
      });
    }

    if (progress > 0) {
      return (
        <div className={classes.centeredText}>
          <CircularProgressWithLabel variant="determinate" value={progress} />
        </div>
      );
    } else {
      return (
        <div className={classes.centeredText}>
          <Typography variant="h6">Fill in your details to generate signature</Typography>
        </div>
      );
    }
  };

  const copyToClipboard = () => {
    let copyText = document.querySelector('#signature');
    const range = document.createRange();
    if (copyText) {
      range.selectNode(copyText);
    }
    const windowSelection = window.getSelection();
    if (windowSelection) {
      windowSelection.removeAllRanges();
      windowSelection.addRange(range);
    }

    try {
      let successful = document.execCommand('copy');
      console.log(successful ? 'Success' : 'Fail');
      if (window) {
        window.localStorage.setItem('items', JSON.stringify({ ...state, copied: false }));
      }
      setState((prevState) => ({
        ...prevState,
        copied: true,
      }));
    } catch (err) {
      console.log('Fail');
    }
  };

  const isStateChanged = () => {
    return JSON.stringify(state) === JSON.stringify(initialState);
  };

  const clearState = () => {
    setState(initialState);
  };

  return (
    <Container maxWidth="xl" style={{ marginBottom: '50px' }}>
      <img className={classes.centeredImage} src={Logo} alt={'logo'} />
      <Typography variant="h3" gutterBottom className={classes.centeredText}>
        Signature generator
      </Typography>
      <Grid container justifyContent="center">
        <Grid item xs={5}>
          <Paper className={classes.paper}>
            <form className={classes.root} noValidate autoComplete="off">
              <TextField
                fullWidth={true}
                required
                label="Full Name"
                value={state.fullName}
                name={'fullName'}
                onChange={handleChange}
                autoFocus={true}
              />
              <TextField
                fullWidth={true}
                required
                label="Position"
                value={state.position}
                name={'position'}
                onChange={handleChange}
              />
              <TextField
                fullWidth={true}
                required
                label="Mail"
                value={state.mail}
                name={'mail'}
                onChange={handleChange}
              />
              <TextField
                fullWidth={true}
                required
                label="Telephone"
                value={state.phone}
                name={'phone'}
                onChange={handleChange}
              />
              <FormControl required fullWidth={true} style={{ margin: '8px' }}>
                <InputLabel id="demo-simple-select-helper-label">Language</InputLabel>
                <Select
                  labelId="demo-simple-select-required-label"
                  id="demo-simple-select-required"
                  value={state.language}
                  onChange={handleChangeSelect}
                  fullWidth={true}
                  // className={classes.selectEmpty}
                >
                  <MenuItem value="vi">Vi</MenuItem>
                  <MenuItem value="en">En</MenuItem>
                </Select>
              </FormControl>

              <br />
              <Button disabled={isStateChanged()} onClick={clearState} color={'secondary'}>
                Clear
              </Button>
            </form>
          </Paper>
        </Grid>
        <Grid item xs={7}>
          {enoughData()}
        </Grid>
      </Grid>
    </Container>
  );
}

export default App;
