import Layout from '@components/common/layout-main';
import Seo from '@components/common/seo';
import { useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Col, Row } from 'react-bootstrap';

export const getStaticProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'smartLighting',
        'menu',
        'footer',
        'seo',
      ])),
      locale: locale,
    },
  };
};

function PolicyTerm() {
  const { t } = useTranslation('seo');

  return (
    <>
      <Seo title={t('policy.title')} description={t('policy.description')} url="/policy" />
      <Layout>
        <section className="page-policy-terms container-v2">
          <Row className="row-main-post-details d-flex justify-content-center">
            <Row className="max-width-1180 padding-left-right-register-account">
              <Col className="px-0">
                <Row>
                  <Col>
                    <h3 className="title-privacy-details">CHÍNH SÁCH BẢO MẬT</h3>
                    <h3 className="privacy-title-number">1. QUY ĐỊNH CHUNG</h3>
                    <ul className="ul-privacy">
                      <li className="details-post-main">
                        Tại Unicloud, chúng tôi luôn coi trọng quyền riêng tư của Quý Khách. Chúng
                        tôi cam kết tuân thủ tất cả các luật áp dụng về bảo mật dữ liệu/quyền riêng
                        tư.
                      </li>
                      <li className="details-post-main">
                        Trong quá trình cung cấp Dịch Vụ hoặc quyền truy cập vào Ứng dụng Sliving
                        App cho Quý Khách, chúng tôi sẽ thu thập, sử dụng, lưu trữ và/hoặc xử lý
                        những dữ liệu, bao gồm cả dữ liệu cá nhân của Quý Khách cho mục đích của
                        giao dịch được thực hiện trên Ứng dụng.
                      </li>
                      <li>
                        Chính Sách Bảo Mật này được lập để Quý Khách biết về cách thức chúng tôi thu
                        thập, sử dụng, lưu trữ và/hoặc xử lý dữ liệu mà chúng tôi thu thập và tiếp
                        nhận trong quá trình cung cấp Dịch Vụ hoặc quyền truy cập vào Ứng dụng
                        Sunshine App cho Quý Khách. Chúng tôi sẽ tiết lộ dữ liệu cá nhân của Quý
                        Khách trong trường hợp được phép theo Chính Sách Bảo Mật này.
                      </li>
                      <li>
                        BẰNG CÁCH NHẤP VÀO “ĐĂNG KÝ”, “ TÔI ĐỒNG Ý VỚI CHÍNH SÁCH BẢO MẬT CỦA
                        UNICLOUD, “TÔI ĐỒNG Ý VÀ CHO PHÉP VIỆC THU THẬP, SỬ DỤNG, TIẾT LỘ, LƯU TRỮ
                        VÀ/HOẶC XỬ LÝ DỮ LIỆU CÁ NHÂN CỦA TÔI CHO MỤC ĐÍCH ĐÃ NÊU TRONG VÀ PHÙ HỢP
                        VỚI CÁC ĐIỀU KHOẢN CỦA CHÍNH SÁCH BẢO MẬT CỦA UNICLOUD HOẶC NHỮNG NỘI DUNG
                        TƯƠNG TỰ ĐƯỢC THỂ HIỆN TẠI TRANG ĐĂNG KÝ CỦA Unicloud, QUÝ KHÁCH XÁC NHẬN
                        RẰNG QUÝ KHÁCH ĐÃ ĐƯỢC THÔNG BÁO VÀ HIỂU RÕ CÁC ĐIỀU KHOẢN CỦA CHÍNH SÁCH
                        BẢO MẬT NÀY VÀ QUÝ KHÁCH ĐÃ ĐỌC, HIỂU, ĐỒNG Ý VÀ CHO PHÉP VIỆC THU THẬP, SỬ
                        DỤNG, TIẾT LỘ, LƯU TRỮ VÀ/HOẶC XỬ LÝ DỮ LIỆU CÁ NHÂN NHƯ ĐÃ ĐƯỢC MÔ TẢ VÀ
                        QUY ĐỊNH TẠI CHÍNH SÁCH BẢO MẬT NÀY.
                      </li>
                      <li>
                        Sửa đổi, cập nhật bổ sung Chính Sách Bảo Mật
                        <p>
                          Tại từng thời điểm, chúng tôi có toàn quyền sửa đổi, bổ sung, và/hoặc cập
                          nhật Chính Sách Bảo Mật này. Chúng tôi sẽ đăng tải Chính Sách Bảo Mật này
                          được sửa đổi, bổ sung và/hoặc cập nhật trên Ứng dụng Sliving App và đề
                          nghị Quý Khách đồng ý với nội dung sửa đổi, bổ sung, và/hoặc cập nhật.
                          Việc Quý Khách tiếp tục sử dụng, truy cập ứng dụng Sliving App được hiểu
                          là Quý Khách đồng ý với nội dung Chính Sách Bảo Mật sửa đổi, bổ sung,
                          và/hoặc cập nhật đó.
                        </p>
                      </li>
                    </ul>
                    <h3 className="privacy-title-number">
                      2. DỮ LIỆU CÁ NHÂN CHÚNG TÔI THU THẬP TỪ QUÝ KHÁCH
                    </h3>
                    <span className="details-post-main">
                      2.1. Dữ Liệu Cá Nhân có nghĩa là bất kỳ thông tin nào được ghi nhận dưới định
                      dạng điện tử hoặc văn bản do Quý Khách cung cấp và/hoặc trong quá trình thực
                      hiện giao dịch với Unicloud, bao gồm nhưng không chỉ giới hạn các thông tin
                      xác định danh tính, liên lạc, chi tiết giao dịch và lịch sử/đặc tính truy cập
                      của Quý Khách, cụ thể gồm:
                    </span>
                    <ul className="ul-privacy">
                      <li className="details-post-main">
                        Dữ liệu định danh như tên, giới tính, ảnh hồ sơ và ngày sinh, tình trạng hôn
                        nhân;
                      </li>
                      <li className="details-post-main">
                        Dữ liệu liên lạc như địa chỉ thanh toán, địa chỉ giao hàng, địa chỉ email và
                        số điện thoại;
                      </li>
                      <li className="details-post-main">
                        Dữ liệu tài khoản như tài khoản ngân hàng và chi tiết thanh toán;
                      </li>
                      <li className="details-post-main">
                        Dữ liệu giao dịch như chi tiết được gửi đến và đi từ Quý Khách cũng như các
                        chi tiết khác về các sản phẩm và dịch vụ mà Quý Khách đã mua hoặc có được
                        thông qua Ứng dụng Sliving App;
                      </li>
                      <li className="details-post-main">
                        Dữ liệu kỹ thuật như địa chỉ giao thức trực tuyến (Internet Protocol – IP),
                        dữ liệu đăng nhập, loại và phiên bản trình duyệt, cài đặt múi giờ và vị trí,
                        loại và phiên bản trình cắm trình duyệt (plug-in browser), hệ điều hành và
                        nền tảng, mã nhận dạng thiết bị di động quốc tế, nhận dạng thiết bị và các
                        thông tin và công nghệ khác trên các thiết bị Quý Khách sử dụng để truy cập
                        Ứng dụng Sunshine App;
                      </li>
                      <li className="details-post-main">
                        Dữ liệu hồ sơ như tên người dùng và mật khẩu, dữ liệu về mua hàng hoặc đơn
                        đặt hàng của Quý Khách, sở thích, phản hồi và trả lời khảo sát của Quý
                        Khách;
                      </li>
                      <li>
                        Dữ liệu sử dụng như thông tin về việc sử dụng Ứng dụng Sliving App của Quý
                        Khách, sản phẩm và Dịch Vụ hoặc việc Quý Khách xem bất kỳ nội dung nào trên
                        Ứng dụng Sunshine App;
                      </li>
                      <li>
                        Dữ liệu về vị trí như khi Quý Khách chụp và chia sẻ vị trí của Quý Khách với
                        chúng tôi dưới dạng hình ảnh hoặc video và tải nội dung đó lên Ứng dụng
                        Sliving App;
                      </li>
                      <li>
                        Dữ liệu về sinh trắc học như tệp tin giọng nói khi Quý Khách sử dụng chức
                        năng tìm kiếm bằng giọng nói của chúng tôi cũng như các tính năng cơ thể và
                        giọng nói khác của chính Quý Khách và/hoặc người khác có trong video của Quý
                        Khách khi Quý Khách tải video lên Ứng dụng Sliving App;
                      </li>
                      <li>
                        Dữ liệu về tiếp thị và truyền thông như sở thích của Quý Khách trong việc
                        nhận thông tin tiếp thị từ chúng tôi và các bên thứ ba của chúng tôi và các
                        sở thích giao tiếp của Quý Khách.
                      </li>
                    </ul>
                    <h3 className="privacy-title-number">
                      2.2. Trong quá trình Quý Khách sử dụng Ứng dụng Sliving App và Dịch Vụ, chúng
                      tôi có thể thu thập dữ liệu cá nhân từ Quý Khách trong các trường hợp sau:
                    </h3>
                    <ul className="ul-privacy">
                      <li>(a) Khi Quý Khách tạo một tài khoản với chúng tôi;</li>
                      <li>
                        (b) Khi Quý Khách đăng ký bất kỳ Dịch Vụ nào hoặc mua bất kỳ Sản Phẩm nào có
                        sẵn trên Ứng dụng Sliving App;
                      </li>
                      <li>
                        (c) Khi Quý Khách sử dụng bất kỳ tính năng hoặc chức năng nào có sẵn trên
                        Ứng dụng Sliving App hoặc Dịch Vụ;
                      </li>
                      <li>
                        (d) Khi Quý Khách ghi lại bất kỳ nội dung nào do người dùng khởi tạo được
                        tải lên trên Ứng dụng Sliving App;
                      </li>
                      <li>
                        (e) Khi Quý Khách sử dụng chức năng trò chuyện trên Ứng dụng Sliving App;
                      </li>
                      <li>
                        (f) Khi Quý Khách đăng ký các ấn phẩm hoặc tài sản tiếp thị của chúng tôi;
                      </li>
                      <li>
                        (g) Khi Quý Khách tham gia một cuộc thi, đợt khuyến mãi hoặc khảo sát;
                      </li>
                      <li>
                        (h) Khi Quý Khách tham gia vào bất kỳ hoạt động hoặc chiến dịch khuyến mãi
                        nào trên Ứng dụng Sliving App;
                      </li>
                      <li>
                        (i) Khi Quý Khách đăng nhập vào tài khoản của mình trên Ứng dụng Sliving App
                        hoặc tương tác với chúng tôi thông qua một dịch vụ hoặc ứng dụng khác như
                        Facebook hoặc Google;
                      </li>
                      <li>
                        (j) Khi bất kỳ người dùng nào khác trên Ứng dụng Sunshine App đăng bất kỳ
                        nhận xét nào về nội dung Quý Khách đã tải lên trên Ứng dụng Sliving App hoặc
                        khi Quý Khách đăng bất kỳ nhận xét nào về nội dung của người dùng khác được
                        tải lên Ứng dụng Sliving App;
                      </li>
                      <li>
                        (k) Khi bên thứ ba gửi khiếu nại về Quý Khách hoặc nội dung Quý Khách đã
                        đăng trên Ứng dụng Sliving App;
                      </li>
                      <li>
                        (l) Khi Quý Khách truy cập hoặc sử dụng các trò chơi di động (bao gồm các
                        trò chơi tương tác ảo của chúng tôi) trên Ứng dụng Sliving App;
                      </li>
                      <li>
                        (m) Khi Quý Khách tương tác với chúng tôi ngoại tuyến, kể cả khi Quý Khách
                        tương tác với các tổ chức dịch vụ chăm sóc khách hàng thuê ngoài của chúng
                        tôi.
                      </li>
                    </ul>
                    <h3 className="privacy-title-number">
                      2.3. Trong quá trình Quý Khách sử dụng Ứng dụng Sliving App và sử dụng Dịch
                      Vụ, Quý Khách hoàn toàn đồng ý cho phép việc chuyển dữ liệu cá nhân của Quý
                      Khách trong nội bộ Sunshine Tech cho các mục đích xử lý các giao dịch khi sử
                      dụng Ứng dụng.
                    </h3>
                    <h3 className="privacy-title-number">
                      2.4. Quý Khách phải gửi dữ liệu cá nhân một cách chính xác và không gây nhầm
                      lẫn và Quý Khách phải cập nhật dữ liệu thường xuyên và thông báo cho chúng tôi
                      về bất kỳ thay đổi nào đối với dữ liệu cá nhân Quý Khách đã cung cấp cho chúng
                      tôi. Chúng tôi sẽ có quyền yêu cầu những tài liệu để xác minh dữ liệu cá nhân
                      do Quý Khách cung cấp như một phần của quy trình xác minh thông tin khách hàng
                      của chúng tôi.
                    </h3>
                    <h3 className="privacy-title-number">
                      2.5. Chúng tôi sẽ chỉ có thể thu thập dữ liệu cá nhân của Quý Khách nếu Quý
                      Khách tự nguyện gửi dữ liệu cá nhân cho chúng tôi hoặc theo quy định khác
                      trong Chính Sách Bảo Mật này. Trong trường hợp Quý Khách không gửi dữ liệu cá
                      nhân của mình cho chúng tôi hoặc sau đó thu hồi sự chấp thuận của Quý Khách
                      đối với việc chúng tôi sử dụng dữ liệu cá nhân của Quý Khách, chúng tôi không
                      thể cung cấp cho Quý Khách Dịch Vụ hoặc quyền truy cập vào Ứng dụng Sliving
                      App.
                    </h3>
                    <h3 className="privacy-title-number">
                      2.6. Quý Khách có thể truy cập và cập nhật thông tin cá nhân của Quý Khách gửi
                      cho chúng tôi bất cứ lúc nào.
                    </h3>
                    <h3 className="privacy-title-number">
                      2.7. Nếu Quý Khách cung cấp dữ liệu cá nhân của bất kỳ bên thứ ba nào cho
                      chúng tôi, Quý Khách cam đoan và bảo đảm rằng Quý Khách đã có được sự đồng ý,
                      chấp thuận và cho phép cần thiết từ bên thứ ba đó để chia sẻ và chuyển dữ liệu
                      cá nhân của họ cho chúng tôi và để chúng tôi thu thập, lưu trữ, sử dụng và
                      tiết lộ dữ liệu đó theo Chính Sách Bảo Mật này.
                    </h3>
                    <h3 className="privacy-title-number">
                      2.8. Nếu Quý Khách đăng ký làm người dùng trên Ứng dụng Sliving App của chúng
                      tôi bằng tài khoản mạng xã hội của Quý Khách hoặc liên kết tài khoản trên Ứng
                      dụng Sliving App với tài khoản mạng xã hội của Quý Khách hoặc sử dụng một số
                      tính năng phương tiện truyền thông khác của Unicloud, chúng tôi có thể truy
                      cập dữ liệu cá nhân về Quý Khách mà Quý Khách đã tự nguyện cung cấp các nhà
                      cung cấp mạng xã hội theo chính sách của họ và chúng tôi sẽ quản lý dữ liệu cá
                      nhân của Quý Khách theo Chính Sách Bảo Mật này.
                    </h3>
                    <h3 className="privacy-title-number">
                      3. CÁCH THỨC CHÚNG TÔI SỬ DỤNG DỮ LIỆU CÁ NHÂN CỦA QUÝ KHÁCH
                    </h3>
                    <h3 className="privacy-title-number">
                      3.1. Chúng tôi sử dụng các thông tin, dữ liệu thu thập từ của Quý Khách để vận
                      hành, cung cấp và cải thiện các sản phẩm, dịch vụ cung cấp trên Ứng dụng
                      Sliving App, cho các mục đích sử dụng của chúng tôi (“Mục Đích”) bao gồm:
                    </h3>

                    <ul className="ul-privacy">
                      <li>
                        (a) Cung cấp, cá nhân hóa, duy trì, thực hiện và cải thiện các sản phẩm,
                        dịch vụ trên Ứng dụng Sliving App;
                      </li>
                      <li>
                        (b) Để xác định danh tính của Quý Khách cho các mục đích phát hiện gian lận;
                      </li>
                      <li>(c) Để quản lý tài khoản của Quý Khách mở tại chúng tôi (nếu có);</li>
                      <li>
                        (d) Để xác minh và thực hiện các giao dịch tài chính liên quan đến các khoản
                        thanh toán mà Quý Khách thực hiện trực tuyến;
                      </li>
                      <li>(e) Để kiểm tra việc tải dữ liệu từ Ứng dụng Sliving App xuống;</li>
                      <li>
                        (f) Để cải thiện cách trình bày hoặc nội dung của các trang trong Ứng dụng
                        Sliving App và tùy chỉnh chúng cho người dùng;
                      </li>
                      <li>(g) Để xác định người truy cập trên Ứng dụng Sliving App;</li>
                      <li>
                        (h) Để thực hiện nghiên cứu về hành vi của người dùng và thống kê số lượng
                        người dùng;
                      </li>
                      <li>
                        (i) Để cung cấp cho Quý Khách những thông tin mà chúng tôi nghĩ rằng Quý
                        Khách có thể thấy hữu ích hoặc Quý Khách đã yêu cầu từ chúng tôi, bao gồm
                        thông tin về các Sản Phẩm và Dịch Vụ của Bên bán thứ ba của chúng tôi, với
                        điều kiện là Quý Khách cho biết rằng Quý Khách không phản đối việc liên hệ
                        vì các mục đích này;
                      </li>
                      <li>
                        (j) Để hiển thị tên dùng hoặc tên và hồ sơ của Quý Khách trên Ứng dụng
                        Sliving App;
                      </li>
                    </ul>
                    <h3 className="privacy-title-number">
                      4. CÁCH THỨC CHÚNG TÔI CHIA SẺ DỮ LIỆU CÁ NHÂN CỦA QUÝ KHÁCH
                    </h3>
                    <h4 className="sub-title">
                      4.1 Dữ Liệu Cá Nhân là một phần quan trọng trong hoạt động của chúng tôi, và
                      chúng tôi không bán thông tin của Quý Khách cho bất kỳ bên thứ ba nào khác.
                      Chúng tôi chỉ chia sẻ Dữ Liệu Cá Nhân của Quý Khách, trong phạm vi cần thiết,
                      như được nêu dưới đây, theo Chính Sách Bảo Mật này. Các bên được chia sẻ quyền
                      truy cập Dữ Liệu Cá Nhân của Quý Khách phải cam kết tuân thủ nghiêm ngặt các
                      quy định của pháp luật và các quy định liên quan tại Chính Sách Bảo Mật này.
                    </h4>

                    <ul className="ul-privacy">
                      <li>
                        (a) Thực hiện theo yêu cầu của các cơ quan nhà nước có thẩm quyền, hoặc theo
                        quy định của pháp luật;
                      </li>
                      <li>
                        (b) Chia sẻ Dữ Liệu Cá Nhân (trong phạm vi hạn chế như tên, phương thức liên
                        lạc…) với người dùng khác trong trường hợp Quý Khách sử dụng công cụ tương
                        tác trên Sliving App và đồng ý chia sẻ để liên lạc.
                      </li>
                      <li>
                        (c) Trao đổi, cung cấp, chia sẻ Dữ Liệu Cá Nhân (trong phạm vi hạn chế) với
                        các công ty con, công ty liên kết, các công ty trong mô hình nhóm công ty,
                        chi nhánh và các đơn vị có liên quan khác trong Tập Đoàn Sunshine và các đối
                        tác.
                      </li>
                    </ul>
                    <h3 className="privacy-title-number">
                      5. CÁCH THỨC CHÚNG TÔI LƯU TRỮ VÀ BẢO VỆ DỮ LIỆU CÁ NHÂN CỦA QUÝ KHÁCH
                    </h3>
                    <h4 className="sub-title">
                      5.1. Tại Sliving App, bảo mật là ưu tiên cao nhất của chúng tôi. Hệ thống của
                      chúng tôi được thiết kế có tính đến khả năng bảo đảm an toàn và riêng tư cho
                      thông tin của quý khách. Mọi Dữ Liệu Cá Nhân của Quý Khách đều được lưu giữ và
                      bảo mật bởi hệ thống của Sliving App hoặc đơn vị cung cấp dịch vụ cho Sliving
                      App theo quy định của pháp luật và Chính Sách Bảo Mật này.
                    </h4>
                    <h4 className="sub-title">
                      5.2. Khi thu thập, tiếp nhận dữ liệu, Sliving App sẽ nỗ lực tối đa trong phạm
                      vi cho phép để thực hiện lưu giữ và bảo mật Dữ Liệu Cá Nhân của Quý Khách tại
                      hệ thống máy chủ và các dữ liệu này được bảo đảm an toàn bằng các hệ thống
                      tường lửa (firewall), các biện pháp kiểm soát truy cập, mã hóa dữ liệu.
                      Sliving App có các biện pháp thích hợp về kỹ thuật và an ninh để ngăn chặn tối
                      đa trong phạm vi có thể việc truy cập, sử dụng trái phép Dữ Liệu Cá Nhân.
                      Sliving App cũng thường xuyên phối hợp với các chuyên gia bảo mật nhằm cập
                      nhật những thông tin mới nhất về an ninh mạng để đảm bảo sự an toàn cho Dữ
                      Liệu Cá Nhân thông tin người dùng.
                    </h4>
                    <h4 className="sub-title">
                      5.3. Để bảo vệ dữ liệu cá nhân của Quý Khách khỏi sự truy cập, thu thập, sử
                      dụng, tiết lộ, xử lý, sao chép, sửa đổi, xử lý, làm mất, lạm dụng hoặc rủi ro
                      tương tự, chúng tôi áp dụng các biện pháp vận hành, vật lý và kỹ thuật phù hợp
                      như:
                    </h4>
                    <ul className="ul-privacy">
                      <li>
                        (a) Hạn chế quyền truy cập vào dữ liệu cá nhân đối với các cá nhân yêu cầu
                        quyền truy cập;
                      </li>
                      <li>
                        (b) Bảo trì các sản phẩm công nghệ để ngăn chặn truy cập máy tính trái phép;
                      </li>
                    </ul>
                    <h4 className="sub-title">
                      5.4. Tuy nhiên, Quý Khách cần lưu ý rằng không có phương thức truyền tin qua
                      internet hoặc phương pháp lưu trữ điện tử nào là an toàn tuyệt đối. Mặc dù
                      việc bảo mật không thể được đảm bảo tuyệt đối, chúng tôi sẽ luôn nỗ lực bảo
                      mật của thông tin của Quý Khách và liên tục kiểm tra và tăng cường các biện
                      pháp bảo mật thông tin của chúng tôi.
                    </h4>
                    <h4 className="sub-title">
                      5.5. Mật khẩu của Quý Khách là chìa khóa cho tài khoản của Quý Khách. Vui lòng
                      sử dụng số, chữ cái và ký tự đặc biệt và không chia sẻ mật khẩu Unicloud của
                      Quý Khách với bất kỳ ai. Nếu Quý Khách chia sẻ mật khẩu của mình với người
                      khác, Quý Khách sẽ chịu trách nhiệm cho tất cả các hành động được thực hiện
                      dưới tên tài khoản của Quý Khách và các hậu quả từ các hoạt động đó. Nếu Quý
                      Khách mất quyền kiểm soát mật khẩu của mình, Quý Khách có thể mất quyền kiểm
                      soát đáng kể đối với dữ liệu cá nhân của mình và các dữ liệu khác được gửi tới
                      Unicloud. Quý Khách cũng có thể là đối tượng phải chịu các trách nhiệm ràng
                      buộc về mặt pháp lý do những hành vi đã được thực hiện nhân danh Quý Khách. Do
                      đó, nếu mật khẩu của Quý Khách đã bị xâm phạm vì bất kỳ lý do nào hoặc nếu Quý
                      Khách có cơ sở để tin rằng mật khẩu của Quý Khách đã bị xâm phạm, Quý Khách
                      nên liên hệ ngay với chúng tôi và thay đổi mật khẩu của mình. Quý Khách được
                      nhắc đăng xuất khỏi tài khoản của mình và đóng trình duyệt khi Quý Khách kết
                      thúc việc sử dụng máy tính dùng chung.
                    </h4>
                    <h4 className="sub-title">
                      5.6. Quý khách có trách nhiệm bảo vệ thông tin tài khoản của mình và không
                      cung cấp bất kỳ thông tin nào liên quan đến tài khoản, mật khẩu hay các phương
                      thức xác thực (vd: OTP) truy cập trên các website, ứng dụng, phần mềm và các
                      công cụ khác (nếu có).
                    </h4>
                    <h3 className="privacy-title-number">
                      6. CÁC QUYỀN CỦA QUÝ KHÁCH LIÊN QUAN TỚI BẢO MẬT DỮ LIỆU CÁ NHÂN
                    </h3>
                    <h4 className="sub-title">6.1. Cập nhật Dữ Liệu Cá Nhân Của Quý Khách</h4>
                    <ul className="ul-privacy">
                      <li>
                        (a) Dữ liệu cá nhân Quý Khách cung cấp cho chúng tôi phải chính xác và đầy
                        đủ để Quý Khách tiếp tục sử dụng Ứng dụng Sliving App và để chúng tôi cung
                        cấp Dịch Vụ cho Quý Khách. Quý Khách chịu trách nhiệm thông báo cho chúng
                        tôi về các thay đổi đối với dữ liệu cá nhân của Quý Khách hoặc trong trường
                        hợp Quý Khách tin rằng dữ liệu cá nhân chúng tôi có về Quý Khách không chính
                        xác, không đầy đủ, sai lệch hoặc hết hạn.{' '}
                      </li>
                      <li>
                        (b) Quý Khách có thể cập nhật dữ liệu cá nhân của mình bất cứ lúc nào bằng
                        cách truy cập tài khoản của Quý Khách trên Ứng dụng Sliving App.{' '}
                      </li>
                      <li>
                        (c) Chúng tôi thực hiện các quy trình để chia sẻ các cập nhật dữ liệu cá
                        nhân của Quý Khách với các bên thứ ba và các bên liên kết của chúng tôi,
                        những cá nhân, tổ chức mà chúng tôi đã chia sẻ dữ liệu cá nhân của Quý Khách
                        nếu dữ liệu cá nhân của Quý Khách vẫn cần thiết cho các mục đích nêu trên.
                      </li>
                    </ul>
                    <h4 className="sub-title">
                      6.2. Truy Cập Dữ Liệu Cá Nhân Của Quý Khách thông qua tài khoản và mật khẩu
                      người dùng do Sliving App cung cấp.
                    </h4>
                    <h3 className="privacy-title-number">7. TRANG WEB CỦA BÊN THỨ BA</h3>
                    <p>
                      Ứng dụng Sliving App có thể chứa các liên kết đến các trang web khác được điều
                      hành bởi các bên khác, chẳng hạn như các bên liên kết kinh doanh, thương nhân
                      hoặc cổng thanh toán của chúng tôi. Chúng tôi không chịu trách nhiệm đối với
                      các hoạt động bảo mật của các trang web được điều hành bởi các bên khác. Quý
                      Khách nên kiểm tra các chính sách bảo mật hiện hành của các trang web đó để
                      xác định cách họ sẽ xử lý bất kỳ thông tin nào họ thu thập từ Quý Khách.
                    </p>
                    <h3 className="privacy-title-number">
                      8. CÂU HỎI, PHẢN HỒI, THẮC MẮC, KIẾN NGHỊ HOẶC KHIẾU NẠI
                    </h3>
                    <p>
                      Nếu Quý Khách có bất kỳ câu hỏi, phản hồi, thắc mắc kiến nghị hoặc khiếu nại
                      nào về bảo mật dữ liệu cá nhân hoặc quyền riêng tư về dữ liệu, vui lòng liên
                      hệ trực tiếp với chúng tôi theo thông tin liên hệ được công bố trên Ứng dụng
                      Sliving App. Bản Chính Sách Bảo Mật này có hiệu lực kể từ ngày 10 tháng 02 năm
                      2020
                    </p>
                    <div className="div-under-post-detail" />
                  </Col>
                </Row>
              </Col>
            </Row>
          </Row>
        </section>
      </Layout>
    </>
  );
}

export default PolicyTerm;
