module.exports = {
  i18n: {
    defaultLocale: "vi",
    localeDetection: false,
    locales: ["vi", "en"]
  },
};
