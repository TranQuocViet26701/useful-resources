declare namespace API {
  type AppFormControllerDeleteModelByIdParams = {
    id: string;
  };

  type AppFormControllerGetDetailByIdParams = {
    id: string;
  };

  type AppFormControllerGetDetailWithFilterParams = {
    codeForm: string;
    workspaceId: string;
  };

  type AppFormControllerGetInfoPageWithFilterParams = {
    name?: string;
    workspaceId: string;
    all?: boolean;
    number?: number;
    size?: number;
  };

  type AppFormFieldControllerDeleteModelByIdParams = {
    id: string;
  };

  type AppFormFieldControllerGetDetailByIdParams = {
    id: string;
  };

  type AppFormFieldControllerGetInfoPageWithFilterParams = {
    name?: string;
    workspaceId: string;
    number?: number;
    size?: number;
  };

  type AppTemplateFormControllerGetDetailByIdParams = {
    id: string;
  };

  type AppTemplateFormControllerGetInfoPageWithFilterParams = {
    name?: string;
    workspaceId: string;
    all?: boolean;
    number?: number;
    size?: number;
  };

  type AppTemplateFormFieldControllerGetDetailByIdParams = {
    id: string;
  };

  type AppTemplateFormFieldControllerGetInfoPageWithFilterParams = {
    name?: string;
    number?: number;
    size?: number;
  };

  type BasePagingResponseFormFieldDetail = {
    content?: FormFieldDetail[];
    pageNumber?: number;
    pageSize?: number;
    totalElements?: number;
  };

  type BasePagingResponseFormInfo = {
    content?: FormInfo[];
    pageNumber?: number;
    pageSize?: number;
    totalElements?: number;
  };

  type BasePagingResponseTemplateFormFieldDetail = {
    content?: TemplateFormFieldDetail[];
    pageNumber?: number;
    pageSize?: number;
    totalElements?: number;
  };

  type BasePagingResponseTemplateFormInfo = {
    content?: TemplateFormInfo[];
    pageNumber?: number;
    pageSize?: number;
    totalElements?: number;
  };

  type BaseResponseBasePagingResponseFormFieldDetail = {
    success?: boolean;
    data?: BasePagingResponseFormFieldDetail;
  };

  type BaseResponseBasePagingResponseFormInfo = {
    success?: boolean;
    data?: BasePagingResponseFormInfo;
  };

  type BaseResponseBasePagingResponseTemplateFormFieldDetail = {
    success?: boolean;
    data?: BasePagingResponseTemplateFormFieldDetail;
  };

  type BaseResponseBasePagingResponseTemplateFormInfo = {
    success?: boolean;
    data?: BasePagingResponseTemplateFormInfo;
  };

  type BaseResponseBoolean = {
    success?: boolean;
    data?: boolean;
  };

  type BaseResponseFactoryDeleteResponse = {
    success?: boolean;
    data?: FactoryDeleteResponse;
  };

  type BaseResponseFactoryGetResponseUUIDFormDetail = {
    success?: boolean;
    data?: FactoryGetResponseUUIDFormDetail;
  };

  type BaseResponseFactoryGetResponseUUIDFormFieldDetail = {
    success?: boolean;
    data?: FactoryGetResponseUUIDFormFieldDetail;
  };

  type BaseResponseFactoryGetResponseUUIDTemplateFormDetail = {
    success?: boolean;
    data?: FactoryGetResponseUUIDTemplateFormDetail;
  };

  type BaseResponseFactoryGetResponseUUIDTemplateFormFieldDetail = {
    success?: boolean;
    data?: FactoryGetResponseUUIDTemplateFormFieldDetail;
  };

  type BaseResponseFormDetail = {
    success?: boolean;
    data?: FormDetail;
  };

  type BaseResponseFormFieldDetail = {
    success?: boolean;
    data?: FormFieldDetail;
  };

  type BaseResponseListGetInfoFieldResponse = {
    success?: boolean;
    data?: GetInfoFieldResponse[];
  };

  type BaseResponseTemplateFormDetail = {
    success?: boolean;
    data?: TemplateFormDetail;
  };

  type BaseResponseTemplateFormFieldDetail = {
    success?: boolean;
    data?: TemplateFormFieldDetail;
  };

  type CmsTemplateFormControllerDeleteModelByIdParams = {
    id: string;
  };

  type CmsTemplateFormControllerGetDetailByIdParams = {
    id: string;
  };

  type CmsTemplateFormControllerGetInfoPageWithFilterParams = {
    name?: string;
    workspaceId: string;
    all?: boolean;
    number?: number;
    size?: number;
  };

  type CmsTemplateFormFieldControllerDeleteModelByIdParams = {
    id: string;
  };

  type CmsTemplateFormFieldControllerGetDetailByIdParams = {
    id: string;
  };

  type CmsTemplateFormFieldControllerGetInfoPageWithFilterParams = {
    name?: string;
    number?: number;
    size?: number;
  };

  type FactoryCreateRequestUUIDFormDetail = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    workspaceId: string;
    codeName: string;
    formFieldIds?: string[];
    formFieldDetails?: FormFieldDetail[];
  };

  type FactoryCreateRequestUUIDFormFieldDetail = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    workspaceId: string;
    defaultValue?: string;
    maxValue?: number;
    minValue?: number;
    editable: boolean;
    showUI: boolean;
    lines?: number;
    regEx?: string;
    maxLength: number;
    minLength: number;
    required: boolean;
    replaceCharacter?: string;
    placeHolder?: string;
    typeFormat:
      | 'STRING'
      | 'DECIMAL'
      | 'NUMBER'
      | 'BOOLEAN'
      | 'DROPDOWN'
      | 'SELECTOR'
      | 'MULTI_SELECTOR'
      | 'FILE'
      | 'DATE'
      | 'DATE_TIME';
    listSelectValues?: SubFieldDetail[];
  };

  type FactoryCreateRequestUUIDTemplateFormDetail = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    codeName?: string;
    listFormField?: TemplateFormFieldDetail[];
  };

  type FactoryCreateRequestUUIDTemplateFormFieldDetail = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    workspaceId: string;
    defaultValue?: string;
    maxValue?: number;
    minValue?: number;
    editable: boolean;
    showUI: boolean;
    lines?: number;
    regEx?: string;
    maxLength: number;
    minLength: number;
    required: boolean;
    replaceCharacter?: string;
    placeHolder?: string;
    typeFormat:
      | 'STRING'
      | 'DECIMAL'
      | 'NUMBER'
      | 'BOOLEAN'
      | 'DROPDOWN'
      | 'SELECTOR'
      | 'MULTI_SELECTOR'
      | 'FILE'
      | 'DATE'
      | 'DATE_TIME';
    listSelectValues?: SubFieldDetail[];
  };

  type FactoryDeleteResponse = {
    success?: boolean;
  };

  type FactoryGetResponseUUIDFormDetail = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    workspaceId: string;
    codeName: string;
    formFieldIds?: string[];
    formFieldDetails?: FormFieldDetail[];
  };

  type FactoryGetResponseUUIDFormFieldDetail = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    workspaceId: string;
    defaultValue?: string;
    maxValue?: number;
    minValue?: number;
    editable: boolean;
    showUI: boolean;
    lines?: number;
    regEx?: string;
    maxLength: number;
    minLength: number;
    required: boolean;
    replaceCharacter?: string;
    placeHolder?: string;
    typeFormat:
      | 'STRING'
      | 'DECIMAL'
      | 'NUMBER'
      | 'BOOLEAN'
      | 'DROPDOWN'
      | 'SELECTOR'
      | 'MULTI_SELECTOR'
      | 'FILE'
      | 'DATE'
      | 'DATE_TIME';
    listSelectValues?: SubFieldDetail[];
  };

  type FactoryGetResponseUUIDTemplateFormDetail = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    codeName?: string;
    listFormField?: TemplateFormFieldDetail[];
  };

  type FactoryGetResponseUUIDTemplateFormFieldDetail = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    workspaceId: string;
    defaultValue?: string;
    maxValue?: number;
    minValue?: number;
    editable: boolean;
    showUI: boolean;
    lines?: number;
    regEx?: string;
    maxLength: number;
    minLength: number;
    required: boolean;
    replaceCharacter?: string;
    placeHolder?: string;
    typeFormat:
      | 'STRING'
      | 'DECIMAL'
      | 'NUMBER'
      | 'BOOLEAN'
      | 'DROPDOWN'
      | 'SELECTOR'
      | 'MULTI_SELECTOR'
      | 'FILE'
      | 'DATE'
      | 'DATE_TIME';
    listSelectValues?: SubFieldDetail[];
  };

  type FactoryUpdateRequestUUIDFormDetail = {
    id: string;
    data: FormDetail;
  };

  type FactoryUpdateRequestUUIDFormFieldDetail = {
    id: string;
    data: FormFieldDetail;
  };

  type FactoryUpdateRequestUUIDTemplateFormDetail = {
    id: string;
    data: TemplateFormDetail;
  };

  type FactoryUpdateRequestUUIDTemplateFormFieldDetail = {
    id: string;
    data: TemplateFormFieldDetail;
  };

  type FormDetail = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    workspaceId: string;
    codeName: string;
    formFieldIds?: string[];
    formFieldDetails?: FormFieldDetail[];
  };

  type FormFieldDetail = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    workspaceId: string;
    defaultValue?: string;
    maxValue?: number;
    minValue?: number;
    editable: boolean;
    showUI: boolean;
    lines?: number;
    regEx?: string;
    maxLength: number;
    minLength: number;
    required: boolean;
    replaceCharacter?: string;
    placeHolder?: string;
    typeFormat:
      | 'STRING'
      | 'DECIMAL'
      | 'NUMBER'
      | 'BOOLEAN'
      | 'DROPDOWN'
      | 'SELECTOR'
      | 'MULTI_SELECTOR'
      | 'FILE'
      | 'DATE'
      | 'DATE_TIME';
    listSelectValues?: SubFieldDetail[];
  };

  type FormInfo = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    workspaceId: string;
    codeName: string;
  };

  type GetInfoFieldResponse = {
    name?: string;
    fieldId?: string;
  };

  type ModelValidateField = {
    listValuesField?: Record<string, any>;
  };

  type SubFieldDetail = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
  };

  type TemplateFormDetail = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    codeName?: string;
    listFormField?: TemplateFormFieldDetail[];
  };

  type TemplateFormFieldDetail = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    workspaceId: string;
    defaultValue?: string;
    maxValue?: number;
    minValue?: number;
    editable: boolean;
    showUI: boolean;
    lines?: number;
    regEx?: string;
    maxLength: number;
    minLength: number;
    required: boolean;
    replaceCharacter?: string;
    placeHolder?: string;
    typeFormat:
      | 'STRING'
      | 'DECIMAL'
      | 'NUMBER'
      | 'BOOLEAN'
      | 'DROPDOWN'
      | 'SELECTOR'
      | 'MULTI_SELECTOR'
      | 'FILE'
      | 'DATE'
      | 'DATE_TIME';
    listSelectValues?: SubFieldDetail[];
  };

  type TemplateFormInfo = {
    id?: string;
    name: string;
    description?: string;
    icon?: string;
    codeName?: string;
  };
}
