// @ts-ignore
/* eslint-disable */
import { request } from '@/utils/request';

/** 此处后端没有提供注释 DELETE /api/app/v1/form-fields/${param0}/delete */
export async function AppFormFieldControllerDeleteModelById(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AppFormFieldControllerDeleteModelByIdParams,
  options?: { [key: string]: any },
) {
  const { id: param0, ...queryParams } = params;
  return request<API.BaseResponseFactoryDeleteResponse>(
    `/api/app/v1/form-fields/${param0}/delete`,
    {
      method: 'DELETE',
      params: { ...queryParams },
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 GET /api/app/v1/form-fields/${param0}/detail */
export async function AppFormFieldControllerGetDetailById(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AppFormFieldControllerGetDetailByIdParams,
  options?: { [key: string]: any },
) {
  const { id: param0, ...queryParams } = params;
  return request<API.BaseResponseFactoryGetResponseUUIDFormFieldDetail>(
    `/api/app/v1/form-fields/${param0}/detail`,
    {
      method: 'GET',
      params: { ...queryParams },
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 POST /api/app/v1/form-fields/create */
export async function AppFormFieldControllerCreateModel(
  body: API.FactoryCreateRequestUUIDFormFieldDetail,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseFormFieldDetail>('/api/app/v1/form-fields/create', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 GET /api/app/v1/form-fields/page */
export async function AppFormFieldControllerGetInfoPageWithFilter(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AppFormFieldControllerGetInfoPageWithFilterParams,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseBasePagingResponseFormFieldDetail>(
    '/api/app/v1/form-fields/page',
    {
      method: 'GET',
      params: {
        // size has a default value: 20
        size: '20',
        ...params,
      },
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 PUT /api/app/v1/form-fields/update */
export async function AppFormFieldControllerUpdateModel(
  body: API.FactoryUpdateRequestUUIDFormFieldDetail,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseFormFieldDetail>('/api/app/v1/form-fields/update', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 POST /api/app/v1/form-fields/validate/fields */
export async function AppFormFieldControllerValidateValuesField(
  body: API.ModelValidateField,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseBoolean>('/api/app/v1/form-fields/validate/fields', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
