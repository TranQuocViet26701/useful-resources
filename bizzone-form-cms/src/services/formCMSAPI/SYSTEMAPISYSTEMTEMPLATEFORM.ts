// @ts-ignore
/* eslint-disable */
import { request } from '@/utils/request';

/** 此处后端没有提供注释 DELETE /api/system/v1/system-templates/forms/${param0}/delete */
export async function CmsTemplateFormControllerDeleteModelById(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.CmsTemplateFormControllerDeleteModelByIdParams,
  options?: { [key: string]: any },
) {
  const { id: param0, ...queryParams } = params;
  return request<API.BaseResponseFactoryDeleteResponse>(
    `/api/system/v1/system-templates/forms/${param0}/delete`,
    {
      method: 'DELETE',
      params: { ...queryParams },
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 GET /api/system/v1/system-templates/forms/${param0}/detail */
export async function CmsTemplateFormControllerGetDetailById(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.CmsTemplateFormControllerGetDetailByIdParams,
  options?: { [key: string]: any },
) {
  const { id: param0, ...queryParams } = params;
  return request<API.BaseResponseFactoryGetResponseUUIDTemplateFormDetail>(
    `/api/system/v1/system-templates/forms/${param0}/detail`,
    {
      method: 'GET',
      params: { ...queryParams },
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 POST /api/system/v1/system-templates/forms/create */
export async function CmsTemplateFormControllerCreateModel(
  body: API.FactoryCreateRequestUUIDTemplateFormDetail,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseTemplateFormDetail>(
    '/api/system/v1/system-templates/forms/create',
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      data: body,
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 GET /api/system/v1/system-templates/forms/page */
export async function CmsTemplateFormControllerGetInfoPageWithFilter(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.CmsTemplateFormControllerGetInfoPageWithFilterParams,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseBasePagingResponseTemplateFormInfo>(
    '/api/system/v1/system-templates/forms/page',
    {
      method: 'GET',
      params: {
        // size has a default value: 20
        size: '20',
        ...params,
      },
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 PUT /api/system/v1/system-templates/forms/update */
export async function CmsTemplateFormControllerUpdateModel(
  body: API.FactoryUpdateRequestUUIDTemplateFormDetail,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseTemplateFormDetail>(
    '/api/system/v1/system-templates/forms/update',
    {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      data: body,
      ...(options || {}),
    },
  );
}
