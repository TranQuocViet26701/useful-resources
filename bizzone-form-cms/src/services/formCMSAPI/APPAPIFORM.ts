// @ts-ignore
/* eslint-disable */
import { request } from '@/utils/request';

/** 此处后端没有提供注释 DELETE /api/app/v1/forms/${param0}/delete */
export async function AppFormControllerDeleteModelById(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AppFormControllerDeleteModelByIdParams,
  options?: { [key: string]: any },
) {
  const { id: param0, ...queryParams } = params;
  return request<API.BaseResponseFactoryDeleteResponse>(`/api/app/v1/forms/${param0}/delete`, {
    method: 'DELETE',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 GET /api/app/v1/forms/${param0}/detail */
export async function AppFormControllerGetDetailById(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AppFormControllerGetDetailByIdParams,
  options?: { [key: string]: any },
) {
  const { id: param0, ...queryParams } = params;
  return request<API.BaseResponseFactoryGetResponseUUIDFormDetail>(
    `/api/app/v1/forms/${param0}/detail`,
    {
      method: 'GET',
      params: { ...queryParams },
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 POST /api/app/v1/forms/create */
export async function AppFormControllerCreateModel(
  body: API.FactoryCreateRequestUUIDFormDetail,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseFormDetail>('/api/app/v1/forms/create', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 GET /api/app/v1/forms/detail */
export async function AppFormControllerGetDetailWithFilter(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AppFormControllerGetDetailWithFilterParams,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseFactoryGetResponseUUIDFormDetail>('/api/app/v1/forms/detail', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 GET /api/app/v1/forms/page */
export async function AppFormControllerGetInfoPageWithFilter(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AppFormControllerGetInfoPageWithFilterParams,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseBasePagingResponseFormInfo>('/api/app/v1/forms/page', {
    method: 'GET',
    params: {
      // size has a default value: 20
      size: '20',
      ...params,
    },
    ...(options || {}),
  });
}

/** 此处后端没有提供注释 PUT /api/app/v1/forms/update */
export async function AppFormControllerUpdateModel(
  body: API.FactoryUpdateRequestUUIDFormDetail,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseFormDetail>('/api/app/v1/forms/update', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
