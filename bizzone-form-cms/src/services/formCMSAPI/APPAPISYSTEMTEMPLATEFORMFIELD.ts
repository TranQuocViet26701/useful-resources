// @ts-ignore
/* eslint-disable */
import { request } from '@/utils/request';

/** 此处后端没有提供注释 GET /api/app/v1/system-templates/form-fields/${param0}/detail */
export async function AppTemplateFormFieldControllerGetDetailById(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AppTemplateFormFieldControllerGetDetailByIdParams,
  options?: { [key: string]: any },
) {
  const { id: param0, ...queryParams } = params;
  return request<API.BaseResponseFactoryGetResponseUUIDTemplateFormFieldDetail>(
    `/api/app/v1/system-templates/form-fields/${param0}/detail`,
    {
      method: 'GET',
      params: { ...queryParams },
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 GET /api/app/v1/system-templates/form-fields/page */
export async function AppTemplateFormFieldControllerGetInfoPageWithFilter(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.AppTemplateFormFieldControllerGetInfoPageWithFilterParams,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseBasePagingResponseTemplateFormFieldDetail>(
    '/api/app/v1/system-templates/form-fields/page',
    {
      method: 'GET',
      params: {
        // size has a default value: 20
        size: '20',
        ...params,
      },
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 GET /api/app/v1/system-templates/form-fields/templates */
export async function AppTemplateFormFieldControllerGetAllNames(options?: { [key: string]: any }) {
  return request<API.BaseResponseListGetInfoFieldResponse>(
    '/api/app/v1/system-templates/form-fields/templates',
    {
      method: 'GET',
      ...(options || {}),
    },
  );
}
