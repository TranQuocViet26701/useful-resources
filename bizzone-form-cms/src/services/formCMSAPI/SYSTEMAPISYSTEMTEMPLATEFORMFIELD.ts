// @ts-ignore
/* eslint-disable */
import { request } from '@/utils/request';

/** 此处后端没有提供注释 DELETE /api/system/v1/system-templates/form-fields/${param0}/delete */
export async function CmsTemplateFormFieldControllerDeleteModelById(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.CmsTemplateFormFieldControllerDeleteModelByIdParams,
  options?: { [key: string]: any },
) {
  const { id: param0, ...queryParams } = params;
  return request<API.BaseResponseFactoryDeleteResponse>(
    `/api/system/v1/system-templates/form-fields/${param0}/delete`,
    {
      method: 'DELETE',
      params: { ...queryParams },
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 GET /api/system/v1/system-templates/form-fields/${param0}/detail */
export async function CmsTemplateFormFieldControllerGetDetailById(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.CmsTemplateFormFieldControllerGetDetailByIdParams,
  options?: { [key: string]: any },
) {
  const { id: param0, ...queryParams } = params;
  return request<API.BaseResponseFactoryGetResponseUUIDTemplateFormFieldDetail>(
    `/api/system/v1/system-templates/form-fields/${param0}/detail`,
    {
      method: 'GET',
      params: { ...queryParams },
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 POST /api/system/v1/system-templates/form-fields/create */
export async function CmsTemplateFormFieldControllerCreateModel(
  body: API.FactoryCreateRequestUUIDTemplateFormFieldDetail,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseTemplateFormFieldDetail>(
    '/api/system/v1/system-templates/form-fields/create',
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      data: body,
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 GET /api/system/v1/system-templates/form-fields/page */
export async function CmsTemplateFormFieldControllerGetInfoPageWithFilter(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.CmsTemplateFormFieldControllerGetInfoPageWithFilterParams,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseBasePagingResponseTemplateFormFieldDetail>(
    '/api/system/v1/system-templates/form-fields/page',
    {
      method: 'GET',
      params: {
        // size has a default value: 20
        size: '20',
        ...params,
      },
      ...(options || {}),
    },
  );
}

/** 此处后端没有提供注释 PUT /api/system/v1/system-templates/form-fields/update */
export async function CmsTemplateFormFieldControllerUpdateModel(
  body: API.FactoryUpdateRequestUUIDTemplateFormFieldDetail,
  options?: { [key: string]: any },
) {
  return request<API.BaseResponseTemplateFormFieldDetail>(
    '/api/system/v1/system-templates/form-fields/update',
    {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      data: body,
      ...(options || {}),
    },
  );
}
