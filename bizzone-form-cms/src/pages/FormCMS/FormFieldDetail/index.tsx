import PageHeaderFormCMS from '@/pages/FormCMS/components/PageHeaderFormCMS';
import {
  AppFormFieldControllerDeleteModelById,
  AppFormFieldControllerGetDetailById,
  AppFormFieldControllerUpdateModel,
} from '@/services/formCMSAPI/APPAPIFORMFIELD';
import type {
  ProDescriptionsActionType,
  ProDescriptionsItemProps,
} from '@ant-design/pro-components';
import { PageContainer, ProDescriptions } from '@ant-design/pro-components';
import { Button, Card, message } from 'antd';
import { useRef, useState } from 'react';
import { useHistory, useParams } from 'umi';

type FormFieldDetailParams = {
  formFieldId: string;
};

const descriptionItem = [
  {
    dataIndex: 'id',
    label: 'Id',
    valueType: 'text',
    editable: false,
    copyable: true,
  },
  {
    dataIndex: 'name',
    label: 'Name',
    valueType: 'text',
  },
  {
    dataIndex: 'description',
    label: 'Description',
    valueType: 'text',
  },
  {
    dataIndex: 'icon',
    label: 'Icon',
    valueType: 'text',
  },
  {
    dataIndex: 'workspaceId',
    label: 'Workspace Id',
    valueType: 'text',
  },
  {
    dataIndex: 'defaultValue',
    label: 'Default value',
    valueType: 'text',
  },
  {
    dataIndex: 'maxValue',
    label: 'Max value',
    valueType: 'digit',
  },
  {
    dataIndex: 'minValue',
    label: 'Min value',
    valueType: 'digit',
  },
  {
    dataIndex: 'maxLength',
    label: 'Max length',
    valueType: 'digit',
  },
  {
    dataIndex: 'minLength',
    label: 'Min length',
    valueType: 'digit',
  },
  {
    dataIndex: 'editable',
    label: 'Editable',
    valueType: 'text',
  },
  {
    dataIndex: 'showUI',
    label: 'Show UI',
    valueType: 'checkbox',
  },
  {
    dataIndex: 'required',
    label: 'Required',
    valueType: 'checkbox',
  },
  {
    dataIndex: 'replaceCharacter',
    label: 'Replace Character',
    valueType: 'text',
  },
  {
    dataIndex: 'placeHolder',
    label: 'Placeholder',
    valueType: 'text',
  },
  {
    dataIndex: 'typeFormat',
    label: 'Type format',
    valueType: 'option',
  },
  {
    dataIndex: 'lines',
    label: 'Lines',
    valueType: 'digit',
  },
  {
    dataIndex: 'regEx',
    label: 'RegEx',
    valueType: 'text',
  },
];

const routes = [
  {
    href: '',
    title: 'Home',
  },
  {
    href: '',
    title: 'Form CMS',
  },
  {
    href: '',
    title: 'Form Field Detail',
  },
];

export default () => {
  const actionRef = useRef<ProDescriptionsActionType>();
  const { formFieldId } = useParams<FormFieldDetailParams>();
  const [editedData, setEditedData] = useState<any>(null);
  const history = useHistory();

  const handleUpdateFormField = async () => {
    try {
      // assertion type as any for fixing problem error res.succes with typescript
      const res = (await AppFormFieldControllerUpdateModel({
        id: editedData.id,
        data: editedData,
      })) as any;

      if (res && res.success) message.success('Update successfully');
    } catch (error) {
      message.error('Update failed');
    }

    setEditedData(null);
  };

  const handleRemoveFormField = async () => {
    try {
      const res = (await AppFormFieldControllerDeleteModelById({
        id: formFieldId,
      })) as any;

      if (res && res.success) message.success('Delete successfully');
      history.push('/form-cms/form-field');
    } catch (error) {
      message.error('Delete failed');
    }
  };

  return (
    <>
      <PageHeaderFormCMS itemList={routes} />
      <PageContainer title="Form field detail" pageHeaderRender={false}>
        <Card>
          <ProDescriptions
            actionRef={actionRef}
            editable={{
              onSave: async (keypath, newInfo) => {
                setEditedData({ ...newInfo });
                return true;
              },
            }}
            request={async () => {
              return AppFormFieldControllerGetDetailById({ id: formFieldId });
            }}
            extra={
              <>
                <Button type="primary" disabled={!editedData} onClick={handleUpdateFormField}>
                  Update
                </Button>
                <Button type="primary" danger onClick={handleRemoveFormField}>
                  Delete
                </Button>
              </>
            }
            bordered
            columns={descriptionItem as ProDescriptionsItemProps[]}
          >
            <ProDescriptions.Item valueType="option">
              <Button
                type="ghost"
                onClick={() => {
                  actionRef.current?.reload();
                }}
                key="reload"
              >
                Reload
              </Button>
            </ProDescriptions.Item>
          </ProDescriptions>
        </Card>
      </PageContainer>
    </>
  );
};
