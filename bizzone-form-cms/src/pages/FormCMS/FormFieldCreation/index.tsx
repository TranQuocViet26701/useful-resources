import { AppFormFieldControllerCreateModel } from '@/services/formCMSAPI/APPAPIFORMFIELD';
import {
  PageContainer,
  ProForm,
  ProFormDigit,
  ProFormDigitRange,
  ProFormSelect,
  ProFormSwitch,
  ProFormText,
} from '@ant-design/pro-components';
import { Card, message } from 'antd';
import { useState } from 'react';
import { useHistory } from 'umi';

const typeFormatEnum = {
  STRING: 'STRING',
  DECIMAL: 'DECIMAL',
  NUMBER: 'NUMBER',
  BOOLEAN: 'BOOLEAN',
  DROPDOWN: 'DROPDOWN',
  SELECTOR: 'SELECTOR',
  MULTI_SELECTOR: 'MULTI_SELECTOR',
  FILE: 'FILE',
  DATE: 'DATE',
  DATE_TIME: 'DATE_TIME',
};

function isNumericType(type: string) {
  return type === 'DECIMAL' || type === 'NUMBER';
}

function isStringType(type: string) {
  return type === 'STRING';
}

function formatFields(fields: any, workspaceId: string) {
  const res = {
    workspaceId,
    editable: true,
    showUI: true,
    required: true,
    maxLength: 0,
    minLength: 0,
    ...fields,
  };

  if (res.rangeValue && res.rangeValue.length !== 0) {
    res.minValue = fields.rangeValue[0];
    res.maxValue = fields.rangeValue[1];

    delete res.rangeValue;
  }

  if (res.rangeLength && res.rangeLength.length !== 0) {
    res.minLength = fields.rangeLength[0];
    res.maxLength = fields.rangeLength[1];

    delete res.rangeLength;
  }

  return res;
}

export default () => {
  const [formatType, setFormatType] = useState<'numeric' | 'string' | 'rest'>('rest');
  const history = useHistory();

  const handleAddFormField = async (fields: any) => {
    const defaultWorkspaceId = '3fa85f64-5717-4562-b3fc-2c963f66afa6';

    const formattedFields = formatFields(
      fields,
      defaultWorkspaceId,
    ) as API.FactoryCreateRequestUUIDFormFieldDetail;

    console.log('fields: ', formattedFields);

    try {
      const res = (await AppFormFieldControllerCreateModel(
        formattedFields,
      )) as unknown as API.BaseResponseFormFieldDetail;

      if (res && res.success) {
        message.success('Add successfully');
        history.push('/form-field');
      }
    } catch (error) {
      console.log(error);
      message.error('Add failed');
    }
  };

  const handleValuesChange = (changedValues: any) => {
    // check typeFormat change
    if (!changedValues.typeFormat) return;

    if (isNumericType(changedValues.typeFormat)) {
      setFormatType('numeric');
    } else if (isStringType(changedValues.typeFormat)) {
      setFormatType('string');
    } else {
      setFormatType('rest');
    }
  };

  return (
    <PageContainer title="Tạo Form Field">
      <Card>
        <ProForm
          onFinish={async (values) => handleAddFormField(values)}
          onValuesChange={(changedValues) => handleValuesChange(changedValues)}
        >
          <ProForm.Group>
            <ProFormSelect
              name="typeFormat"
              label="Type format"
              placeholder="Please choose your format"
              width="md"
              valueEnum={typeFormatEnum}
              rules={[{ required: true, message: 'Type format must be required!' }]}
            />
            <ProFormText
              name="name"
              label="Name"
              placeholder="Name"
              width="md"
              rules={[{ required: true, message: 'Name must be required!' }]}
            />
            <ProFormText
              width="md"
              name="description"
              label="Description"
              placeholder="Description"
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormText name="icon" label="Icon" width="md" placeholder="Icon" />
            <ProFormText
              width="md"
              name="defaultValue"
              label="Default value"
              placeholder="Default value"
            />
            <ProFormText
              width="md"
              name="placeHolder"
              label="Placeholder"
              placeholder="Placeholder"
            />
          </ProForm.Group>
          <ProForm.Group>
            <ProFormText name="regEx" label="RegEx" width="md" placeholder="regEx" />
            <ProFormText
              width="md"
              name="replaceCharacter"
              label="Replace character"
              placeholder="Replace character"
            />
          </ProForm.Group>
          <ProForm.Group>
            {formatType === 'numeric' && (
              <ProFormDigitRange
                label="Range Value"
                name="rangeValue"
                width="sm"
                separator=" - "
                separatorWidth={60}
                placeholder={['Min value', 'Max value']}
              />
            )}
            {formatType === 'string' && (
              <ProFormDigitRange
                label="Range Length"
                name="rangeLength"
                width="sm"
                separator=" - "
                separatorWidth={60}
                placeholder={['Min length', 'Max length']}
              />
            )}
          </ProForm.Group>
          <ProForm.Group>
            <ProFormDigit width="xs" name="lines" label="Lines" />
            <ProFormSwitch name="editable" label="Editable" />
            <ProFormSwitch name="showUI" label="Show UI" />
            <ProFormSwitch name="required" label="Required" />
          </ProForm.Group>
        </ProForm>
      </Card>
    </PageContainer>
  );
};
