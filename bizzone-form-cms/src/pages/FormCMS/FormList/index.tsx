import { AppFormControllerGetInfoPageWithFilter } from '@/services/formCMSAPI/APPAPIFORM';
import { PageContainer, ProTable } from '@ant-design/pro-components';
// import { useDebounce } from 'ahooks';
import { Breadcrumb, Button, Card, Input, PageHeader } from 'antd';
import React, { useCallback, useMemo, useState } from 'react';
import { Link, useRequest } from 'umi';
import ColumnsTableFormField from './main/ColumnTableTrust';
import { SearchOutlined } from '@ant-design/icons';

import './index.less';

const FormList = () => {
  const pageSizeRef = React.useRef(10);

  const handleTableChange = useCallback(() => {}, []);

  const handleColumnsStateChange = () => {};

  const [length, setLength] = useState<number>();
  const [page, setPage] = useState<number>();
  const [pageSize, setPageSize] = useState<number>();

  const { run: runGetAllFormList } = useRequest(
    (
      params: API.AppFormControllerGetInfoPageWithFilterParams = {
        name: '',
        workspaceId: '98733478-c086-4826-818e-a5362078a67a',
        all: true,
        number: 0,
        size: 20,
      },
    ) => AppFormControllerGetInfoPageWithFilter(params),
    {
      manual: true,
      onSuccess(data) {
        console.log('data', data);
        const dataNews = data as API.BasePagingResponseFormInfo;
        if (dataNews) {
          setLength(dataNews?.totalElements);
        }
      },
      onError(error) {
        console.log('error', error);
      },
    },
  );

  //   useEffect(() => {
  //     runGetAllFormList();
  //   }, [runGetAllFormList]);

  //   const [valueinput, setValueInput] = useState('');
  //   const debouncedValue = useDebounce(valueinput, { wait: 500 });

  //   useEffect(() => {
  //     if (debouncedValue) {
  //       runGetAllCA({ phone: valueinput, page: 0, size: 10 });
  //     }
  //     if (valueinput === '') {
  //       runGetAllCA({ phone: '', page: 0, size: 10 });
  //     }
  //     // eslint-disable-next-line react-hooks/exhaustive-deps
  //   }, [debouncedValue]);

  //   function handleSearchChange(value: string) {
  //     setValueInput(value);
  //   }

  const BuildBreadcrumb = useCallback(() => {
    return (
      <Breadcrumb separator=">" className="breadcrumb-custom">
        <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
        <Breadcrumb.Item>Form CMS</Breadcrumb.Item>
        <Breadcrumb.Item href="/form-cms">Form Field List</Breadcrumb.Item>
      </Breadcrumb>
    );
  }, []);

  const BuildPageHeader = useMemo(() => {
    return (
      <PageHeader
        breadcrumbRender={BuildBreadcrumb}
        title="Danh Sách Form"
        extra={[
          <Link key="1" to={`/form-cms/form-detail?isCreated=1`}>
            <Button className="btn-custom btn-add">Tạo mới</Button>
          </Link>,
        ]}
        className="custom-page-header"
      >
        <div className="group-filter">
          <Input
            prefix={<SearchOutlined />}
            placeholder="Tìm kiếm"
            size="large"
            className="input-custom"
            type="text"
            name=""
            style={{ width: '250px' }}
            // onChange={(e) => handleSearchChange(e.target.value)}
            // value={valueinput}
            allowClear
          />
        </div>
      </PageHeader>
    );
  }, [BuildBreadcrumb]);

  return (
    <>
      <div className="wrapper-page-container">
        {BuildPageHeader}
        <PageContainer className="page-container-custom-v2" pageHeaderRender={false}>
          <Card
            style={{
              width: '100%',
            }}
          >
            <ProTable
              scroll={{ x: 1440 }}
              rowKey={'key-1'}
              onColumnsStateChange={handleColumnsStateChange}
              onDataSourceChange={handleTableChange}
              toolBarRender={false}
              defaultSize="middle"
              search={false}
              cardBordered={true}
              columns={ColumnsTableFormField({ page: page || 0, pageSize: pageSize || 0 })}
              //   dataSource={listCAActive}
              bordered={true}
              request={async (params = {}) => {
                const pageCurrent = params?.current ? Math.floor(params?.current - 1) : '0';
                const res = await runGetAllFormList({
                  name: '',
                  workspaceId: '0a763dfb-2562-4d7e-8313-d54a88ea4b72',
                  all: true,
                  number: +pageCurrent,
                  size: params.pageSize,
                });
                const newRes = res as API.BasePagingResponseFormInfo;
                console.log('res', res);
                return {
                  data: newRes?.content,
                  total: newRes?.totalElements,
                };
              }}
              pagination={{
                onChange(current, pageSizeOnchange) {
                  setPage(current);
                  setPageSize(pageSizeOnchange);
                },
                className: 'pagination-custom',
                locale: { items_per_page: '' },
                total: length,
                pageSize: pageSizeRef.current,
                showSizeChanger: true,
                showTotal: (total, range) =>
                  `Từ ${range[0]} đến ${range[1]} trên tổng số ${total} kết quả`,
              }}
            />
          </Card>
        </PageContainer>
      </div>
    </>
  );
};

export default FormList;
