import { Link } from 'umi';

export const ColumnsTableFormField = (props: { page: number; pageSize: number }) => {
  return [
    {
      title: 'STT',
      dataIndex: 'index',
      valueType: 'index',
      render: (_: any) => {
        return <>{Math.floor((props.page - 1) * props.pageSize) + _.props.text + 1}</>;
      },
    },
    {
      title: 'Tên Form',
      key: 'name',
      dataIndex: 'name',
      //   sorter: (a: API.InquiryCADto, b: API.InquiryCADto) =>
      //     a?.user?.fullName?.localeCompare(b?.user?.fullName as string),
      render: (_: any, record: API.FormInfo) => {
        return (
          <Link to={`/form-cms/form-detail?isCreated=0&id=${record.id}`}>
            <span style={{ textTransform: 'capitalize' }}>{record?.name}</span>
          </Link>
        );
      },
    },
    {
      title: 'Mô tả',
      key: 'description',
      dataIndex: 'description',
      //   sorter: (a: API.InquiryCADto, b: API.InquiryCADto) =>
      //     a?.user?.fullName?.localeCompare(b?.user?.fullName as string),
      render: (_: any, record: API.FormInfo) => {
        return <span>{record?.description}</span>;
      },
    },
    {
      title: 'Code Name',
      //   sorter: (a: any, b: any) => a.individualOrEnterprise - b.individualOrEnterprise,

      render: (_: any, record: API.FormInfo) => {
        return <span>{record?.codeName}</span>;
      },
    },
    {
      title: 'Icon',
      render: (_: any, record: API.FormInfo) => {
        return <span>{record?.icon ? record?.icon : 'NULL'}</span>;
      },
    },
  ];
};

export default ColumnsTableFormField;
