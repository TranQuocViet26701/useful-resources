import { PageHeader } from 'antd';
import type { BreadcrumbFormCMSProps } from '../BreadcrumbFormCMS';
import BreadcrumbFormCMS from '../BreadcrumbFormCMS';

export default function PageHeaderFormCMS({ itemList }: BreadcrumbFormCMSProps) {
  const { title } = itemList[itemList.length - 1];

  return <PageHeader breadcrumb={<BreadcrumbFormCMS itemList={itemList} />} title={title} />;
}
