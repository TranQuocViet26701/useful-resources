import { Breadcrumb } from 'antd';

interface BreadcrumbItem {
  href?: string;
  title: string;
}

export interface BreadcrumbFormCMSProps {
  itemList: BreadcrumbItem[];
}

export default function BreadcrumbFormCMS({ itemList }: BreadcrumbFormCMSProps) {
  return (
    <Breadcrumb separator=">" className="breadcrumb-custom">
      {itemList &&
        itemList.map((item) => (
          <Breadcrumb.Item key={item.title} href={item.href || undefined}>
            {item.title}
          </Breadcrumb.Item>
        ))}
    </Breadcrumb>
  );
}
