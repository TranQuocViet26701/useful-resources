import { Link } from 'umi';

export const ColumnsTableFormField = () => {
  return [
    {
      title: 'STT',
      dataIndex: 'index',
      valueType: 'index',
    },
    {
      title: 'Tên',
      key: 'name',
      dataIndex: 'name',
      //   sorter: (a: API.InquiryCADto, b: API.InquiryCADto) =>
      //     a?.user?.fullName?.localeCompare(b?.user?.fullName as string),
      render: (_: any, record: API.FormFieldDetail) => {
        return (
          <span>
            <Link to={`/form-cms/form-field/${record?.id}`}>{record?.name}</Link>
          </span>
        );
      },
    },
    {
      title: 'Mô tả',
      key: 'description',
      dataIndex: 'description',
      //   sorter: (a: API.InquiryCADto, b: API.InquiryCADto) =>
      //     a?.user?.fullName?.localeCompare(b?.user?.fullName as string),
      render: (_: any, record: API.FormFieldDetail) => {
        return <span>{record?.description}</span>;
      },
    },
    {
      title: 'Kiểu dữ liệu',
      //   sorter: (a: any, b: any) => a.individualOrEnterprise - b.individualOrEnterprise,

      render: (_: any, record: API.FormFieldDetail) => {
        return <span>{record?.typeFormat}</span>;
      },
    },
    {
      title: 'Điều kiện Regex',
      //   sorter: (a: API.InquiryCADto, b: API.InquiryCADto) =>
      //     a?.user?.fullName?.localeCompare(b?.user?.fullName as string),
      render: (_: any, record: API.FormFieldDetail) => {
        return <span>{record?.regEx}</span>;
      },
    },
    {
      title: 'Placeholder',
      //   sorter: (a: API.InquiryCADto, b: API.InquiryCADto) =>
      //     moment(a.dateBegin).unix() - moment(b.dateExpire).unix(),
      render: (_: any, record: API.FormFieldDetail) => {
        return <span>{record?.placeHolder ? record?.placeHolder : 'NULL'}</span>;
      },
    },
    {
      title: 'Icon',
      render: (_: any, record: API.FormFieldDetail) => {
        return <span>{record?.icon ? record?.icon : 'NULL'}</span>;
      },
    },
    {
      title: 'Giá trị mặc định',
      render: (_: any, record: API.FormFieldDetail) => {
        return <span>{record?.defaultValue ? record?.defaultValue : 'NULL'}</span>;
      },
    },
  ];
};

export default ColumnsTableFormField;
