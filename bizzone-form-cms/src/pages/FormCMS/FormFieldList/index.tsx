import { AppFormFieldControllerGetInfoPageWithFilter } from '@/services/formCMSAPI/APPAPIFORMFIELD';
import { PageContainer, ProTable } from '@ant-design/pro-components';
// import { useDebounce } from 'ahooks';
import { Breadcrumb, Button, Card, Input, PageHeader } from 'antd';
import React, { useCallback, useMemo, useState } from 'react';
import { Link, useRequest } from 'umi';
import ColumnsTableFormField from './main/ColumnTableTrust';

import './index.less';
import { SearchOutlined } from '@ant-design/icons';

const FormFieldList = () => {
  const pageSizeRef = React.useRef(10);

  const handleTableChange = useCallback(() => {}, []);

  const handleColumnsStateChange = () => {};

  const [length, setLength] = useState<number>();

  const { run: runGetAllFormField } = useRequest<API.BasePagingResponseFormFieldDetail>(
    (
      params = {
        name: '',
        workspaceId: '98733478-c086-4826-818e-a5362078a67a',
        number: 0,
        size: 20,
      },
    ) => AppFormFieldControllerGetInfoPageWithFilter(params),
    {
      manual: true,
      onSuccess(data) {
        console.log('data', data);
        const dataNews = data as API.BasePagingResponseFormFieldDetail;
        if (dataNews) {
          setLength(dataNews?.totalElements);
        }
      },
      onError(error) {
        console.log('error', error);
      },
    },
  );

  //   const [valueinput, setValueInput] = useState('');
  //   const debouncedValue = useDebounce(valueinput, { wait: 500 });

  //   useEffect(() => {
  //     if (debouncedValue) {
  //       runGetAllCA({ phone: valueinput, page: 0, size: 10 });
  //     }
  //     if (valueinput === '') {
  //       runGetAllCA({ phone: '', page: 0, size: 10 });
  //     }
  //     // eslint-disable-next-line react-hooks/exhaustive-deps
  //   }, [debouncedValue]);

  //   function handleSearchChange(value: string) {
  //     setValueInput(value);
  //   }

  const BuildBreadcrumb = useCallback(() => {
    return (
      <Breadcrumb separator=">" className="breadcrumb-custom">
        <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
        <Breadcrumb.Item>Form CMS</Breadcrumb.Item>
        <Breadcrumb.Item href="/form-cms/form-field">Form Field List</Breadcrumb.Item>
      </Breadcrumb>
    );
  }, []);

  const BuildPageHeader = useMemo(() => {
    return (
      <PageHeader
        breadcrumbRender={BuildBreadcrumb}
        title="Danh Sách FormField"
        extra={[
          <Link key="1" to={`/form-cms/form-field/create`}>
            <Button className="btn-custom btn-add">Tạo mới</Button>
          </Link>,
        ]}
        className="custom-page-header"
      >
        <div className="group-filter">
          <Input
            prefix={<SearchOutlined />}
            placeholder="Tìm kiếm"
            size="large"
            className="input-custom"
            type="text"
            name=""
            style={{ width: '250px' }}
            // onChange={(e) => handleSearchChange(e.target.value)}
            // value={valueinput}
            allowClear
          />
        </div>
      </PageHeader>
    );
  }, [BuildBreadcrumb]);

  return (
    <>
      <div className="wrapper-page-container">
        {BuildPageHeader}
        <PageContainer className="page-container-custom-v2" pageHeaderRender={false}>
          <Card
            style={{
              width: '100%',
            }}
          >
            <ProTable
              scroll={{ x: 1440 }}
              rowKey={'key-1'}
              onColumnsStateChange={handleColumnsStateChange}
              onDataSourceChange={handleTableChange}
              toolBarRender={false}
              defaultSize="middle"
              search={false}
              cardBordered={true}
              columns={ColumnsTableFormField()}
              bordered={true}
              request={async (params = {}) => {
                const pageCurrent = params?.current ? Math.floor(params?.current - 1) : '0';
                // return runGetAllFormField({
                //   name: '',
                //   workSpaceId: '0a763dfb-2562-4d7e-8313-d54a88ea4b72',
                //   number: pageCurrent,
                //   size: params.pageSize,
                // }) as API.BasePagingResponseFormFieldDetail;

                // return new Promise(() => {
                //   runGetAllFormField({
                //     name: '',
                //     workSpaceId: '0a763dfb-2562-4d7e-8313-d54a88ea4b72',
                //     number: pageCurrent,
                //     size: params.pageSize,
                //   });
                // });

                const res = (await runGetAllFormField({
                  name: '',
                  workspaceId: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
                  number: pageCurrent,
                  size: params.pageSize,
                })) as API.BasePagingResponseFormFieldDetail;

                return {
                  data: res?.content,
                  total: res?.totalElements,
                };
              }}
              pagination={{
                className: 'pagination-custom',
                locale: { items_per_page: '' },
                total: length,
                pageSize: pageSizeRef.current,
                showSizeChanger: true,
                showTotal: (total, range) =>
                  `Từ ${range[0]} đến ${range[1]} trên tổng số ${total} kết quả`,
              }}
            />
          </Card>
        </PageContainer>
      </div>
    </>
  );
};

export default FormFieldList;
