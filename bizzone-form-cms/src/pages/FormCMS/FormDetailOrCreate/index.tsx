import {
  AppFormControllerCreateModel,
  AppFormControllerGetDetailById,
  AppFormControllerUpdateModel,
} from '@/services/formCMSAPI/APPAPIFORM';
import { PageContainer, ProForm, ProFormText } from '@ant-design/pro-components';
import { Breadcrumb, Button, Card, Input, Modal, notification, PageHeader } from 'antd';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useLocation, useRequest } from 'umi';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import { ExclamationCircleOutlined, SearchOutlined } from '@ant-design/icons';

import './index.less';
import {
  AppFormFieldControllerDeleteModelById,
  AppFormFieldControllerGetInfoListWithFilter,
} from '@/services/formCMSAPI/APPAPIFORMFIELD';

interface typeColumnFromBackend {
  name: string;
  items: API.FormFieldDetail[];
}
const FormDetailOrCreate = () => {
  const location = useLocation();
  const isCreated = (location?.query?.isCreated as string) === '1';
  const idForm = location?.query?.id;

  const [resultDetailForm, setResultDetailForm] = useState<API.FactoryGetResponseUUIDFormDetail>();
  const [formDetail] = ProForm.useForm();

  const [columnsFromBackend, setColumnsFromBackend] = useState<typeColumnFromBackend[]>([
    {
      name: 'Danh sách Field',
      items: [],
    },
    {
      name: 'Danh sách Field trong Form',
      items: [],
    },
  ]);
  const onDragEnd = (result: any, columnsDragEnd: any, setColumnsDragEnd: any) => {
    if (!result.destination) return;
    const { source, destination } = result;

    if (source.droppableId !== destination.droppableId) {
      const sourceColumn = columnsDragEnd[source.droppableId];
      const destColumn = columnsDragEnd[destination.droppableId];
      const sourceItems = [...sourceColumn.items];
      const destItems = [...destColumn.items];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);
      setColumnsDragEnd({
        ...columnsDragEnd,
        [source.droppableId]: {
          ...sourceColumn,
          items: sourceItems,
        },
        [destination.droppableId]: {
          ...destColumn,
          items: destItems,
        },
      });
    } else {
      const column = columnsDragEnd[source.droppableId];
      const copiedItems = [...column.items];
      const [removed] = copiedItems.splice(source.index, 1);
      copiedItems.splice(destination.index, 0, removed);
      setColumnsDragEnd({
        ...columnsDragEnd,
        [source.droppableId]: {
          ...column,
          items: copiedItems,
        },
      });
    }
  };

  const { run: runGetFormFieldList, loading: loadingGetFormFieldList } = useRequest(
    (
      params: API.AppFormFieldControllerGetInfoListWithFilterParams = {
        workspaceId: '98733478-c086-4826-818e-a5362078a67a',
      },
    ) => AppFormFieldControllerGetInfoListWithFilter(params),
    {
      manual: true,
      onSuccess(data) {
        console.log('data runGetFormFieldList', data);
        const newData = data as API.FormFieldDetail[];
        console.log('columnsFromBackend[1]?.items', columnsFromBackend[1]?.items);

        const finalData = newData?.filter(function (objFromA) {
          return !columnsFromBackend[1]?.items?.find(function (objFromB) {
            return objFromA.id === objFromB.id;
          });
        });
        console.log('finalData', finalData);

        setColumnsFromBackend((props) => [
          {
            ...props[0],
            items: finalData,
            // items: newData,
          },
          {
            ...props[1],
          },
        ]);
      },
      onError(error) {
        console.log(error);
      },
    },
  );
  const { run: runGetDetailForm, loading: loadingGetDetailForm } = useRequest(
    (
      params: API.AppFormControllerGetDetailByIdParams = {
        id: idForm,
      },
    ) => AppFormControllerGetDetailById(params),
    {
      manual: true,
      onSuccess(data) {
        const newData = data as API.FactoryGetResponseUUIDFormDetail;
        setResultDetailForm(newData);

        // console.log('columnsFromBackend[1]?.items', columnsFromBackend[1]?.items);
        // const arrayFormFieldDetail = newData.formFieldDetails;
        // const finalDatacolumns1 = columnsFromBackend[1]?.items?.filter(function (objFromA) {
        //   return !arrayFormFieldDetail?.find(function (objFromB) {
        //     return objFromA.id === objFromB.id;
        //   });
        // });
        // console.log('finalDatacolumns1', finalDatacolumns1);
        // setColumnsFromBackend((props) => [
        //   {
        //     ...props[0],
        //     // items: [],
        //   },
        //   {
        //     ...props[1],
        //     items: arrayFormFieldDetail as API.FormFieldDetail[],
        //   },
        // ]);
        setColumnsFromBackend((props) => [
          {
            ...props[0],
            // items: [],
          },
          {
            ...props[1],
            items: newData?.formFieldDetails as API.FormFieldDetail[],
          },
        ]);
        formDetail.resetFields();
        runGetFormFieldList();
      },
      onError(error) {
        console.log(error);
      },
    },
  );

  const { run: runUpdateForm, loading: loadingUpdateForm } = useRequest(
    (params: API.FactoryUpdateRequestUUIDFormDetail) => AppFormControllerUpdateModel(params),
    {
      manual: true,
      onSuccess() {
        notification.success({
          message: `Cập nhật Form thành công`,
        });
      },
      onError(error) {
        console.log(error);
        notification.success({
          message: `Lỗi gọi API`,
        });
      },
    },
  );

  const { run: runCreateForm, loading: loadingCreateForm } = useRequest(
    (params: API.FactoryCreateRequestUUIDFormDetail) => AppFormControllerCreateModel(params),
    {
      manual: true,
      onSuccess() {
        notification.success({
          message: `Thêm mới Form thành công`,
        });
      },
      onError(error) {
        console.log(error);
        notification.success({
          message: `Lỗi gọi API`,
        });
      },
    },
  );

  const { run: runDelForm, loading: loadingDelForm } = useRequest(
    (params: API.AppFormFieldControllerDeleteModelByIdParams) =>
      AppFormFieldControllerDeleteModelById(params),
    {
      manual: true,
      onSuccess() {
        notification.success({
          message: `Xóa Form thành công`,
        });
      },
      onError(error) {
        console.log(error);
        notification.success({
          message: `Lỗi gọi API`,
        });
      },
    },
  );

  const handleDelForm = () => {
    Modal.confirm({
      title: 'Confirm',
      icon: <ExclamationCircleOutlined />,
      content: 'Bạn có chắc muốn xóa Form? Thao tác này không thể quay lại',
      okText: 'Đồng ý',
      cancelText: 'Hủy',
      onOk: () => {
        runDelForm({ id: resultDetailForm?.id as string });
        // history.push('/form-cms/form-list');
      },
    });
  };

  useEffect(() => {
    if (!isCreated) {
      runGetDetailForm();
    }
    runGetFormFieldList();
  }, [isCreated, runGetDetailForm, runGetFormFieldList]);

  const BuildBreadcrumb = useCallback(() => {
    return (
      <Breadcrumb separator=">" className="breadcrumb-custom">
        <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
        <Breadcrumb.Item>Form CMS</Breadcrumb.Item>
        <Breadcrumb.Item href="/sale-manager/ticket-list">Form Detail</Breadcrumb.Item>
      </Breadcrumb>
    );
  }, []);

  const BuildPageHeader = useMemo(() => {
    return (
      <PageHeader
        breadcrumbRender={BuildBreadcrumb}
        title="Form Detail"
        className="custom-page-header"
      >
        <div className="group-filter" />
      </PageHeader>
    );
  }, [BuildBreadcrumb]);

  return (
    <>
      <div className="wrapper-page-container">
        {BuildPageHeader}
        <PageContainer className="page-container-custom-v2" pageHeaderRender={false}>
          <Card
            style={{
              width: '100%',
            }}
            loading={
              loadingGetDetailForm ||
              loadingUpdateForm ||
              loadingGetFormFieldList ||
              loadingCreateForm ||
              loadingDelForm
            }
          >
            <div className="wrapper-detail-ticket">
              <ProForm
                form={formDetail}
                submitter={{
                  render: ({ form, reset }) => {
                    return [
                      <Button
                        key="rest"
                        className={`${isCreated ? 'disabled-btn' : ''}`}
                        onClick={() => handleDelForm()}
                      >
                        Xóa Form
                      </Button>,
                      <Button
                        key="rest"
                        className="btn-decline-modal"
                        onClick={(e) => {
                          reset();
                          e.preventDefault();
                        }}
                      >
                        Reset
                      </Button>,
                      <Button
                        key="next"
                        className="btn-accept-modal"
                        onClick={(e) => {
                          form?.submit();
                          e.preventDefault();
                        }}
                      >
                        {isCreated ? 'Tạo mới' : 'Cập nhật'}
                      </Button>,
                    ];
                  },
                }}
                onValuesChange={(_, values) => {
                  console.log(values);
                }}
                onFinish={async (value) => {
                  console.log(value);
                  if (isCreated) {
                    if (columnsFromBackend[1]?.items.length < 1) {
                      notification.error({
                        message: `Vui lòng kéo thả Form Field thuộc Form này`,
                      });
                    } else {
                      const arrayId = new Array();
                      columnsFromBackend[1].items.forEach((v) => arrayId.push(v.id));
                      Modal.confirm({
                        title: 'Confirm',
                        icon: <ExclamationCircleOutlined />,
                        content: 'Bạn có chắc muốn tạo mới?',
                        okText: 'Đồng ý',
                        cancelText: 'Hủy',
                        onOk: () => {
                          runCreateForm({
                            name: value?.name,
                            description: value?.description,
                            icon: value.icon,
                            workspaceId: '98733478-c086-4826-818e-a5362078a67a',
                            formFieldIds: arrayId,
                            codeName: value.codeName,
                          });
                        },
                      });
                    }
                  } else {
                    const arrayId = new Array();
                    columnsFromBackend[1].items.forEach((v) => arrayId.push(v.id));
                    Modal.confirm({
                      title: 'Confirm',
                      icon: <ExclamationCircleOutlined />,
                      content: 'Bạn có chắc muốn cập nhật?',
                      okText: 'Đồng ý',
                      cancelText: 'Hủy',
                      onOk: () => {
                        runUpdateForm({
                          id: resultDetailForm?.id?.toString() || '',
                          data: {
                            name: value?.name,
                            description: value?.description,
                            icon: value?.icon,
                            workspaceId: '98733478-c086-4826-818e-a5362078a67a',
                            codeName: resultDetailForm?.codeName || '',
                            formFieldIds: arrayId,
                          },
                        });
                      },
                    });
                  }

                  return true;
                }}
              >
                <ProForm.Group>
                  <ProFormText
                    name="name"
                    label="Tên"
                    // tooltip="最长为 24 位"
                    placeholder="Tên Form"
                    initialValue={!isCreated && resultDetailForm ? resultDetailForm.name : ''}
                    rules={[{ required: true, message: 'Vui lòng nhập tên Form' }]}
                  />
                  <ProFormText
                    width="md"
                    name="description"
                    label="Mô tả"
                    placeholder="Mô tả"
                    initialValue={
                      !isCreated && resultDetailForm ? resultDetailForm.description : ''
                    }
                    rules={[{ required: true, message: 'Vui lòng nhập mô tả' }]}
                  />
                  <ProFormText
                    width="md"
                    name="icon"
                    label="Icon"
                    placeholder="Icon"
                    initialValue={!isCreated && resultDetailForm ? resultDetailForm.icon : ''}
                    rules={[{ required: true, message: 'Vui lòng nhập icon' }]}
                  />
                  <ProFormText
                    width="md"
                    name="codeName"
                    label="CodeName"
                    placeholder="codeName"
                    initialValue={!isCreated && resultDetailForm ? resultDetailForm.codeName : ''}
                    rules={[{ required: true, message: 'Vui lòng nhập codeName' }]}
                  />
                </ProForm.Group>
                <div style={{ display: 'flex', justifyContent: 'left' }}>
                  <DragDropContext
                    onDragEnd={(result) =>
                      onDragEnd(result, columnsFromBackend, setColumnsFromBackend)
                    }
                  >
                    {Object.entries(columnsFromBackend).map(([columnId, column]) => {
                      return (
                        <div key={columnId} className="wrapper-dragdrop">
                          <h2 className="title-dragdrop">{column.name}</h2>
                          <div style={{ margin: 8 }}>
                            <Droppable
                              droppableId={columnId}
                              key={columnId}
                              //   isCombineEnabled={false}
                              //   isDropDisabled={true}
                            >
                              {(provided, snapshot) => {
                                return (
                                  <div
                                    {...provided.droppableProps}
                                    ref={provided.innerRef}
                                    style={{
                                      background: snapshot.isDraggingOver
                                        ? 'lightblue'
                                        : 'rgb(235, 236, 240)',
                                    }}
                                    className="wrapper-dragdrop-context"
                                    key={columnId}
                                  >
                                    <Input
                                      prefix={<SearchOutlined />}
                                      placeholder="Tìm kiếm"
                                      //   size="large"
                                      className="input-custom"
                                      type="text"
                                      name=""
                                      // onChange={(e) => handleSearchChange(e.target.value)}
                                      // value={valueinput}
                                      allowClear
                                    />
                                    {column?.items.map((item, index) => {
                                      return (
                                        <>
                                          <Draggable
                                            key={item.id}
                                            draggableId={item.id as string}
                                            index={index}
                                          >
                                            {(providedDraggable, snapshotDraggable) => {
                                              return (
                                                <div
                                                  ref={providedDraggable.innerRef}
                                                  {...providedDraggable.draggableProps}
                                                  {...providedDraggable.dragHandleProps}
                                                  style={{
                                                    backgroundColor: snapshotDraggable.isDragging
                                                      ? '#91d5ff'
                                                      : 'rgb(255, 255, 255)',
                                                    ...providedDraggable.draggableProps.style,
                                                  }}
                                                  className="wrapprer-draggable"
                                                >
                                                  {item.name}
                                                </div>
                                              );
                                            }}
                                          </Draggable>
                                        </>
                                      );
                                    })}
                                    {provided.placeholder}
                                  </div>
                                );
                              }}
                            </Droppable>
                          </div>
                        </div>
                      );
                    })}
                  </DragDropContext>
                </div>
              </ProForm>
            </div>
          </Card>
        </PageContainer>
      </div>
    </>
  );
};

export default FormDetailOrCreate;
