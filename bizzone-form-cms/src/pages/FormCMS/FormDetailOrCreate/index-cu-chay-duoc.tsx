import { AppFormControllerGetDetailById } from '@/services/formCMSAPI/APPAPIFORM';
import { PageContainer, ProForm, ProFormText } from '@ant-design/pro-components';
import { Breadcrumb, Button, Card, Input, PageHeader } from 'antd';
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useLocation, useRequest } from 'umi';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import './index.less';
import { SearchOutlined } from '@ant-design/icons';

const FormDetailOrCreate = () => {
  const location = useLocation();
  const isCreated = (location?.query?.isCreated as string) === '1';
  const idForm = location?.query?.id;

  const [resultDetailForm, setResultDetailForm] = useState<API.FactoryGetResponseUUIDFormDetail>();
  const [formDetail] = ProForm.useForm();

  const itemsFromBackend = [
    { id: 1, content: 'First task' },
    { id: 2, content: 'Second task' },
    { id: 3, content: 'Third task' },
    { id: 4, content: 'Fourth task' },
    { id: 5, content: 'Fifth task' },
    { id: 6, content: 'First task' },
    { id: 7, content: 'Second task' },
    { id: 8, content: 'Third task' },
    { id: 9, content: 'Fourth task' },
    { id: 10, content: 'Fifth task' },
  ];

  const columnsFromBackend = [
    {
      name: 'Danh sách Field',
      items: itemsFromBackend,
    },
    {
      name: 'Danh sách Field trong Form',
      items: [],
    },
  ];
  const [columns, setColumns] = useState(columnsFromBackend);

  const onDragEnd = (result: any, columnsDragEnd: any, setColumnsDragEnd: any) => {
    if (!result.destination) return;
    const { source, destination } = result;

    if (source.droppableId !== destination.droppableId) {
      const sourceColumn = columnsDragEnd[source.droppableId];
      const destColumn = columnsDragEnd[destination.droppableId];
      const sourceItems = [...sourceColumn.items];
      const destItems = [...destColumn.items];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);
      setColumnsDragEnd({
        ...columnsDragEnd,
        [source.droppableId]: {
          ...sourceColumn,
          items: sourceItems,
        },
        [destination.droppableId]: {
          ...destColumn,
          items: destItems,
        },
      });
    } else {
      const column = columnsDragEnd[source.droppableId];
      const copiedItems = [...column.items];
      const [removed] = copiedItems.splice(source.index, 1);
      copiedItems.splice(destination.index, 0, removed);
      setColumnsDragEnd({
        ...columnsDragEnd,
        [source.droppableId]: {
          ...column,
          items: copiedItems,
        },
      });
    }
  };

  const { run: runGetDetailForm, loading: loadingGetDetailForm } = useRequest(
    (
      params: API.AppFormControllerGetDetailByIdParams = {
        id: idForm,
      },
    ) => AppFormControllerGetDetailById(params),
    {
      manual: true,
      onSuccess(data) {
        console.log('data', data);
        setResultDetailForm(data as API.FactoryGetResponseUUIDFormDetail);
        formDetail.resetFields();
      },
      onError(error) {
        console.log(error);
      },
    },
  );

  useEffect(() => {
    if (!isCreated) {
      runGetDetailForm();
    }
  }, [isCreated, runGetDetailForm]);

  const BuildBreadcrumb = useCallback(() => {
    return (
      <Breadcrumb separator=">" className="breadcrumb-custom">
        <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
        <Breadcrumb.Item>Form CMS</Breadcrumb.Item>
        <Breadcrumb.Item href="/sale-manager/ticket-list">Form Detail</Breadcrumb.Item>
      </Breadcrumb>
    );
  }, []);

  const BuildPageHeader = useMemo(() => {
    return (
      <PageHeader
        breadcrumbRender={BuildBreadcrumb}
        title="Form Detail"
        className="custom-page-header"
      >
        <div className="group-filter" />
      </PageHeader>
    );
  }, [BuildBreadcrumb]);

  return (
    <>
      <div className="wrapper-page-container">
        {BuildPageHeader}
        <PageContainer className="page-container-custom-v2" pageHeaderRender={false}>
          <Card
            style={{
              width: '100%',
            }}
            loading={loadingGetDetailForm}
          >
            <div className="wrapper-detail-ticket">
              <ProForm
                form={formDetail}
                submitter={{
                  render: ({ form, reset }) => {
                    return [
                      <Button
                        key="rest"
                        className="btn-decline-modal"
                        onClick={(e) => {
                          reset();
                          e.preventDefault();
                        }}
                      >
                        Hủy bỏ
                      </Button>,
                      <Button
                        key="next"
                        className="btn-accept-modal"
                        onClick={(e) => {
                          form?.submit();
                          e.preventDefault();
                        }}
                      >
                        {isCreated ? 'Tạo mới' : 'Cập nhật'}
                      </Button>,
                    ];
                  },
                }}
                onValuesChange={(_, values) => {
                  console.log(values);
                }}
                onFinish={async (value) => {
                  console.log(value);
                  return true;
                }}
              >
                <ProForm.Group>
                  <ProFormText
                    name="name"
                    label="Tên"
                    tooltip="最长为 24 位"
                    placeholder="Tên Form"
                    initialValue={!isCreated && resultDetailForm ? resultDetailForm.name : ''}
                  />
                  <ProFormText
                    width="md"
                    name="description"
                    label="Mô tả"
                    placeholder="Mô tả"
                    initialValue={
                      !isCreated && resultDetailForm ? resultDetailForm.description : ''
                    }
                  />
                  <ProFormText
                    width="md"
                    name="icon"
                    label="Icon"
                    placeholder="Icon"
                    initialValue={!isCreated && resultDetailForm ? resultDetailForm.icon : ''}
                  />
                </ProForm.Group>
                <div style={{ display: 'flex', justifyContent: 'left', marginBottom: 24 }}>
                  <DragDropContext onDragEnd={(result) => onDragEnd(result, columns, setColumns)}>
                    {Object.entries(columns).map(([columnId, column]) => {
                      return (
                        <div key={columnId} className="wrapper-dragdrop">
                          <h2 className="title-dragdrop">{column.name}</h2>
                          <div style={{ margin: 8 }}>
                            <Droppable droppableId={columnId} key={columnId}>
                              {(provided, snapshot) => {
                                return (
                                  <div
                                    {...provided.droppableProps}
                                    ref={provided.innerRef}
                                    style={{
                                      background: snapshot.isDraggingOver
                                        ? 'lightblue'
                                        : 'rgb(235, 236, 240)',
                                    }}
                                    className="wrapper-dragdrop-context"
                                  >
                                    <Input
                                      prefix={<SearchOutlined />}
                                      placeholder="Tìm kiếm"
                                      //   size="large"
                                      className="input-custom"
                                      type="text"
                                      name=""
                                      // onChange={(e) => handleSearchChange(e.target.value)}
                                      // value={valueinput}
                                      allowClear
                                    />
                                    {column?.items.map((item, index) => {
                                      return (
                                        <>
                                          <Draggable
                                            key={item.id}
                                            draggableId={item.id.toString()}
                                            index={index}
                                          >
                                            {(providedDraggable, snapshotDraggable) => {
                                              return (
                                                <div
                                                  ref={providedDraggable.innerRef}
                                                  {...providedDraggable.draggableProps}
                                                  {...providedDraggable.dragHandleProps}
                                                  style={{
                                                    backgroundColor: snapshotDraggable.isDragging
                                                      ? '#91d5ff'
                                                      : 'rgb(255, 255, 255)',
                                                    ...providedDraggable.draggableProps.style,
                                                  }}
                                                  className="wrapprer-draggable"
                                                >
                                                  {item.content}
                                                </div>
                                              );
                                            }}
                                          </Draggable>
                                        </>
                                      );
                                    })}
                                    {provided.placeholder}
                                  </div>
                                );
                              }}
                            </Droppable>
                          </div>
                        </div>
                      );
                    })}
                  </DragDropContext>
                </div>
              </ProForm>
            </div>
          </Card>
        </PageContainer>
      </div>
    </>
  );
};

export default FormDetailOrCreate;
