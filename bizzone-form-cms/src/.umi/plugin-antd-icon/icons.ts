// @ts-nocheck

import MenuUnfoldOutlined from '@ant-design/icons/MenuUnfoldOutlined';
import CrownOutlined from '@ant-design/icons/CrownOutlined';
import SmileOutlined from '@ant-design/icons/SmileOutlined'

export default {
  MenuUnfoldOutlined,
CrownOutlined,
SmileOutlined
}
    