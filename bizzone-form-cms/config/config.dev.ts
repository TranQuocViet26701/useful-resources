// https://umijs.org/config/
import { defineConfig } from 'umi';

export default defineConfig({
  plugins: [
    // https://github.com/zthxxx/react-dev-inspector
    'react-dev-inspector/plugins/umi/react-inspector',
  ],
  // https://github.com/zthxxx/react-dev-inspector#inspector-loader-props
  inspectorConfig: {
    exclude: [],
    babelPlugins: [],
    babelOptions: {},
  },
  define: {
    API_ENDPOINT: 'https://api-dev.bizzone.unicloud.ai/form',
    // API_ENDPOINT: 'http://10.30.1.59:8080',
    API_PREFIX: '',
  },
  openAPI: [
    {
      requestLibPath: "import { request } from '@/utils/request'",
      // 或者使用在线的版本
      schemaPath: 'https://api-dev.bizzone.unicloud.ai/form/v3/api-docs',
      projectName: 'formCMSAPI',
      // schemaPath: join(__dirname, 'oneapi.json'),
      mock: false,
    },
  ],
});
