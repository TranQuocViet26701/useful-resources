﻿export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        name: 'login',
        path: '/user/login',
        component: './user/Login',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/form-cms',
    name: 'Quản-lý-Form',
    icon: 'MenuUnfoldOutlined',
    access: 'canAdmin',
    routes: [
      {
        path: 'form-field',
        name: 'Danh sách Form Field',
        component: './FormCMS/FormFieldList',
      },
      {
        path: 'form-field/create',
        component: './FormCMS/FormFieldCreation',
      },
      {
        path: 'form-field/:formFieldId',
        component: './FormCMS/FormFieldDetail',
      },
      {
        path: '/form-cms/form-list',
        name: 'Danh sách Form',
        component: './FormCMS/FormList',
      },
      {
        path: '/form-cms/form-detail',
        name: 'Chi tiết form',
        component: './FormCMS/FormDetailOrCreate',
        hideInMenu: true,
      },
    ],
  },
  {
    path: '/admin',
    name: 'admin',
    icon: 'crown',
    access: 'canAdmin',
    routes: [
      {
        path: '/admin/sub-page',
        name: 'sub-page',
        icon: 'smile',
        component: './Welcome',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/',
    redirect: '/form-cms/form-field',
  },
  {
    component: './404',
  },
];
