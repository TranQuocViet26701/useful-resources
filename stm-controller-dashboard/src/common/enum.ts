export enum MachineType {
  STM = 'STM',
  ATM = 'ATM',
  CDM = 'CDM',
  UNKNOWN = 'UNKNOWN',
}

export enum KeyType {
  KEY_3DES = 'KEY_3DES',
}

export enum Protocol {
  NDC = 'NDC',
}

export enum DenominationRule {
  'Minimum Notes' = 'MINIMUM_NOTES',
  'Equal Emptying' = 'EQUAL_EMPTYING',
  'Maximum Notes' = 'MAXIMUM_NOTES',
}
