export default {
  unitForm_createForm_title: 'Tạo đơn vị quản lý mới',
  unitForm_updateForm_title: 'Chỉnh sửa đơn vị quản lý',

  form_inputGroup_unitCode_title: 'Mã đơn vị',
  form_inputGroup_unitCode_placeholder: 'Nhập mã đơn vị',

  form_inputGroup_unitName_title: 'Tên đơn vị',
  form_inputGroup_unitName_placeholder: 'Nhập tên đơn vị',

  form_inputGroup_unitLocation_title: 'Khu vực',
  form_inputGroup_unitLocation_placeholder: 'Chọn khu vực',

  form_inputGroup_unitProvince_title: 'Tỉnh/Thành phố',
  form_inputGroup_unitProvince_placeholder: 'Chọn Tỉnh/Thành phố',

  form_inputGroup_unitDistrict_title: 'Quận/Huyện',
  form_inputGroup_unitDistrict_placeholder: 'Chọn Quận/Huyện',

  form_inputGroup_unitWard_title: 'Phường/Xã',
  form_inputGroup_unitWard_placeholder: 'Chọn Phường/Xã',

  form_inputGroup_unitAddress_title: 'Tên đường, số nhà',
  form_inputGroup_unitAddress_placeholder: 'Nhập địa chỉ',

  createUnit_successStatus_message: 'Thêm đơn vị mới thành công',
  updateUnit_successStatus_message: 'Chỉnh sửa đơn vị thành công',
  deleteUnit_successStatus_message: 'Xoá đơn vị thành công',
};
