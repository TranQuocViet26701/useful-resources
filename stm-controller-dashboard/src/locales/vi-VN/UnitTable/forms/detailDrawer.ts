export default {
  detailDrawer_title: 'Chi tiết đơn vị',

  detailDrawer_infoCard_title: 'Thông tin đơn vị',
  detailDrawer_infoCard_unitCodeName: 'Mã - Tên đơn vị',
  detailDrawer_infoCard_unitAddress: 'Địa chỉ đơn vị',

  detailDrawer_userCard_title: 'Danh sách người dùng',
  detailDrawer_userCard_columnGroup_staffName: 'Tên nhân viên',
  detailDrawer_userCard_columnGroup_staffCode: 'Mã nhân viên',
  detailDrawer_userCard_columnGroup_staffPhoneNumber: 'Số điện thoại',
  detailDrawer_userCard_columnGroup_staffEmail: 'Email',
  detailDrawer_userCard_columnGroup_staffStatus: 'Trạng thái',

  detailDrawer_machineCard_title: 'Danh sách máy quản lý',
  detailDrawer_machineCard_columnGroup_machineName: 'Tên máy',
  detailDrawer_machineCard_columnGroup_machineCode: 'Terminal ID',
  detailDrawer_machineCard_columnGroup_machineIPAddress: 'Địa chỉ IP',
  detailDrawer_machineCard_columnGroup_machineStatus: 'Tình trạng',
};
