export default {
  managementUnit_tableTitle: 'Danh sách đơn vị quản lý',
  managementUnit_tableColumn_unitCode: 'Mã đơn vị',
  managementUnit_tableColumn_unitName: 'Tên đơn vị',
  managementUnit_tableColumn_location: 'Khu vực',
  managementUnit_tableColumn_province: 'Tỉnh/Thành',
  managementUnit_tableColumn_district: 'Quận/Huyện',
  managementUnit_tableColumn_ward: 'Phường/Xã',
  managementUnit_tableColumn_address: 'Tên đường/Số nhà',
};
