export default {
  'actiStatisDetail.title': 'Chi tiết hoạt động',
  'actiStatisDetail.cardTitle': 'Thông tin máy',
  'actiStatisDetail.machineStatus': 'Tình trạng máy',
  'actiStatisDetail.machineAddress': 'Địa chỉ máy',
};
