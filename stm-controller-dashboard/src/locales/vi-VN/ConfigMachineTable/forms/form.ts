export default {
  configMachine_createForm_title: 'Tạo mới dòng máy',
  configMachine_updateForm_title: 'Chỉnh sửa cấu hình dòng máy',

  form_inputGroup_machineType_title: 'Loại máy',
  form_inputGroup_machineType_placeholder: 'Chọn loại máy',

  form_inputGroup_machineSeries_title: 'Tên dòng máy',
  form_inputGroup_machineSeries_placeholder: 'Nhập tên dòng máy',

  form_editableTable_columnGroup_headerTitle: 'Header',
  form_editableTable_columnGroup_unitTitle: 'Đơn vị tính',
  form_editableTable_columnGroup_miniCapacity: 'Sức chứa tối thiểu ',

  createConfigMachine_successStatus_message: 'Thêm cấu hình mới thành công',
  updateConfigMachine_successStatus_message: 'Chỉnh sửa cấu hình dòng máy thành công ',
  deleteConfigMachine_successStatus_message: 'Delete model configuration successfully',
};
