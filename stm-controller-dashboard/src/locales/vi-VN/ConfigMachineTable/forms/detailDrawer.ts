export default {
  detailDrawer_title: 'Chi tiết cấu hình dòng máy',

  detailDrawer_infoCard_title: 'Thông tin dòng máy',
  detailDrawer_inputGroup_machineType_tile: 'Loại máy',
  detailDrawer_inputGroup_machineType_placeholder: 'Lorem Ipsum',
  detailDrawer_inputGroup_machineSeries_tile: 'Dòng máy',
  detailDrawer_inputGroup_machineSeries_placeholder: 'Tên dòng máy',

  detailDrawer_configCard_title: 'Cấu hình',
  detailDrawer_configCard_columnGroup_deviceType: 'Loại thiết bị',
  detailDrawer_configCard_columnGroup_unit: 'Đơn vị tính ',
  detailDrawer_configCard_columnGroup_miniCapacity: 'Sức chứa tối thiểu',

  update_configMachine_cta_delete_question: 'Bạn có chắc chắn muốn xóa nhóm quyền',
};
