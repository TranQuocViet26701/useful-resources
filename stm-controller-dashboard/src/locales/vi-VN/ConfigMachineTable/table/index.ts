export default {
  configMachine_tableTitle: 'Cấu hình dòng máy',
  configMachine_tableColumn_machineType: 'Loại máy',
  configMachine_tableColumn_machineSeries: 'Dòng máy ',
  configMachine_tableColumn_machineCreatedBy: 'Người tạo',
};
