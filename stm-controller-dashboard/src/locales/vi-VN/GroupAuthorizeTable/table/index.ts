export default {
  roleGroup_tableTitle: 'Danh sách nhóm quyền',
  roleGroup_tableColumn_roleGroupName: 'Tên nhóm quyền',
  roleGroup_tableColumn_roleGroupOwner: 'Nhân viên sở hữu nhóm quyền',
  roleGroup_tableColumn_roleGroupCreatedBy: 'Người tạo',
};
