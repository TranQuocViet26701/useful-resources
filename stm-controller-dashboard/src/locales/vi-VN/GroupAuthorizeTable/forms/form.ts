export default {
  roleGroup_createForm_title: 'Tạo nhóm quyền',
  roleGroup_updateForm_title: 'Chỉnh sửa nhóm quyền',

  form_inputGroup_roleGroupName_title: 'Tên nhóm quyền',
  form_inputGroup_roleGroupName_placeholder: 'Nhập tên nhóm quyền',

  form_inputGroup_roleGroupList_title: 'Danh sách quyền',

  createRoleGroup_successStatus_message: 'Thêm nhóm quyền mới thành công',
  updateRoleGroup_successStatus_message: 'Chỉnh sửa nhóm quyền thành công',
  deleteRoleGroup_successStatus_message: 'Xoá nhóm quyền thành công',
};
