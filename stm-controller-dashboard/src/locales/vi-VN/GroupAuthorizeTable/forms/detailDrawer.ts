export default {
  detailDrawer_title: 'Chi tiết nhóm quyền',

  detailDrawer_inputGroup_tile: 'Tên nhóm quyền',
  detailDrawer_inputGroup_placeholder: 'Lorem Ipsum',

  detailDrawer_correspondingRole_cardTile: 'Quyền tương ứng',

  detailDrawer_roleGroup_ownerCard_title: 'Nhân viên sở hữu nhóm quyền',
  detailDrawer_roleGroup_ownerCard_columnGroup_staffCode: 'Mã NV',
  detailDrawer_roleGroup_ownerCard_columnGroup_staffName: 'Họ và tên',
  detailDrawer_roleGroup_ownerCard_columnGroup_staffPhoneNumber: 'Số điện thoại',
  detailDrawer_roleGroup_ownerCard_columnGroup_staffEmail: 'Email',
  detailDrawer_roleGroup_ownerCard_columnGroup_staffStatus: 'Trạng thái',
};
