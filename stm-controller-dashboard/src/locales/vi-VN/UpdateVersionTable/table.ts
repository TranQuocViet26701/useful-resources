export default {
  'updateVersionTable.title': 'Danh sách phiên bản hệ thống',
  'updateVersionTable.versionName': 'Tên phiên bản',
  'updateVersionTable.description': 'Nội dung',
  'updateVersionTable.updateCondition': 'Điều kiện nâng cấp',
  'updateVersionTable.date': 'Thời gian tải lên',
};
