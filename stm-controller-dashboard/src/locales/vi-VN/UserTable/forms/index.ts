export default {
  'userTable.form.title.newUser': 'Tạo người dùng mới',
  'userTable.form.title.updateUser': 'Chỉnh sửa người dùng',
  'userTable.form.roleTitle': 'Quyền tương ứng',
  'userTable.form.avatar': 'Ảnh đại diện',
  'userTable.form.button.uploadImage': 'Tải ảnh lên',
  'userTable.form.button.removeImage': 'Xóa ảnh',
  'userTable.form.placeholder.staffCode': 'Nhập mã nhân viên',
  'userTable.form.placeholder.staffName': 'Nhập tên nhân viên',
  'userTable.form.placeholder.phoneNumber': 'Nhập số điện thoại',
  'userTable.form.placeholder.email': 'Nhập email',
  'userTable.form.placeholder.managementUnit': 'Chọn đơn vị',
  'userTable.form.placeholder.roleGroup': 'Chọn nhóm quyền',
};
