export default {
  'userList.tables.headCell.index': 'STT',
  'userList.tables.headCell.staffId': 'Mã nhân viên',
  'userList.tables.headCell.name': 'Tên nhân viên',
  'userList.tables.headCell.managementUnit': 'Mã - Tên đơn vị',
  'userList.tables.headCell.email': 'Email',
  'userList.tables.headCell.phoneNumber': 'Số điện thoại',
  'userList.tables.headCell.status': 'Trạng thái hoạt động',
};
