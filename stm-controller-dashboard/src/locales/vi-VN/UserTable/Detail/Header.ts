export default {
  'userTable.detail.header.title.personal': 'Chi tiết cá nhân',
  'userTable.detail.header.title.user': 'Chi tiết người dùng',
  'userTable.detail.header.button.changePassword': 'Thay đổi mật khẩu',
  'userTable.detail.header.tooltip.unBlock': 'Mở khóa người dùng',
  'userTable.detail.header.tooltip.block': 'Tạm khóa người dùng',
  'userTable.detail.header.lockModal.title': 'Tạm khoá người dùng',
  'userTable.detail.header.lockModal.desc1': 'Bạn có chắc chắn muốn tạm khóa',
  'userTable.detail.header.lockModal.desc2': 'Người dùng này sẽ không thể truy cập vào hệ thống.',
};
