export default {
  'userTable.detail.userInfoRow.title': 'Thông tin người dùng',
  'userTable.detail.userInfoRow.staffName': 'Mã - Tên nhân viên quản lý',
  'userTable.detail.userInfoRow.phoneNumber': 'Số điện thoại',
  'userTable.detail.userInfoRow.email': 'Email',
  'userTable.detail.roleGroup.title': 'Nhóm quyền sở hữu',
};
