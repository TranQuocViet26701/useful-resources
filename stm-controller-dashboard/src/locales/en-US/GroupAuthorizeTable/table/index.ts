export default {
  roleGroup_tableTitle: 'Permissions',
  roleGroup_tableColumn_roleGroupName: 'Name Permissions',
  roleGroup_tableColumn_roleGroupOwner: 'Owner',
  roleGroup_tableColumn_roleGroupCreatedBy: 'Create by',
};
