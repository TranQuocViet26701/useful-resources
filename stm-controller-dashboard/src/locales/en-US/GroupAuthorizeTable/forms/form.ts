export default {
  roleGroup_createForm_title: 'Create permissions',
  roleGroup_updateForm_title: 'Edit role group',

  form_inputGroup_roleGroupName_title: 'Permissions name',
  form_inputGroup_roleGroupName_placeholder: 'Fill in permissions name',

  form_inputGroup_roleGroupList_title: 'List of permissions',

  createRoleGroup_successStatus_message: 'New permission group added successfully',
  updateRoleGroup_successStatus_message: 'Edit permission group successfully',
  deleteRoleGroup_successStatus_message: 'Delete permission group successfully',
};
