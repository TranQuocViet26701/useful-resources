export default {
  detailDrawer_title: 'Role Group Detail',

  detailDrawer_inputGroup_tile: 'Permissions name',
  detailDrawer_inputGroup_placeholder: 'Lorem Ipsum',

  detailDrawer_correspondingRole_cardTile: 'Corresponding Role',

  detailDrawer_roleGroup_ownerCard_title: 'Owner',
  detailDrawer_roleGroup_ownerCard_columnGroup_staffCode: 'Staff Code',
  detailDrawer_roleGroup_ownerCard_columnGroup_staffName: 'Fullname',
  detailDrawer_roleGroup_ownerCard_columnGroup_staffPhoneNumber: 'Phone number',
  detailDrawer_roleGroup_ownerCard_columnGroup_staffEmail: 'Email',
  detailDrawer_roleGroup_ownerCard_columnGroup_staffStatus: 'Status',
};
