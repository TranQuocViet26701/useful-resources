export default {
  'updateVersionTable.title': 'System Version List',
  'updateVersionTable.versionName': 'Version name',
  'updateVersionTable.description': 'Description',
  'updateVersionTable.updateCondition': 'Update condition',
  'updateVersionTable.date': 'Date',
};
