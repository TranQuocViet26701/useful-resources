export default {
  'userTable.detail.header.title.personal': 'Personal profile',
  'userTable.detail.header.title.user': 'User details',
  'userTable.detail.header.button.changePassword': 'Change password',
  'userTable.detail.header.tooltip.unBlock': 'Unblock user',
  'userTable.detail.header.tooltip.block': 'Block user',
  'userTable.detail.header.lockModal.title': 'Block user',
  'userTable.detail.header.lockModal.desc1': 'Are you sure you want to block',
  'userTable.detail.header.lockModal.desc2': 'This user will not be able to access the system.',
};
