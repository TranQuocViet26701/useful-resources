export default {
  'userTable.detail.userInfoRow.title': 'User information',
  'userTable.detail.userInfoRow.staffName': "ID - Staff's name",
  'userTable.detail.userInfoRow.phoneNumber': 'Phone number',
  'userTable.detail.userInfoRow.email': 'Email',
  'userTable.detail.roleGroup.title': 'Ownership group',
  'userTable.form.roleTitle': 'Corresponding roles',
};
