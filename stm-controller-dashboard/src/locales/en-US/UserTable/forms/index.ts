export default {
  'userTable.form.title.newUser': 'Create new user',
  'userTable.form.title.updateUser': 'Update user',
  'userTable.form.roleTitle': 'Corresponding roles',
  'userTable.form.avatar': 'Avatar',
  'userTable.form.button.uploadImage': 'Upload image',
  'userTable.form.button.removeImage': 'Remove image',
  'userTable.form.placeholder.staffCode': 'Enter your staff code',
  'userTable.form.placeholder.staffName': 'Enter your name',
  'userTable.form.placeholder.phoneNumber': 'Enter your phone number',
  'userTable.form.placeholder.email': 'Enter your email',
  'userTable.form.placeholder.managementUnit': 'Select management unit',
  'userTable.form.placeholder.roleGroup': 'Select role group',
};
