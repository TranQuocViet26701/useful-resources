export default {
  'userList.tables.headCell.index': 'No.',
  'userList.tables.headCell.staffId': 'Code staff',
  'userList.tables.headCell.name': 'Name',
  'userList.tables.headCell.managementUnit': "Code - Unit's name",
  'userList.tables.headCell.email': 'Email',
  'userList.tables.headCell.phoneNumber': 'Phone number',
  'userList.tables.headCell.status': 'Activity status',
};
