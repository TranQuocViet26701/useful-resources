export default {
  unitForm_createForm_title: 'Create a new management unit',
  unitForm_updateForm_title: 'Update management unit',

  form_inputGroup_unitCode_title: 'Unit code',
  form_inputGroup_unitCode_placeholder: 'Fill in unit code',

  form_inputGroup_unitName_title: "Unit's name",
  form_inputGroup_unitName_placeholder: "Fill in unit's name",

  form_inputGroup_unitLocation_title: 'Location',
  form_inputGroup_unitLocation_placeholder: 'Choose location',

  form_inputGroup_unitProvince_title: 'Province/City',
  form_inputGroup_unitProvince_placeholder: 'Choose province/city',

  form_inputGroup_unitDistrict_title: 'District',
  form_inputGroup_unitDistrict_placeholder: 'Choose district',

  form_inputGroup_unitWard_title: 'Ward',
  form_inputGroup_unitWard_placeholder: 'Choose ward',

  form_inputGroup_unitAddress_title: 'Street name, house number',
  form_inputGroup_unitAddress_placeholder: 'Fill in street name, house number',

  createUnit_successStatus_message: 'Successfully added new unit',
  updateUnit_successStatus_message: 'Successful unit editing',
  deleteUnit_successStatus_message: 'Delete unit successfully',
};
