export default {
  detailDrawer_title: 'Unit Detail',

  detailDrawer_infoCard_title: 'Unit information',
  detailDrawer_infoCard_unitCodeName: "Code - Unit's name",
  detailDrawer_infoCard_unitAddress: 'Address',

  detailDrawer_userCard_title: 'User List',
  detailDrawer_userCard_columnGroup_staffName: 'Staff name',
  detailDrawer_userCard_columnGroup_staffCode: 'Staff code',
  detailDrawer_userCard_columnGroup_staffPhoneNumber: 'Phone number',
  detailDrawer_userCard_columnGroup_staffEmail: 'Email',
  detailDrawer_userCard_columnGroup_staffStatus: 'Status',

  detailDrawer_machineCard_title: 'Management machines',
  detailDrawer_machineCard_columnGroup_machineName: 'Machine name',
  detailDrawer_machineCard_columnGroup_machineCode: 'Terminal ID',
  detailDrawer_machineCard_columnGroup_machineIPAddress: 'IP Address',
  detailDrawer_machineCard_columnGroup_machineStatus: 'Status',
};
