export default {
  managementUnit_tableTitle: 'Management units',
  managementUnit_tableColumn_unitCode: 'Unit code',
  managementUnit_tableColumn_unitName: "Unit's name",
  managementUnit_tableColumn_location: 'Location',
  managementUnit_tableColumn_province: 'Province/City',
  managementUnit_tableColumn_district: 'District',
  managementUnit_tableColumn_ward: 'Ward',
  managementUnit_tableColumn_address: 'Street name',
};
