export default {
  configMachine_createForm_title: 'Create New Model',
  configMachine_updateForm_title: 'Update Model Configuration',

  form_inputGroup_machineType_title: 'Machine type',
  form_inputGroup_machineType_placeholder: 'Choose machine type',

  form_inputGroup_machineSeries_title: 'Machine model',
  form_inputGroup_machineSeries_placeholder: 'Fill in machine model',

  form_editableTable_columnGroup_headerTitle: 'Header',
  form_editableTable_columnGroup_unitTitle: 'Unit',
  form_editableTable_columnGroup_miniCapacity: 'Min capacity',

  createConfigMachine_successStatus_message: 'Successfully added new configuration',
  updateConfigMachine_successStatus_message: 'Edit the model configuration successfully',
  deleteConfigMachine_successStatus_message: 'Delete model configuration successfully',
};
