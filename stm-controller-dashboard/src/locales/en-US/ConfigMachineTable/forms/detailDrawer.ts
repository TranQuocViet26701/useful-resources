export default {
  detailDrawer_title: 'Details of machine configuration',

  detailDrawer_infoCard_title: 'Model information',
  detailDrawer_inputGroup_machineType_tile: 'Machine type',
  detailDrawer_inputGroup_machineType_placeholder: 'Lorem Ipsum',
  detailDrawer_inputGroup_machineSeries_tile: 'Model',
  detailDrawer_inputGroup_machineSeries_placeholder: 'Model name',

  detailDrawer_configCard_title: 'Configuration',
  detailDrawer_configCard_columnGroup_deviceType: 'Device type',
  detailDrawer_configCard_columnGroup_unit: 'Unit',
  detailDrawer_configCard_columnGroup_miniCapacity: 'Min capacity',
  update_configMachine_cta_delete_question: 'Are you sure you want to delete this permission group',
};
