export default {
  configMachine_tableTitle: 'Configure Machine Model',
  configMachine_tableColumn_machineType: 'Machine type',
  configMachine_tableColumn_machineSeries: 'Model',
  configMachine_tableColumn_machineCreatedBy: 'Create by',
};
