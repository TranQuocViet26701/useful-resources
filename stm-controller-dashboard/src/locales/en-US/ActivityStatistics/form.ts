export default {
  'actiStatisDetail.title': 'Activity detail',
  'actiStatisDetail.cardTitle': 'Machine information',
  'actiStatisDetail.machineStatus': 'Machine status',
  'actiStatisDetail.machineAddress': 'Machine address',
};
