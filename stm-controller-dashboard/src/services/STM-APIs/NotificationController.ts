// @ts-ignore
/* eslint-disable */
import { request } from '@/utils';

/** Get user notifications - Get user notifications GET /api/v1/notifications */
export async function getUserNotification(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.getUserNotificationParams,
  options?: { [key: string]: any },
) {
  return request<API.ResponseBasePageResponseNotification>('/api/v1/notifications', {
    method: 'GET',
    params: {
      // pageSize has a default value: 10
      pageSize: '10',
      ...params,
    },
    ...(options || {}),
  });
}

/** Read all notifications - Read all notifications PUT /api/v1/notifications/read-all */
export async function markAsReadAll(options?: { [key: string]: any }) {
  return request<API.ResponseBaseReadNotificationResponse>('/api/v1/notifications/read-all', {
    method: 'PUT',
    ...(options || {}),
  });
}

/** Read notification - Read notification PUT /api/v1/notifications/read/${param0} */
export async function markAsRead(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.markAsReadParams,
  options?: { [key: string]: any },
) {
  const { notificationId: param0, ...queryParams } = params;
  return request<API.ResponseBaseReadNotificationResponse>(`/api/v1/notifications/read/${param0}`, {
    method: 'PUT',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** Unread notification - Unread notification PUT /api/v1/notifications/unread/${param0} */
export async function markAsUnRead(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.markAsUnReadParams,
  options?: { [key: string]: any },
) {
  const { notificationId: param0, ...queryParams } = params;
  return request<API.ResponseBaseReadNotificationResponse>(
    `/api/v1/notifications/unread/${param0}`,
    {
      method: 'PUT',
      params: { ...queryParams },
      ...(options || {}),
    },
  );
}
