// @ts-ignore
/* eslint-disable */
import { request } from '@/utils';

/** Create admin API - Sử dụng để tạo System administrator POST /api/v1/admin/create-system-admin */
export async function createSystemAdmin(
  body: API.CreateAdminRequest,
  options?: { [key: string]: any },
) {
  return request<API.ResponseBaseUserResponse>('/api/v1/admin/create-system-admin', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** Get list system admins API - Lấy danh sách System administrators GET /api/v1/admin/get-system-admins */
export async function getSystemAdmins(options?: { [key: string]: any }) {
  return request<API.ResponseBaseListAdminsResponse>('/api/v1/admin/get-system-admins', {
    method: 'GET',
    ...(options || {}),
  });
}

/** Reset authentication - Reset authentication PUT /api/v1/admin/reset-authentication */
export async function resetAuthentication(options?: { [key: string]: any }) {
  return request<API.ResponseBaseResetAuthenticationResponse>(
    '/api/v1/admin/reset-authentication',
    {
      method: 'PUT',
      ...(options || {}),
    },
  );
}

/** Update admin API - Sử dụng để update System administrator PUT /api/v1/admin/update-system-admin/${param0} */
export async function updateSystemAdmin(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.updateSystemAdminParams,
  body: API.UpdateAdminRequest,
  options?: { [key: string]: any },
) {
  const { adminId: param0, ...queryParams } = params;
  return request<API.ResponseBaseUserResponse>(`/api/v1/admin/update-system-admin/${param0}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...queryParams },
    data: body,
    ...(options || {}),
  });
}
