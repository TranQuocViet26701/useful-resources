// @ts-ignore
/* eslint-disable */
import { request } from '@/utils';

/** Lấy danh sách file màn hình hiển thị  - Lấy danh sách file màn hình hiển thị GET /api/v1/displays */
export async function getDisplayMedias(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.getDisplayMediasParams,
  options?: { [key: string]: any },
) {
  return request<any>('/api/v1/displays', {
    method: 'GET',
    params: {
      // pageSize has a default value: 10
      pageSize: '10',
      // sortDirection has a default value: DESC
      sortDirection: 'DESC',
      // sortBy has a default value: createdAt
      sortBy: 'createdAt',
      ...params,
    },
    ...(options || {}),
  });
}

/** Update thông tin file màn hình hiển thị  - Update thông tin file màn hình hiển thị PUT /api/v1/displays/${param0} */
export async function updateDisplayMedia(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.updateDisplayMediaParams,
  body: API.UpdateDisplayMediaRequest,
  options?: { [key: string]: any },
) {
  const { mediaId: param0, ...queryParams } = params;
  return request<any>(`/api/v1/displays/${param0}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    params: { ...queryParams },
    data: body,
    ...(options || {}),
  });
}

/** Xóa file file màn hình hiển thị  - Update thông tin file màn hình hiển thị DELETE /api/v1/displays/${param0} */
export async function deleteDisplayMedia(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.deleteDisplayMediaParams,
  options?: { [key: string]: any },
) {
  const { mediaId: param0, ...queryParams } = params;
  return request<any>(`/api/v1/displays/${param0}`, {
    method: 'DELETE',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** Xem danh sách set up màn hình hiển thị  - Xem danh sách Set up màn hình hiển thị cho dòng máy GET /api/v1/displays/settings */
export async function getSetupScreenDisplay(options?: { [key: string]: any }) {
  return request<any>('/api/v1/displays/settings', {
    method: 'GET',
    ...(options || {}),
  });
}

/** Set up màn hình hiển thị  - Set up màn hình hiển thị cho dòng máy POST /api/v1/displays/settings */
export async function setUpScreenDisplay(
  body: API.SetupScreenRequest,
  options?: { [key: string]: any },
) {
  return request<any>('/api/v1/displays/settings', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
