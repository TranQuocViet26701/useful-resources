// @ts-ignore
/* eslint-disable */
import { request } from '@/utils';

/** Get machine warnings  - Get machine warnings (physical warnings and status warnings) GET /api/v1/warnings */
export async function getMachineWarnings(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.getMachineWarningsParams,
  options?: { [key: string]: any },
) {
  return request<API.ResponseBasePageResponseMachineWarningResponse>('/api/v1/warnings', {
    method: 'GET',
    params: {
      // pageSize has a default value: 10
      pageSize: '10',

      ...params,
    },
    ...(options || {}),
  });
}

/** Mark a warning as solved  - Mark a warning as solved PUT /api/v1/warnings/mark-as-solved/${param0} */
export async function markAsSolvedWarning(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.markAsSolvedWarningParams,
  options?: { [key: string]: any },
) {
  const { id: param0, ...queryParams } = params;
  return request<API.ResponseBaseSolveWarningResponse>(
    `/api/v1/warnings/mark-as-solved/${param0}`,
    {
      method: 'PUT',
      params: { ...queryParams },
      ...(options || {}),
    },
  );
}
