// @ts-ignore
/* eslint-disable */
import { request } from '@/utils';

/** Get machine activities statistics  - Get machine status and warning statistic GET /api/v1/statistics/machine-activities */
export async function getMachineActivityStatistic(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.getMachineActivityStatisticParams,
  options?: { [key: string]: any },
) {
  return request<any>('/api/v1/statistics/machine-activities', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** Get machine type statistics  - Get machine type statistic GET /api/v1/statistics/machine-types */
export async function getMachineTypeStatistic(options?: { [key: string]: any }) {
  return request<any>('/api/v1/statistics/machine-types', {
    method: 'GET',
    ...(options || {}),
  });
}

/** Get transaction statistics  - Get transaction statistic GET /api/v1/statistics/transactions */
export async function getTransactionStatistic(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.getTransactionStatisticParams,
  options?: { [key: string]: any },
) {
  return request<any>('/api/v1/statistics/transactions', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

/** Get user statistics  - Get user statistic GET /api/v1/statistics/users */
export async function getUserStatistic(options?: { [key: string]: any }) {
  return request<any>('/api/v1/statistics/users', {
    method: 'GET',
    ...(options || {}),
  });
}
