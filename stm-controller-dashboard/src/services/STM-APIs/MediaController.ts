// @ts-ignore
/* eslint-disable */
import { request } from '@/utils';

/** Download file from storage - Download file (avatar, screen displays) from storage GET /api/v1/storage/download/${param0} */
export async function download(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.downloadParams,
  options?: { [key: string]: any },
) {
  const { objectName: param0, ...queryParams } = params;
  return request<string>(`/api/v1/storage/download/${param0}`, {
    method: 'GET',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** Preview file from storage - preview file from storage GET /api/v1/storage/preview/${param0} */
export async function preview(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.previewParams,
  options?: { [key: string]: any },
) {
  const { objectName: param0, ...queryParams } = params;
  return request<string>(`/api/v1/storage/preview/${param0}`, {
    method: 'GET',
    params: { ...queryParams },
    ...(options || {}),
  });
}

/** Upload file to storage - Upload file (avatar, screen displays) to storage, use returned objectName for other APIs POST /api/v1/storage/upload */
export async function upload(
  // 叠加生成的Param类型 (非body参数swagger默认没有生成对象)
  params: API.uploadParams,
  body: {},
  file?: File,
  options?: { [key: string]: any },
) {
  const formData = new FormData();

  if (file) {
    formData.append('file', file);
  }

  Object.keys(body).forEach((ele) => {
    const item = (body as any)[ele];

    if (item !== undefined && item !== null) {
      formData.append(
        ele,
        typeof item === 'object' && !(item instanceof File) ? JSON.stringify(item) : item,
      );
    }
  });

  return request<API.ResponseBaseUploadMediaResponse>('/api/v1/storage/upload', {
    method: 'POST',
    params: {
      ...params,
    },
    data: formData,
    requestType: 'form',
    ...(options || {}),
  });
}
