// @ts-ignore
/* eslint-disable */
// API 更新时间：
// API 唯一标识：
import * as WarningController from './WarningController';
import * as STMVersionController from './STMVersionController';
import * as UserController from './UserController';
import * as RoleController from './RoleController';
import * as NotificationController from './NotificationController';
import * as STMModelController from './STMModelController';
import * as ManagementUnitController from './ManagementUnitController';
import * as STMController from './STMController';
import * as ScreenDisplayController from './ScreenDisplayController';
import * as AuthController from './AuthController';
import * as AdminController from './AdminController';
import * as MediaController from './MediaController';
import * as StmStatusController from './StmStatusController';
import * as TransactionController from './TransactionController';
import * as DashboardController from './DashboardController';
import * as LogController from './LogController';
import * as LocationController from './LocationController';
import * as PhysicalDevicesController from './PhysicalDevicesController';
import * as DenominationsController from './DenominationsController';
export default {
  WarningController,
  STMVersionController,
  UserController,
  RoleController,
  NotificationController,
  STMModelController,
  ManagementUnitController,
  STMController,
  ScreenDisplayController,
  AuthController,
  AdminController,
  MediaController,
  StmStatusController,
  TransactionController,
  DashboardController,
  LogController,
  LocationController,
  PhysicalDevicesController,
  DenominationsController,
};
