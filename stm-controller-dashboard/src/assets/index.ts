import CameraLogIcon from './icon/camera-log.svg';
import CameraManagementIcon from './icon/camera-management.svg';
import CollapseIcon from './icon/collapse.svg';
import ConfigIcon from './icon/config.svg';
import DisplayManagementStorageIcon from './icon/display-management-storage.svg';
import DisplayManagementIcon from './icon/display-management.svg';
import GroupPolicyIcon from './icon/group-policy.svg';
import MachineAnalyticsIcon from './icon/machine-analytics.svg';
import MachineHistoryIcon from './icon/machine-history.svg';
import MachineListIcon from './icon/machine-list.svg';
import MachineManagementIcon from './icon/machine-management.svg';
import MachineUpdateIcon from './icon/machine-update.svg';
import MachineWarningIcon from './icon/machine-warning.svg';
import ScreenDisplayIcon from './icon/screen-display.svg';
import TransactionListIcon from './icon/transaction-list.svg';
import UnCollapseIcon from './icon/un-collapse.svg';
import UnitManagementIcon from './icon/unit-management.svg';
import UserManagementListIcon from './icon/user-management-list.svg';
import UserManagementIcon from './icon/user-management.svg';
import Logo from './logo/Logo.svg';
import CloseIcon from './images/svg/icon/close-icon.svg';
import MapIcon from './images/svg/icon/Map.svg';
import ExportIcon from './images/svg/icon/Export.svg';
import DashboardIcon from './icons/dashboard-icon.svg';
export {
  Logo,
  CameraLogIcon,
  CameraManagementIcon,
  ConfigIcon,
  DisplayManagementIcon,
  DisplayManagementStorageIcon,
  GroupPolicyIcon,
  MachineAnalyticsIcon,
  MachineHistoryIcon,
  MachineListIcon,
  MachineManagementIcon,
  MachineUpdateIcon,
  MachineWarningIcon,
  ScreenDisplayIcon,
  TransactionListIcon,
  UnitManagementIcon,
  UserManagementIcon,
  UserManagementListIcon,
  CollapseIcon,
  UnCollapseIcon,
  CloseIcon,
  MapIcon,
  ExportIcon,
  DashboardIcon,
};
