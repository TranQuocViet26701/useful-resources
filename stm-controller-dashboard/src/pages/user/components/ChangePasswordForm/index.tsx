import iconCheckActive from '@/assets/icons/icon-check-active.svg';
import iconCheckDisable from '@/assets/icons/icon-check-disable.svg';
import { changePassword } from '@/services/STM-APIs/AuthController';
import {
  isContainLowerCase,
  isContainNumber,
  isContainSpecialLetter,
  isContainUpperCase,
  isMinimumCharacter,
  openNotification,
} from '@/utils';
import { LockOutlined } from '@ant-design/icons';
import { Button, Form } from 'antd';
import React, { useEffect, useState } from 'react';
import { history, useIntl } from 'umi';
import InputPassword from '../InputPassword';
import styles from './index.less';

type FormSetupPasswordType = {
  currentPassword: string;
  password: string;
  retypePassword: string;
};

const ChangePasswordForm: React.FC = () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const [newPassword, setNewPassword] = useState<string>('');
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);

  const validationList = [
    {
      id: 0,
      message: intl.formatMessage({
        id: 'user.setupPassword.validation.minEightCharacter',
      }),
      checkFunction: (str: string) => isMinimumCharacter(str, 8),
    },
    {
      id: 1,
      message: intl.formatMessage({
        id: 'user.setupPassword.validation.lowerCaseLetter',
      }),
      checkFunction: isContainLowerCase,
    },
    {
      id: 2,
      message: intl.formatMessage({
        id: 'user.setupPassword.validation.upperCaseLetter',
      }),
      checkFunction: isContainUpperCase,
    },
    {
      id: 3,
      message: intl.formatMessage({
        id: 'user.setupPassword.validation.number',
      }),
      checkFunction: isContainNumber,
    },
    {
      id: 4,
      message: intl.formatMessage({
        id: 'user.setupPassword.validation.specialCharacter',
      }),
      checkFunction: isContainSpecialLetter,
    },
  ];

  const handleBeforeUnload = (e: BeforeUnloadEvent) => {
    e.preventDefault();
    const message = 'Are you sure you want to leave? All provided data will be lost.';
    e.returnValue = message;
    return message;
  };

  useEffect(() => {
    window.addEventListener('beforeunload', handleBeforeUnload);
    return () => {
      window.removeEventListener('beforeunload', handleBeforeUnload);
    };
  }, []);

  const handleSubmit = async (values: API.ChangePasswordRequest) => {
    try {
      const res = await changePassword({
        ...values,
      });

      // reset password successfull
      if (res.code === 0) {
        const message = 'Change password successfully!';
        const desc = 'Please login again to continue';
        openNotification('success', message, desc);
        if (!history) return;
        history.push('/user/login');
        return;
      }

      const message = 'Thay đổi mật khẩu không thành công!';
      openNotification(
        'error',
        message,
        res.code === 11 ? 'Mật khẩu hiện tại không đúng' : res.message,
      );
    } catch (error) {
      console.log('error login: ', error);
    }
  };

  const onFinish = async (values: FormSetupPasswordType) => {
    // compare password and retype password
    if (values.password !== values.retypePassword) {
      const message = 'Password not match';
      const desc = 'Please enter your password again';
      openNotification('error', message, desc);
      return;
    }

    const { currentPassword, password } = values;

    setIsSubmitting(true);
    await handleSubmit({
      currentPassword,
      newPassword: password,
    });
    setIsSubmitting(false);
  };

  const handleCurrentPasswordChange = (password: string) => {
    form.setFieldValue('currentPassword', password);
  };

  const handleNewPasswordChange = (password: string) => {
    setNewPassword(password);
    form.setFieldValue('password', password);
  };

  const handleRetypePasswordChange = (password: string) => {
    form.setFieldValue('retypePassword', password);
  };

  return (
    <div className={styles['setup-password-form-wrapper']}>
      <h1 className={styles.title}>{intl.formatMessage({ id: 'user.changePassword.title' })}</h1>
      <Form
        form={form}
        name="change-password-form"
        onFinish={onFinish}
        className={styles.form}
        layout="vertical"
      >
        <Form.Item
          name="currentPassword"
          label={intl.formatMessage({ id: 'user.changePassword.currentPassword' })}
          rules={[
            {
              required: true,
              message: intl.formatMessage({ id: 'user.changePassword.currentPassword.message' }),
            },
          ]}
        >
          <InputPassword
            onChange={handleCurrentPasswordChange}
            placeholder={intl.formatMessage({
              id: 'user.changePassword.currentPassword.placeholder',
            })}
            prefix={<LockOutlined />}
          />
        </Form.Item>
        <Form.Item
          name="password"
          label={intl.formatMessage({ id: 'user.setupPassword.newPassword' })}
          rules={[
            {
              required: true,
              message: intl.formatMessage({ id: 'user.setupPassword.newPassword.message' }),
            },
          ]}
        >
          <InputPassword
            onChange={handleNewPasswordChange}
            placeholder={intl.formatMessage({ id: 'user.setupPassword.newPassword.placeholder' })}
            prefix={<LockOutlined />}
          />
        </Form.Item>
        <div className={styles['section-validate']}>
          {validationList.map((item) => (
            <li key={item.id} className={styles['item-validate']}>
              <img
                src={item.checkFunction(newPassword) ? iconCheckActive : iconCheckDisable}
                alt="icon-check"
              />
              <span>{item.message}</span>
            </li>
          ))}
        </div>
        <Form.Item
          name="retypePassword"
          label={intl.formatMessage({ id: 'user.setupPassword.retypePassword' })}
          rules={[
            {
              required: true,
              message: intl.formatMessage({ id: 'user.setupPassword.retypePassword.message' }),
            },
          ]}
        >
          <InputPassword
            onChange={handleRetypePasswordChange}
            placeholder={intl.formatMessage({
              id: 'user.setupPassword.retypePassword.placeholder',
            })}
            prefix={<LockOutlined />}
          />
        </Form.Item>
      </Form>
      <div className={styles['btn-submit']}>
        <Button
          type="primary"
          htmlType="submit"
          form="change-password-form"
          disabled={!validationList.every((item) => item.checkFunction(newPassword))}
          loading={isSubmitting}
        >
          {intl.formatMessage({ id: 'submit' })}
        </Button>
      </div>
    </div>
  );
};

export default ChangePasswordForm;
