// @ts-nocheck
import { Plugin } from '/Users/tranviet/unicloud-jobs/stm-controller-dashboard/node_modules/umi/node_modules/@umijs/runtime';

const plugin = new Plugin({
  validKeys: ['modifyClientRenderOpts','patchRoutes','rootContainer','render','onRouteChange','__mfsu','getInitialState','initialStateConfig','locale','request',],
});

export { plugin };
