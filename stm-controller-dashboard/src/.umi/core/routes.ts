// @ts-nocheck
import React from 'react';
import { ApplyPluginsType, dynamic } from '/Users/tranviet/unicloud-jobs/stm-controller-dashboard/node_modules/umi/node_modules/@umijs/runtime';
import * as umiExports from './umiExports';
import { plugin } from './plugin';
import LoadingComponent from '@ant-design/pro-layout/es/PageLoading';

export function getRoutes() {
  const routes = [
  {
    "path": "/umi/plugin/openapi",
    "component": dynamic({ loader: () => import(/* webpackChunkName: '.umi__plugin-openapi__openapi' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/.umi/plugin-openapi/openapi.tsx'), loading: LoadingComponent})
  },
  {
    "path": "/~demos/:uuid",
    "layout": false,
    "wrappers": [dynamic({ loader: () => import(/* webpackChunkName: 'wrappers' */'../dumi/layout'), loading: LoadingComponent})],
    "component": ((props) => dynamic({
          loader: async () => {
            const React = await import('react');
            const { default: getDemoRenderArgs } = await import(/* webpackChunkName: 'dumi_demos' */ '/Users/tranviet/unicloud-jobs/stm-controller-dashboard/node_modules/@umijs/preset-dumi/lib/plugins/features/demo/getDemoRenderArgs');
            const { default: Previewer } = await import(/* webpackChunkName: 'dumi_demos' */ 'dumi-theme-default/es/builtins/Previewer.js');
            const { usePrefersColor, context } = await import(/* webpackChunkName: 'dumi_demos' */ 'dumi/theme');

            return props => {
              
      const { demos } = React.useContext(context);
      const [renderArgs, setRenderArgs] = React.useState([]);

      // update render args when props changed
      React.useLayoutEffect(() => {
        setRenderArgs(getDemoRenderArgs(props, demos));
      }, [props.match.params.uuid, props.location.query.wrapper, props.location.query.capture]);

      // for listen prefers-color-schema media change in demo single route
      usePrefersColor();

      switch (renderArgs.length) {
        case 1:
          // render demo directly
          return renderArgs[0];

        case 2:
          // render demo with previewer
          return React.createElement(
            Previewer,
            renderArgs[0],
            renderArgs[1],
          );

        default:
          return `Demo ${props.match.params.uuid} not found :(`;
      }
    
            }
          },
          loading: () => null,
        }))()
  },
  {
    "path": "/_demos/:uuid",
    "redirect": "/~demos/:uuid"
  },
  {
    "__dumiRoot": true,
    "layout": false,
    "path": "/~docs",
    "wrappers": [dynamic({ loader: () => import(/* webpackChunkName: 'wrappers' */'../dumi/layout'), loading: LoadingComponent}), dynamic({ loader: () => import(/* webpackChunkName: 'wrappers' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/node_modules/dumi-theme-default/es/layout.js'), loading: LoadingComponent})],
    "routes": [
      {
        "path": "/~docs",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'README.md' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/README.md'), loading: LoadingComponent}),
        "exact": true,
        "meta": {
          "locale": "en-US",
          "order": null,
          "filePath": "README.md",
          "updatedTime": 1661746653000,
          "slugs": [
            {
              "depth": 1,
              "value": "Ant Design Pro",
              "heading": "ant-design-pro"
            },
            {
              "depth": 2,
              "value": "Environment Prepare",
              "heading": "environment-prepare"
            },
            {
              "depth": 2,
              "value": "Provided Scripts",
              "heading": "provided-scripts"
            },
            {
              "depth": 3,
              "value": "Start project",
              "heading": "start-project"
            },
            {
              "depth": 3,
              "value": "Build project",
              "heading": "build-project"
            },
            {
              "depth": 3,
              "value": "Check code style",
              "heading": "check-code-style"
            },
            {
              "depth": 3,
              "value": "Test code",
              "heading": "test-code"
            },
            {
              "depth": 2,
              "value": "More",
              "heading": "more"
            }
          ],
          "title": "Ant Design Pro"
        },
        "title": "Ant Design Pro"
      },
      {
        "path": "/~docs/components",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'components__index.md' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/components/index.md'), loading: LoadingComponent}),
        "exact": true,
        "meta": {
          "filePath": "src/components/index.md",
          "updatedTime": 1661746653000,
          "title": "业务组件",
          "sidemenu": false,
          "slugs": [
            {
              "depth": 1,
              "value": "业务组件",
              "heading": "业务组件"
            },
            {
              "depth": 2,
              "value": "Footer 页脚组件",
              "heading": "footer-页脚组件"
            },
            {
              "depth": 2,
              "value": "HeaderDropdown 头部下拉列表",
              "heading": "headerdropdown-头部下拉列表"
            },
            {
              "depth": 2,
              "value": "HeaderSearch 头部搜索框",
              "heading": "headersearch-头部搜索框"
            },
            {
              "depth": 3,
              "value": "API",
              "heading": "api"
            },
            {
              "depth": 2,
              "value": "NoticeIcon 通知工具",
              "heading": "noticeicon-通知工具"
            },
            {
              "depth": 3,
              "value": "NoticeIcon API",
              "heading": "noticeicon-api"
            },
            {
              "depth": 3,
              "value": "NoticeIcon.Tab API",
              "heading": "noticeicontab-api"
            },
            {
              "depth": 3,
              "value": "NoticeIconData",
              "heading": "noticeicondata"
            },
            {
              "depth": 2,
              "value": "RightContent",
              "heading": "rightcontent"
            }
          ],
          "hasPreviewer": true,
          "group": {
            "path": "/~docs/components",
            "title": "Components"
          }
        },
        "title": "业务组件 - ant-design-pro"
      }
    ],
    "title": "ant-design-pro",
    "component": (props) => props.children
  },
  {
    "path": "/user",
    "layout": false,
    "component": dynamic({ loader: () => import(/* webpackChunkName: 'layouts__LoginLayout' */'@/layouts/LoginLayout'), loading: LoadingComponent}),
    "routes": [
      {
        "name": "login",
        "path": "/user/login",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__user__Login' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/user/Login'), loading: LoadingComponent}),
        "exact": true
      },
      {
        "name": "forgot-password",
        "path": "/user/forgot-password",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__user__ForgotPassword' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/user/ForgotPassword'), loading: LoadingComponent}),
        "exact": true
      },
      {
        "name": "reset-password",
        "path": "/user/reset-password",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__user__ResetPassword' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/user/ResetPassword'), loading: LoadingComponent}),
        "exact": true
      },
      {
        "name": "change-password",
        "path": "/user/change-password",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__user__ChangePassword' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/user/ChangePassword'), loading: LoadingComponent}),
        "exact": true
      },
      {
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__404' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/404'), loading: LoadingComponent}),
        "exact": true
      }
    ]
  },
  {
    "path": "/",
    "component": dynamic({ loader: () => import(/* webpackChunkName: 'layouts__BaseLayout' */'@/layouts/BaseLayout'), loading: LoadingComponent}),
    "menu": {
      "flatMenu": true
    },
    "routes": [
      {
        "path": "/index.html",
        "redirect": "/dashboard",
        "exact": true
      },
      {
        "path": "/",
        "redirect": "/dashboard",
        "exact": true
      },
      {
        "path": "/dashboard",
        "name": "dashboard",
        "icon": "dashboard",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__AdminDashboard__Analysis' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/AdminDashboard/Analysis'), loading: LoadingComponent}),
        "exact": true
      },
      {
        "path": "/machine",
        "name": "Quản trị máy",
        "icon": "machine-management",
        "routes": [
          {
            "path": "/machine/list",
            "name": "Danh sách máy",
            "icon": "machine-list",
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__MachineTable' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/MachineTable'), loading: LoadingComponent}),
            "exact": true
          },
          {
            "path": "/machine/analytics",
            "name": "Thống kê hoạt động",
            "icon": "machine-stats",
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__StatisActivityTable' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/StatisActivityTable'), loading: LoadingComponent}),
            "exact": true
          },
          {
            "path": "/machine/config",
            "name": "Cấu hình dòng máy",
            "icon": "machine-config",
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__ConfigMachineTable' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/ConfigMachineTable'), loading: LoadingComponent}),
            "exact": true
          },
          {
            "path": "/machine/update-firmware",
            "name": "Cập nhật phần mềm",
            "icon": "machine-update",
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__UpdateVersionTable' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/UpdateVersionTable'), loading: LoadingComponent}),
            "exact": true
          }
        ]
      },
      {
        "path": "/camera",
        "name": "Giám sát Camera",
        "icon": "camera-management",
        "routes": [
          {
            "path": "/camera/log",
            "name": "Log hoạt động",
            "icon": "camera-log",
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__CameraMonitor__ActivityLog' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/CameraMonitor/ActivityLog'), loading: LoadingComponent}),
            "exact": true
          }
        ]
      },
      {
        "path": "/users",
        "name": "Quản trị người dùng",
        "icon": "user-management",
        "routes": [
          {
            "path": "/users/list",
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__UserTable' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/UserTable'), loading: LoadingComponent}),
            "name": "Danh sách người dùng",
            "icon": "user-list",
            "exact": true
          },
          {
            "path": "/users/group-authorize",
            "name": "Nhóm quyền",
            "icon": "group-policy",
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__GroupAuthorizeTable' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/GroupAuthorizeTable'), loading: LoadingComponent}),
            "exact": true
          },
          {
            "path": "/users/management-unit",
            "name": "Đơn vị quản lý",
            "icon": "management-unit",
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__UnitTable' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/UnitTable'), loading: LoadingComponent}),
            "exact": true
          },
          {
            "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__404' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/404'), loading: LoadingComponent}),
            "exact": true
          }
        ]
      },
      {
        "path": "/history",
        "name": "Lịch sử",
        "icon": "machine-history",
        "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__HistoryList' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/HistoryList'), loading: LoadingComponent}),
        "exact": true
      }
    ]
  },
  {
    "component": dynamic({ loader: () => import(/* webpackChunkName: 'p__404' */'/Users/tranviet/unicloud-jobs/stm-controller-dashboard/src/pages/404'), loading: LoadingComponent}),
    "exact": true
  }
];

  // allow user to extend routes
  plugin.applyPlugins({
    key: 'patchRoutes',
    type: ApplyPluginsType.event,
    args: { routes },
  });

  return routes;
}
